//
//  ItemsDashBoardViewController.swift
//  FXone
//
//  Created by Vinayak on 24/04/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class ItemsDashBoardViewController: UIViewController {

    @IBOutlet weak var collectionview_Items: UICollectionView!
    var ary_MenuList = [MenuModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        var menu1 = MenuModel()
        menu1.name = "FX Front Desk"
        var menu2 = MenuModel()
        menu2.name = "FX POS"
        var menu3 = MenuModel()
        menu3.name = "FX HouseKeeping"
        var menu4 = MenuModel()
        menu4.name = "FX GuestService"
        var menu5 = MenuModel()
        menu5.name = "FX CRS"
        collectionview_Items.register(UINib(nibName: "QuickLinkCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "QuickLink")
        ary_MenuList.append(menu1)
        ary_MenuList.append(menu2)
        ary_MenuList.append(menu3)
        ary_MenuList.append(menu4)
        ary_MenuList.append(menu5)
        self.collectionview_Items.delegate = self
        self.collectionview_Items.dataSource = self
        
        self.navigationItem.title = "Subscribed"
        self.navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.hexStringToUIColor(hex: "FF8E00")

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ItemsDashBoardViewController : UICollectionViewDelegate,UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ary_MenuList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview_Items.dequeueReusableCell(withReuseIdentifier: "QuickLink", for: indexPath) as! QuickLinkCollectionViewCell
        cell.menuObj = ary_MenuList[indexPath.row]
        cell.backgroundColor = UIColor.white
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionview_Items.cellForItem(at: indexPath) as! QuickLinkCollectionViewCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! QuickLinkCollectionViewCell
        print("selected Item is :\(cell.lbl_title.text ?? "")")
    }
}
