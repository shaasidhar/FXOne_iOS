//
//  FProfitLoassTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 01/06/22.
//  Copyright © 2022 IDS. All rights reserved.
//

import UIKit
import LinearProgressBar

class FProfitLoassTableViewCell: UITableViewCell {

    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var expenseProgressbar: LinearProgressBar!
    @IBOutlet weak var incomeprogressBar: LinearProgressBar!
    @IBOutlet weak var lblExpenses: UILabel!
    @IBOutlet weak var lblIncome: UILabel!
    @IBOutlet weak var lblNetProfit: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        shadowView.layer.cornerRadius = 10
        shadowView.layer.masksToBounds = true
        shadowView.dropShadow()
        self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
