//
//  CalendarDatePickerHeaderView.swift
//  CalendarDatePickerViewController
//
//  Created by Vinayak on 16/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class CalendarDatePickerHeaderView: UICollectionReusableView {
    
    var label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initLabel()
    }
    
    func initLabel() {
        label = UILabel(frame: frame)
        label.center = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        label.font = UIFont(name: "HelveticaNeue-Light", size: 17.0)
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.center
        self.addSubview(label)
    }
    
}
