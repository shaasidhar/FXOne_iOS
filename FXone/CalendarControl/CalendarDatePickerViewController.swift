//
//  CalendarDatePickerViewController.swift
//  CalendarDatePickerViewController
//
//  Created by Vinayak on 16/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit


public protocol CalendarDatePickerViewControllerDelegate {
    func didTapCancel()
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!)
}

public class CalendarDatePickerViewController: UICollectionViewController {
    
    //#define iPadPro12 (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad && UIScreen.mainScreen.nativeBounds.size.height > 1024)

    let cellReuseIdentifier = "CalendarDateRangePickerCell"
    let headerReuseIdentifier = "CalendarDateRangePickerHeaderView"
    
    public var delegate: CalendarDatePickerViewControllerDelegate!
    
    let itemsPerRow = 7
    let itemHeight: CGFloat = 40
    
    var collectionViewInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)

    public var minimumDate: Date!
    public var maximumDate: Date!
    
    public var selectedStartDate: Date?
    public var selectedEndDate: Date?

    override public func viewDidLoad() {
        super.viewDidLoad()
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
             
             if UIDevice().userInterfaceIdiom == .phone {
                 switch UIScreen.main.nativeBounds.height {
                 case 1136:
                     print("IPHONE 5,5S,5C")
                    collectionViewInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                 case 1334:
                     print("IPHONE 6,7,8 IPHONE 6S,7S,8S ")
                    collectionViewInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
                 case 1920, 2208:
                     print("IPHONE 6PLUS, 6SPLUS, 7PLUS, 8PLUS")
                    collectionViewInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                 case 2436:
                     print("IPHONE X, IPHONE XS, IPHONE 11 PRO")
                    collectionViewInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
                 case 2688:
                     print("IPHONE XS MAX, IPHONE 11 PRO MAX")
                    collectionViewInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                 case 1792:
                     print("IPHONE XR, IPHONE 11")
                    collectionViewInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                 default:
                     print("UNDETERMINED")
                 }
             }
            break
        // It's an iPhone
        case .pad:
            
            if(UIScreen.main.bounds.size.width >= 1024) {
                // iPad pro (or hypothetical/future huge-screened iOS device)
                collectionViewInsets = UIEdgeInsets(top: 0, left: 18, bottom: 0, right: 18)

            } else {
                // not iPad pro
                collectionViewInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            }
            
            break
            // It's an iPad
        case .unspecified:
            print("For unspecified")
            break
        // Uh, oh! What could it be?
        case .tv:
            print("For TV")
        case .carPlay:
            print("For carPlay")
        case .mac:
            print("For Mac")
        case .vision:
            print("For Vision")
        @unknown default:
            print("For default")
        }
        self.title = "Select Dates"
        print("minimumDate-----",minimumDate)
        print("maximumDate-----",maximumDate)
        collectionView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.backgroundColor = UIColor.white

        collectionView?.register(CalendarDatePickerCell.self, forCellWithReuseIdentifier: cellReuseIdentifier)
        collectionView?.register(CalendarDatePickerHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerReuseIdentifier)
        collectionView?.contentInset = collectionViewInsets
        
        if minimumDate == nil {
            minimumDate = Date()
        }
        if maximumDate == nil {
            maximumDate = Calendar.current.date(byAdding: .year, value: 3, to: minimumDate)
        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(CalendarDatePickerViewController.didTapCancel))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(CalendarDatePickerViewController.didTapDone))
        self.navigationItem.rightBarButtonItem?.isEnabled = selectedStartDate != nil && selectedEndDate != nil
        self.navigationController?.navigationBar.tintColor = UIColor.hexStringToUIColor(hex: "FF8E00")
    }
    
    @objc func didTapCancel() {
        delegate.didTapCancel()
    }
    
    @objc func didTapDone() {
        if selectedStartDate == nil || selectedEndDate == nil {
            return
        }
        delegate.didTapDoneWithDateRange(startDate: selectedStartDate!, endDate: selectedEndDate!)
    }
    
}

extension CalendarDatePickerViewController {
    
    // UICollectionViewDataSource
    
    override public func numberOfSections(in collectionView: UICollectionView) -> Int {
        let difference = Calendar.current.dateComponents([.month], from: minimumDate, to: maximumDate)
        return difference.month! + 1
    }
    
    override public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let firstDateForSection = getFirstDateForSection(section: section)
        let weekdayRowItems = 7
        let blankItems = getWeekday(date: firstDateForSection) - 1
        let daysInMonth = getNumberOfDaysInMonth(date: firstDateForSection)
        return weekdayRowItems + blankItems + daysInMonth
    }
    
    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CalendarDatePickerCell
        cell.reset()
        let blankItems = getWeekday(date: getFirstDateForSection(section: indexPath.section)) - 1
        if indexPath.item < 7 {
            cell.label.text = getWeekdayLabel(weekday: indexPath.item + 1)
        } else if indexPath.item < 7 + blankItems {
            cell.label.text = ""
        } else {
            let dayOfMonth = indexPath.item - (7 + blankItems) + 1
            let date = getDate(dayOfMonth: dayOfMonth, section: indexPath.section)
            cell.date = date
            cell.label.text = "\(dayOfMonth)"
            
            if isAfter(dateA: date, dateB: maximumDate) {
                cell.disable()
            }
            
            if selectedStartDate != nil && selectedEndDate != nil && isBefore(dateA: selectedStartDate!, dateB: date) && isBefore(dateA: date, dateB: selectedEndDate!) {
                // Cell falls within selected range
                if dayOfMonth == 1 {
                    cell.highlightRight()
                } else if dayOfMonth == getNumberOfDaysInMonth(date: date) {
                    cell.highlightLeft()
                } else {
                    cell.highlight()
                }
            } else if selectedStartDate != nil && areSameDay(dateA: date, dateB: selectedStartDate!) {
                // Cell is selected start date
                cell.select()
                if selectedEndDate != nil {
                    cell.highlightRight()
                }
            } else if selectedEndDate != nil && areSameDay(dateA: date, dateB: selectedEndDate!) {
                cell.select()
                cell.highlightLeft()
            }
            /*if isAfter(dateA: date, dateB: minimumDate) {
                cell.disable()
            }*/
        }
        //cell.select()
        return cell
    }
    
    override public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerReuseIdentifier, for: indexPath) as! CalendarDatePickerHeaderView
            headerView.label.text = getMonthLabel(date: getFirstDateForSection(section: indexPath.section))
            return headerView
        default:
            fatalError("Unexpected element kind")
        }
    }
    
}

extension CalendarDatePickerViewController : UICollectionViewDelegateFlowLayout {
    
    override public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CalendarDatePickerCell
        if cell.date == nil {
            return
        }
        if isAfter(dateA: cell.date!, dateB: maximumDate) {
            return
        }
        
//        selectedStartDate = cell.date
//        selectedEndDate = cell.date
//        self.navigationItem.rightBarButtonItem?.isEnabled = true
        
        if selectedStartDate == nil {
            selectedStartDate = cell.date
        } else if selectedEndDate == nil {
            
                if isBefore(dateA: selectedStartDate!, dateB: cell.date!)  || areSameDay(dateA: selectedStartDate!, dateB: cell.date!) {
                    
                    selectedEndDate = cell.date
                    let diffInDays = Calendar.current.dateComponents([.day], from: selectedStartDate ?? Date(), to: selectedEndDate ?? Date()).day
                    if diffInDays ?? 0 < 60 {
                        
                    }
                    else{
                        selectedEndDate = nil
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: "Please select endDate range in between 60 days", title: "Validation")
                    }
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                } else {
                    // If a cell before the currently selected start date is selected then just set it as the new start date
                    selectedStartDate = cell.date
                    let diffInDays = Calendar.current.dateComponents([.day], from: selectedStartDate ?? Date(), to: selectedEndDate ?? Date()).day
                    if diffInDays ?? 0 < 60 {
                        
                    }
                    else{
                        selectedEndDate = nil
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: "Please select endDate range in between 60 days", title: "Validation")
                    }
                }
            
        } else {
            selectedStartDate = cell.date
            selectedEndDate = nil
        }
        collectionView.reloadData()
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding = collectionViewInsets.left + collectionViewInsets.right
        let availableWidth = view.frame.width - padding
        let itemWidth = round(availableWidth / CGFloat(itemsPerRow))
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: 50)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

extension CalendarDatePickerViewController {
    
    // Helper functions
    
    func getFirstDate() -> Date {
        var components = Calendar.current.dateComponents([.month, .year], from: minimumDate)
        components.day = 1
        return Calendar.current.date(from: components)!
    }
    
    func getFirstDateForSection(section: Int) -> Date {
        return Calendar.current.date(byAdding: .month, value: section, to: getFirstDate())!
    }
    
    func getMonthLabel(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        return dateFormatter.string(from: date)
    }
    
    func getWeekdayLabel(weekday: Int) -> String {
        var components = DateComponents()
        components.calendar = Calendar.current
        components.weekday = weekday
        let date = Calendar.current.nextDate(after: Date(), matching: components, matchingPolicy: Calendar.MatchingPolicy.strict)
        if date == nil {
            return "E"
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEEE"
        return dateFormatter.string(from: date!)
    }
    
    func getWeekday(date: Date) -> Int {
        return Calendar.current.dateComponents([.weekday], from: date).weekday!
    }
    
    func getNumberOfDaysInMonth(date: Date) -> Int {
        return Calendar.current.range(of: .day, in: .month, for: date)!.count
    }
    
    func getDate(dayOfMonth: Int, section: Int) -> Date {
        var components = Calendar.current.dateComponents([.month, .year], from: getFirstDateForSection(section: section))
        components.day = dayOfMonth
        return Calendar.current.date(from: components)!
    }
    
    func areSameDay(dateA: Date, dateB: Date) -> Bool {
        return Calendar.current.compare(dateA, to: dateB, toGranularity: .day) == ComparisonResult.orderedSame
    }
    
    func isBefore(dateA: Date, dateB: Date) -> Bool {
        return Calendar.current.compare(dateA, to: dateB, toGranularity: .day) == ComparisonResult.orderedAscending
    }
    
    func isAfter(dateA: Date, dateB: Date) -> Bool {
        return Calendar.current.compare(dateA, to: dateB, toGranularity: .day) == ComparisonResult.orderedDescending
    }
    
}
