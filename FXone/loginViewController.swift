//
//  loginViewController.swift
//  FXone
//
//  Created by Vinayak on 4/16/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import Alamofire

class loginViewController: UIViewController {
    
    @IBOutlet weak var userNametxt: UITextField!
    @IBOutlet weak var passwordtxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        userNametxt.text = "development7582@idsnext.com"
//        passwordtxt.text = "Ids@1001"
        self.setupTextfields()
    }
    func getToken(username:String,password:String,urlstring:String)  {
        
        let dic_params = ["userid":username,"password":password,"productcode":"100"]
        var headers: HTTPHeaders = [:]
        headers = ["Content-Type":"application/json"]
        loadingView.showLoader(text: "")
        AF.request(urlstring, method:.post, parameters:dic_params ,encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print(response)
                loadingView.hideLoader()
                do {
                    let response = try JSONDecoder().decode(LoginModel.self, from: response.data ?? Data())
                    if response.Status == "Success" {
                        let token = response.Response?.Access_Token ?? ""
                                                UserDefaults.standard.set(token, forKey: "Token")
                        //UICoreEngine.sharedInstance.userRights = response.Response?.UserRights ?? []
                        UserDefaults.standard.setCodableObject(response.Response?.UserRights ?? [], forKey: "USERRIGHTS")
                        UserDefaults.standard.setCodableObject(response.Response?.SubscribedProducts ?? [], forKey: "SubscribedProducts")
                        UserDefaults.standard.setCodableObject(response.Response?.UserDetails ?? UserDetailsData(), forKey: "USERDETAILS")
                        
                        let imageLogoUrl = response.Response?.DefaultHotel?[0].ShortLogo ?? ""
                        UserDefaults.standard.set(imageLogoUrl, forKey: "imageLogo_URL")
                        let colorCode = response.Response?.SubscribedProducts?[0].ColorCode ?? ""
                        UserDefaults.standard.set(colorCode, forKey: "Color_Code")
                        let userName = response.Response?.UserDetails?.UserName ?? ""
                        
                        let property_name = response.Response?.DefaultHotel?[0].HotelName
                        UserDefaults.standard.set(property_name, forKey: "Hotel_Name")
                        UserDefaults.standard.set(userName, forKey: "User_Name")
                        UserDefaults.standard.set(token, forKey: "Token")
                        UserDefaults.standard.set(username, forKey: "User_Id")
                        UserDefaults.standard.set(response.Response?.UserDetails?.DefaultGroupCode ?? "", forKey: "DefaultGroupCode")
                        UserDefaults.standard.set(response.Response?.DefaultHotel?.first?.ID ?? 0, forKey: "Product_Id")
                        //UserDefaults.standard.set(username, forKey: "User_Name")
                        UserDefaults.standard.set(password, forKey: "Password")
                        UserDefaults.standard.set(true, forKey: "ISAUTOLOGIN")
                        let pmsCode = response.Response?.UserDetails?.DefaultPMSCode ?? 0
                        UserDefaults.standard.set(String(pmsCode), forKey: "PmsCustCode")
                        UserDefaults.standard.synchronize()
                        self.subscribedProductsCode(loginModel: response)
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    print(error.localizedDescription)
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
                break
            case .failure(let error):
                
                loadingView.hideLoader()
                print(error)
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    @IBAction func loginBtn(_ sender: UIButton) {
        let response = Validation.shared.validate(values: (ValidationType.email, userNametxt.text ?? ""),(ValidationType.password,passwordtxt.text ?? ""))
        var errorMessage = ""
        switch response {
        case .success:
            self.getToken(username: userNametxt.text ?? "", password: passwordtxt.text ?? "", urlstring: "\(CommonDataManager.shared.baseUrl)\(GET_TOKEN)")
            break
        case .failure(_, let message):
            UICoreEngine.sharedInstance.showAlert(message: message.rawValue, title: "", isNoBUtton: false,viewController :self)
        }
    }
    //dismissing the keyBoard from the txtfield by creating doneBtn
    func setupTextfields() {
        let toolbar = UIToolbar(frame: CGRect(origin: .zero, size: .init(width: view.frame.size.width, height: 30)))
        //create left side empty space so that Done btn set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
    
        // setting toolbar as inputaccessoryView
        toolbar.setItems([flexSpace,doneBtn], animated: false)
        toolbar.sizeToFit()
    
        userNametxt.inputAccessoryView = toolbar
        passwordtxt.inputAccessoryView = toolbar
    }
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    func subscribedProductsCode(loginModel: LoginModel)  {
        let subscribedProducts = loginModel.Response?.SubscribedProducts ?? []
        var productCode100 = Int()
        var productCode10010 = Int()
        for subscribed in subscribedProducts {
            if subscribed.ProductsCode == 100 {
                productCode100 = subscribed.ProductsCode ?? 0
            } else if subscribed.ProductsCode == 10010 {
                productCode10010 = subscribed.ProductsCode ?? 0
            }
        }
        let userDefault = UserDefaults.standard
        userDefault.set(productCode100, forKey: "productCode100")
        userDefault.set(productCode10010, forKey: "productCode10010")
        userDefault.synchronize()
        if productCode100 == 100 && productCode10010 == 10010 {
            self.navigateToHome(navigate: NavigateToHome.RoomOccupancy)
        } else if productCode100 == 100  {
            self.navigateToHome(navigate: NavigateToHome.RoomOccupancy)
        } else  if productCode10010 == 10010 {
            self.navigateToHome(navigate: NavigateToHome.Finance)
        } else {
            self.navigateToHome(navigate: NavigateToHome.RoomOccupancy)
        }
        //        //Hard code for 100
        //        if productCode100 == 100 {
        //            self.navigateToHome(navigate: NavigateToHome.RoomOccupancy)
        //        }
        //        //Hard coad For 10010
        //        if productCode10010 == 10010 {
        //            self.navigateToHome(navigate: NavigateToHome.Finance)
        //        }
    }
    
    func navigateToHome(navigate: NavigateToHome)  {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        switch navigate {
        case .RoomOccupancy:
            let vc = story.instantiateViewController(withIdentifier: "Dashboard") as! ViewController
            self.navigationController?.pushViewController(vc, animated: true)
        default:
           let vc = story.instantiateViewController(withIdentifier: "FinanceViewController") as! FinanceViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension UserDefaults {
  func setCodableObject<T: Codable>(_ data: T?, forKey defaultName: String) {
    let encoded = try? JSONEncoder().encode(data)
    set(encoded, forKey: defaultName)
  }
}

extension UserDefaults {
  func codableObject<T : Codable>(dataType: T.Type, key: String) -> T? {
    guard let userDefaultData = data(forKey: key) else {
      return nil
    }
    return try? JSONDecoder().decode(T.self, from: userDefaultData)
  }
}


enum NavigateToHome: String {
    case RoomOccupancy = "Room Occupancy"
    case Finance = "Finance"
}

enum ShowNavigationBarFinance: String {
    case Login = "Login"
    case SideMenu = "SideMenu"
}
