//
//  TopsellingMenuTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 21/11/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class TopsellingMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lbl_ItemName: UILabel!
    @IBOutlet weak var lbl_Percentage: UILabel!
    @IBOutlet weak var lbl_Count: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        progressView.transform = progressView.transform.scaledBy(x: 1, y: 7.9)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
