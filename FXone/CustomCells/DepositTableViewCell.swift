//
//  DepositTableViewCell.swift
//  FXone
//
//  Created by upendra.x.nimmala on 15/04/20.
//  Copyright © 2020 IDS. All rights reserved.
//

import UIKit

class DepositTableViewCell: UITableViewCell {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var paidOutBaseView: UIView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var currency_Lbl: UILabel!
    @IBOutlet weak var description_Lbl: UILabel!
    @IBOutlet weak var amount_Lbl: UILabel!
    @IBOutlet weak var headingTitleLbl: UILabel!
    @IBOutlet weak var paidOutAmount_Lbl: UILabel!
    @IBOutlet weak var lbl_LivestatusDepst: UILabel!
    @IBOutlet weak var liveSwitch_GuestP: UISwitch!
    @IBOutlet weak var lbl_LiveStatusGuestp: UILabel!
    @IBOutlet weak var liveSwitch_Dep: UISwitch!
    @IBOutlet weak var paidCurrency: UILabel!
    @IBOutlet weak var lyCdCmLbl: UILabel!
    @IBOutlet weak var dpsitCurncyLbl: UILabel!
    @IBOutlet weak var dpsitLyAmouunt: UILabel!
    @IBOutlet weak var gPOutlyCdCmLbl: UILabel!
    @IBOutlet weak var gPOutCurncyLbl: UILabel!
    @IBOutlet weak var gPOutLyAmouunt: UILabel!
    
    @IBOutlet weak var loaderView: UIView!
    
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
