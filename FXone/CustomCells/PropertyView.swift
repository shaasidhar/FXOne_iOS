//
//  PropertyView.swift
//  FXone
//
//  Created by Vinayak on 28/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class PropertyView: UIView {

    var view:UIView!
    
    
    @IBOutlet weak var hSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var btn_selection: UIButton!
    @IBOutlet weak var lbl_title: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        
        btn_selection.setImage(UIImage.init(named: "Deselect"), for: .normal)
        btn_selection.setImage(UIImage.init(named: "Select"), for: .selected)
    }
    
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "PropertyView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
    }
}
