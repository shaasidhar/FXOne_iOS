//
//  PieChartLegendLabelsTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 19/04/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import MarqueeLabel

class PieChartLegendLabelsTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_status: UILabel!
    
    @IBOutlet weak var lbl_title: MarqueeLabel!
    
    @IBOutlet weak var lbl_count: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // MarqueeLabel
        //if lbl_statusname.text?.count ?? 0 >= 10 {
        //MarQuee
        lbl_title.type = .continuous
        lbl_title.speed = .duration(10.0)
        lbl_title.animationCurve = .easeInOut
        lbl_title.fadeLength = 10.0
        lbl_title.leadingBuffer = 5.0
        lbl_title.trailingBuffer = 20.0
            
        lbl_status.layer.cornerRadius = lbl_status.frame.width / 5
        lbl_status.layer.masksToBounds = true
        //lbl_count.textColor = .black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
