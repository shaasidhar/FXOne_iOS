//
//  SalesByMenuTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 20/11/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class SalesByMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lbl_totalPercentage: UILabel!
    @IBOutlet weak var percentage_View: UIView!
    @IBOutlet weak var progress_View: UIView!
    @IBOutlet weak var lbl_MenuItem: UILabel!
    @IBOutlet weak var lbl_Amount: UILabel!
    
    @IBOutlet weak var lbl_apCheck: UILabel!
    @IBOutlet weak var lbl_budgetAmount: UILabel!
    
    @IBOutlet weak var lbl_cyAmount: UILabel!
    
    @IBOutlet weak var lbl_lyAmount: UILabel!
    
    @IBOutlet weak var lbl_ApCoverAmount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        progressView.transform = progressView.transform.scaledBy(x: 1, y: 7.9)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
