//
//  UICoreEngine.swift
//  FXone
//
//  Created by Vinayak on 2/14/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import Foundation
import UIKit

public class UICoreEngine {
    public static let sharedInstance = UICoreEngine()
    private init() {} //Singleton!
    var userRoles: [LoginRoleModel]? {
        var items = [LoginRoleModel]()
        if let userRights = UserDefaults.standard.codableObject(dataType: [LoginUserRights].self, key: "USERRIGHTS") {
            
            for each in userRights.first?.Roles ?? [] {
                let funcCode = each.FunctionCode ?? ""
                if funcCode.contains("FXONE") {
                    items.append(each)
                }
            }
            return items
            
        } else {
            return items
        }
        
    }
    var userAdmin: UserDetailsData? {
        let propertyAdmin = UserDefaults.standard.codableObject(dataType: UserDetailsData.self, key: "USERDETAILS")
        return propertyAdmin ?? UserDetailsData()
    }
    var subscribedProducts: [SubscribedProducts]? {
        var items = [SubscribedProducts]()
        if let subscribed = UserDefaults.standard.codableObject(dataType: [SubscribedProducts].self, key: "SubscribedProducts") {
            return subscribed
            
        } else {
            return items
        }
        
    }
    
    
//    var subscribedProducts: SubscribedProducts? {
//        let propertyAdmin = UserDefaults.standard.codableObject(dataType: SubscribedProducts.self, key: "SubscribedProducts")
//        return propertyAdmin ?? SubscribedProducts()
//    }
    public func getKeyWindow() -> UIWindow? {
        return UIApplication.shared.keyWindow
    }
    func ShowSuccessAlertOnWindows(message:String,title:String,onSuccess: @escaping (String) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            onSuccess("success fuly submited")
        }
        alertController.addAction(okAction)
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
            window.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        //UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func topMostController() -> UIViewController {
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    func logOutclicked() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removeObject(forKey: "ISAUTOLOGIN")
        UserDefaults.standard.removeObject(forKey: "PmsCustCode")
        UserDefaults.standard.removeObject(forKey: "Token")
        UserDefaults.standard.removeObject(forKey: "User_Id")
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        let initial = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
        let nav = UINavigationController(rootViewController: initial)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = nav
    }
    
    func showAlertOnLogout(message:String,title:String,isNoBUtton:Bool, viewController : UIViewController) {
        
        DispatchQueue.main.async {
            if !isNoBUtton{
                let alert = UIAlertController(title: title, message: message , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                viewController.present(alert, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: title, message: message , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                    self.logOutclicked()
                }))
                alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
                viewController.present(alert, animated: true, completion: nil)
                
            }
        }
        
        
    }
    
    func ShowAlertOnWindows(message:String,title:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed1")
        }
        alertController.addAction(okAction)
        let vc = self.topMostController()
        vc.present(alertController, animated: true, completion: nil)

    }
    func showAlert(message:String,title:String,isNoBUtton:Bool, viewController : UIViewController) {
        
        DispatchQueue.main.async {
        if !isNoBUtton{
            let alert = UIAlertController(title: title, message: message , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            viewController.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: title, message: message , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in

            }))
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
            viewController.present(alert, animated: true, completion: nil)

        }
        }

        
    }

    

}
