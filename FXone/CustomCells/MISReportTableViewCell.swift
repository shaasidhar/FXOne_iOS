//
//  MISReportTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 12/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class MISReportTableViewCell: UITableViewCell {

    @IBOutlet weak var view_misReport: MISRFooterView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var misRoport:MISReportModel? {
        didSet{
            self.updateCellData()
        }
    }
    func updateCellData(){
        view_misReport.lbl_title.text = misRoport?.name ?? ""
        //view_misReport.lbl_actualAmount.text = misRoport?.acyValue ?? ""
        let floatVale = misRoport?.acyValue ?? ""
        let myFloat = (floatVale as NSString).doubleValue
        //view_misReport.lbl_actualAmount.text = String(format:"%.02f", myFloat )
//        view_ssViewLabels.lbl2.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.ADR ?? 0.0))"
        view_misReport.lbl_actualAmount.text = CommonDataManager.shared.getNumberWithCommas(cash: myFloat)
        //view_misReport.lbl_actualAmount.text = String(format:"%.02f", misRoport?.acyValue ?? "")
        view_misReport.lbl_budgetAmount.text = misRoport?.bcyValue ?? ""
        view_misReport.lbl_LyAmount.text = misRoport?.actualLY ?? ""
        /*
         view_misReport.lbl_title.text = misRoport?.Description ?? ""
         view_misReport.lbl_actualAmount.text = "\(misRoport?.NetAmount ?? 0.0)"
         view_misReport.lbl_budgetAmount.text = "\(misRoport?.Budget ?? 0)"
         view_misReport.lbl_LyAmount.text = "\(misRoport?.LastyearDay ?? 0)"
         */
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
