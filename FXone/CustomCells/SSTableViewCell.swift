//
//  SSTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 13/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class SSTableViewCell: UITableViewCell {

    @IBOutlet weak var view_ssViewLabels: SSHeaderView!
    var selectedSegmentIndex = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var statsReport:SalesStatisticsDataModel? {
        
        didSet {
            print(self.selectedSegmentIndex)
            switch self.selectedSegmentIndex {
            case 0:
                self.updateCellDataDay()
            case 1:
                self.updateCellDataMonth()
            case 2:
                self.updateCellDataYear()
            case 3:
                self.updateCellDataCustom()
            default:
                print("No segment Selected")
            }
        }
    }

    func updateCellDataCustom(){
        view_ssViewLabels.backgroundColor = UIColor.clear
        view_ssViewLabels.lbl1.text = "\(String(describing: statsReport?.Custom?[0].HotelName ?? ""))"
        view_ssViewLabels.lbl2.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Custom?[0].ADR ?? 0.0))"
        //self.lbl_amount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: double_todayCost))"
        if let lyADR = statsReport?.Custom?[0] .LYADR {
            if lyADR == "decr" {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyADR == "incr" {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        view_ssViewLabels.lbl3.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Custom?[0] .RevPer ?? 0.0))"
        if let lyRevPar  = statsReport?.Custom?[0].LYRevPer {
            if lyRevPar == "decr" {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyRevPar == "incr" {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        view_ssViewLabels.lbl4.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Custom?[0].Amount ?? 0.0))"
        if let lyAmount = statsReport?.Custom?[0].LYAmount {
            if lyAmount == "decr" {
                view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyAmount == "incr" {
                view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
            view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        view_ssViewLabels.lbl1.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl2.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl3.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl4.font = UIFont(name: "Roboto-Regular", size: 14)
    }
    func updateCellDataYear() {
        view_ssViewLabels.backgroundColor = UIColor.clear
        
        view_ssViewLabels.lbl1.text = "\(String(describing: statsReport?.Year?[0].HotelName ?? ""))"
        view_ssViewLabels.lbl2.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Year?[0].ADR ?? 0.0))"
        //self.lbl_amount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: double_todayCost))"
        if let lyADR = statsReport?.Year?[0] .LYADR {
            if lyADR == "decr" {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyADR == "incr" {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        view_ssViewLabels.lbl3.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Year?[0] .RevPer ?? 0.0))"
        if let lyRevPar  = statsReport?.Year?[0].LYRevPer {
            if lyRevPar == "decr" {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyRevPar == "incr" {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        view_ssViewLabels.lbl4.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Year?[0].Amount ?? 0.0))"
        if let lyAmount = statsReport?.Year?[0].LYAmount {
            if lyAmount == "decr" {
                view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyAmount == "incr" {
                view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
                view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        
        view_ssViewLabels.lbl1.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl2.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl3.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl4.font = UIFont(name: "Roboto-Regular", size: 14)
    }
    func updateCellDataMonth() {
        view_ssViewLabels.backgroundColor = UIColor.clear
        
        view_ssViewLabels.lbl1.text = "\(String(describing: statsReport?.Month?[0].HotelName ?? ""))"
        view_ssViewLabels.lbl2.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Month?[0].ADR ?? 0.0))"
        //self.lbl_amount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: double_todayCost))"
        if let lyADR = statsReport?.Month?[0] .LYADR {
            if lyADR == "decr" {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyADR == "incr" {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        view_ssViewLabels.lbl3.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Month?[0] .RevPer ?? 0.0))"
        if let lyRevPar  = statsReport?.Month?[0].LYRevPer {
            if lyRevPar == "decr" {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyRevPar == "incr" {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
            view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        view_ssViewLabels.lbl4.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Month?[0].Amount ?? 0.0))"
        if let lyAmount = statsReport?.Month?[0].LYAmount {
            if lyAmount == "decr" {
                view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyAmount == "incr" {
                view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
            view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        
        view_ssViewLabels.lbl1.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl2.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl3.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl4.font = UIFont(name: "Roboto-Regular", size: 14)
    }
    func updateCellDataDay(){
        view_ssViewLabels.backgroundColor = UIColor.clear
        
        view_ssViewLabels.lbl1.text = "\(String(describing: statsReport?.Day?[0].HotelName ?? ""))"
        view_ssViewLabels.lbl2.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Day?[0].ADR ?? 0.0))"
        //self.lbl_amount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: double_todayCost))"
        if let lyADR = statsReport?.Day?[0] .LYADR {
            if lyADR == "decr" {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyADR == "incr" {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
                view_ssViewLabels.lbl2.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        view_ssViewLabels.lbl3.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Day?[0] .RevPer ?? 0.0))"
        if let lyRevPar  = statsReport?.Day?[0].LYRevPer {
            if lyRevPar == "decr" {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyRevPar == "incr" {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
                view_ssViewLabels.lbl3.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        view_ssViewLabels.lbl4.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: statsReport?.Day?[0].Amount ?? 0.0))"
        if let lyAmount = statsReport?.Day?[0].LYAmount {
            if lyAmount == "decr" {
                view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "Decrease 2", behindText: true, boundX: -2, boundY: 10)
            }
            else if lyAmount == "incr" {
                view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "Increase 2", behindText: true, boundX: -2, boundY: 10)
            }
            else {
            view_ssViewLabels.lbl4.addImageWithBoundaryDetailsPage(name: "", behindText: true, boundX: -2, boundY: 10)
            }
        }
        
        view_ssViewLabels.lbl1.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl2.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl3.font = UIFont(name: "Roboto-Regular", size: 14)
        view_ssViewLabels.lbl4.font = UIFont(name: "Roboto-Regular", size: 14)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
