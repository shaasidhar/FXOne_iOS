//
//  CollectionCell.swift
//  FXone
//
//  Created by Vinayak on 08/04/20.
//  Copyright © 2020 IDS. All rights reserved.
//

import UIKit

class CollectionCell: UITableViewCell {

    @IBOutlet weak var collectionValue_Lbl: UILabel!
    @IBOutlet weak var collectionTitle_Lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
