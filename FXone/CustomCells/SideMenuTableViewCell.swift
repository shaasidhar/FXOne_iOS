//
//  SideMenuTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 12/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var image_menuItem: UIImageView!
    @IBOutlet weak var lbl_menuTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var menuObj : MenuModel? {
        didSet{
            self.setUpMenuData()
        }
    }
    func setUpMenuData(){
        
        self.lbl_menuTitle.text = menuObj?.name ?? ""
        
        if menuObj?.key == "misReport"{
            self.image_menuItem.image = UIImage.init(named: "MIS Report")
        }
        else if menuObj?.key == "Front Office" || menuObj?.key == "financeDashboard" {
            self.image_menuItem.image = UIImage.init(named: "Dashboard")
        }
        else if menuObj?.key == "logout"{
            self.image_menuItem.image = UIImage.init(named: "logout")
        }
        else if menuObj?.key == "salesStatistics"{
            self.image_menuItem.image = UIImage.init(named: "Sales Statistics")
        }
        else if menuObj?.key == "Property"{
            self.image_menuItem.image = UIImage.init(named: "property")
            self.image_menuItem.tintColor = .gray
            self.image_menuItem.backgroundColor = .gray
        }
        else {
            self.image_menuItem.image = UIImage.init(named: "dashboard")
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
