//
//  CollectionTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 08/04/20.
//  Copyright © 2020 IDS. All rights reserved.
//

import UIKit

class CollectionTableViewCell: UITableViewCell {

    
    @IBOutlet weak var viewContent: UIView!
    
    var collectionDict:[String:Int] = ["Cash": 10000, "Card": 1234,"City Ledger": 2409, "Others": 123124, "Credit Card": 9075, "Debit Card":36057]

    @IBOutlet weak var collectionview: CollectionView!
    //let collectionview = CollectionView.init()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //self.baseView.addSubview(collectionview)
         
        //self.viewContent.frame = super.frame
        self.viewContent.backgroundColor = .clear
        self.viewContent.layer.cornerRadius = 10
        self.viewContent.layer.masksToBounds = true

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
