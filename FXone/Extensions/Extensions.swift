//
//  Extensions.swift
//  FXone
//
//  Created by Vinayak on 08/04/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func stringToDate(dateString:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateString)
        return date ?? Date()
    }
    
}
extension UIColor {
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension Double {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal

        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
//9 september
extension UIColor
{
    
    //color codes from dharma
    class func whiteColor() -> UIColor{
        
        return UIColor(red:255/255, green:255/255 ,blue:255/255 , alpha:1.0)
    }
    class func FXYellowColor() -> UIColor{
        
        return UIColor(red:255/255, green:136/255 ,blue:45/255 , alpha:1.0)
    }
    
}

extension UILabel {
    
    func addImageWithDetailsPage(name: String, behindText: Bool) {
        addImageWithBoundaryDetailsPage(name: name, behindText: behindText, boundX: 0, boundY: -2)
    }
    func addImageWithBoundaryDetailsPage(name: String, behindText: Bool, boundX: CGFloat, boundY: CGFloat) {
        
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: name)
        let attachmentString = NSAttributedString(attachment: attachment)
        guard let imageSize = UIImage(named: name)?.size
            else { return }
        attachment.bounds = CGRect(x: boundX, y: boundY, width: imageSize.width, height: imageSize.height)
        
        guard let txt = self.text else {
            return
        }
        
        if behindText {
            let strLabelText = NSMutableAttributedString(string: " \(txt)  ")
            strLabelText.append(attachmentString)
            self.attributedText = strLabelText
        } else {
            let strLabelText = NSAttributedString(string: " \(txt)  ")
            let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)
            self.attributedText = mutableAttachmentString
        }
    }
}


//For custom fonts
extension UIFont {
    
    class func FXRobotoRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Regular", size: size)!
    }
    
//    class func FXRobotoBlackItalic(size: CGFloat) -> UIFont {
//        return UIFont(name: "Roboto-BlackItalic", size: size)!
//    }
    class func FXRobotoBlackMedium(size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Medium", size: size)!
    }
    
    class func FXRobotoBlackBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Bold", size: size)!
    }
    
    
}




extension UIView {
    
    func showSlideMenu(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                       animations: {
                        //self.center.x +=  self.bounds.width
                        self.frame = CGRect(x: 0, y: 0, width:self.bounds.width, height: self.frame.height)
                        self.layoutIfNeeded()
        }, completion: nil)
        
    }
    func hideSlideMenu(){
        UIView.animate(withDuration: 0, delay: 0, options: [.curveEaseOut],
                       animations: {
                        self.center.x -=  self.bounds.width + 50
                        self.layoutIfNeeded()
                        
        },  completion: {(_ completed: Bool) -> Void in
            
        })
    }
}
extension UIViewController {
    
    func addAlphaView() {
        let alphaView1 = UIView(frame: self.view.bounds)
        alphaView1.alpha = 0.5
        alphaView1.tag = 1010
        alphaView1.backgroundColor = UIColor.black
        alphaView1.isHidden = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        alphaView1.addGestureRecognizer(tap)
        let window = UIApplication.shared.keyWindow
        window?.addSubview(alphaView1)
        
        
    }
    @objc func handleTap(sender: UITapGestureRecognizer) {
        
    }
    func removeAlphaView() {
        let window = UIApplication.shared.keyWindow
        let vobj = window?.viewWithTag(1010)
        vobj?.removeFromSuperview()
    }
}
protocol Loopable {
    func allProperties() throws -> [String: Any]
}

extension Loopable {
    func allProperties() throws -> [String: Any] {
        
        var result: [String: Any] = [:]
        
        let mirror = Mirror(reflecting: self)
        
        // Optional check to make sure we're iterating over a struct or class
        guard let style = mirror.displayStyle, style == .struct || style == .class else {
            throw NSError()
        }
        
        for (property, value) in mirror.children {
            guard let property = property else {
                continue
            }
            
            result[property] = value as? Any
        }
        
        return result
    }
}
extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    func dateStringToMonthDayString(dateStr:String) -> String {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        guard let date = dateFormatter1.date(from: dateStr) else { return "" }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "d MMM yyyy" // "EE" to get short style
        let dayInWeek = dateFormatter.string(from: date)
        return dayInWeek
    }
    
    
    func dateToString(date:Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
}

extension Date{
    
    func toString(withFormat format: String = "dMMMyy") -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let strMonth = dateFormatter.string(from: self)
        
        return strMonth
    }
}
extension UIColor {
    var coreImageColor: CIColor {
        return CIColor(color: self)
    }
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        let coreImageColor = self.coreImageColor
        return (coreImageColor.red, coreImageColor.green, coreImageColor.blue, coreImageColor.alpha)
    }
}

extension Dictionary {
    func getElement(index: Int) -> (key: Dictionary.Key, value:Dictionary.Value)? {
        var dict_i = self.startIndex
        for i1 in 0...index {
            guard dict_i != self.endIndex else {
                // at the end of the dictionary
                return nil
            }
            if i1==0 {
                dict_i = self.startIndex
                continue
            }
            dict_i = self.index(after: dict_i)
        }
        return self[dict_i]
    }
}

