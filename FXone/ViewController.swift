//
//  ViewController.swift
//  FXone
//
//  Created by Vinayak on 4/1/19.
//  Copyright © 2019 IDS. All rights reserved.

import UIKit
import Alamofire
import CalendarDateRangePickerViewController
import PanModal
import DropDown
import SpreadsheetView
import MarqueeLabel

struct DashboardAPIConstants {
    static let  FxOneDashBoard_RoomSale =
   // "\(CommonDataManager.shared.baseUrl)\(DashBoard_RoomSale)"
    "http://fooperationsapi.azurewebsites.net/V1.0.0/KPI/FxOneDashBoard_RoomSale"
    static let  FxOneDashBoard_FNBSale =
    //"\(CommonDataManager.shared.baseUrl)\(DashBoard_FNBSale)"
    "http://fooperationsapi.azurewebsites.net/V1.0.0/KPI/FxOneDashBoard_FNBSale"
    static let FxOneDashBoard_NonFNBSale =
    //"\(CommonDataManager.shared.baseUrl)\(DashBoard_NonFNBSale)"
    "http://fooperationsapi.azurewebsites.net/V1.0.0/KPI/FxOneDashBoard_NonFNBSale"
    static let FxOneDashBoard_Collection =
    //"\(CommonDataManager.shared.baseUrl)\(DashBoard_Collection)"
    "http://fooperationsapi.azurewebsites.net/V1.0.0/KPI/FxOneDashBoard_Collection"
    static let FxOneDashBoard_Deposit =
    //"\(CommonDataManager.shared.baseUrl)\(DashBoard_Deposit)"
    "http://fooperationsapi.azurewebsites.net/V1.0.0/KPI/FxOneDashBoard_Deposit"
    static let FxOneDashBoard_PaidOut =
    //"\(CommonDataManager.shared.baseUrl)\(DashBoard_PaidOut)"
    "http://fooperationsapi.azurewebsites.net/V1.0.0/KPI/FxOneDashBoard_PaidOut"
}
struct RoomACS {
    var statusColor : UIColor?
    var name :String?
    var count :String?
}
struct RoomStatusByDay {
    var date:String?
    var list : [RoomACS]?
}
struct Item: Decodable{
    let url: URL
}
class CellClass: UITableViewCell{
    
}
class ViewController: UIViewController ,UIPopoverPresentationControllerDelegate {
    @IBOutlet weak var currencyTitileLbl: UILabel!
    let dispatchGroup = DispatchGroup()
    var dataloadedFirstTime = true
    @IBOutlet weak var lblCurrencyValue: UILabel!
    @IBOutlet weak var lblSelectedCurrencyName: UILabel!
    @IBOutlet weak var tfdCurrencyValue: UITextField!
    @IBOutlet weak var baseHeightConst: NSLayoutConstraint!
    @IBOutlet weak var tableview_listSales: UITableView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topItemView: UIView!
    @IBOutlet weak var image_Logo: UIImageView!
    @IBOutlet weak var btnNotificationIcon: UIButton!
    @IBOutlet weak var lbl_PropName: MarqueeLabel!
    @IBOutlet weak var lbl_DashboardTitle: UILabel!
    @IBOutlet weak var appLogoTitleCarryView: UIView!
    @IBOutlet weak var view_bgColor: UIView!
    var isLive = false
    var dAcDate = ""
    var aryMainList: [String?] = [nil,nil,nil,nil,nil,nil]
    var dashRoomSaleData = DashBoardModel()
    var dashFAndBData =  DFANDBModel()
    var dashOthersData =  DFANDBModel()
    var dashCollectionData = DashCollectionDataModel()
    var dashDepositData = DashCollectionDataModel()
    var dashPaidOutData =  DashCollectionDataModel()
    var olddashRoomSaleData = DashBoardModel()
    var olddashFAndBData =  DFANDBModel()
    var olddashOthersData =  DFANDBModel()
    var olddashCollectionData = DashCollectionDataModel()
    var olddashDepositData = DashCollectionDataModel()
    var olddashPaidOutData =  DashCollectionDataModel()
    //curncyCode
    @IBOutlet weak var viewCurncyItems: UIView!
    @IBOutlet weak var btnCurncyCode: UIButton!
    @IBOutlet weak var btnCurncyIcon: UIView!
    @IBOutlet weak var btnConfirmCurncyRate: UIButton!
    @IBOutlet weak var logo_IconFX: UIImageView!
    let transparentView = UIView()
    let curncyCodeTableview = UITableView()
    var selectedBtn = UIButton()
    var dataSource = [String]()
    var aryCurrency = [CurrencyListResponseModel]()
    var selectedCurrencyValueIndex = 0
    var selectedCurrencyValue = ""
    var selectedCurrencyCode = ""
    var selectedStartDate = ""
    var selectedEndDate = ""
    var ary_listofRAStatus = [RoomACS]()
    var listAry = [RoomStatusByDay]()
    var totalProperties = [PropertyModel]()
    var isFinanceDashboard = false
    var hotel_Name = String()
    
    var mainList = [[String:Any]]()
    var coustomList = [[String:Any]]()
    var selectedSegmentIndex = 1
    var currencyCode = String()
    var accountingDate = String()
    var acDateOccupancy = String()
    var percentageOccupied = Float()
    var percentageOccupiedLy = Float()
    var totalRooms = Int()
    var str = String()
    var ac_date : Date?
        
    @IBOutlet weak var collectionview_roomOccupancy: UICollectionView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet var lbl_Headertitle: UILabel!
    @IBOutlet weak var custom_DateSelector: UIButton!
    @IBOutlet weak var lbl_accountingDate: UILabel!
    
    var sideBarView : SideMenuView!
    var selectHotelsVC: SelectHotelsPopupVC?
    
    var dayCollections = CollectionsDataModel()
    var daydepoist = CollectionsDataModelArry()
    var monthdepoist = CollectionsDataModelArry()
    var yeardepoist = CollectionsDataModelArry()
    var dayPaidOut = CollectionsDataModelArry()
    var monthPaidOut = CollectionsDataModelArry()
    var yearPaidOut = CollectionsDataModelArry()
    
    var curncyList = CurrencyListResponseModel()

   // var monthCollections = CollectionsDataModelArry()
   // var yearCollections = CollectionsDataModelArry()

    // for previous property selected
    var ispropertySelected : Bool?
    var isCustom : Bool?
    var isDetailedTap : Bool?
    var isGroupPropertySelected : Bool?
    var hasDataDetails = false
    var isFromCoustome = false
    var isDepositLive = false
    var isPaidLive = false
    var startDate = NSDate()
    var endDate = NSDate()
    var navigateToHome = NavigateToHome.RoomOccupancy
    var bubbleCell: BubbleTableViewCell?
    var isLoader = true
    var isRoomOccupancyLoader = true
    var isRoomSalesLoader = true
    var isFBRevenueLoader = true
    var isOtherRevenueLoader = true
    var isCollectionDataLoader = true
    var isDepositePaidLoader = true
    var roomSaleFooterView = RoomSaleFooterView()
    var isPassData = true
    
    
    @IBOutlet weak var dateView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
        self.listAry.removeAll()
        self.ary_listofRAStatus.removeAll()
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bubbleCell = BubbleTableViewCell()
        self.dateView.isHidden = false
        tableview_listSales.backgroundColor = UIColor.clear
        self.registerUInibCell()
        self.SourceandDelegate()
        // hide notifications for time being
        self.btnNotificationIcon.isHidden = true
        self.marqueeLabel()
        self.getAppLogoImage()
        self.appLogoTitleCarryView.layer.cornerRadius = 3
        self.logo_IconFX.layer.cornerRadius = 5
        //self.propertyColor()
        //Demo in dubai hidding currency elements
        self.btnNotificationIcon.isHidden = true
        //undo to enbla currency
        self.btnCurncyIcon.isHidden = true
        self.currencyTitileLbl.isHidden = true
        self.lblCurrencyValue.isHidden = true
        self.lblSelectedCurrencyName.isHidden = true
        self.tableview_listSales.separatorStyle = .none
        self.tableview_listSales.allowsSelection = true
        self.custom_DateSelector.layer.borderWidth = 1
        self.custom_DateSelector.layer.cornerRadius = 5.0
        self.collectionview_roomOccupancy.layer.cornerRadius = 10
        self.title = "Dashboard"
        self.tableview_listSales.showsVerticalScrollIndicator = false
        self.getCurrencyList()
        let roles = UICoreEngine.sharedInstance.userRoles
        let propAdmin = UICoreEngine.sharedInstance.userAdmin
        //undo code for room occu access right
        if propAdmin?.UserType == "PropertyAdmin" {
            self.getRoomOccupancyData()
        }
        else {
            let result1 =  roles?.filter { $0.FunctionCode == "FXONE01"  &&  $0.AccessRights == "View" }
            if result1?.count ?? 0 > 0 {
                self.getRoomOccupancyData()
            } else {
                self.collectionViewHeightConstraint.constant = 0
            }
        }
        self.updateUI()
        self.getAccountentDate()
        self.getSingleProperty()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.didPullToRefresh), for: .valueChanged)
        self.tableview_listSales.addSubview(refreshControl)
    }
    
    func marqueeLabel(){
        lbl_PropName.type = .continuous
        lbl_PropName.speed = .duration(10.0)
        lbl_PropName.animationCurve = .easeInOut
        lbl_PropName.fadeLength = 10.0
        lbl_PropName.leadingBuffer = 5.0
        lbl_PropName.trailingBuffer = 20.0
        lbl_PropName.text = UserDefaults.standard.object(forKey: "Hotel_Name") as? String
    }
    
    func getAppLogoImage() {
        if let image_logoURL = UserDefaults.standard.object(forKey: "imageLogo_URL") as? String {
            let imageUrl = URL(string: image_logoURL)
            let urlRequest = URLRequest(url: imageUrl ?? URL(fileURLWithPath: ""))
            let dataTask = URLSession.shared.dataTask(with: urlRequest, completionHandler: { data, response, error in
                if let imageData = data {
                    DispatchQueue.main.async {
                        self.image_Logo.image = UIImage(data: imageData)
                    }
                }
            })
            
            dataTask.resume()
        }
    }
    func registerUInibCell(){
        collectionview_roomOccupancy.register(UINib(nibName: "RoomOccupancyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RoomOccupancyCollectionViewCell")
        self.tableview_listSales.register(UINib.init(nibName: "BubbleTableViewCell", bundle: nil), forCellReuseIdentifier: "BubbleCell")
        self.tableview_listSales.register(UINib.init(nibName: "CollectionTableViewCell", bundle: nil), forCellReuseIdentifier: "CollectionTCCell")
        self.tableview_listSales.register(UINib.init(nibName: "DepositTableViewCell", bundle: nil), forCellReuseIdentifier: "DepositTCCell")
        self.tableview_listSales.register(UINib.init(nibName: "BubbuleFooterViewTableViewCell", bundle: nil), forCellReuseIdentifier: "BubbleFooterCell")
        self.curncyCodeTableview.register(CellClass.self, forCellReuseIdentifier: "Cell")
    }
    func SourceandDelegate(){
        self.tableview_listSales.delegate = self
        self.tableview_listSales.dataSource = self
        collectionview_roomOccupancy.dataSource = self as UICollectionViewDataSource
        collectionview_roomOccupancy.delegate = self as UICollectionViewDelegate
        //CurncyCodeTableview
        self.curncyCodeTableview.delegate = self
        self.curncyCodeTableview.dataSource = self
    }
    
    let colorCode = UserDefaults.standard.object(forKey: "Color_Code") as! String
    func propertyColor(){
        view_bgColor.backgroundColor = UIColor.hexStringToUIColor(hex: colorCode)
        custom_DateSelector.layer.borderColor = UIColor.hexStringToUIColor(hex: colorCode).cgColor
        segmentControl.selectedSegmentTintColor = UIColor.hexStringToUIColor(hex: colorCode)
    }
    
    func segmentTitleConversion(){
        let lv_Date = dAcDate
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MM-dd-yyyy"
        var live_Date = String()
        if let date = inputFormatter.date(from: lv_Date) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "ddMMMyyyy"

            live_Date = outputFormatter.string(from: date)
            print(live_Date)
        } else {
            print("No date")
        }
        GlobalAccountDateData.shared.accountDate_Global_Live = dAcDate
        let ac_date : Date? = CommonDataManager.shared.parseDate(dateString: self.accountingDate)
        let today_Date = CommonDataManager.shared.getDateStringDDMMMYYYYWithOutSpaces(fromDate: ac_date ?? Date())
        
        let defaultAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
        DispatchQueue.main.async {
            
            self.segmentControl.setTitleTextAttributes(defaultAttributes, for: .normal)
            let largerFontAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 9)]
            self.segmentControl.setTitleTextAttributes(largerFontAttributes, for: .normal)
            self.segmentControl.setTitle(live_Date, forSegmentAt: 0)
            self.segmentControl.setTitle(today_Date, forSegmentAt: 1)
        }
    }
    
    func addTransparentView(frames: CGRect) {
        let window = UIApplication.shared.keyWindow
        transparentView.frame = window?.frame ?? self.view.frame
        self.view.addSubview(transparentView)
        curncyCodeTableview.frame = CGRect(x: frames.origin.x, y: frames.origin.y + frames.height, width: frames.width, height: 0)
        self.view.addSubview(curncyCodeTableview)
        curncyCodeTableview.layer.cornerRadius = 5
        
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        curncyCodeTableview.reloadData()
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(removeTransparentView))
        transparentView.addGestureRecognizer(tapgesture)
        transparentView.alpha = 0
        UIView.animate(withDuration: 0.4, delay: 0.0,usingSpringWithDamping: 1.0,initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.5
            self.curncyCodeTableview.frame = CGRect(x: frames.origin.x + 20, y: frames.origin.y + frames.height + 60, width: frames.width, height: CGFloat(self.dataSource.count * 30))
        }, completion: nil)
    }
    @objc func removeTransparentView() {
        let frames = selectedBtn.frame
        UIView.animate(withDuration: 0.4, delay: 0.0,usingSpringWithDamping: 1.0,initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0
            self.curncyCodeTableview.frame = CGRect(x: frames.origin.x, y: frames.origin.y + frames.height, width: frames.width, height: 0)
        }, completion: nil)
    }
    @IBAction func onClickCurncyCodeBtn(_ sender: Any) {
        dataSource = ["INR", "KEY", "USD", "EU", "GBP"]
        selectedBtn = btnCurncyCode
        addTransparentView(frames: btnCurncyCode.frame)
    }
    @IBAction func onClickBtnCrncyIcon(_ sender: Any) {
//        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.frame = view.bounds
//        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        topItemView.addSubview(blurEffectView)
//        tableview_listSales.addSubview(blurEffectView)
        //viewCurncyItems.isHidden = false
        //collectionview_roomOccupancy.isHidden = true
        //topItemView.isHidden = true
        let st = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: "CurrencyTypeViewController") as! CurrencyTypeViewController
        vc.aryCurrency = self.aryCurrency
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickBtnConfirmCurnyRate(_ sender: Any) {
        viewCurncyItems.isHidden = true
        collectionview_roomOccupancy.isHidden = false
        topItemView.isHidden = false
    }
    @objc func didPullToRefresh() {
        // get pms code from properties list
        self.listAry.removeAll()
        self.ary_listofRAStatus.removeAll()
        if let pmsCustCode = getPMSFromSelectedProperty() {
            // set pmscode globally
            UserDefaults.standard.set(String(pmsCustCode), forKey: "PmsCustCode")
        }
        //self.getSalesByDuration()
        self.callAllDashboardAPIs()
        self.getRoomOccupancyData()
        DispatchQueue.main.async {
            let subviews = self.tableview_listSales.subviews
            for view in subviews {
                if view is UIRefreshControl {
                    let control = view as? UIRefreshControl
                    control?.endRefreshing()
                }
            }
        }
        DispatchQueue.main.async {
                   let subviews = self.collectionview_roomOccupancy.subviews
                   for view in subviews {
                       if view is UIRefreshControl {
                           let control = view as? UIRefreshControl
                           control?.endRefreshing()
                       }
                   }
               }
    }
    
    //We are presenting hotels selection popup
    @IBAction func buttonActionProperty(_ sender: UIButton) {
        
        let selectHotelsVC = self.storyboard?.instantiateViewController(withIdentifier: "selectHotelsVC") as! SelectHotelsPopupVC
        selectHotelsVC.pDelegate = self
//        if totalProperties.count > 0 {
//            totalProperties[0].property?[0].selected = true
//        }
//        else {
//            totalProperties[0].property?[0].selected = false
//        }
        print("self.totalProperties is ======>>>>>",self.totalProperties)
        selectHotelsVC.totalProperties = self.totalProperties
        presentPanModal(selectHotelsVC)
    }
    func parseGroupProperties(list:[PropertyResponseObj]?){
        
            for each in list ?? [] {
            var property1 = PropertyModel()
            property1.title = each.GroupDetails?.GroupName ?? ""
            //In case all properties selected (All properties in the group)
            if isGroupPropertySelected ?? false {
                 property1.selected = true
            }
            else {
                property1.selected = false
            }
            // show Group default selected in check box
            
            var ary = [Property1Model]()
            for property in each.PropertyDetails ?? [] {
                var p = Property1Model()
                p.title = property.PropertyName
                p.titlePms = property.PmsCustCode
                
                if ispropertySelected ?? false {
                   p.selected = true
                }
                else {
                    p.selected = false

                }
                //In case few property OR properties selected in the gourp
                 // show Property default selected in check box// here is the line show privious selectoion
                ary.append(p)
            }
            property1.property = ary
            totalProperties.append(property1)
            
            //totalProperties.append(contentsOf: totalProperties)
        }
        if totalProperties.count > 0 {
            totalProperties[0].property?[0].selected = true
        }

    }
    
    @IBAction func Segments(_ sender: UISegmentedControl) {
        if #available(iOS 13.0, *) {
            //segmentControl.selectedSegmentTintColor = UIColor.hexStringToUIColor(hex: colorCode)
        } else {
            // Fallback on earlier versions
        }
        selectedStartDate = ""
        selectedEndDate = ""
        isLive = false
        isFromCoustome = false
        self.dashRoomSaleData = self.olddashRoomSaleData
        coustomList.removeAll()
        self.selectedSegmentIndex = sender.selectedSegmentIndex
        if selectedSegmentIndex == 0 {
            isLive = true
            dataloadedFirstTime = true

            self.isReloadData(isLoad: true)
                self.getAccountentDate()
            
            lbl_accountingDate.isHidden = true
            custom_DateSelector.isEnabled = false
            self.isPassData = false
            
        } else {
            lbl_accountingDate.isHidden = false
            custom_DateSelector.isEnabled = true
            self.isPassData = true
                if self.dataloadedFirstTime {
                    self.isReloadData(isLoad: true)
                    self.callAllDashboardAPIs()
                    self.dataloadedFirstTime = false
                } else {
                    self.isRoomSalesLoader = false
                    self.tableview_listSales.reloadData()
                }
           
        }
        self.custom_DateSelector.tintColor = .red
        self.custom_DateSelector.setTitle("Custom", for: .normal)
        self.custom_DateSelector.backgroundColor = UIColor.hexStringToUIColor(hex: "F2F2F3")
        //F2F2F3
        self.custom_DateSelector.setTitleColor(UIColor.hexStringToUIColor(hex: "FC7A45"), for: .normal)
        self.custom_DateSelector.layer.borderWidth = 1
        self.custom_DateSelector.layer.borderColor = UIColor.hexStringToUIColor(hex: "#000000").cgColor
        self.custom_DateSelector.titleLabel?.font = UIFont.FXRobotoRegular(size: 13)//systemFont(ofSize: 15)
        self.segmentControl.tintColor = UIColor.hexStringToUIColor(hex: colorCode)
        self.segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)

    }
    
    func isReloadData(isLoad: Bool)  {
         
         isRoomSalesLoader = isLoad
         isFBRevenueLoader = isLoad
         isOtherRevenueLoader = isLoad
         isCollectionDataLoader = isLoad
         isDepositePaidLoader = isLoad
        tableview_listSales.reloadData()
    }
    
    @IBAction func showDatePicker(_ sender: Any) {
        // Get data from Userdegaults as Dste fomat
        isLive = false
        isFromCoustome = true
        dataloadedFirstTime = true
        self.isPassData = false
        let accountDate = UserDefaults.standard.object(forKey: "Account_Date") as?  Date ?? Date ()
        let dateRangePickerViewController = CalendarDatePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        
        let fcDate = UserDefaults.standard.object(forKey: "Financial_Date") as? Date
        dateRangePickerViewController.minimumDate = Calendar.current.date(byAdding: .year, value: -1, to: accountDate)
        
        if fcDate != nil {
           dateRangePickerViewController.minimumDate = fcDate
        }
        
        dateRangePickerViewController.maximumDate = accountDate
        dateRangePickerViewController.selectedStartDate = Calendar.current.date(byAdding: .day, value: -15, to: accountDate)
        dateRangePickerViewController.selectedEndDate = accountDate
        let sectionsCount = dateRangePickerViewController.numberOfSections(in: dateRangePickerViewController.collectionView!)
        let lastItemIndex = IndexPath.init(row: 0, section: sectionsCount - 1)
        dateRangePickerViewController.collectionView?.scrollToItem(at: lastItemIndex, at: UICollectionView.ScrollPosition.top, animated: false)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    func getSingleProperty(){
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        //let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginId":username]//,"PmsCustCode":pmsCode]
        AF.request("\(CommonDataManager.shared.baseUrl)\(SINGLEPROPERTY)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print(response)
                do {
                    let response = try JSONDecoder().decode(SinglePropertyModel.self, from: response.data ?? Data())
                    if response.Status == "Success" {
                        if let groupProperties = response.Response {
                            self.parseGroupProperties(list: [groupProperties])//comment : single group to ary of group
                        }
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    func getCommonAPIRequest(urlString: String) -> URLRequest {
        
        let Url = String(format: urlString)
        let serviceUrl = URL.init(string: Url)!
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        var parameterDictionary: [String: Any]  = ["LoginID":username,"PmsCustCode":pmsCode,"CurrencyCode":selectedCurrencyCode,"ExchangeRate":Int(Double(selectedCurrencyValue) ?? 0.0) ,"FromDate":selectedStartDate,"ToDate": selectedEndDate,"IsCustom": 0]
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        if isLive {
            
            let date = dAcDate.getDateFromString(isoDate: dAcDate)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let acdate = formatter.string(from: date)
            
            parameterDictionary.updateValue(acdate, forKey: "FromDate")
            parameterDictionary.updateValue(acdate, forKey: "ToDate")
            parameterDictionary.updateValue(1, forKey: "IsCustom")
            
        } else if isFromCoustome  {
            parameterDictionary.updateValue(1, forKey: "IsCustom")
        }
        
        if let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) {
            request.httpBody = httpBody
        }
        return request
    }
    func sortArray() {
        var temp: [String?] = [nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
        for each in self.aryMainList {
            if each == "FXONE02" {
                temp[0] = each
            } else if each == "FXONE03" {
                temp[1] = each
            } else if each == "FXONE04" {
                temp[2] = each
            } else if each == "FXONE05" {
                temp[3] = each
            } else if each == "FXONE07" {
                temp[4] = each
            } else if each == "FXONE06" {
                temp[5] = each
            }
        }
        self.aryMainList = temp.filter( { $0 != nil })
        
    }
    func callAllDashboardAPIs(){
       
        aryMainList = [nil,nil,nil,nil,nil,nil]
        self.updateUI()
        self.getRoomSalesData { result in
                if result == true {
                    DispatchQueue.main.async {
                        self.isRoomSalesLoader = false
                        self.dateView.isHidden = true
                        self.tableview_listSales.reloadData()
                    }
                }
            }
        getFandBdata()
        otherRevenueData()
        getCollectionData()
        getDepositData()
        getPaidOutData()
        
    }
    
    func updateUI()  {
        DispatchQueue.main.async {
                        loadingView.hideLoader()
                        self.sortArray()
                        let roles = UICoreEngine.sharedInstance.userRoles
                        var temp = [String?]()
                        temp = self.aryMainList
                        //undo for access right
                        let propAdmin = UICoreEngine.sharedInstance.userAdmin
                        self.aryMainList.removeAll()
                        if propAdmin?.UserType == "PropertyAdmin" {
        
                            self.aryMainList.append("FXONE02")
                            self.aryMainList.append("FXONE03")
                            self.aryMainList.append("FXONE04")
                            self.aryMainList.append("FXONE05")
                            self.aryMainList.append("FXONE07")
                            self.aryMainList.append("FXONE06")
                        }
                        else if propAdmin?.UserType == "User" {
                            for each in roles ?? [] {
                               // if temp.contains(each.FunctionCode ?? "" ) {
                                    if each.AccessRights == "View" {
                                        self.aryMainList.append(each.FunctionCode ?? "")
                                    }
                               // }
                            }
                            self.sortArray()
                        }
                        print("Data loaded ")
                    }
    }
    
    func getRoomSalesData(sucess: @escaping(Bool)-> ()) {

        let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_RoomSale)
        self.dispatchGroup.enter()
        URLSession.shared.request(request: request, expecting: DashBoardModel.self) { [weak self]  result in
            switch result {
            case .success(let dash):
                self?.parseDatAndLoadUI(dash: dash)
              //  self?.aryMainList.insert("FXONE02", at: 0)
                self?.dashRoomSaleData = dash
                self?.olddashRoomSaleData = dash
                self?.dispatchGroup.leave()
                if self?.isPassData == true {
                    todayRoomSalesObj = dash.Response?.RoomSale?.ToDayDetail?.first
                    monthRoomSalesObj = dash.Response?.RoomSale?.MonthDetail?.first
                    yearRoomSalesObj = dash.Response?.RoomSale?.YearDetail?.first
                    UserDefaults.standard.set(dash.Response?.CurrencyCode ?? "", forKey: "currencyCode")
                    UserDefaults.standard.synchronize()
                }
               sucess(true)
            case .failure(let error):
                print(error)
                self?.dispatchGroup.leave()
                loadingView.hideLoader()
            }
        }
        
    }

    func getFandBdata() {
        let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_FNBSale)
        URLSession.shared.request(request: request, expecting: DFANDBModel.self) { [weak self]  result in
            switch result {
            case .success(let dash):
                // today list
               // self?.aryMainList.insert("FXONE03", at: 1)
                self?.dashFAndBData = dash
                self?.olddashFAndBData = dash
                DispatchQueue.main.async {
                    self?.isFBRevenueLoader = false
                    self?.tableview_listSales.reloadData()
                }
                if self?.isPassData == true {
                    todayFBSalesObj = dash.Response?.ToDayDetail?.first
                    monthFBSalesObj = dash.Response?.MonthDetail?.first
                    yearFBSalesObj = dash.Response?.YearDetail?.first
                }
                print(dash)
            case .failure(let error):
                print(error)
                DispatchQueue.main.async {
                    self?.isFBRevenueLoader = true
                    self?.tableview_listSales.reloadData()
                }
            }
        }
    }
    
    func otherRevenueData() {
        let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_NonFNBSale)
        URLSession.shared.request(request: request, expecting: DFANDBModel.self) { [weak self]  result in
            switch result {
            case .success(let dash):
                self?.dashOthersData = dash
                self?.olddashOthersData = dash
              //  self?.aryMainList.insert("FXONE04", at: 2)
                print(dash)
                DispatchQueue.main.async {
                    self?.isOtherRevenueLoader = false
                    self?.tableview_listSales.reloadData()
                }
                if self?.isPassData == true {
                    todayOFBSalesObj = dash.Response?.ToDayDetail?.first
                    monthOFBSalesObj = dash.Response?.MonthDetail?.first
                    yearOFBSalesObj = dash.Response?.YearDetail?.first
                }
            case .failure(let error):
                print(error)
                DispatchQueue.main.async {
                    self?.isOtherRevenueLoader = true
                    self?.tableview_listSales.reloadData()
                }
            }
        }
    }
    
    func getCollectionData() {
        let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_Collection)
        URLSession.shared.request(request: request, expecting: DashCollectionDataModel.self) { [weak self]  result in
            switch result {
            case .success(let dash):
               // self?.aryMainList.insert("FXONE05", at: 3)
                self?.dashCollectionData = dash
                self?.olddashCollectionData = dash
                self?.dayCollections = dash.Response ?? CollectionsDataModel()
                print(dash)
                DispatchQueue.main.async {
                    self?.isCollectionDataLoader = false
                    self?.tableview_listSales.reloadData()
                }
                
            case .failure(let error):
                print(error)
                DispatchQueue.main.async {
                    self?.isCollectionDataLoader = true
                    self?.tableview_listSales.reloadData()
                }
            }
        }
    }
    
    func getDepositData() {
        let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_Deposit)
        URLSession.shared.request(request: request, expecting: DashCollectionDataModel.self) { [weak self]  result in
            switch result {
            case .success(let dash):
              //  self?.aryMainList.insert("FXONE07", at: 4)
                self?.dashDepositData = dash
                self?.olddashDepositData = dash
                DispatchQueue.main.async {
                    self?.isDepositePaidLoader = false
                    self?.tableview_listSales.reloadData()
                }
                
                print(dash)
            case .failure(let error):
                print(error)
                DispatchQueue.main.async {
                    self?.isDepositePaidLoader = true
                    self?.tableview_listSales.reloadData()
                }
            }
        }
    }
    
    func getPaidOutData() {
        let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_PaidOut)
        URLSession.shared.request(request: request, expecting: DashCollectionDataModel.self) { [weak self]  result in
            switch result {
            case .success(let dash):
               // self?.aryMainList.insert("FXONE06", at: 5)
                self?.dashPaidOutData = dash
                self?.olddashPaidOutData = dash
                DispatchQueue.main.async {
                    self?.isDepositePaidLoader = false
                    self?.tableview_listSales.reloadData()
               }
                print(dash)
            case .failure(let error):
                print(error)
                DispatchQueue.main.async {
                    self?.isDepositePaidLoader = true
                    self?.tableview_listSales.reloadData()
               }
            }
        }
    }
    
    func getAccountentDate() {
        let Url = String(format: "\(CommonDataManager.shared.baseUrl)\(AccountDateNightAudit)")
        let serviceUrl = URL.init(string: Url)!
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let parameterDictionary: [String: Any]  = ["LoginID":username,"PmsCustCode":pmsCode]
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        if let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) {
            request.httpBody = httpBody
        }
        
        URLSession.shared.request(request: request, expecting: DAccountCodeModel.self) { [weak self]  result in
            switch result {
            case .success(let dash):
	                self!.dAcDate = dash.Response?.AccountingDate  ?? ""
                let ac_date = CommonDataManager.shared.parseDate(dateString: self!.dAcDate)
                DispatchQueue.main.async {
                    self!.lbl_accountingDate.text = "(AD : " + CommonDataManager.shared.getDateStngDDMMMYYYY(fromDate: self!.dAcDate) + ")"
                }
                UserDefaults.standard.set(ac_date, forKey: "Account_Date")
                UserDefaults.standard.synchronize()
                print(dash)
                
                self?.callAllDashboardAPIs()
            case .failure(let error):
                print(error)
                self?.tableview_listSales.reloadData()
            }
        }
        
    }

    func parseDatAndLoadUI(dash: DashBoardModel) {
        let data_response = dash
        if data_response.Status == "Success" {
            // Currency Code
            if let currency_code = data_response.Response?.CurrencyCode {
                DispatchQueue.main.async {
                UserDefaults.standard.set(self.currencyCode, forKey: "currency_Code")
                UserDefaults.standard.synchronize()
                }
                self.currencyCode = currency_code
            }
            // Accounting date
            if let accounting_date = data_response.Response?.AccountingDate {
                self.accountingDate = accounting_date
                GlobalAccountDateData.shared.accountDate_Global = accountingDate
                DispatchQueue.main.async {
                    if self.segmentControl.selectedSegmentIndex != 0 {
                        self.segmentTitleConversion()
                    }
                    else {
                        print("current ac date showing correct")
                    }
                }
            }
            
            if let finance_date = data_response.Response?.FinancialDate {
                
                let fc_date : Date? = CommonDataManager.shared.parseDate(dateString:finance_date)
                DispatchQueue.main.async {
                UserDefaults.standard.set(fc_date, forKey: "Finance_Date")
                UserDefaults.standard.synchronize()
                }
            }
            else{
                
                let fc_date : Date? = CommonDataManager.shared.parseDate(dateString:"01/01/2022 5:30:00 AM")
                DispatchQueue.main.async {
                UserDefaults.standard.set(fc_date, forKey: "Finance_Date")
                UserDefaults.standard.synchronize()
                }
            }
            
            if let monthfrom_date = data_response.Response?.FromMonthDate {
                let mf_date : Date? = CommonDataManager.shared.parseDate(dateString:monthfrom_date)
                DispatchQueue.main.async {
                UserDefaults.standard.set(mf_date, forKey: "MonthFrom_Date")
                UserDefaults.standard.synchronize()
                }
            }
            else{
                
                let mf_date : Date? = CommonDataManager.shared.parseDate(dateString:"04/01/2022 5:30:00 AM")
                DispatchQueue.main.async {
                UserDefaults.standard.set(mf_date, forKey: "MonthFrom_Date")
                UserDefaults.standard.synchronize()
                }
            }
        }
        else{
            DispatchQueue.main.async {
            UICoreEngine.sharedInstance.ShowAlertOnWindows(message: data_response.StatusDescription ?? "", title: "")
            }
        }

    }
    func getSalesByDuration(){
        //no data label creation
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        label.center = self.tableview_listSales.center
        label.textAlignment = .center
        label.text = "No Data"
        label.tag = 123
        //NO DATA LABEL DELETE
        if let NoDataView = self.view.viewWithTag(label.tag) {
            NoDataView.removeFromSuperview()
        }
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        
        var dicParams: [String: Any]  = ["LoginID":username,"PmsCustCode":pmsCode,"CurrencyCode":selectedCurrencyCode,"ExchangeRate":Int(Double(selectedCurrencyValue) ?? 0.0) ,"FromDate":selectedStartDate,"ToDate": selectedEndDate,"IsCustom": 0]
            print("pms :", pmsCode)
        if isLive {
            
            dicParams.updateValue(dAcDate, forKey: "")
            dicParams.updateValue("", forKey: "")
            dicParams.updateValue("", forKey: "")
        }
        print("DashBoard URL:","\(CommonDataManager.shared.baseUrl)\(DASHBOARDDETAIL)")
        var url = "\(CommonDataManager.shared.baseUrl)\(DASHBOARDDETAIL)"
        AF.request("\(CommonDataManager.shared.baseUrl)\(DASHBOARDDETAIL)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print("DashBoard URL:","\(CommonDataManager.shared.baseUrl)\(DASHBOARDDETAIL)")
                //print("params:","\(dicParams, dic_headers)")
                print("DashBoardAPI Response",response)
                do {
                    self.isCustom = false
                    let data_response = try JSONDecoder().decode(DashBoardModel.self, from: response.data ?? Data())
                    if data_response.Status == "Success" {
                        // Currency Code
                        if let currency_code = data_response.Response?.CurrencyCode {
                            DispatchQueue.main.async {
                            UserDefaults.standard.set(self.currencyCode, forKey: "currency_Code")
                            UserDefaults.standard.synchronize()
                            }
                            self.currencyCode = currency_code
                        }
                        // Accounting date
                        if let accounting_date = data_response.Response?.AccountingDate {
                            self.accountingDate = accounting_date
                            
                        }
                        
                        if let finance_date = data_response.Response?.FinancialDate {
                            
                            let fc_date : Date? = CommonDataManager.shared.parseDate(dateString:finance_date)
                            DispatchQueue.main.async {
                            UserDefaults.standard.set(fc_date, forKey: "Finance_Date")
                            UserDefaults.standard.synchronize()
                            }
                        }
                        else{
                            
                            let fc_date : Date? = CommonDataManager.shared.parseDate(dateString:"01/01/2022 5:30:00 AM")
                            DispatchQueue.main.async {
                            UserDefaults.standard.set(fc_date, forKey: "Finance_Date")
                            UserDefaults.standard.synchronize()
                            }
                        }
                        
                        if let monthfrom_date = data_response.Response?.FromMonthDate {
                            let mf_date : Date? = CommonDataManager.shared.parseDate(dateString:monthfrom_date)
                            DispatchQueue.main.async {
                            UserDefaults.standard.set(mf_date, forKey: "MonthFrom_Date")
                            UserDefaults.standard.synchronize()
                            }
                        }
                        else{
                            
                            let mf_date : Date? = CommonDataManager.shared.parseDate(dateString:"04/01/2022 5:30:00 AM")
                            DispatchQueue.main.async {
                            UserDefaults.standard.set(mf_date, forKey: "MonthFrom_Date")
                            UserDefaults.standard.synchronize()
                            }
                        }
                        
                        
                        
                       // self.collectionview_roomOccupancy.reloadData()
                        
                        // today list
                        var roomSale          = RoomSaleCustomModel()
                        //var roomSaleDetail = RoomSaleDetailModel()
                        //var alos = aLosModel()
                        var foodAndBaverage = RoomSaleCustomModel()
                        var nonfoodAndBaverage = RoomSaleCustomModel()
                        var collections = CollectionsDataModelArry()
                        var depoist = DepoistDataModelArry()
                        var paidOut = PaidOutDataModelArry()
                        
                        
                        var todayArray = [Any]()
                        
                        if (data_response.Response?.RoomSale?.ToDayDetail?.count ?? 0) > 0 {
                            roomSale = data_response.Response?.RoomSale?.ToDayDetail?[0] ?? RoomSaleCustomModel()
                            roomSale.key = "roomSales"
                            todayArray.append(roomSale )
                        }
                        
                        if data_response.Response?.FoodandBeverage?.ToDayDetail?.count ?? 0 > 0 {
                            foodAndBaverage = data_response.Response?.FoodandBeverage?.ToDayDetail?[0] ?? RoomSaleCustomModel()
                            foodAndBaverage.key = "foodAndBaverage"
                            todayArray.append(foodAndBaverage )
                        }
                        
                        if data_response.Response?.NonFoodandBeverage?.TodayDetail?.count ?? 0 > 0 {
                            nonfoodAndBaverage = data_response.Response?.NonFoodandBeverage?.TodayDetail?[0] ?? RoomSaleCustomModel()
                            nonfoodAndBaverage.key = "nonfoodAndBaverage"
                            todayArray.append(nonfoodAndBaverage )
                        }
                        
                        if data_response.Response?.Collections?.Day?.count ?? 0 > 0 {
                            collections = data_response.Response?.Collections?.Day?[0] ?? CollectionsDataModelArry()
                            self.dayCollections = data_response.Response?.Collections ?? CollectionsDataModel()
                            collections.key = "collections"
                            todayArray.append(collections)
                        }
                        
                        if data_response.Response?.Depoist?.Day?.count ?? 0 > 0 {
                            depoist = data_response.Response?.Depoist?.Day?[0] ?? DepoistDataModelArry()
                            //self.daydepoist = data_response.Response?.Depoist?.Day?[0] ?? DepoistDataModelArry()
                            depoist.key = "depoist"
                            todayArray.append(depoist)
                        }
                        
                        if data_response.Response?.PaidOut?.Day?.count ?? 0 > 0 {
                            paidOut = data_response.Response?.PaidOut?.Day?[0] ?? PaidOutDataModelArry()
                            //self.dayPaidOut = data_response.Response?.PaidOut?.Day?[0] ?? PaidOutDataModelArry()
                            paidOut.key = "paidOut"
                            todayArray.append(paidOut)
                        }
                        
                        let dic1 = ["TodayList":todayArray,"hasDetails":true,"key": "today"] as [String : Any]
                        
                        
                        // Month list
                        var monthArray = [Any]()
                        var roomSale1 = RoomSaleCustomModel()
                        var foodAndBaverage1 = RoomSaleCustomModel()
                        var nonfoodAndBaverage1 = RoomSaleCustomModel()
                        var collections1 = CollectionsDataModelArry()
                        var depoist1 = DepoistDataModelArry()
                        var paidOut1 = PaidOutDataModelArry()

                        if data_response.Response?.RoomSale?.MonthDetail?.count ?? 0 > 0 {
                            roomSale1 = data_response.Response?.RoomSale?.MonthDetail?[0] ?? RoomSaleCustomModel()
                            roomSale1.key = "roomSales"
                            monthArray.append(roomSale1 )

                        }
                        if data_response.Response?.FoodandBeverage?.MonthDetail?.count ?? 0 > 0 {
                            
                            foodAndBaverage1 = data_response.Response?.FoodandBeverage?.MonthDetail?[0] ?? RoomSaleCustomModel()
                            foodAndBaverage1.key = "foodAndBaverage"
                            monthArray.append(foodAndBaverage1 )
                        }
                        
                        if data_response.Response?.NonFoodandBeverage?.MonthDetail?.count ?? 0 > 0 {
                            nonfoodAndBaverage1 = data_response.Response?.NonFoodandBeverage?.MonthDetail?[0] ?? RoomSaleCustomModel()
                            nonfoodAndBaverage1.key = "nonfoodAndBaverage"
                            monthArray.append(nonfoodAndBaverage1 )
                        }
                        if data_response.Response?.Collections?.Month?.count ?? 0 > 0 {
                            collections1 = data_response.Response?.Collections?.Month?[0] ?? CollectionsDataModelArry()
                        self.dayCollections = data_response.Response?.Collections ?? CollectionsDataModel()
                            collections1.key = "collections"
                            monthArray.append(collections1)
                        }
                        
                        if data_response.Response?.Depoist?.Month?.count ?? 0 > 0 {
                            depoist1 = data_response.Response?.Depoist?.Month?[0] ?? DepoistDataModelArry()
                            //self.monthdepoist = data_response.Response?.Depoist?.Month?[0] ?? DepoistDataModelArry()
                            depoist1.key = "depoist"
                            monthArray.append(depoist1)
                        }
                        
                        if data_response.Response?.PaidOut?.Month?.count ?? 0 > 0 {
                            paidOut1 = data_response.Response?.PaidOut?.Month?[0] ?? PaidOutDataModelArry()
                            //self.monthPaidOut = data_response.Response?.PaidOut?.Month?[0] ?? PaidOutDataModelArry()
                            paidOut1.key = "paidOut"
                            monthArray.append(paidOut1)
                        }
                        
                        
                        
                        
                        
                        
                        let dic2 = ["MTDList":monthArray,"hasDetails":false,"key": "monthly"] as [String : Any]
                        
                        // Year list
                        var yearArray = [Any]()
                        var roomSale2 = RoomSaleCustomModel()
                        var foodAndBaverage2 = RoomSaleCustomModel()
                        var nonfoodAndBaverage2 = RoomSaleCustomModel()
                        var collections2 = CollectionsDataModelArry()
                        var depoist2 = DepoistDataModelArry()
                        var paidOut2 = PaidOutDataModelArry()
                        if data_response.Response?.RoomSale?.YearDetail?.count ?? 0 > 0 {
                            roomSale2 = data_response.Response?.RoomSale?.YearDetail?[0] ?? RoomSaleCustomModel()
                            roomSale2.key = "roomSales"
                            yearArray.append(roomSale2 )
                        }
                        if data_response.Response?.FoodandBeverage?.YearDetail?.count ?? 0 > 0 {
                            
                            foodAndBaverage2 = data_response.Response?.FoodandBeverage?.YearDetail?[0] ?? RoomSaleCustomModel()
                            foodAndBaverage2.key = "foodAndBaverage"
                            yearArray.append(foodAndBaverage2 )
                        }
                        
                        if data_response.Response?.NonFoodandBeverage?.YearDetail?.count ?? 0 > 0 {
                            nonfoodAndBaverage2 = data_response.Response?.NonFoodandBeverage?.YearDetail?[0] ?? RoomSaleCustomModel()
                            nonfoodAndBaverage2.key = "nonfoodAndBaverage"
                            yearArray.append(nonfoodAndBaverage2 )
                        }
                        if data_response.Response?.Collections?.Year?.count ?? 0 > 0 {
                            collections2 = data_response.Response?.Collections?.Year?[0] ?? CollectionsDataModelArry()
                        self.dayCollections = data_response.Response?.Collections ?? CollectionsDataModel()
                            collections2.key = "collections"
                            yearArray.append(collections2)
                        }
                        
                        if data_response.Response?.Depoist?.Year?.count ?? 0 > 0 {
                            depoist2 = data_response.Response?.Depoist?.Year?[0] ?? DepoistDataModelArry()
                            //self.yeardepoist = data_response.Response?.Depoist?.Year?[0] ?? DepoistDataModelArry()
                            depoist2.key = "depoist"
                            yearArray.append(depoist2)
                        }
                        
                        if data_response.Response?.PaidOut?.Year?.count ?? 0 > 0 {
                            paidOut2 = data_response.Response?.PaidOut?.Year?[0] ?? PaidOutDataModelArry()
                            //self.yearPaidOut = data_response.Response?.PaidOut?.Year?[0] ?? PaidOutDataModelArry()
                            paidOut2.key = "paidOut"
                            yearArray.append(paidOut2)
                        }
                        
                        let dic3 = ["YTDList":yearArray,"hasDetails":false,"key": "yearly"] as [String : Any]
                        
                        self.mainList.append(dic1)
                        self.mainList.append(dic2)
                        self.mainList.append(dic3)
                        self.tableview_listSales.reloadData()
                        self.segmentControl.isUserInteractionEnabled = true
                        //self.lbl_accountingDate.text = self.accountingDate
                        // convert to Date obj from string-date
                          if self.accountingDate == "" || self.accountingDate.isEmpty {
                            //self.accountingDate = ""
                            self.lbl_accountingDate.text = "No Date"
                        }
                        else {
                            let ac_date : Date? = CommonDataManager.shared.parseDate(dateString: self.accountingDate)
//                            UserDefaults.standard.set(ac_date, forKey: "Account_Date")
//                            UserDefaults.standard.synchronize()
//                            let ac_lastdate = ac_date?.addingTimeInterval(-1 * 24 * 60 * 60)
//                            print("put yesterday of Account Date",ac_lastdate ?? "")
                            // convert Date Obj to needed dateformat( 31 May 2019)
//                            if let acDate = ac_date {
//                                self.lbl_accountingDate.text = "(AD : " + CommonDataManager.shared.getDateStringDDMMMYYYY(fromDate: acDate) + ")"
//                            }
                        }
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: data_response.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.mainList = [[String:Any]]()
                self.tableview_listSales.reloadData()
                self.segmentControl.isUserInteractionEnabled = false
                self.view.addSubview(label)
                //need to show No Data lable here
                //UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    
    func getRoomOccupancyData(){
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        label.center = self.collectionview_roomOccupancy.center
        label.textAlignment = .center
        label.text = " "
        label.tag = 122
        if let NoDataView = self.view.viewWithTag(122) {
            NoDataView.removeFromSuperview()
        }
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginID":username,"PmsCustCode":pmsCode]
        let url = CommonDataManager.shared.baseUrl + OCCUPANYAPI
        print("pms :", pmsCode)
        print("Room Occupancy URL: ", url)
        print("Room Occupancy Request:", username, token, headers, pmsCode, dicParams)
        AF.request("\(CommonDataManager.shared.baseUrl)\(OCCUPANYAPI)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case.success:
                print("Room Occupancy URL: ", url)
                print("Room Occupancy Request:", username, token, headers, pmsCode, dicParams)
                print("Room Occupancyresponse-------------->",response)
                do {
                    let response = try JSONDecoder().decode(RoomOccupancyModel.self, from: response.data ?? Data())
                    if response.Status == "Success" {
                        print("==========*=========",response.Response?.Rooms ?? "")
                        print("PercentageOccupied",response.Response?.PercentageOccupied ?? "")
                        print("PercentageOccupiedLy",response.Response?.PercentageOccupiedLy ?? "")
                        // roomOcupy Total rooms from api in pie
                        if let percentageOccupied = response.Response?.PercentageOccupied {
                            self.percentageOccupied = percentageOccupied
                            print("self.PercentageOccupied",self.percentageOccupied)
                        }
                        if let percentageOccupiedLy = response.Response?.PercentageOccupiedLy {
                            self.percentageOccupiedLy = percentageOccupiedLy
                            print("self.percentageOccupiedLy",self.percentageOccupiedLy)

                        }
                        if let acDateOcpy = response.Response?.AccountingDate {
                            self.acDateOccupancy = acDateOcpy
                        }
                        var dic = [String:Any]()
                        do {
                            dic = try response.Response?.Rooms?.allProperties() ?? [String:Any]()
                        }catch let error {
                            print(error.localizedDescription)
                        }
                        for (key,value) in dic {
                            
                            let val = value as? Int
                            var room = RoomACS()
                            room.name = key
                            room.count = String(val ?? 0)
                            room.statusColor = self.getRandomColor()
                            
                            if key == "TotalRooms" {
                                self.totalRooms = val ?? 0
                            } else {
                                self.ary_listofRAStatus.append(room)
                            }
                        }
                        for i in 0..<self.ary_listofRAStatus.count {
                            let each = self.ary_listofRAStatus[i]
                            if each.name == "OOOandOOS"{
                                self.ary_listofRAStatus.move(at: i, to: 2)
                            }
                            else if each.name == "MGT"{
                                self.ary_listofRAStatus.move(at: i, to: 4)
                            }
                            else if each.name == "Occupied"{
                                self.ary_listofRAStatus.move(at: i, to: 0)
                            }
                            else if each.name == "TodaysExpectedArrival"{
                                self.ary_listofRAStatus.move(at: i, to: 1)
                            }
                            else if each.name == "ToSell"{
                                self.ary_listofRAStatus.move(at: i, to: 3)
                            }
                        }
                    
                        var statusbyday = RoomStatusByDay()
                        statusbyday.date = "Today"
                        statusbyday.list = self.ary_listofRAStatus
                        self.listAry.append(statusbyday)
                        self.isRoomOccupancyLoader = false
                        self.collectionview_roomOccupancy.reloadData()
                        
                    }
                    else {
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                //self.listAry = [RoomStatusByDay]()
                //self.collectionview_roomOccupancy.reloadData()
                //self.segmentControl.isUserInteractionEnabled = false
                //need to show No Data lable here
                //UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                self.view.addSubview(label)
            }
        }
        
    }
    
    func getCurrencyList(){
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginID":username,"PmsCustCode":pmsCode]
            print("pms :", pmsCode)
        print("CurencyListURL:","\(CommonDataManager.shared.baseUrl)\(GET_CURRENCYLIST)")
        print("dicParams: \(dicParams)")
        print("Bearertoken \(token)")
        AF.request("\(CommonDataManager.shared.baseUrl)\(GET_CURRENCYLIST)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON {(response) in
            switch response.result {
            case .success(_):
                print("CurrencyListResponse \(response)")
                do {
                    let data_Response = try JSONDecoder().decode(CurrencyListModel.self, from: response.data ?? Data())
                    self.aryCurrency = data_Response.Response ?? []
                    let results = data_Response.Response?.filter {$0.Classification == "LocalCurrency" }
                    if results?.count ?? 0 > 0 {
                        self.selectedCurrencyCode = results?.first?.CurrencyCode ?? ""
                        self.selectedCurrencyValue = "\(results?.first?.ExchangeRate ?? 0.0)"
                        self.btnCurncyCode.setTitle(self.selectedCurrencyCode, for: .normal)
                        self.tfdCurrencyValue.text = self.selectedCurrencyValue
                        self.tfdCurrencyValue.isHidden = true
                        
                        self.lblSelectedCurrencyName.text = ""
                        self.lblCurrencyValue.text = ""
                    }
                    //self.getSalesByDuration()
                   // self.getRoomSalesData()
                    DispatchQueue.main.async {
                        self.curncyCodeTableview.reloadData()
                    }
                    print(data_Response.Response)
                }
                catch { print(error) }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func getRandomColor() -> UIColor {
        //Generate between 0 to 1
        let red:CGFloat = CGFloat(drand48())
        let green:CGFloat = CGFloat(drand48())
        let blue:CGFloat = CGFloat(drand48())
        
        return UIColor(red:red, green: green, blue: blue, alpha: 1.0)
    }
    @objc func presentRate(){
        
        let stObj = UIStoryboard.init(name: "Main", bundle: nil)
        guard let roomRates = stObj.instantiateViewController(withIdentifier: "RoomRatePopupVC") as? RoomRatePopupVC else { return }
        presentPanModal((roomRates as? UIViewController & PanModalPresentable)!)
    }
    
    @IBAction func notifications(_ sender: Any) {
//        let stObj = UIStoryboard.init(name: "Main", bundle: nil)
//        let notification = stObj.instantiateViewController(withIdentifier: "Notifications") as! NotificationsViewController
//        self.navigationController?.pushViewController(notification, animated: true)
        
    }
    @IBAction func menuAction(_ sender: Any) {
        
        sideBarView = SideMenuView(frame:  CGRect(x:50, y:0, width: self.view.frame.width - 50, height: self.view.frame.height), viewController: self,navigateTo: "RoomOccupancy")
        sideBarView.delegate = self
    }
    
    @objc override func handleTap(sender: UITapGestureRecognizer) {
        self.removeAlphaView()
        sideBarView.hideSlideMenu()
    }
    
}
extension ViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            if tableView == curncyCodeTableview {
                return aryCurrency.count
            }
            return 1
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

            if tableView ==  tableview_listSales {
                
                let key = aryMainList[indexPath.section]
                
                if key == "FXONE02" {
                    return 240
                } else if key == "FXONE05" {
                    return 280
                }
                return 180
                
            }
            else if tableView == curncyCodeTableview {
                return 30
            }
            else {
                return 0
            }
       
    }
    func numberOfSections(in tableView: UITableView) -> Int {

            if tableView ==  tableview_listSales {
                
                if aryMainList.contains("FXONE07") || aryMainList.contains("FXONE06") {
                    return aryMainList.count - 1
                }
                print(aryMainList)
                return aryMainList.count
            }
            else {
                return 1
            }
       
    }
    @objc func switchStateChanged(_ mjSwitch: UISwitch) {
        let key = mjSwitch.accessibilityLabel
        if mjSwitch.isOn {
            loadingView.showLoader(text: "")
            if key == "FXONE02"  {
                isLive = true
                let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_RoomSale)
                URLSession.shared.request(request: request, expecting: DashBoardModel.self) { [weak self]  result in
                    loadingView.hideLoader()
                    self?.isLive = false
                    switch result {
                    case .success(let dash):
                        self?.parseDatAndLoadUI(dash: dash)
                        if self?.selectedSegmentIndex == 1 {
                            
                            let count = self?.dashRoomSaleData.Response?.RoomSale?.ToDayDetail?.count ?? 0
                            self?.dashRoomSaleData.Response?.RoomSale?.ToDayDetail = dash.Response?.RoomSale?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashRoomSaleData.Response?.RoomSale?.ToDayDetail?[0].islive = true
                                self?.dashRoomSaleData.Response?.RoomSale?.ToDayDetail?[0].Today = dash.Response?.RoomSale?.CustomDetail?.first?.Yesterday_TD
                            } else {
                                var model = RoomSaleCustomModel()
                                model.islive = true
                                self?.dashFAndBData.Response?.ToDayDetail?.append(model)
                            }
                            
                            
                        } else if self?.selectedSegmentIndex == 2 {
                            
                            let count = self?.dashRoomSaleData.Response?.RoomSale?.MonthDetail?.count ?? 0
                            self?.dashRoomSaleData.Response?.RoomSale?.MonthDetail = dash.Response?.RoomSale?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashRoomSaleData.Response?.RoomSale?.MonthDetail?[0].islive = true
                                self?.dashRoomSaleData.Response?.RoomSale?.MonthDetail?[0].Month = dash.Response?.RoomSale?.CustomDetail?.first?.Yesterday_TD
                            } else {
                                var model = RoomSaleCustomModel()
                                model.islive = true
                                self?.dashFAndBData.Response?.ToDayDetail?.append(model)
                            }
                            
                            
                        } else if self?.selectedSegmentIndex == 3 {
                            
                            
                            let count = self?.dashRoomSaleData.Response?.RoomSale?.YearDetail?.count ?? 0
                            self?.dashRoomSaleData.Response?.RoomSale?.YearDetail = dash.Response?.RoomSale?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashRoomSaleData.Response?.RoomSale?.YearDetail?[0].islive = true
                                self?.dashRoomSaleData.Response?.RoomSale?.YearDetail?[0].Year = dash.Response?.RoomSale?.CustomDetail?.first?.Yesterday_TD
                            } else {
                                var model = RoomSaleCustomModel()
                                model.islive = true
                                self?.dashFAndBData.Response?.ToDayDetail?.append(model)
                            }
                            
                        }
                        if self?.isFromCoustome ?? false {
                            
                            let count = self?.dashRoomSaleData.Response?.RoomSale?.CustomDetail?.count ?? 0
                            self?.dashRoomSaleData.Response?.RoomSale?.CustomDetail = dash.Response?.RoomSale?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashRoomSaleData.Response?.RoomSale?.CustomDetail?[0].islive = true
                                self?.dashRoomSaleData.Response?.RoomSale?.CustomDetail?[0].Year = dash.Response?.RoomSale?.CustomDetail?.first?.Yesterday_TD
                            } else {
                                var model = RoomSaleCustomModel()
                                model.islive = true
                                self?.dashFAndBData.Response?.ToDayDetail?.append(model)
                            }
                        }
                        DispatchQueue.main.async {
                            
                            self?.tableview_listSales.reloadData()
                        }
                        
                    case .failure(let error):
                        DispatchQueue.main.async {
                        }
                        print(error)
                    }
                }
                
            } else if key == "FXONE03" {
                isLive = true
                let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_FNBSale)
                self.dispatchGroup.enter()
                URLSession.shared.request(request: request, expecting: DFANDBModel.self) { [weak self]  result in
                    self?.isLive = false
                    loadingView.hideLoader()
                    switch result {
                    case .success(let dash):
                        if self?.selectedSegmentIndex == 1 {
                            
                            let count = self?.dashFAndBData.Response?.ToDayDetail?.count ?? 0
                            self?.dashFAndBData.Response?.ToDayDetail = dash.Response?.CustomDetail
                            
                            if self?.dashFAndBData.Response?.ToDayDetail?.count ?? 0 > 0 {
                                self?.dashFAndBData.Response?.ToDayDetail?[0].islive = true
                                self?.dashFAndBData.Response?.ToDayDetail?[0].Today = dash.Response?.CustomDetail?.first?.Yesterday_TD
                            } else {
                                var model = RoomSaleCustomModel()
                                model.islive = true
                                self?.dashFAndBData.Response?.ToDayDetail?.append(model)
                            }
                            
                            
                        } else if self?.selectedSegmentIndex == 2 {
                            
                            let count = self?.dashFAndBData.Response?.MonthDetail?.count ?? 0
                            self?.dashFAndBData.Response?.MonthDetail = dash.Response?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashFAndBData.Response?.MonthDetail?[0].islive = true
                                self?.dashFAndBData.Response?.MonthDetail?[0].Month = dash.Response?.CustomDetail?.first?.Yesterday_TD
                            } else {
                                var model = RoomSaleCustomModel()
                                model.islive = true
                                self?.dashFAndBData.Response?.ToDayDetail?.append(model)
                            }
                        
                            
                        } else if self?.selectedSegmentIndex == 3 {
                            
                            
                            let count = self?.dashFAndBData.Response?.YearDetail?.count ?? 0
                            self?.dashFAndBData.Response?.YearDetail = dash.Response?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashFAndBData.Response?.YearDetail?[0].islive = true
                                self?.dashFAndBData.Response?.YearDetail?[0].Year = dash.Response?.CustomDetail?.first?.Yesterday_TD
                            } else {
                                var model = RoomSaleCustomModel()
                                model.islive = true
                                self?.dashFAndBData.Response?.ToDayDetail?.append(model)
                            }
                            
                        }
                        if self?.isFromCoustome ?? false {
                            
                            let count = self?.dashFAndBData.Response?.CustomDetail?.count ?? 0
                            self?.dashFAndBData.Response?.CustomDetail = dash.Response?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashFAndBData.Response?.CustomDetail?[0].islive = true
                                self?.dashFAndBData.Response?.CustomDetail?[0].Year = dash.Response?.CustomDetail?.first?.Yesterday_TD
                            } else {
                                var model = RoomSaleCustomModel()
                                model.islive = true
                                self?.dashFAndBData.Response?.ToDayDetail?.append(model)
                            }
                            
                        }
                        DispatchQueue.main.async {
                            
                            self?.tableview_listSales.reloadData()
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
                
                
            } else if key == "FXONE04" {
                isLive = true
                let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_NonFNBSale)
                self.dispatchGroup.enter()
                URLSession.shared.request(request: request, expecting: DFANDBModel.self) { [weak self]  result in
                    self?.isLive = false
                    loadingView.hideLoader()
                    switch result {
                    case .success(let dash):
                        if self?.selectedSegmentIndex == 1 {
                            
                            let count = self?.dashOthersData.Response?.ToDayDetail?.count ?? 0
                            self?.dashOthersData.Response?.ToDayDetail = dash.Response?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashOthersData.Response?.ToDayDetail?[0].islive = true
                                self?.dashOthersData.Response?.ToDayDetail?[0].Today = dash.Response?.CustomDetail?.first?.Yesterday_TD
                            }
                            
                            
                        } else if self?.selectedSegmentIndex == 2 {
                            
                            let count = self?.dashOthersData.Response?.MonthDetail?.count ?? 0
                            self?.dashOthersData.Response?.MonthDetail = dash.Response?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashOthersData.Response?.MonthDetail?[0].islive = false
                                self?.dashOthersData.Response?.MonthDetail?[0].Month = dash.Response?.CustomDetail?.first?.Yesterday_TD
                            }
                        
                            
                        } else if self?.selectedSegmentIndex == 3 {
                            
                            
                            let count = self?.dashOthersData.Response?.YearDetail?.count ?? 0
                            self?.dashOthersData.Response?.YearDetail = dash.Response?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashOthersData.Response?.YearDetail?[0].islive = true
                                self?.dashOthersData.Response?.YearDetail?[0].Year = dash.Response?.CustomDetail?.first?.Yesterday_TD
                            }
                            
                        }
                        if self?.isFromCoustome ?? false {
                            
                            let count = self?.dashOthersData.Response?.CustomDetail?.count ?? 0
                            self?.dashOthersData.Response?.CustomDetail = dash.Response?.CustomDetail
                            
                            if count  > 0 {
                                self?.dashOthersData.Response?.CustomDetail?[0].islive = true
                                self?.dashOthersData.Response?.CustomDetail?[0].Year = dash.Response?.CustomDetail?.first?.Yesterday_TD
                            }
                            
                        }
                        DispatchQueue.main.async {
                            
                            self?.tableview_listSales.reloadData()
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
                
            }
            else if key == "FXONE05" {
                isLive = true
                self.isFromCoustome = true
                let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_Collection)
                self.dispatchGroup.enter()
                URLSession.shared.request(request: request, expecting: DashCollectionDataModel.self) { [weak self]  result in
                    self?.isLive = false
                    loadingView.hideLoader()
                    switch result {
                    case .success(let dash):
                        
                        self?.dashCollectionData = dash
                        var collection = dash.Response ?? CollectionsDataModel()
                        collection.isLive = true
                        self?.dayCollections = collection
                        
                        DispatchQueue.main.async {
                            
                            self?.tableview_listSales.reloadData()
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
                
            } else if key == "FXONE06" {
                isLive = true
                isDepositLive = true
                let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_Deposit)
                self.dispatchGroup.enter()
                URLSession.shared.request(request: request, expecting: DashCollectionDataModel.self) { [weak self]  result in
                    self?.isLive = false
                    loadingView.hideLoader()
                    switch result {
                    case .success(let dash):
                        var dash1 = dash
                        var deposit = dash.Response ?? CollectionsDataModel()
                        deposit.isLive = true
                        dash1.Response = deposit
                        self?.dashDepositData = dash1
                        self?.daydepoist = self?.dashDepositData.Response?.CustomDetail?.first ?? CollectionsDataModelArry()
                        DispatchQueue.main.async {
                            
                            self?.tableview_listSales.reloadData()
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            } else if key == "FXONE07" {
                isLive = true
                self.isPaidLive = true
                let request = self.getCommonAPIRequest(urlString: DashboardAPIConstants.FxOneDashBoard_PaidOut)
                self.dispatchGroup.enter()
                URLSession.shared.request(request: request, expecting: DashCollectionDataModel.self) { [weak self]  result in
                    self?.isLive = false
                    loadingView.hideLoader()
                    switch result {
                    case .success(let dash):
                        var paidout = dash
                        var deposit = dash.Response ?? CollectionsDataModel()
                        deposit.isLive = true
                        paidout.Response = deposit
                        self?.dashPaidOutData = paidout
                        self?.dayPaidOut = self?.dashPaidOutData.Response?.CustomDetail?.first ?? CollectionsDataModelArry()
                        //self?.dayCollections = deposit
                        DispatchQueue.main.async {
                            self?.tableview_listSales.reloadData()
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        } else {
            self.isFromCoustome = false
            isDepositLive = false
            isPaidLive = false
            self.dashRoomSaleData = self.olddashRoomSaleData
            self.dashFAndBData = self.olddashFAndBData
            self.dashOthersData = self.olddashOthersData
            self.dashCollectionData = self.olddashCollectionData
            self.dayCollections = self.olddashCollectionData.Response ?? CollectionsDataModel()
            self.dashDepositData = self.olddashDepositData
            self.dashPaidOutData = self.olddashPaidOutData
            DispatchQueue.main.async {
                self.tableview_listSales.reloadData()
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            if tableView == curncyCodeTableview {
                let cell = curncyCodeTableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
                cell.textLabel?.text = aryCurrency[indexPath.row].CurrencyName ?? ""
                cell.textLabel?.font = UIFont.init(name: "Arial", size: 10)
                return cell
            } else {
               
                let key = aryMainList[indexPath.section]
                if key == "FXONE02" || key == "FXONE03" || key == "FXONE04" {
                    let cell = tableview_listSales.dequeueReusableCell(withIdentifier: "BubbleCell") as! BubbleTableViewCell
                    let roomsaleview = RoomSaleFooterView.init(frame: CGRect(x: cell.lbl_actualAmount.frame.origin.x, y: cell.lbl_actualAmount.frame.origin.y + cell.lbl_actualAmount.frame.size.height + 10, width: cell.frame.width - ( cell.lbl_actualAmount.frame.origin.x * 2), height: 70))
                    cell.backgroundColor = .clear
                    cell.layer.cornerRadius = 10
                    cell.layer.masksToBounds = true
                    var hasDetails = false
                    if  key == "FXONE02"  {
                        hasDetails = true
                    }
                    if hasDetails {
                        DispatchQueue.main.async {

                            roomsaleview.btn_expand.isHidden = true
                            //lbl_revParCurrencyCode
                            //lbl_aDRCurrencyCode
                            roomsaleview.lbl_aDRCurrencyCode.text = self.currencyCode
                            roomsaleview.lbl_revParCurrencyCode.text = self.currencyCode
                            
                            if self.selectedSegmentIndex == 1 {
                                let ary1 = self.dashRoomSaleData.Response?.RoomSale?.ToDayDetail ?? []
                                roomsaleview.updateRoomSaleData(details: (ary1.first ))
                                
                            }
                            else if self.selectedSegmentIndex == 2 {
                                let ary2 = self.dashRoomSaleData.Response?.RoomSale?.MonthDetail ?? []
                                roomsaleview.updateRoomSaleData(details: (ary2.first ))
                                
                            }
                            else if self.selectedSegmentIndex == 3 {
                                let ary3 = self.dashRoomSaleData.Response?.RoomSale?.YearDetail ?? []
                                roomsaleview.updateRoomSaleData(details: (ary3.first ))
                                
                            } else if self.selectedSegmentIndex == 0  || self.isFromCoustome == true {
                                let ary3 = self.dashRoomSaleData.Response?.RoomSale?.CustomDetail ?? []
                                roomsaleview.updateRoomSaleData(details: (ary3.first ))
                                
                            }
                            let viewObj = UIView()
                            viewObj.frame = CGRect(x: 0, y:5, width: roomsaleview.frame.width, height: 1)
                            viewObj.backgroundColor = UIColor.lightGray
                            roomsaleview.addSubview(viewObj)
                            cell.contentView.addSubview(roomsaleview)
                        }
                    }
                    else {
                        let subviews = cell.contentView.subviews
                        for view in subviews {
                            if view is RoomSaleFooterView {
                                view.removeFromSuperview()
                            }
                        }
                    }
                    cell.liveSwitch.isOn = false
                    cell.lblLiveStatus.text = "Live"
                    cell.lblLiveStatus.textColor = .black
                    if isLive {
                        cell.liveSwitch.isOn = true
                        cell.liveSwitch.isUserInteractionEnabled = false
                    } else {
                        cell.liveSwitch.isUserInteractionEnabled = true
                        cell.liveSwitch.isOn = false
                    }
                    cell.selectedIndex = selectedSegmentIndex
                    cell.sectionIndex = indexPath.section
                    cell.liveSwitch.tag = indexPath.section
                    cell.liveSwitch.accessibilityLabel = aryMainList[indexPath.section]
                    cell.liveSwitch.addTarget(self, action: #selector(switchStateChanged(_:)), for: UIControl.Event.valueChanged)
                    cell.lbl_currency.text = self.selectedCurrencyCode
                    cell.lbl_CyCurrency.text = self.selectedCurrencyCode
                    cell.isFromCustom = self.isFromCoustome
                    let accountDate = UserDefaults.standard.object(forKey: "Account_Date") as? Date ?? Date()
                    
                    if selectedSegmentIndex == 1 {
                        cell.lbl_dateStatus.isHidden = true
                        //cell.lbl_dateStatus.text = "Today:\(CommonDataManager.shared.getDateStringDDMMMYYYY(fromDate: accountDate))"
                    }
                    else if selectedSegmentIndex == 2 {
                        cell.lbl_dateStatus.isHidden = false
                        let mfDate = UserDefaults.standard.object(forKey: "MonthFrom_Date") as?  Date ?? Date ()
                        cell.lbl_dateStatus.text = "MTD:\(CommonDataManager.shared.getDateStringDDMMMYYYY(fromDate: mfDate)) To \(CommonDataManager.shared.getDateStringDDMMMYYYY(fromDate:accountDate )) "
                    }
                    else if selectedSegmentIndex == 3 {
                        cell.lbl_dateStatus.isHidden = false
                        let fcDate = UserDefaults.standard.object(forKey: "Finance_Date") as?  Date ?? Date ()
                        cell.lbl_dateStatus.text = "YTD:\(CommonDataManager.shared.getDateStringDDMMMYYYY(fromDate: fcDate)) To \(CommonDataManager.shared.getDateStringDDMMMYYYY(fromDate: accountDate))"
                    } else if selectedSegmentIndex == 0 {
                        cell.lbl_dateStatus.text = "Live"
                    }
                    
                    if isRoomSalesLoader == true {
                       
                        cell.activityView.isHidden = false
                        cell.isUserInteractionEnabled = false
                        cell.activityIndicator.startAnimating()
                        roomsaleview.loaderView.isHidden = false
                        let subviews = cell.contentView.subviews
                        for view in subviews {
                            if view is RoomSaleFooterView {
                                view.removeFromSuperview()
                            }
                        }
                        cell.lbl_title.text = "0.0"
                        cell.lbl_budget.text = "0.0"
                        cell.lbl_todayAmount.text = "0.0"
                        cell.lbl_lastyearAmount.text = "0.0"
                        cell.lbl_actualAmount.text = "0.0"
                    } else {
                        
                        cell.activityIndicator.stopAnimating()
                       cell.activityView.isHidden = true
                       cell.isUserInteractionEnabled = true
                        roomsaleview.loaderView.isHidden = true
                        if key == "FXONE02" {
                            var live = false
                            if selectedSegmentIndex == 1 {
                                let ary1 = dashRoomSaleData.Response?.RoomSale?.ToDayDetail ?? []
                                cell.todaySales = ary1.first
                                live = dashRoomSaleData.Response?.RoomSale?.ToDayDetail?.first?.islive ?? false
                            }
                            else if selectedSegmentIndex == 2 {
                                let ary2 = dashRoomSaleData.Response?.RoomSale?.MonthDetail ?? []
                                cell.monthSales = ary2.first
                                live = dashRoomSaleData.Response?.RoomSale?.MonthDetail?.first?.islive ?? false
                            }
                            else if selectedSegmentIndex == 3 {
                                let ary3 = dashRoomSaleData.Response?.RoomSale?.YearDetail ?? []
                                cell.yearSales = ary3.first
                                live = dashRoomSaleData.Response?.RoomSale?.YearDetail?.first?.islive ?? false
                            } else if selectedSegmentIndex == 0  || isFromCoustome {
                                let ary4 = dashRoomSaleData.Response?.RoomSale?.CustomDetail ?? []
                                cell.yearSales = ary4.first
                                live = dashRoomSaleData.Response?.RoomSale?.CustomDetail?.first?.islive ?? false
                            }
                            if isLive {
                                cell.liveSwitch.isOn = true
                            } else {
                                if live {
                                    cell.liveSwitch.isOn = true
                                } else {
                                    cell.liveSwitch.isOn = false
                                }
                            }
                        }
                        //self.isRoomSalesLoader = true
                    }
                     
                    if isFBRevenueLoader == true {
                        cell.activityView.isHidden = false
                        cell.isUserInteractionEnabled = false
                        cell.activityIndicator.startAnimating()
                        let subviews = cell.contentView.subviews
                        for view in subviews {
                            if view is RoomSaleFooterView {
                                view.removeFromSuperview()
                            }
                        }
                        cell.lbl_title.text = "0.0"
                        cell.lbl_budget.text = "0.0"
                        cell.lbl_todayAmount.text = "0.0"
                        cell.lbl_lastyearAmount.text = "0.0"
                        cell.lbl_actualAmount.text = "0.0"
                    } else {
                        cell.activityIndicator.stopAnimating()
                        cell.activityView.isHidden = true
                        cell.isUserInteractionEnabled = true
                        if key == "FXONE03" {
                            var live = false
                            if selectedSegmentIndex == 1 {
                                let ary1 = dashFAndBData.Response?.ToDayDetail ?? []
                                cell.todaySales = ary1.first
                                live = dashFAndBData.Response?.ToDayDetail?.first?.islive ?? false
                            }
                            else if selectedSegmentIndex == 2 {
                                let ary2 = dashFAndBData.Response?.MonthDetail ?? []
                                cell.monthSales = ary2.first
                                live = dashFAndBData.Response?.MonthDetail?.first?.islive ?? false
                            }
                            else if selectedSegmentIndex == 3 {
                                let ary3 = dashFAndBData.Response?.YearDetail ?? []
                                cell.yearSales = ary3.first
                                live = dashFAndBData.Response?.YearDetail?.first?.islive ?? false
                            } else if selectedSegmentIndex == 0 || isFromCoustome {
                                let ary4 = dashFAndBData.Response?.CustomDetail ?? []
                                cell.yearSales = ary4.first
                                live = dashFAndBData.Response?.CustomDetail?.first?.islive ?? false
                            }
                            if isLive {
                                cell.liveSwitch.isOn = true
                                
                            } else {
                                if live {
                                    cell.liveSwitch.isOn = true
                                    
                                } else {
                                    cell.liveSwitch.isOn = false
                                }
                            }
                        }
                       // self.isFBRevenueLoader = true
                    }
                    
                    if isOtherRevenueLoader == true {
                        cell.activityView.isHidden = false
                        cell.isUserInteractionEnabled = false
                        cell.activityIndicator.startAnimating()
                        let subviews = cell.contentView.subviews
                        for view in subviews {
                            if view is RoomSaleFooterView {
                                view.removeFromSuperview()
                            }
                        }
                        cell.lbl_title.text = "0.0"
                        cell.lbl_budget.text = "0.0"
                        cell.lbl_todayAmount.text = "0.0"
                        cell.lbl_lastyearAmount.text = "0.0"
                        cell.lbl_actualAmount.text = "0.0"
                    } else {
                        cell.activityIndicator.stopAnimating()
                        cell.activityView.isHidden = true
                        cell.isUserInteractionEnabled = true
                        if key == "FXONE04" {
                            var live = false
                            if selectedSegmentIndex == 1 {
                                let ary1 = dashOthersData.Response?.ToDayDetail ?? []
                                cell.todaySales = ary1.first
                                live = dashOthersData.Response?.ToDayDetail?.first?.islive ?? false
                            }
                            else if selectedSegmentIndex == 2 {
                                let ary2 = dashOthersData.Response?.MonthDetail ?? []
                                cell.monthSales = ary2.first
                                live = dashOthersData.Response?.MonthDetail?.first?.islive ?? false
                            }
                            else if selectedSegmentIndex == 3 {
                                let ary3 = dashOthersData.Response?.YearDetail ?? []
                                cell.yearSales = ary3.first
                                live = dashOthersData.Response?.YearDetail?.first?.islive ?? false
                            } else if selectedSegmentIndex == 0 || isFromCoustome {
                                let ary4 = dashOthersData.Response?.CustomDetail ?? []
                                cell.yearSales = ary4.first
                                live = dashOthersData.Response?.CustomDetail?.first?.islive ?? false
                            }
                            
                            if isLive {
                                cell.liveSwitch.isOn = true
                            } else {
                                if live {
                                    cell.liveSwitch.isOn = true
                                } else {
                                    cell.liveSwitch.isOn = false
                                }
                            }
                        }
                        //self.isOtherRevenueLoader = true
                    }
                    
                    if cell.liveSwitch.isOn {
                        cell.lblLiveStatus.text = "● Live"
                        cell.lblLiveStatus.textColor = .green
                        let date = dAcDate.getDateFromString(isoDate: dAcDate)
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd MMM yyyy"
                        let acdate = formatter.string(from: date)
                        cell.lbl_dateStatus.text = acdate
                    }
                    
                    
                    return cell
                    
                } else if key == "FXONE05" {
                    
                    let cell = tableview_listSales.dequeueReusableCell(withIdentifier: "CollectionTCCell") as! CollectionTableViewCell
                    if self.isCollectionDataLoader == true {
                        cell.collectionview.loaderView.isHidden = false
                        cell.collectionview.activityView.startAnimating()
                        
                    } else {
                        
                        cell.collectionview.loaderView.isHidden = true
                        cell.collectionview.activityView.stopAnimating()
                        cell.backgroundColor = .red
                        cell.layer.cornerRadius = 10
                        cell.collectionview.layer.cornerRadius = 10
                        //cell.collectionview.frame = CGRect(x: 10 , y: 0, width: cell.frame.width - 20, height: cell.frame.height)
                        cell.collectionview.currency_Lbl.text = currencyCode
                        cell.collectionview.lyCurncy.text = currencyCode
                        cell.collectionview.view_CollLbl.layer.cornerRadius = 10
                        cell.collectionview.bgView.layer.cornerRadius = 10
                        cell.collectionview.bgView.layer.masksToBounds = true
                        //cell.collectionview.viewContainer.backgroundColor = .clear
                        cell.collectionview.bgView.backgroundColor = .white
                        cell.viewContent.addSubview(cell.collectionview)
                        //cell.baseView.layer.cornerRadius = 10
                        cell.collectionview.liveSwitch.accessibilityLabel = aryMainList[indexPath.section]
                        cell.collectionview.liveSwitch.addTarget(self, action: #selector(switchStateChanged(_:)), for: UIControl.Event.valueChanged)
                        cell.layer.masksToBounds = true
                        cell.collectionview.isFromCoustome = self.isFromCoustome
                        if isLive {
                            cell.collectionview.liveSwitch.isUserInteractionEnabled = false
                            cell.collectionview.liveSwitch.isOn = true
                            
                        } else {
                            cell.collectionview.liveSwitch.isOn = false
                            cell.collectionview.liveSwitch.isUserInteractionEnabled = true
                        }
                        cell.collectionview.lblLiveStatus.text = "Live"
                        cell.collectionview.lblLiveStatus.textColor = .lightGray
                        let lv = self.dayCollections.isLive ?? false
                        if lv || isLive {
                            cell.collectionview.liveSwitch.isOn = true
                            if cell.collectionview.liveSwitch.isOn {
                                cell.collectionview.lblLiveStatus.text = "● Live"
                                cell.collectionview.lblLiveStatus.textColor = .green
                            } else {
                                cell.collectionview.lblLiveStatus.text = "Live"
                                cell.collectionview.lblLiveStatus.textColor = .lightGray
                            }
                        } else {
                            
                            cell.collectionview.liveSwitch.isOn = false
                        }
                        if self.selectedSegmentIndex == 1 {
                            print(self.dayCollections)
                            cell.collectionview.selectedSegmentIndex = self.selectedSegmentIndex
                            cell.collectionview.lyCDCM_Lbl.text = "LYCD"
                            cell.collectionview.lyAmount.text = "0"
                        }
                        else if self.selectedSegmentIndex == 2 {
                            cell.collectionview.selectedSegmentIndex = self.selectedSegmentIndex
                            cell.collectionview.lyCDCM_Lbl.text = "LYCM"
                            cell.collectionview.lyAmount.text = "0"
                        }
                        else if self.selectedSegmentIndex == 3 {
                            cell.collectionview.selectedSegmentIndex = self.selectedSegmentIndex
                            cell.collectionview.lyCDCM_Lbl.text = "LY"
                            cell.collectionview.lyAmount.text = "0"
                        } else if self.selectedSegmentIndex == 0 || self.selectedSegmentIndex == 4 {
                            cell.collectionview.selectedSegmentIndex = self.selectedSegmentIndex
                            cell.collectionview.lyCDCM_Lbl.text = "LY"
                            cell.collectionview.lyAmount.text = "0"
                        }
                        
                        cell.collectionview.updateCollectionData(colectionArray: self.dayCollections)
                        cell.backgroundColor = .clear
//                        self.isCollectionDataLoader = true
                    }
                    return cell
                } else if key == "FXONE07" || key == "FXONE06" {
                    print("This is Deposit")
                    let cell = tableview_listSales.dequeueReusableCell(withIdentifier: "DepositTCCell") as! DepositTableViewCell
                    if self.isDepositePaidLoader == true {
                        cell.loaderView.isHidden = false
                        cell.activityLoader.startAnimating()
                    
                    }else {
                        cell.loaderView.isHidden = true
                        cell.activityLoader.stopAnimating()
                    cell.backgroundColor = .clear
                    cell.layer.cornerRadius = 10
                    cell.baseView.layer.cornerRadius = 10
                    cell.paidOutBaseView.layer.cornerRadius = 10
                    cell.layer.masksToBounds = true
                    cell.currency_Lbl.text = currencyCode
                    cell.headingTitleLbl.text = "Deposit"
                    cell.dpsitCurncyLbl.text = currencyCode
                    cell.gPOutCurncyLbl.text = currencyCode
                    cell.paidCurrency.text = currencyCode
                    
                    cell.liveSwitch_Dep.accessibilityLabel = "FXONE06"
                    cell.liveSwitch_Dep.addTarget(self, action: #selector(switchStateChanged(_:)), for: UIControl.Event.valueChanged)
                    
                    cell.liveSwitch_GuestP.accessibilityLabel = "FXONE07"
                    cell.liveSwitch_GuestP.addTarget(self, action: #selector(switchStateChanged(_:)), for: UIControl.Event.valueChanged)
                    
                    cell.layer.masksToBounds = true
                    
                    cell.lbl_LivestatusDepst.text = "Live"
                    cell.lbl_LivestatusDepst.textColor = .lightGray
                    let lv = self.dashDepositData.Response?.isLive ?? false
                    if lv || isLive {
                        cell.liveSwitch_Dep.isOn = true
                        if cell.liveSwitch_Dep.isOn {
                            cell.lbl_LivestatusDepst.text = "● Live"
                            cell.lbl_LivestatusDepst.textColor = .green
                        } else {
                            cell.lbl_LivestatusDepst.text = "Live"
                            cell.lbl_LivestatusDepst.textColor = .lightGray
                        }
                    } else {
                        cell.liveSwitch_Dep.isOn = false
                    }
                    
                    cell.lbl_LiveStatusGuestp.text = "Live"
                    cell.lbl_LiveStatusGuestp.textColor = .lightGray
                    let lv1 = self.dashPaidOutData.Response?.isLive ?? false
                    if lv1 || isLive {
                        cell.liveSwitch_GuestP.isOn = true
                        if cell.liveSwitch_GuestP.isOn {
                            cell.lbl_LiveStatusGuestp.text = "● Live"
                            cell.lbl_LiveStatusGuestp.textColor = .green
                        } else {
                            cell.lbl_LiveStatusGuestp.text = "Live"
                            cell.lbl_LiveStatusGuestp.textColor = .lightGray
                        }
                    } else {
                        cell.liveSwitch_GuestP.isOn = false
                    }
                    
                    if isFromCoustome || isDepositLive || isPaidLive {
                        cell.amount_Lbl.text = "\(self.daydepoist.Amount ?? 0.0)"
                        cell.paidOutAmount_Lbl.text = "\(self.dayPaidOut.Amount ?? 0.0)"
                        cell.lyCdCmLbl.text = "LYCD"
                        cell.gPOutlyCdCmLbl.text = "LYCD"
                        cell.dpsitLyAmouunt.text = "0"
                        cell.gPOutLyAmouunt.text = "0"
                    }
                    
                    else if self.selectedSegmentIndex == 1 {
                        
                        self.daydepoist = dashDepositData.Response?.Day?.first ?? CollectionsDataModelArry()
                        self.dayPaidOut = dashPaidOutData.Response?.Day?.first ?? CollectionsDataModelArry()
                        cell.amount_Lbl.text = "\(self.daydepoist.Amount ?? 0.0)"
                        cell.paidOutAmount_Lbl.text = "\(self.dayPaidOut.Amount ?? 0.0)"
                        cell.lyCdCmLbl.text = "LYCD"
                        cell.gPOutlyCdCmLbl.text = "LYCD"
                        cell.dpsitLyAmouunt.text = "0"
                        cell.gPOutLyAmouunt.text = "0"
                        
                    }
                    else if self.selectedSegmentIndex == 2 {
                        self.monthdepoist = dashDepositData.Response?.Month?.first ?? CollectionsDataModelArry()
                        cell.amount_Lbl.text = "\(self.monthdepoist.Amount ?? 0.0)"
                        
                        self.monthPaidOut = dashPaidOutData.Response?.Month?.first ?? CollectionsDataModelArry()
                        
                        cell.paidOutAmount_Lbl.text = "\(self.monthPaidOut.Amount ?? 0.0)"
                        cell.lyCdCmLbl.text = "LYCM"
                        cell.gPOutlyCdCmLbl.text = "LYCM"
                        cell.dpsitLyAmouunt.text = "0"
                        cell.gPOutLyAmouunt.text = "0"
                    }
                    else if self.selectedSegmentIndex == 3 {
                        self.yeardepoist = dashDepositData.Response?.Year?.first ?? CollectionsDataModelArry()
                        self.yearPaidOut = dashPaidOutData.Response?.Year?.first ?? CollectionsDataModelArry()
                        
                        cell.amount_Lbl.text = "\(self.yeardepoist.Amount ?? 0.0)"
                        cell.paidOutAmount_Lbl.text = "\(self.yearPaidOut.Amount ?? 0.0)"
                        cell.lyCdCmLbl.text = "LY"
                        cell.gPOutlyCdCmLbl.text = "LY"
                        cell.dpsitLyAmouunt.text = "0"
                        cell.gPOutLyAmouunt.text = "0"
                    } else if self.selectedSegmentIndex == 0  || isFromCoustome {
                        self.yeardepoist = dashDepositData.Response?.CustomDetail?.first ?? CollectionsDataModelArry()
                        
                        self.yearPaidOut = dashPaidOutData.Response?.CustomDetail?.first ?? CollectionsDataModelArry()
                        
                        cell.amount_Lbl.text = "\(self.yeardepoist.Amount ?? 0.0)"
                        cell.paidOutAmount_Lbl.text = "\(self.yearPaidOut.Amount ?? 0.0)"
                        cell.lyCdCmLbl.text = "LY"
                        cell.gPOutlyCdCmLbl.text = "LY"
                        cell.dpsitLyAmouunt.text = "0"
                        cell.gPOutLyAmouunt.text = "0"
                    }
                    else {
                        
                        cell.amount_Lbl.text = "\(self.daydepoist.Amount ?? 0.0)"
                        cell.paidOutAmount_Lbl.text = "\(self.dayPaidOut.Amount ?? 0.0)"
                        cell.lyCdCmLbl.text = "LYCD"
                        cell.gPOutlyCdCmLbl.text = "LYCD"
                        cell.dpsitLyAmouunt.text = "0"
                        cell.gPOutLyAmouunt.text = "0"
                    }
                    cell.amount_Lbl.text = CommonDataManager.shared.getNumberWithCommas(cash: Double(cell.amount_Lbl.text ?? "") ?? 0.0)
                    cell.paidOutAmount_Lbl.text = CommonDataManager.shared.getNumberWithCommas(cash: Double(cell.paidOutAmount_Lbl.text ?? "") ?? 0.0)
                }
                    return cell
                } else {
                    return UITableViewCell()
                }
            }
      //  }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == curncyCodeTableview {
            //btnCurncyCode.reloadInputViews()
            //selectedBtn.setTitle(dataSource[indexPath.row], for: .normal)
            self.tfdCurrencyValue.isHidden = false
            selectedBtn.setTitle(aryCurrency[indexPath.row].CurrencyName ?? "", for: .normal)
            self.tfdCurrencyValue.text = "\(aryCurrency[indexPath.row].ExchangeRate ?? 0.0)"
            self.selectedCurrencyValueIndex = indexPath.row
            selectedCurrencyValue = "\(aryCurrency[indexPath.row].ExchangeRate ?? 0.0)"
            selectedCurrencyValue = "\(aryCurrency[indexPath.row].CurrencyCode ?? "")"
            //aryCurrency[self.selectedCurrencyValueIndex].ExchangeRate
            removeTransparentView()
            //self.getSalesByDuration()
            self.callAllDashboardAPIs()
            print("didSelectRowAt", indexPath.row)
        }
        else {
            tableView.deselectRow(at: indexPath, animated: true)
            let key = aryMainList[indexPath.section]
            
            if key == "FXONE02" || key == "FXONE03" || key == "FXONE04" {
                
                let story = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "RoomDetails") as! RoomOcpcyViewController
                vc.selectedIndex = selectedSegmentIndex
                vc.isLiveEnabled = isLive
                vc.dAcDate = dAcDate
                vc.accounttingDate = self.accountingDate
                print("Selected SegMnet Is:",selectedSegmentIndex )
                if self.isFromCoustome {
                    vc.startDateDetailed = self.startDate
                    vc.endDateDetailed = self.endDate
                    if key == "FXONE02" {
                        vc.customeDetails =  dashRoomSaleData.Response?.RoomSale?.CustomDetail?.first
                        vc.str_TypeOFSale = vc.todaySales?.Selection ?? ""
                    } else if key == "FXONE03" {
                        vc.customeDetails = dashFAndBData.Response?.CustomDetail?.first
                        vc.str_screenTtile = vc.todaySales?.Selection ?? ""
                    } else if key == "FXONE04" {
                        vc.customeDetails = dashOthersData.Response?.CustomDetail?.first
                        vc.str_screenTtile = vc.todaySales?.Selection ?? ""
                        
                    }
                    vc.str_TypeOFSale = key ?? ""
                    vc.str_screenTtile = key ?? ""
                    vc.isCustomDetailed = true
                    self.isDetailedTap = false
                    
                }
                else {
                    
                        if key == "FXONE02" {
                            vc.todaySales = dashRoomSaleData.Response?.RoomSale?.ToDayDetail?.first
                            vc.monthSales = dashRoomSaleData.Response?.RoomSale?.MonthDetail?.first
                            vc.yearSales = dashRoomSaleData.Response?.RoomSale?.YearDetail?.first
                            vc.customeDetails = dashRoomSaleData.Response?.RoomSale?.CustomDetail?.first
                            vc.str_TypeOFSale = vc.todaySales?.Selection ?? ""
                            todaySalesObj = todayRoomSalesObj
                            monthSalesObj = monthRoomSalesObj
                            yearSalesObj = yearRoomSalesObj
                        } else if key == "FXONE03" {
                            vc.todaySales = dashFAndBData.Response?.ToDayDetail?.first
                            vc.monthSales = dashFAndBData.Response?.MonthDetail?.first
                            vc.yearSales = dashFAndBData.Response?.YearDetail?.first
                            vc.customeDetails = dashFAndBData.Response?.CustomDetail?.first
                            vc.str_TypeOFSale = vc.todaySales?.Selection ?? ""
                            todaySalesObj = todayFBSalesObj
                            monthSalesObj = monthFBSalesObj
                            yearSalesObj = yearFBSalesObj
                        } else if key == "FXONE04" {
                            vc.todaySales = dashOthersData.Response?.ToDayDetail?.first
                            vc.monthSales = dashOthersData.Response?.MonthDetail?.first
                            vc.yearSales = dashOthersData.Response?.YearDetail?.first
                            vc.customeDetails = dashOthersData.Response?.CustomDetail?.first
                            
                            vc.str_TypeOFSale = vc.todaySales?.Selection ?? ""
                            todaySalesObj = todayOFBSalesObj
                            monthSalesObj = monthOFBSalesObj
                            yearSalesObj = yearOFBSalesObj
                        }
                    vc.str_TypeOFSale = key ?? ""
                    vc.str_screenTtile = key ?? ""
                    if selectedSegmentIndex == 1  || selectedSegmentIndex == 0 {
                        vc.slelectedsegmentIndexValue = 1
                    } else if selectedSegmentIndex == 2 {
                        vc.slelectedsegmentIndexValue = 2
                    } else if selectedSegmentIndex == 3 {
                        vc.slelectedsegmentIndexValue = 3
                    }
                }
                let cell = tableview_listSales.cellForRow(at: indexPath) as! BubbleTableViewCell
                cell.activityIndicator.startAnimating()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    // get PMSCode
    func getPMSFromSelectedProperty() -> Int? {
        for i in 0..<self.totalProperties.count {
            let count = self.totalProperties[i].property?.count ?? 0
            
            if totalProperties[i].selected == true {
                for j in 0..<count {
                    if totalProperties[i].property?[j].selected == true{
                        return totalProperties[i].property?[j].titlePms
                    }
                }
            }
            else {
                for j in 0..<count {
                    if totalProperties[i].property?[j].selected == true{
                        return totalProperties[i].property?[j].titlePms
                    }
                }
            }
        }
        return nil
    }
    
}
extension ViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isRoomOccupancyLoader == true {
            return 1
        }else {
            return listAry.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview_roomOccupancy.dequeueReusableCell(withReuseIdentifier: "RoomOccupancyCollectionViewCell", for: indexPath) as! RoomOccupancyCollectionViewCell
        if isRoomOccupancyLoader == true {
            cell.activityIndicator.startAnimating()
            cell.isUserInteractionEnabled = false
            cell.activityView.isHidden = false
        }else {
            cell.activityIndicator.stopAnimating()
            cell.isUserInteractionEnabled = true
            cell.activityView.isHidden = true
            cell.roomData = listAry[indexPath.row]
            cell.count_Lbl.text = "\(self.totalRooms)" // for totalrooms from API
            
            cell.backgroundColor = .clear
            cell.layer.cornerRadius = 10
            cell.layer.masksToBounds = true
            cell.lbl_percentage.text = String(self.percentageOccupied) + "%"
            cell.lbl_percentage2.text = String(self.percentageOccupiedLy) + "%"
            cell.lbl_percentage3.text = String(self.percentageOccupiedLy - self.percentageOccupied) + "%"
            if self.percentageOccupiedLy - self.percentageOccupied >= 0 {
                cell.percentage3StatusImgView.image = UIImage(named: "Decrease 2")
            }else {
                cell.percentage3StatusImgView.image = UIImage(named: "Increase 2")
            }
        
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "MM-dd-yyyy"

            if let date = inputFormatter.date(from: dAcDate) {
                let outputFormatter = DateFormatter()
                outputFormatter.dateFormat = "dd MMM yyyy"
                
                let ocupyAcntDate = outputFormatter.string(from: date)
                print(ocupyAcntDate)
                cell.lbl_date.text = ocupyAcntDate
            } else {
                print("Date is missing")
            }
             //CommonDataManager.shared.getDateStringDDMMMYYYY(fromDate: dat)

            cell.btn_rates.addTarget(self, action: #selector(presentRate), for: .touchUpInside)
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionview_roomOccupancy.frame.width, height: collectionview_roomOccupancy.frame.height)
    }
    
}

extension ViewController :SideMenuDelegate{
    func sideBarDelegateRow(menuObj: String) {
        self.removeAlphaView()
        sideBarView.hideSlideMenu()
        if menuObj == "Subscribed"{
            
            let stObj = UIStoryboard.init(name: "Main", bundle: nil)
            let itemsDash = stObj.instantiateViewController(withIdentifier: "ItemsDashBoard") as! ItemsDashBoardViewController
            self.navigationController?.pushViewController(itemsDash, animated: true)
        }
        else if menuObj == "About"{
            
        }
        else if menuObj == "Logout"{
            
            UICoreEngine.sharedInstance.showAlertOnLogout(message: "Do you want to logout", title: "Alert", isNoBUtton: true, viewController: self)
        }
        
    }
    
}

extension ViewController:CalendarDatePickerViewControllerDelegate{
    func didTapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        self.startDate = startDate! as NSDate
        self.endDate = endDate! as NSDate
        applyCoustomeDateWithRange(startDate: startDate, endDate: endDate)
        //print("Selected Date Range : \(startDate)-\(endDate)")
        let str = "\(startDate.toString())-\(endDate.toString())"
        UserDefaults.standard.set(String(str), forKey: "str")
        custom_DateSelector.setTitle(str, for: .normal)
        custom_DateSelector.titleLabel?.font = UIFont.FXRobotoRegular(size: 9) //systemFont(ofSize: 10)
        custom_DateSelector.titleLabel?.numberOfLines = 2
        custom_DateSelector.backgroundColor = UIColor.hexStringToUIColor(hex: "FC7A45")
        custom_DateSelector.titleLabel?.textAlignment = .center
        custom_DateSelector.setTitleColor(.white, for: .normal)
        self.dismiss(animated: true, completion: nil)
        self.segmentControl.tintColor = UIColor.systemGray
        segmentControl.selectedSegmentTintColor = UIColor.hexStringToUIColor(hex: "FC7A45")
    }
    
    func applyCoustomeDateWithRange(startDate: Date!, endDate: Date!) {
        self.mainList.removeAll()
        self.selectedSegmentIndex = 333
        if #available(iOS 13.0, *) {
           // segmentControl.selectedSegmentTintColor = .gray
        } else {
            // Fallback on earlier versions
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let startacDate = formatter.string(from: startDate)
        let endacDate = formatter.string(from: endDate)
        selectedStartDate = startacDate
        selectedEndDate = endacDate
        
        self.segmentControl.selectedSegmentIndex = -1
        self.segmentControl.selectedSegmentTintColor = .none
        self.isReloadData(isLoad: true)
        self.tableview_listSales.reloadData()
            self.callAllDashboardAPIs()
       
    }
}

extension ViewController:PropertyDelegate {
    func selectedProperties(list: [PropertyModel]) {
        self.totalProperties.removeAll()
        self.mainList.removeAll()
        self.totalProperties = list
        didPullToRefresh()
        
        //totalProperties[indexPath.section].property?[dataIndex].selected ?? false
        for i in 0..<self.totalProperties.count {
            if self.totalProperties[i].selected == true {
                isGroupPropertySelected = true
            }
            let count = self.totalProperties[i].property?.count ?? 0
            for j in 0..<count {
            if totalProperties[i].property?[j].selected == true{
                    ispropertySelected = true
            }else {
                    ispropertySelected = false
                }
            }
        }
        //self.tableview_listSales.reloadData()
        var aryCount = list.filter { (each) -> Bool in
            each.selected == true
        }
        if aryCount.count > 1 {
            self.baseHeightConst.constant = 80
            self.collectionViewHeightConstraint.constant = 0
            self.tableview_listSales.allowsSelection = false
        }
        else {
            self.baseHeightConst.constant = 180
            self.tableview_listSales.allowsSelection = true
            self.collectionViewHeightConstraint.constant = 180
        }
        
    }
    
}
extension ViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedCurrencyValue = textField.text ?? ""
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        selectedCurrencyValue = textField.text ?? ""
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfdCurrencyValue.resignFirstResponder()
        return true
    }
}

extension Array where Element: Equatable {
    mutating func move(_ item: Element, to newIndex: Index) {
        if let index = index(of: item) {
            move(at: index, to: newIndex)
        }
    }

    mutating func bringToFront(item: Element) {
        move(item, to: 0)
    }

    mutating func sendToBack(item: Element) {
        move(item, to: endIndex-1)
    }
}

extension Array {
    mutating func move(at index: Index, to newIndex: Index) {
        insert(remove(at: index), at: newIndex)
    }
}
//extension ViewController: UITableViewDelegate, UITableViewDataSource {
////    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
////        return dataSource.count
////    }
//    func tableView(_ tableView: UITableView, forRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = curncyCodeTableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
//        cell.textLabel?.text = dataSource[indexPath.row]
//        return cell
//    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//    }
////    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
////        selectedBtn.setTitle(dataSource[indexPath.row], for: .normal)
////        removeTransparentView()
////    }
//}

extension ViewController: SelectedCurrencyDelegate {
    func selectedCurrency(curency: CurrencyListResponseModel, index: Int, amount: Double) {
        
        selectedBtn.setTitle(aryCurrency[index].CurrencyName ?? "", for: .normal)
        if amount == 0 {
            self.tfdCurrencyValue.text = "\(aryCurrency[index].ExchangeRate ?? 0.0)"
            selectedCurrencyValue = "\(aryCurrency[index].ExchangeRate ?? 0.0)"
        } else {
            self.tfdCurrencyValue.text = "\(amount)"
            selectedCurrencyValue = "\(amount)"
            aryCurrency[index].ExchangeRate = amount
        }
        
        self.selectedCurrencyValueIndex = index
        selectedCurrencyValue = "\(aryCurrency[index].CurrencyCode ?? "")"
        
        selectedCurrencyCode = "\(aryCurrency[index].CurrencyCode ?? "")"
        
        self.lblSelectedCurrencyName.text = aryCurrency[index].CurrencyName ?? ""
        self.lblCurrencyValue.text = "\(aryCurrency[index].ExchangeRate ?? 0.0)"
        
        removeTransparentView()
        //self.getSalesByDuration()
        self.callAllDashboardAPIs()
        
    }

}

extension URLSession {
    
    enum CustomError: Error {
        case invalidUrl
        case invalidData
    }
    func request<T: Codable>(
        request: URLRequest,
        expecting:T.Type,
        completion: @escaping(Swift.Result<T,Error> ) -> Void
    ){
        let task = dataTask(with: request) { data,_, error in
            guard let data = data else {
                if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.failure(CustomError.invalidData))
                }
                return
            }
            do {
                let result = try JSONDecoder().decode(expecting, from: data)
                
                // make sure this JSON is in the format we expect
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    // try to read out a string array
                    print(json)
                }
                completion(.success(result))
            } catch {
                completion(.failure(error))
            }
        }
        
        task.resume()

    }
}
extension UISegmentedControl {
    func setFontSize(_ fontSize: CGFloat, forSegment segment: Int) {
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize)]
        self.setTitleTextAttributes(attributes, for: .normal)
        self.setTitleTextAttributes(attributes, for: .selected)
    }
}

class GlobalAccountDateData {
    static let shared = GlobalAccountDateData()

    var accountDate_Global : String?
    var accountDate_Global_Live : String?
    private init() {
        // Private initializer to prevent multiple instances
    }
    
}
