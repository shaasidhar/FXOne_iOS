//
//  CustomBartCchartView.swift
//  CustomBarChartView
//
//  Created by Vinayak on 22/08/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class CustomBartCchartView: UIView {
    @IBOutlet weak var view_container: UIView!
    

    var view:UIView!
    //initWithFrame to init view from code
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        var dic = [String:String]()
        dic.updateValue("0", forKey: "XLabels")
       
        var dic1 = [String:String]()
        dic1.updateValue("20k", forKey: "XLabels")

        var dic2 = [String:String]()
        dic2.updateValue("40k", forKey: "XLabels")

        var dic3 = [String:String]()
        dic3.updateValue("60k", forKey: "XLabels")

        var dic4 = [String:String]()
        dic4.updateValue("80k", forKey: "XLabels")

        var dic5 = [String:String]()
        dic5.updateValue("1lk", forKey: "XLabels")
        
        var ary = [[String:String]]()
        ary.append(dic)
        ary.append(dic1)
        ary.append(dic2)
        ary.append(dic3)
        ary.append(dic4)
        ary.append(dic5)
        
        
        var valuedic = [String:String]()
        valuedic.updateValue("15000", forKey: "CYValue")
        valuedic.updateValue("20000", forKey: "LYValue")

        var valuedic1 = [String:String]()
        valuedic1.updateValue("25000", forKey: "CYValue")
        valuedic1.updateValue("50000", forKey: "LYValue")

        var valuedic2 = [String:String]()
        valuedic2.updateValue("40000", forKey: "CYValue")
        valuedic2.updateValue("800000", forKey: "LYValue")

        var valuedic3 = [String:String]()
        valuedic3.updateValue("30000", forKey: "CYValue")
        valuedic3.updateValue("90000", forKey: "LYValue")

        var valuedic4 = [String:String]()
        valuedic4.updateValue("10000", forKey: "CYValue")
        valuedic4.updateValue("30000", forKey: "LYValue")

        var valuedic5 = [String:String]()
        valuedic5.updateValue("60000", forKey: "CYValue")
        valuedic5.updateValue("260000", forKey: "LYValue")

        var valueAry = [[String:String]]()
        valueAry.append(valuedic)
        valueAry.append(valuedic1)
        valueAry.append(valuedic2)
        valueAry.append(valuedic3)
        valueAry.append(valuedic4)
        valueAry.append(valuedic5)
        
        //self.createBarChart(values: ary, chartValues:valueAry)
    }
    
    func createBarChart(values:[[String:String]],chartValues:[[String:String]]){
        // create a X-Values and Y-Values
       
        // create scrollview
        let scrollview = UIScrollView.init(frame: CGRect(x: 25, y: 0, width: self.frame.width - 30, height: self.frame.height))
        scrollview.backgroundColor = UIColor.clear
        scrollview.isUserInteractionEnabled = true
        scrollview.showsHorizontalScrollIndicator = false
        scrollview.contentSize = CGSize(width: CGFloat(chartValues.count * 55), height: self.frame.height)
        self.view.addSubview(scrollview)
        self.backgroundColor = UIColor.clear
        var y =  scrollview.frame.height - 60
        for i in 0..<values.count {
            
            let XLabel = UILabel.init(frame: CGRect(x: 0, y:y , width: 30, height: 20))
            XLabel.text = values[i]["XLabels"]
            XLabel.font = UIFont.systemFont(ofSize: 12)
            self.addSubview(XLabel)
            
            let bgLine = UIView.init(frame: CGRect(x: 0, y: CGFloat(y) + (XLabel.frame.height / 2) , width: scrollview.contentSize.width, height: 1))
            bgLine.backgroundColor = .lightGray
            bgLine.alpha = 0.2
            scrollview.addSubview(bgLine)
            y = CGFloat(Int(XLabel.frame.origin.y) - Int(XLabel.frame.height) - 10)
            
        
        }
        
        // create a scroll view based on values
        var valuesBaseViewXValue : CGFloat = 10.0
        var lblcyValueYValue : CGFloat = scrollview.frame.height - 50
        for i in 0..<chartValues.count {
            
            let valuesBaseView = UIView.init(frame: CGRect(x: valuesBaseViewXValue, y: 0, width: 35, height: scrollview.frame.height))
            valuesBaseView.backgroundColor = UIColor.clear
            valuesBaseViewXValue += valuesBaseView.frame.width + 20
            
        
            let lblcy = UILabel.init(frame: CGRect(x: 0, y:valuesBaseView.frame.height - 40 , width: 20, height: 20))
            lblcy.text = "CY"
            lblcy.font = UIFont.systemFont(ofSize: 10)
            
            let lblly = UILabel.init(frame: CGRect(x: valuesBaseView.frame.width - 15 , y:valuesBaseView.frame.height - 40 , width: 20, height: 20))
            lblly.text = "LY"
            lblly.font = UIFont.systemFont(ofSize: 10)
            
            valuesBaseView.addSubview(lblcy)
            valuesBaseView.addSubview(lblly)
            
            
            
            let lblcyValue = UILabel.init(frame: CGRect(x: 2.5, y: 50, width: 15, height: lblcyValueYValue - 50))
            lblcyValue.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
           
            
            let lblLyValue = UILabel.init(frame: CGRect(x: lblcyValue.frame.origin.x + lblcyValue.frame.width , y: 100, width: 15, height: lblcyValueYValue - 100))
            lblLyValue.backgroundColor = UIColor.purple
            
            valuesBaseView.addSubview(lblcyValue)
            valuesBaseView.addSubview(lblLyValue)
            
            
            scrollview.addSubview(valuesBaseView)
            
            
            
        }
    }
    
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "CustomBartCchartView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
        
    }
}
