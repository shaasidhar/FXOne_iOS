//
//  MISRFooterView.swift
//  FXone
//
//  Created by Vinayak on 12/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import MarqueeLabel

class MISRFooterView: UIView {

    @IBOutlet weak var lbl_actualAmount: UILabel!
    @IBOutlet weak var lbl_budgetAmount: UILabel!
    @IBOutlet weak var lbl_title: MarqueeLabel!
    @IBOutlet weak var lbl_LyAmount: UILabel!
    var view:UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        // MarqueeLabel
        if lbl_title.text?.count ?? 0 >= 10 {
            lbl_title.type = .continuous
            lbl_title.speed = .duration(10.0)
            lbl_title.animationCurve = .easeInOut
            lbl_title.fadeLength = 10.0
            lbl_title.leadingBuffer = 5.0
            lbl_title.trailingBuffer = 20.0
        }
        
    }
    
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "MISRFooterView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
        
    }

}
