//
//  RoomSaleFooterView.swift
//  FXone
//
//  Created by Vinayak on 7/20/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class RoomSaleFooterView: UIView {
    
    @IBOutlet weak var view1_lbl_title: UILabel!
    @IBOutlet weak var view1_lbl_per: UILabel!
    @IBOutlet weak var view1_lbl_cy: UILabel!
    @IBOutlet weak var view1_lbl_cy_amount: UILabel!
    @IBOutlet weak var view1_lbl_ly: UILabel!
    @IBOutlet weak var view1_ly_amount: UILabel!
    @IBOutlet weak var view2_lbl_title: UILabel!
    @IBOutlet weak var view2_lbl_per: UILabel!
    @IBOutlet weak var view2_lbl_cy: UILabel!
    @IBOutlet weak var view2_lbl_cy_amount: UILabel!
    @IBOutlet weak var view2_lbl_ly: UILabel!
    @IBOutlet weak var view2_lbl_ly_amount: UILabel!
    @IBOutlet weak var view3_lbl_title: UILabel!
    @IBOutlet weak var view3_lbl_per: UILabel!
    @IBOutlet weak var view3_lbl_cy: UILabel!
    @IBOutlet weak var view3_lbl_cy_amount: UILabel!
    @IBOutlet weak var view3_lbl_ly: UILabel!
    @IBOutlet weak var view3_lbl_ly_amount: UILabel!
    @IBOutlet weak var btn_expand: UIButton!
    @IBOutlet weak var lbl_aDRCurrencyCode: UILabel!
    @IBOutlet weak var lbl_revParCurrencyCode: UILabel!
    
    @IBOutlet weak var loaderView: UIView!
    
    
    var view:UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        self.btn_expand.setImage(UIImage.init(named: "Collapse 1"), for: .normal)
        self.btn_expand.setImage(UIImage.init(named: "Expand 1"), for: .selected)
    }
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    func updateRoomSaleData(details: RoomSaleCustomModel?){
        
        view1_lbl_title.text     = "ARR"
        view1_lbl_cy_amount.text = String(details?.ARR ?? 0)
        view1_ly_amount.text     = String(details?.ARRLY ?? 0.0 )
        let value1 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.ARRLY ?? 0), currentyear: String(details?.ARR ?? 0.0 ))
        if value1 > 0 {
            self.view1_lbl_per.text = "+\(value1)%"
            self.view1_lbl_per.textColor = .green
            self.view1_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
        }
        else if value1 == 0 {
            self.view1_lbl_per.text = "\(value1)%"
            self.view1_lbl_per.textColor = .black
        }
        else{
            self.view1_lbl_per.text = "\(value1)%"
            self.view1_lbl_per.textColor = .red
            self.view1_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
        }
        //view1_lbl_per.text       = "12 %"
        
        view2_lbl_title.text     = "RevPar"
        view2_lbl_cy_amount.text = String(details?.RevPar ?? 0)
        view2_lbl_ly_amount.text = String(details?.RevParLy ?? 0)
        let value2 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.RevParLy ?? 0), currentyear: String(details?.RevPar ?? 0.0 ))
        if value2 > 0 {
            self.view2_lbl_per.text = "+\(value2)%"
            self.view2_lbl_per.textColor = .green
            self.view2_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
        }
        else if value2 == 0 {
            self.view2_lbl_per.text = "\(value2)%"
            self.view2_lbl_per.textColor = .black
        }
        else{
            self.view2_lbl_per.text = " \(value2)%"
            self.view2_lbl_per.textColor = .red
            self.view2_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
        }
        //view2_lbl_per.text       = "12 %"
        
        view3_lbl_title.text     = "ALOS"
        view3_lbl_cy_amount.text = String(details?.CurrentYearALOS ?? 0)
        view3_lbl_ly_amount.text = String(details?.LastYearALOS ?? 0)
        let value3 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.LastYearALOS ?? 0.0), currentyear: String(details?.CurrentYearALOS ?? 0.0))
        if value3 > 0 {
            self.view3_lbl_per.text = "+\(value3)%"
            self.view3_lbl_per.textColor = .green
            self.view3_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
        }
        else if value3 == 0 {
            self.view3_lbl_per.text = "\(value3)%"
            self.view3_lbl_per.textColor = .black 
        }
        else {
            self.view3_lbl_per.text = "\(value3)%"
            self.view3_lbl_per.textColor = .red
            self.view3_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
        }
        //view3_lbl_per.text       = "11 %"
        
        self.view1_lbl_cy_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.ARR ?? 0))
        self.view1_ly_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.ARRLY ?? 0))
        self.view2_lbl_cy_amount.text = CommonDataManager.shared.getNumberWithCommas(cash: Double(details?.RevPar ?? 0))
        self.view2_lbl_ly_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.RevParLy ?? 0))
    }
//     func updateRoomSaleData(details: RoomSaleCustomModel?){
//
//           view1_lbl_title.text     = "ARR"
//           view1_lbl_cy_amount.text = String(details?.ARR ?? 0)
//           view1_ly_amount.text     = String(details?.ARRLY ?? 0.0 )
//           let value1 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.ARRLY ?? 0), currentyear: String(details?.ARR ?? 0.0 ))
//           if value1 > 0 {
//               self.view1_lbl_per.text = "+\(value1)%"
//               self.view1_lbl_per.textColor = .green
//               self.view1_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
//           }
//           else if value1 == 0 {
//               self.view1_lbl_per.text = "\(value1)%"
//               self.view1_lbl_per.textColor = .black
//           }
//           else{
//               self.view1_lbl_per.text = "\(value1)%"
//               self.view1_lbl_per.textColor = .red
//               self.view1_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
//           }
//           //view1_lbl_per.text       = "12 %"
//
//           view2_lbl_title.text     = "RevPar"
//           view2_lbl_cy_amount.text = String(details?.RevPar ?? 0)
//           view2_lbl_ly_amount.text = String(details?.RevParLy ?? 0)
//           let value2 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.RevParLy ?? 0), currentyear: String(details?.RevPar ?? 0.0 ))
//           if value2 > 0 {
//               self.view2_lbl_per.text = "+\(value2)%"
//               self.view2_lbl_per.textColor = .green
//               self.view2_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
//           }
//           else if value2 == 0 {
//               self.view2_lbl_per.text = "\(value2)%"
//               self.view2_lbl_per.textColor = .black
//           }
//           else{
//               self.view2_lbl_per.text = " \(value2)%"
//               self.view2_lbl_per.textColor = .red
//               self.view2_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
//           }
//           //view2_lbl_per.text       = "12 %"
//
//           view3_lbl_title.text     = "ALOS"
//           view3_lbl_cy_amount.text = String(details?.CurrentYearALOS ?? 0)
//           view3_lbl_ly_amount.text = String(details?.LastYearALOS ?? 0)
//           let value3 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.LastYearALOS ?? 0.0), currentyear: String(details?.CurrentYearALOS ?? 0.0))
//           if value3 > 0 {
//               self.view3_lbl_per.text = "+\(value3)%"
//               self.view3_lbl_per.textColor = .green
//               self.view3_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
//           }
//           else if value3 == 0 {
//               self.view3_lbl_per.text = "\(value3)%"
//               self.view3_lbl_per.textColor = .black
//           }
//           else {
//               self.view3_lbl_per.text = "\(value3)%"
//               self.view3_lbl_per.textColor = .red
//               self.view3_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
//           }
//           //view3_lbl_per.text       = "11 %"
//
//           self.view1_lbl_cy_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.ARR ?? 0))
//           self.view1_ly_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.ARRLY ?? 0))
//           self.view2_lbl_cy_amount.text = CommonDataManager.shared.getNumberWithCommas(cash: Double(details?.RevPar ?? 0))
//           self.view2_lbl_ly_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.RevParLy ?? 0))
//       }
//     func updateRoomSaleData(details: RoomSaleCustomModel?){
//
//           view1_lbl_title.text     = "ARR"
//           view1_lbl_cy_amount.text = String(details?.ARR ?? 0)
//           view1_ly_amount.text     = String(details?.ARRLY ?? 0.0 )
//           let value1 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.ARRLY ?? 0), currentyear: String(details?.ARR ?? 0.0 ))
//           if value1 > 0 {
//               self.view1_lbl_per.text = "+\(value1)%"
//               self.view1_lbl_per.textColor = .green
//               self.view1_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
//           }
//           else if value1 == 0 {
//               self.view1_lbl_per.text = "\(value1)%"
//               self.view1_lbl_per.textColor = .black
//           }
//           else{
//               self.view1_lbl_per.text = "\(value1)%"
//               self.view1_lbl_per.textColor = .red
//               self.view1_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
//           }
//           //view1_lbl_per.text       = "12 %"
//
//           view2_lbl_title.text     = "RevPar"
//           view2_lbl_cy_amount.text = String(details?.RevPar ?? 0)
//           view2_lbl_ly_amount.text = String(details?.RevParLy ?? 0)
//           let value2 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.RevParLy ?? 0), currentyear: String(details?.RevPar ?? 0.0 ))
//           if value2 > 0 {
//               self.view2_lbl_per.text = "+\(value2)%"
//               self.view2_lbl_per.textColor = .green
//               self.view2_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
//           }
//           else if value2 == 0 {
//               self.view2_lbl_per.text = "\(value2)%"
//               self.view2_lbl_per.textColor = .black
//           }
//           else{
//               self.view2_lbl_per.text = " \(value2)%"
//               self.view2_lbl_per.textColor = .red
//               self.view2_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
//           }
//           //view2_lbl_per.text       = "12 %"
//
//           view3_lbl_title.text     = "ALOS"
//           view3_lbl_cy_amount.text = String(details?.CurrentYearALOS ?? 0)
//           view3_lbl_ly_amount.text = String(details?.LastYearALOS ?? 0)
//           let value3 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.LastYearALOS ?? 0.0), currentyear: String(details?.CurrentYearALOS ?? 0.0))
//           if value3 > 0 {
//               self.view3_lbl_per.text = "+\(value3)%"
//               self.view3_lbl_per.textColor = .green
//               self.view3_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
//           }
//           else if value3 == 0 {
//               self.view3_lbl_per.text = "\(value3)%"
//               self.view3_lbl_per.textColor = .black
//           }
//           else {
//               self.view3_lbl_per.text = "\(value3)%"
//               self.view3_lbl_per.textColor = .red
//               self.view3_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
//           }
//           //view3_lbl_per.text       = "11 %"
//
//           self.view1_lbl_cy_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.ARR ?? 0))
//           self.view1_ly_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.ARRLY ?? 0))
//           self.view2_lbl_cy_amount.text = CommonDataManager.shared.getNumberWithCommas(cash: Double(details?.RevPar ?? 0))
//           self.view2_lbl_ly_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.RevParLy ?? 0))
//       }
//    func updateRoomSaleData(details: RoomSaleCustomModel?){
//
//        view1_lbl_title.text     = "ARR"
//        view1_lbl_cy_amount.text = String(details?.ARR ?? 0)
//        view1_ly_amount.text     = String(details?.ARRLY ?? 0.0 )
//        let value1 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.ARRLY ?? 0), currentyear: String(details?.ARR ?? 0.0 ))
//        if value1 > 0 {
//            self.view1_lbl_per.text = "+\(value1)%"
//            self.view1_lbl_per.textColor = .green
//            self.view1_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
//        }
//        else if value1 == 0 {
//            self.view1_lbl_per.text = "\(value1)%"
//            self.view1_lbl_per.textColor = .black
//        }
//        else{
//            self.view1_lbl_per.text = "\(value1)%"
//            self.view1_lbl_per.textColor = .red
//            self.view1_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
//        }
//        //view1_lbl_per.text       = "12 %"
//
//        view2_lbl_title.text     = "RevPar"
//        view2_lbl_cy_amount.text = String(details?.RevPar ?? 0)
//        view2_lbl_ly_amount.text = String(details?.RevParLy ?? 0)
//        let value2 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.RevParLy ?? 0), currentyear: String(details?.RevPar ?? 0.0 ))
//        if value2 > 0 {
//            self.view2_lbl_per.text = "+\(value2)%"
//            self.view2_lbl_per.textColor = .green
//            self.view2_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
//        }
//        else if value2 == 0 {
//            self.view2_lbl_per.text = "\(value2)%"
//            self.view2_lbl_per.textColor = .black
//        }
//        else{
//            self.view2_lbl_per.text = " \(value2)%"
//            self.view2_lbl_per.textColor = .red
//            self.view2_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
//        }
//        //view2_lbl_per.text       = "12 %"
//
//        view3_lbl_title.text     = "ALOS"
//        view3_lbl_cy_amount.text = String(details?.CurrentYearALOS ?? 0)
//        view3_lbl_ly_amount.text = String(details?.LastYearALOS ?? 0)
//        let value3 = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: String(details?.LastYearALOS ?? 0.0), currentyear: String(details?.CurrentYearALOS ?? 0.0))
//        if value3 > 0 {
//            self.view3_lbl_per.text = "+\(value3)%"
//            self.view3_lbl_per.textColor = .green
//            self.view3_lbl_per.addImageWithDetailsPage(name: "Increase 2", behindText: true)
//        }
//        else if value3 == 0 {
//            self.view3_lbl_per.text = "\(value3)%"
//            self.view3_lbl_per.textColor = .black
//        }
//        else {
//            self.view3_lbl_per.text = "\(value3)%"
//            self.view3_lbl_per.textColor = .red
//            self.view3_lbl_per.addImageWithDetailsPage(name: "Decrease 2", behindText: true)
//        }
//        //view3_lbl_per.text       = "11 %"
//
//        self.view1_lbl_cy_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.ARR ?? 0))
//        self.view1_ly_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.ARRLY ?? 0))
//        self.view2_lbl_cy_amount.text = CommonDataManager.shared.getNumberWithCommas(cash: Double(details?.RevPar ?? 0))
//        self.view2_lbl_ly_amount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(details?.RevParLy ?? 0))
//    }
    
    //common func to init our view
    private func setupView() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        
    }
    
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "RoomSaleFooterView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
        
    }

}
