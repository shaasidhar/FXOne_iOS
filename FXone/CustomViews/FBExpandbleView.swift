//
//  FBExpandbleView.swift
//  FXone
//
//  Created by Vinayak on 20/11/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class FBExpandbleView: UIView {
    
    var view:UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // headerView outlets
    @IBOutlet weak var lblMenuTitle: UILabel!
    @IBOutlet weak var view_ActualFill: UIView!
    @IBOutlet weak var lbl_percen: UILabel!
    
    @IBOutlet weak var viewBudgetFill: UIView!
    
    @IBOutlet weak var actualViewWidthConstarint: NSLayoutConstraint!
    @IBOutlet weak var lbl_BudgetAmount: UILabel!
    @IBOutlet weak var lbl_Actual_Amount: UILabel!
    
    @IBOutlet weak var lbl_lastyearAmount: UILabel!
    @IBOutlet weak var lbl_todayAmount: UILabel!
    
    @IBOutlet weak var lbl_cyLyPer: UILabel!
    //
    
    @IBOutlet weak var topSellingMenu_table: UITableView!
    @IBOutlet weak var salesByMenu_Table: UITableView!
    @IBOutlet weak var lbl4: UILabel!
    var salesByMenuAountTotal:Double = 0.0
    var topSellingByMenuAountTotal:Float = 0.00000
    let colors = Colors.simplifyRandomColors().shuffled()
    
    var isFromCustome = false
    var selectedIndex = 0
    var amountAray = [AnyObject]()
    //var salesDetails = [FNBSalesDetailsObj]()
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
        
    }
    
    var fnBDeatailSalesByMenu: [FnBDeatailSalesByMenuObj]?
    var fnBDeatailTopsellingMenu: [FnBDetailSellingMenuObj]?
    var fandBSalesDetails:FNBSalesDetailsObj? {
        
        didSet{
            self.setUpSalesAndMenuData()
            prepareStaticSalesData()
            updateCellData()
        }
    }
    var cutomeSales:FAndBSalesDataObj? {
        
        didSet{
            updateCellData()
        }
    }
    var fandBStaticSalesmenu = [FnBDeatailSalesByMenuObj]()
    func prepareStaticSalesData() {
        var a0 = FnBDeatailSalesByMenuObj()
        a0.MenuType = "Food"
        a0.AMOUNT = 0.0
        fandBStaticSalesmenu.append(a0)
        
        var a1 = FnBDeatailSalesByMenuObj()
        a1.MenuType = "Liquor"
        a1.AMOUNT = 0.0
        fandBStaticSalesmenu.append(a1)
        
        var a2 = FnBDeatailSalesByMenuObj()
        a2.MenuType = "Soft Drink"
        a2.AMOUNT = 0.0
        fandBStaticSalesmenu.append(a2)
        
        var a3 = FnBDeatailSalesByMenuObj()
        a3.MenuType = "Tobacco"
        a3.AMOUNT = 0.0
        fandBStaticSalesmenu.append(a3)
        
        var a4 = FnBDeatailSalesByMenuObj()
        a4.MenuType = "Others"
        a4.AMOUNT = 0.0
        fandBStaticSalesmenu.append(a4)
        syncSalesAmount()
    }
    func syncSalesAmount() {
        
        for i in 0..<fandBStaticSalesmenu.count  {
            let each = fandBStaticSalesmenu[i]
            for item in fnBDeatailSalesByMenu ?? [] {
                
                if each.MenuType == item.MenuType {
                    
                    fandBStaticSalesmenu[i].AMOUNT = item.AMOUNT
                }
            }
        }
       
    }
    func setUpSalesAndMenuData(){
        
        // headerviiew data binding
        

        self.fnBDeatailSalesByMenu = fandBSalesDetails?.SalesByMenu
        self.fnBDeatailTopsellingMenu = fandBSalesDetails?.SellingMenu
        
        for each in fnBDeatailSalesByMenu ?? [] {
            
            salesByMenuAountTotal += Double(each.AMOUNT ?? 0.0)
        }
        print("VarB::::::::\(salesByMenuAountTotal)")
        for each in fnBDeatailTopsellingMenu ?? [] {
            
            topSellingByMenuAountTotal += Float(each.Count ?? Float(0.00000))
        }
        print("varC::::::::\(topSellingByMenuAountTotal)")
        //self.topSellingMenu_table.reloadData()
        self.salesByMenu_Table.reloadData()
    }
    
    
    
    //common func to init our view
    private func setupView() {
        view = loadFromNib()
        //        var varB:Double?
        //        for i in [fnBDeatailSalesByMenu] {
        //            print(i as Any)
        //            varB = varB+i
        //        }
        
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        
        //topSellingMenu_table.delegate  = self
        //topSellingMenu_table.dataSource =  self
        //topSellingMenu_table.allowsSelection = false
        //topSellingMenu_table.register(UINib(nibName: "TopsellingMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "TopeSellingMenuCell")
       // topSellingMenu_table.backgroundColor = .clear
        
        salesByMenu_Table.delegate  = self
        salesByMenu_Table.dataSource =  self
        salesByMenu_Table.allowsSelection = false
        salesByMenu_Table.register(UINib(nibName: "SalesByMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "SalesByMenuCell")
        //salesByMenu_Table.backgroundColor = .clear
                
    }
    
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "FBExpandbleView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
        
    }
//    var cutomeSales :RoomSaleCustomModel? {
//        didSet{
//            self.updateCellData()
//        }
//    }
    func updateCellData(){
        let per = CommonDataManager.shared.getActualBudgetValue(budget: "20000", actual: "7200")
        print("Perce::::\(per)")
        lbl_percen.text = "\(per.0) %"
        actualViewWidthConstarint.constant = CGFloat(per.1)
        view_ActualFill.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
        
        if isFromCustome {
            self.lbl_BudgetAmount.text = "\(cutomeSales?.Budget ?? 0.0)"
            self.lbl_todayAmount.text = "\(cutomeSales?.AMOUNT ?? 0.0)"
            self.lbl_lastyearAmount.text = "\(cutomeSales?.LastYearAmount ?? 0.0)"
            self.lbl_Actual_Amount.text = "\(cutomeSales?.AMOUNT ?? 0.0)"
            let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(cutomeSales?.LastYearAmount ?? 0.0)", currentyear: "\(cutomeSales?.AMOUNT ?? 0.0)")
            if value > 0 {
                self.lbl_cyLyPer.text = "+\(value)%"
                self.lbl_cyLyPer.textColor = .green
                self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else if value == 0 {
                self.lbl_cyLyPer.text = " \(value)%"
                self.lbl_cyLyPer.textColor = .black
                self.lbl_cyLyPer.addImageWithDetailsPage(name: "", behindText: true)
            }
            else {
                self.lbl_cyLyPer.text = " \(value)%"
                self.lbl_cyLyPer.textColor = .red
                self.lbl_cyLyPer.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
            }
            self.isFromCustome = false
        }else {
            if selectedIndex == 0 {
                //self.lbl_title.text = "\(todaySales?.Selection ?? "")"
                self.lbl_BudgetAmount.text = "\(cutomeSales?.Budget ?? 0.0)"
                self.lbl_todayAmount.text = "\(cutomeSales?.AMOUNT ?? 0.0)"
                self.lbl_lastyearAmount.text = "\(cutomeSales?.LastYearAmount ?? 0.0)"
                self.lbl_Actual_Amount.text = "\(cutomeSales?.AMOUNT ?? 0.0)"
                let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(cutomeSales?.LastYearAmount ?? 0.0)", currentyear: "\(cutomeSales?.AMOUNT ?? 0.0)")
                if value > 0 {
                    self.lbl_cyLyPer.text = "+\(value)%"
                    self.lbl_cyLyPer.textColor = .green
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
                }
                else if value == 0 {
                    self.lbl_cyLyPer.text = " \(value)%"
                    self.lbl_cyLyPer.textColor = .black
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "", behindText: true)
                }
                else {
                    self.lbl_cyLyPer.text = " \(value)%"
                    self.lbl_cyLyPer.textColor = .red
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
                }
            }
            else if selectedIndex == 1 {
                //self.lbl_title.text = "\(monthSales?.Selection ?? "")"
                self.lbl_BudgetAmount.text = "\(cutomeSales?.Budget ?? 0.0)"
                self.lbl_todayAmount.text = "\(cutomeSales?.AMOUNT ?? 0.0)"
                self.lbl_lastyearAmount.text = "\(cutomeSales?.LastYearAmount ?? 0.0)"
                self.lbl_Actual_Amount.text = "\(cutomeSales?.AMOUNT ?? 0.0)"
                let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(cutomeSales?.LastYearAmount ?? 0.0)", currentyear: "\(cutomeSales?.AMOUNT ?? 0.0)")
                    if value > 0 {
                        self.lbl_cyLyPer.text = "+\(value)%"
                        self.lbl_cyLyPer.textColor = .green
                        self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
                    }
                    else if value == 0 {
                        self.lbl_cyLyPer.text = " \(value)%"
                        self.lbl_cyLyPer.textColor = .black
                        self.lbl_cyLyPer.addImageWithDetailsPage(name: "", behindText: true)
                    }
                    else {
                        self.lbl_cyLyPer.text = " \(value)%"
                        self.lbl_cyLyPer.textColor = .red
                        self.lbl_cyLyPer.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
                    }
            }
            else if selectedIndex == 2 {
                //self.lbl_title.text = "\(yearSales?.Selection ?? "")"
                self.lbl_BudgetAmount.text = "\(cutomeSales?.Budget ?? 0.0)"
                self.lbl_todayAmount.text = "\(cutomeSales?.AMOUNT ?? 0)"
                self.lbl_lastyearAmount.text = "\(cutomeSales?.LastYearAmount ?? 0.0)"
                self.lbl_Actual_Amount.text = "\(cutomeSales?.AMOUNT ?? 0)"
                let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(cutomeSales?.LastYearAmount ?? 0.0)", currentyear: "\(cutomeSales?.AMOUNT ?? 0)")
                if value > 0 {
                    self.lbl_cyLyPer.text = "+\(value)%"
                    self.lbl_cyLyPer.textColor = .green
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
                }
                else if value == 0 {
                    self.lbl_cyLyPer.text = " \(value)%"
                    self.lbl_cyLyPer.textColor = .black
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "", behindText: true)
                }
                else {
                    self.lbl_cyLyPer.text = " \(value)%"
                    self.lbl_cyLyPer.textColor = .red
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
                }
                
            }
        }
        
        
        let budget = lbl_BudgetAmount.text ?? "0"
        let actual = lbl_Actual_Amount.text ?? "0"
        let b = Int(Double (budget) ?? 0.0)
        let a = Int(Double (actual) ?? 0.0)
        if b <= 0 {
            lbl_percen.text = "100%"
            actualViewWidthConstarint.constant = CGFloat(150)
            //budgetViewWidthConstraint.constant = 2 // butger bar occy
        }
        else if a > b {
            lbl_percen.text = "100%+"
            actualViewWidthConstarint.constant = CGFloat(160)
        }
        else {
            
            let per = CommonDataManager.shared.getActualBudgetValue(budget:lbl_BudgetAmount.text ?? "0", actual: lbl_Actual_Amount.text ?? "0")
            print("Perce::::\(per)")
            lbl_percen.text = "\(per.0)%"
            actualViewWidthConstarint.constant = CGFloat(per.1)
        }
        
//        if b == 0 {
//            view_barChartContainer.isHidden = true
//            lbl_budgethelpMessage.isHidden = false
//            lbl_budgethelpMessage.text = "Budget is not Mentioned"
//        }
//        else {
//            view_barChartContainer.isHidden = false
//            lbl_budgethelpMessage.isHidden = true
//        }
        let double_TodayCost = Double(self.lbl_todayAmount.text ?? "") ?? 0.0
        self.lbl_todayAmount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: double_TodayCost))"
        let double_Budget = Double(self.lbl_BudgetAmount.text ?? "") ?? 0.0
        self.lbl_BudgetAmount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: double_Budget))"
        let double_Actual = Double(self.lbl_Actual_Amount.text ?? "") ?? 0.0
        self.lbl_Actual_Amount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: double_Actual))"
        let double_LyAmont = Double(self.lbl_lastyearAmount.text ?? "") ?? 0.0
        self.lbl_lastyearAmount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: double_LyAmont))"
        
        //let cash = NSNumber(value:double_todayCost )
    }
    
}
extension FBExpandbleView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == topSellingMenu_table {
            
            return fnBDeatailTopsellingMenu?.count ?? 0
        }
        else if tableView == salesByMenu_Table {
            return  self.fandBSalesDetails?.SalesByMenu?.count ?? 0 //fnBDeatailSalesByMenu?.count ?? 0
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == topSellingMenu_table,
            let cell:TopsellingMenuTableViewCell = topSellingMenu_table.dequeueReusableCell(withIdentifier: "TopeSellingMenuCell") as? TopsellingMenuTableViewCell {
            
            
            cell.lbl_ItemName.text = fnBDeatailTopsellingMenu?[indexPath.row].MenuType ?? ""
            
            cell.lbl_Count.text = NumberFormatter.localizedString(from: NSNumber(value: (fnBDeatailTopsellingMenu?[indexPath.row].Count)!), number: .decimal)
            print(fnBDeatailTopsellingMenu?[indexPath.row].Count ?? 0/topSellingByMenuAountTotal)
            cell.progressView.progress = Float((fnBDeatailTopsellingMenu?[indexPath.row].Count ?? Float(0.00000000))) / topSellingByMenuAountTotal
            cell.progressView.progressTintColor = self.colors[indexPath.row]
            cell.lbl_Percentage.text = String(NumberFormatter.localizedString(from: NSNumber(value: (((fnBDeatailTopsellingMenu?[indexPath.row].Count)!) / topSellingByMenuAountTotal) * 100), number: .decimal))+"%"
            cell.backgroundColor = .clear
            return cell
        }
        else if tableView == salesByMenu_Table,
            let cell:SalesByMenuTableViewCell = salesByMenu_Table.dequeueReusableCell(withIdentifier: "SalesByMenuCell") as? SalesByMenuTableViewCell {
            
            let salesByMenu = self.fandBSalesDetails?.SalesByMenu?[indexPath.row]
                               cell.lbl_MenuItem.text = salesByMenu?.MenuType ?? ""
                               //fandBStaticSalesmenu[indexPath.row].MenuType ?? ""
            cell.lbl_Amount.text = NumberFormatter.localizedString(from: NSNumber(value: (salesByMenu?.Budget ?? 0.0)), number: .decimal)
            let progressAmount: Double  = salesByMenu?.AMOUNT ?? 0.0
            if progressAmount > 0.0 {
                cell.progressView.progress = Float((salesByMenu?.AMOUNT ?? 0.0) / salesByMenuAountTotal)
                cell.lbl_totalPercentage.text = String(NumberFormatter.localizedString(from: NSNumber(value: ((salesByMenu?.AMOUNT ?? 0.0) / salesByMenuAountTotal) * 100), number: .decimal))+"%"
            } else {
                cell.progressView.progress  = 0.0
                cell.lbl_totalPercentage.text = "0%"
            }
            cell.lbl_cyAmount.text = "\(salesByMenu?.CYTrend ?? 0)%"
            cell.lbl_lyAmount.text = "\(salesByMenu?.LYTrend ?? 0)%"
            cell.lbl_ApCoverAmount.text = "\(salesByMenu?.APCover ?? 0)"
            cell.lbl_apCheck.text = "\(salesByMenu?.APCheck ?? 0)"
            cell.lbl_budgetAmount.text = "\(salesByMenu?.AMOUNT ?? 0)"
            cell.progressView.progressTintColor = self.colors[indexPath.row]
            cell.backgroundColor = .clear
            cell.lbl_lyAmount.addImageWithDetailsPage(name: "Decrease 3", behindText: false)
            cell.lbl_cyAmount.addImageWithDetailsPage(name: "Increase 3", behindText: false)
            //
            let doubleTodayCost = Double(cell.lbl_budgetAmount.text ?? "") ?? 0.0
            cell.lbl_budgetAmount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: doubleTodayCost))"
            let doubleBudget = Double(cell.lbl_Amount.text ?? "") ?? 0.0
            cell.lbl_Amount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: doubleBudget))"
            return cell
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == salesByMenu_Table {
            
            return  60
        } else {
            return 40
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == topSellingMenu_table {
            let roomsaleview = TopeSellingMenuHeaderView.init(frame: CGRect(x: 0, y: 0 , width: 206, height: 40))
            return  roomsaleview
            
        }
        else if tableView == salesByMenu_Table {
            let roomsaleview = SalesByMenuHeaderView.init(frame: CGRect(x: 0, y: 0 , width: 206, height: 100))
            return  roomsaleview
        }
        else{
            return UIView.init()
        }
        
        
    }
    
}

