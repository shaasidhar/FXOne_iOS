//
//  CBCview.swift
//  BarChartTest
//
//  Created by Vinayak on 02/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import Charts

class CBCview: UIView {

    var view:UIView!
    
    var barChartView: BarChartView?
    
    var months = [String]()
    var unitsSold = [Int]()
    var unitsBought = [Int]()
    var chartcolors = [UIColor]()
    //initWithFrame to init view from code
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        
        
        barChartView = BarChartView()
        barChartView?.frame =  self.view.frame
        barChartView?.delegate = self
        barChartView?.drawValueAboveBarEnabled = true
        //legend
        let legend = barChartView?.legend
        legend?.enabled = false
        legend?.horizontalAlignment = .right
        legend?.verticalAlignment = .top
        legend?.orientation = .vertical
        legend?.drawInside = true
        legend?.yOffset = 10.0;
        legend?.xOffset = 10.0;
        legend?.yEntrySpace = 0.0;
        
        let format = NumberFormatter()
        format.minimumIntegerDigits = 0
        let formatter = DefaultValueFormatter(formatter: format)
        barChartView?.xAxis.valueFormatter = (formatter) as? any AxisValueFormatter
        
        let yaxis = barChartView?.leftAxis
        yaxis?.spaceTop = 0.35
        yaxis?.axisMinimum = 0
        //yaxis?.axisMaximum = 10000000000// as of now given laks
        yaxis?.gridColor = UIColor.lightGray.withAlphaComponent(0.5)
        yaxis?.drawGridLinesEnabled = true
        yaxis?.labelFont = UIFont.FXRobotoRegular(size: 9)
        yaxis?.granularityEnabled = true
        yaxis?.granularity = 1.0
        yaxis?.valueFormatter = (formatter) as? any AxisValueFormatter
        barChartView?.rightAxis.enabled = false
        //axisFormatDelegate = self
        
        //setChart()
        
        self.view.addSubview(barChartView ?? CustomDashBarChartView())
        
    }
    func updateBarChartdata(list:[BarChartModel],colors:[UIColor]){
        self.chartcolors = colors
        for each in list {
            self.months.append("CY  LY")
            self.unitsSold.append(Int(each.CYAmount ?? 0.0))
            self.unitsBought.append(Int(each.LYAmount ?? 0.0))
        }
        //need to give value change by UX////
        let format = NumberFormatter()
        format.numberStyle = .decimal
        let formatter = DefaultValueFormatter(formatter: format)
        let xaxis = barChartView?.xAxis
        xaxis?.valueFormatter = formatter as? any AxisValueFormatter
        xaxis?.drawGridLinesEnabled = false
        xaxis?.labelPosition = .bottom
        xaxis?.labelFont = UIFont.FXRobotoRegular(size: 10)
        xaxis?.centerAxisLabelsEnabled = true
        xaxis?.valueFormatter = IndexAxisValueFormatter(values:self.months)
        xaxis?.granularity = 1.0
        
        self.setChart()
    }
    
    
    func setChart() {
        barChartView?.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries1: [BarChartDataEntry] = []
        
        for i in 0..<self.months.count {
            
            let dataEntry = BarChartDataEntry(x: Double(i) , y: Double(self.unitsSold[i]))
            dataEntries.append(dataEntry)
            
            let dataEntry1 = BarChartDataEntry(x: Double(i) , y: Double(self.self.unitsBought[i]))
            dataEntries1.append(dataEntry1)
            
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "")
        let chartDataSet1 = BarChartDataSet(entries: dataEntries1, label: "")
        
        chartDataSet.valueTextColor = .clear
        chartDataSet1.valueTextColor = .clear
        
        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1]
        chartDataSet.colors = chartcolors
        
        let colors = chartcolors
        var lightcolors = [UIColor]()
        for each in colors {
            var c = each.coreImageColor
            let color = UIColor.init(red: c.red + 40 / 255, green: c.green + 40 / 255, blue: c.blue + 40 / 255, alpha: 1)
            lightcolors.append(color)
        }
        chartDataSet1.colors = lightcolors
        
        let chartData = BarChartData(dataSets: dataSets)
        
        let groupSpace = 0.3
        let barSpace = 0.05
        let barWidth = 0.3
        // (0.3 + 0.05) * 2 + 0.3 = 1.00 -> interval per "group"
        
        let groupCount = self.months.count
        let startYear = 0
        
        chartData.barWidth = barWidth;
        barChartView?.xAxis.axisMinimum = Double(startYear)
        let gg = chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        print("Groupspace: \(gg)")
        barChartView?.xAxis.axisMaximum = Double(startYear) + gg * Double(groupCount)
        
        chartData.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
        //chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        barChartView?.notifyDataSetChanged()
        
        chartData.barWidth = Double(0.35)
        
        barChartView?.data = chartData
        
        
        self.barChartView?.setVisibleXRangeMaximum(4)
        
        //background color
        barChartView?.backgroundColor = .clear
        
        //chart animation
        barChartView?.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption: .linear)
        
        
    }
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "CBCview", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
        
    }

}
extension CBCview : ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight, numberOftaps: Int) {
        
    
    }
    
}
