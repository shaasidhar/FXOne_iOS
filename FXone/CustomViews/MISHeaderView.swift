//
//  MISHeaderView.swift
//  FXone
//
//  Created by Vinayak on 12/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class MISHeaderView: UIView {

    var view:UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var lbl_actualCy: UILabel!
    @IBOutlet weak var lbl_budgetCy: UILabel!
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        
    }
    
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "MISHeaderView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
        
    }

}
