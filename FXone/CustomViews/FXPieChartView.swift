//
//  FXPieChartView.swift
//  FXone
//
//  Created by Vinayak on 08/08/19.
//  Copyright © 2019 IDS. All rights reserved.
//


import UIKit
import Charts

class FXPieChartView: UIView {
    
    var peiChart : PieChartView?
    
    @IBOutlet weak var scrollview_buttons: UIScrollView!
    @IBOutlet weak var tableview_legends: UITableView!
    @IBOutlet weak var view_chartViewBase: UIView!
    
    var aryNumberOfPieEntries = [PieChartDataEntry]()
    var roomdetails_dic = [String: Any]()
    var view:UIView!
    
    let stripView = UIView(frame: CGRect(x:0, y:0, width:0,height:0))
    var selecTedModule = 100
    var previousItem = 100
    var dataLoaded = false
    
    
    let colors = Colors.simplifyRandomColors().shuffled()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        
        self.addSubview(scrollview_buttons)
        self.tableview_legends.delegate  = self
        self.tableview_legends.dataSource =  self
        peiChart = PieChartView()

        tableview_legends.allowsSelection = false
        tableview_legends.register(UINib(nibName: "PieChartLegendLabelsTableViewCell", bundle: nil), forCellReuseIdentifier: "LegendCell")
        
    }
    
    @objc func selectedScrollItem(_ sender : UIButton) {
        print(sender.tag)
        var tmpButton : UIButton?
        var btnDeselected : UIButton?
        
        if previousItem != sender.tag{
            
            
            tmpButton = self.scrollview_buttons.viewWithTag(sender.tag) as? UIButton
            tmpButton?.isSelected = true
            tmpButton?.isUserInteractionEnabled = true
            tmpButton?.setTitleColor(UIColor.hexStringToUIColor(hex: "FF8E00"), for: .selected)
            btnDeselected = self.scrollview_buttons.viewWithTag(self.previousItem) as! UIButton
            btnDeselected?.isUserInteractionEnabled = true
            btnDeselected?.isSelected = false
            btnDeselected?.setTitleColor(UIColor.gray, for: .selected)
            self.stripView.frame  =  CGRect(x:(tmpButton?.frame.origin.x)! , y: (tmpButton?.frame.size.height)! + (tmpButton?.frame.origin.y)! + 1, width: (tmpButton?.frame.size.width)! , height:3)
            self.stripView.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
            
            self.previousItem = sender.tag
            let button_title = sender.title(for: .normal)!
            self.populatePieChart(title: button_title)
            CommonDataManager.shared.selectedDetailItem =  self.previousItem - 99
            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "SELECTEDITEM"), object: nil)
            
        }
    }
    func configureScrollItems(items:[Any]){
       print("configureScrollItems",self.roomdetails_dic)
        let subviews = scrollview_buttons.subviews
        for view in subviews {
            view.removeFromSuperview()
        }
        selecTedModule = 100
        previousItem = 100
        
        scrollview_buttons.showsHorizontalScrollIndicator = false
        DispatchQueue.main.async {
            self.scrollview_buttons.frame = CGRect(x: 5, y:10, width: self.frame.width - 10 , height: 30)
        }
        var x  : CGFloat = 5.0
        //var items = ["Room Typpe","Guest Type","Market Segment","Bussiness Source"]
        for i in 0..<items.count{
            
            
            let each = items[i] as? [String:Any]
            let title = each?["Title"] as! String
            var val1 = each?["Item\(i+1)"] as? [Any]
            if val1 == nil {
                val1 = each?["data"] as? [Any]
            }
            self.roomdetails_dic.updateValue(val1 ?? "", forKey: title)
            
            let button = UIButton()
            button.frame = CGRect(x: x, y: 0, width: 100, height: 20)
            button.setTitle(title, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
            button.setTitleColor(UIColor.gray, for: .normal)
            button.setTitleColor(UIColor.hexStringToUIColor(hex: "FF8E00"), for: .selected)
            button.tag = i + 100
            if i == 0 {
                button.isSelected = true
                stripView.frame  =  CGRect(x:button.frame.origin.x , y:button.frame.origin.y + button.frame.size.height + 1 , width: button.frame.size.width , height:3)
                stripView.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
                self.scrollview_buttons.addSubview(stripView)
                self.populatePieChart(title: title)
                
            }
            
            button.addTarget(self, action: #selector(FXPieChartView.selectedScrollItem(_:)), for:.touchUpInside)
            
            x = button.frame.origin.x + button.frame.width + 5
            self.scrollview_buttons.contentSize = CGSize(width: x, height: self.scrollview_buttons.frame.size.height)
            
            self.scrollview_buttons.addSubview(button)
            if i == items.count - 1 {
            dataLoaded = true
            }
        }
    }
    func populatePieChart(title : String) {
        let obj = roomdetails_dic[title]
        aryNumberOfPieEntries.removeAll()
        var yValue : CGFloat?
        if let room_sales = obj as? [Details_RoomSaleObject] {
            // populate view_piechart.aryNumberOfPieEntries
            for room_type in room_sales {
                let pieEntry = PieChartDataEntry(value: Double(room_type.CurrentYear ?? 0) , label: room_type.RoomTypeName)
                aryNumberOfPieEntries.append(pieEntry)
                yValue = -70.0
            }
        }else if let guest_wise_details =  obj as? [GuestTypeObject] {
            for guest_type in guest_wise_details {
                let pieEntry = PieChartDataEntry(value: Double(guest_type.CurrentYear ?? 0) , label: guest_type.GuestStatusName)
                aryNumberOfPieEntries.append(pieEntry)
                yValue = -60.0
            }
        }else if let market_segments =  obj as? [MarketSegmentWaiseObject] {
            for market_segment in market_segments {
                let pieEntry = PieChartDataEntry(value: Double(market_segment.CurrentYear ?? 0) , label: market_segment.MarketSegmentName)
                aryNumberOfPieEntries.append(pieEntry)
                yValue = -25.0
            }
        }else if let business_sources =  obj as? [BusinessSourceObject] {
            for business_source in business_sources {
                let pieEntry = PieChartDataEntry(value: Double(business_source.CurrentYear ?? 0) , label: business_source.BusinessSourceName)
                aryNumberOfPieEntries.append(pieEntry)
                yValue = -35.0
            }
        }
        else  if let guestData = obj as? [GuestStatusData]{
            
            for each in  guestData {
                
                let pieEntry = PieChartDataEntry(value: Double(each.Amount ?? 0) , label: each.GuestStatusName)
                aryNumberOfPieEntries.append(pieEntry)
                yValue = -65.0
            }
        }
        else if let marketSegmentData = obj as? [MarketSegmentData] {
            for each in  marketSegmentData {
                
                let pieEntry = PieChartDataEntry(value: Double(each.Amount ?? 0) , label: each.MarketSegmentName)
                aryNumberOfPieEntries.append(pieEntry)
                yValue = -35.0
            }
        }
        else if let businessSourceData = obj as? [BusinessSourceData] {
            for each in  businessSourceData {
                
                let pieEntry = PieChartDataEntry(value: Double(each.Amount ?? 0) , label: each.BusinessSourceName)
                aryNumberOfPieEntries.append(pieEntry)
                yValue = -60
            }
        }
        else {
            
        }
        self.callChartDataWithYAxisValue(yAxis: yValue ?? 0.0)
    }
    
    func callChartDataWithYAxisValue(yAxis:CGFloat) {
        //print("YAxisValue>>>>>>>>>>>>", yAxis)
        //self.peiChart?.center = view_chartViewBase.center
        //self.peiChart?.frame = CGRect(x:0,y: 0 ,width:250 ,height:250)
    //Pie Chart sizes for all the devices.
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            self.peiChart?.frame = CGRect(x:0,y: 0 ,width:160 ,height:160)
            break
        // It's an iPhone
        case .pad:
            self.peiChart?.frame = CGRect(x:0,y: 0 ,width:200 ,height:200)
            break
        // It's an iPad
            
        case .unspecified:
            print("For unspecified")
            break
            // Uh, oh! What could it be?
        case .tv:
            print("For TV")
        case .carPlay:
            print("For carPlay")
        case .mac:
            print("For Mac" )
        case .vision:
            print("For Vision" )
        @unknown default:
            print("For Default" )
        }
        
        DispatchQueue.main.async {
            
            
            self.peiChart?.usePercentValuesEnabled = false
            
            let chartDataSet = PieChartDataSet(entries: self.aryNumberOfPieEntries, label: "")

            let chartdata = PieChartData(dataSet: chartDataSet)
            chartDataSet.colors = self.colors
            chartDataSet.drawValuesEnabled = false
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            chartdata.setValueFormatter(DefaultValueFormatter(formatter:formatter))
            self.peiChart?.data = chartdata
            //You can zero all the offsets - by disabling the legend like pieChart.legend.enabled = false.Then if the pie is still too small for you - just adjust the size of the view.
            self.peiChart?.legend.enabled = false
            self.peiChart?.drawHoleEnabled = false
            self.peiChart?.drawEntryLabelsEnabled = false
            self.peiChart?.legend.textColor = UIColor.clear
            self.peiChart?.delegate = self
            self.peiChart?.legend.formSize = 0
            
            self.view_chartViewBase.addSubview(self.peiChart ?? PieChartView())
            self.tableview_legends.reloadData()
        }
        
    }
    
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "FXPieChartView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
        
    }
}
extension FXPieChartView : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryNumberOfPieEntries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell:PieChartLegendLabelsTableViewCell = self.tableview_legends.dequeueReusableCell(withIdentifier: "LegendCell") as! PieChartLegendLabelsTableViewCell
        
        let cell = tableview_legends.dequeueReusableCell(withIdentifier: "LegendCell") as! PieChartLegendLabelsTableViewCell
        
        let amount = aryNumberOfPieEntries[indexPath.row].value

        cell.lbl_title.text = "\(aryNumberOfPieEntries[indexPath.row].label ?? "")"
        cell.lbl_count.text = "\(CommonDataManager.shared.getNumberWithCommas(cash:  amount))"
        //cell.lbl_title.textColor = .red
        cell.lbl_title.font = UIFont.systemFont(ofSize: 12)
        cell.lbl_count.font = UIFont.systemFont(ofSize: 12)
        //cell.lbl_count.text = String("\(Int(aryNumberOfPieEntries[indexPath.row].value))k")
        cell.lbl_status.backgroundColor = self.colors[indexPath.row]
        tableview_legends.separatorStyle = .none
        cell.backgroundColor  = UIColor.clear
        
        return cell
    }
    
    
}

extension FXPieChartView : ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight, numberOftaps: Int) {
        
        
    }
    
}

