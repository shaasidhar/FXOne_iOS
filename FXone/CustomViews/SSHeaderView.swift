//
//  SSHeaderView.swift
//  FXone
//
//  Created by Vinayak on 13/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import MarqueeLabel

class SSHeaderView: UIView {

    var view:UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl1: MarqueeLabel!
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        
    }
    
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "SSHeaderView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
        if lbl1.text?.count ?? 0 >= 5 {
            lbl1.type = .continuous
            lbl1.speed = .duration(10.0)
            lbl1.animationCurve = .easeInOut
            lbl1.fadeLength = 10.0
            lbl1.leadingBuffer = 5.0
            lbl1.trailingBuffer = 20.0
        }
        
    }

}
