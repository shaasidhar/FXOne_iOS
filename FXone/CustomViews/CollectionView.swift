//
//  CollectionView.swift
//  FXone
//
//  Created by Vinayak on 08/04/20.
//  Copyright © 2020 IDS. All rights reserved.
//

import UIKit
import Charts

class CollectionView: UIView {
    @IBOutlet weak var currency_Lbl: UILabel!
    @IBOutlet weak var totalColection_Lbl: UILabel!
    @IBOutlet weak var liveSwitch: UISwitch!
    
    @IBOutlet weak var collectionview_legends: UICollectionView!
    
    @IBOutlet weak var lblLiveStatus: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var cellTitle_Lbl: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var view_container: UIView!
    @IBOutlet weak var view_CollLbl: UIView!
    @IBOutlet weak var lyCDCM_Lbl: UILabel!
    @IBOutlet weak var lyCurncy: UILabel!
    @IBOutlet weak var lyAmount: UILabel!
    
    @IBOutlet weak var selectedPieChartEntryLabel: UILabel!
    
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    var vC = ViewController()
    var dictionaries = [[String: Int]]()
    var selectedSegmentIndex = 110
    var isFromCoustome = false
    var collectionDict:[String:Int] = ["Cash": 10000, "Card": 1234,"City Ledger": 2409, "Others": 123124, "Credit Card": 9075, "Debit Card":36057]
    
    var collectionData = CollectionsDataModel()
    var collectionArra = CollectionsDataModelArry()
    
    var view:UIView!
    var colors = Colors.chartColors()
    var legendsObj: RoomStatusByDay?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
        setupView()
        self.backgroundColor = .clear
        self.bgView.backgroundColor = UIColor.white
        // set target
        self.liveSwitch.addTarget(self, action: #selector(switchStateChanged(_:)), for: UIControl.Event.valueChanged)
        
    }

    @objc func switchStateChanged(_ mjSwitch: MJMaterialSwitch) {
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    var entriesData: [FExpensePieChartModel]? {
        didSet {
            updateDate()
        }
    }
    func updateDate() {
        
        collectionview_legends.register(UINib(nibName: "ChartLegendsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ChartLegendsCollectionViewCell")
        selectedPieChartEntryLabel.isHidden = false
        self.collectionview_legends.dataSource = self
        self.collectionview_legends.delegate = self
        pieChartView.delegate = self
        
        var entries = [ChartDataEntry]()
        let count = entriesData?.count ?? 0
        var total = 0.0
        for x in 0..<count {
            let each = entriesData?[x]
            let x = (Double(each?.amount ?? "") ?? 0.0)
            total += x
            //entries.append(ChartDataEntry(x:x, y: x))
            entries.append(PieChartDataEntry(value: x, label: String("")))
        }
        let set = PieChartDataSet(entries: entries, label: "50")
        set.sliceSpace = 2
        //PieChartDataSet(entries)
        //PieChartDataSet(values: entries, label: "50")
        set.colors = colors
        set.drawValuesEnabled = false
        set.valueLinePart1OffsetPercentage = 0.8
        set.valueLinePart1Length = 0.2
        set.valueLinePart2Length = 0.4
        set.xValuePosition = .insideSlice
        set.yValuePosition = .outsideSlice
        
        pieChartView.legend.formSize = 0
        pieChartView.legend.enabled = false
        pieChartView.setExtraOffsets(left: 5, top: 0, right: 5, bottom: 0)
        pieChartView.animate(xAxisDuration: 1, easingOption: .easeOutBack)
        pieChartView.drawEntryLabelsEnabled = true
        pieChartView.setNeedsDisplay()
        pieChartView.drawCenterTextEnabled = true
        pieChartView.drawEntryLabelsEnabled = true
        //center text
        //pieChartView.centerText = "\(CommonDataManager.shared.suffixNumber(number: total))"
       
        pieChartView.holeRadiusPercent = 0.8
        
        let data = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = ""
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        data.setValueFont(.systemFont(ofSize: 11, weight: .light))
        data.setValueTextColor(.black)
        pieChartView.data = data
        updateLegendsList()
    }
    
//    func updateChartdata(){
//
//        let chartDataSet = PieChartDataSet(values: numberofEntires, label: "")
//        let chartData = PieChartData(dataSet: chartDataSet)
//        let colors = [UIColor.blue,.green]
//        chartDataSet.colors = colors
//        pieChart.data =  chartData
//
//    }
    
    func updateCollectionData(colectionArray: CollectionsDataModel?){
        print(colectionArray ?? CollectionsDataModelArry())
        var colectiontotalAmount = 0.0
        self.collectionData = colectionArray ?? CollectionsDataModel()
        var ary = [FExpensePieChartModel]()
        
        if isFromCoustome {
            for coustome in colectionArray?.CustomDetail ?? [CollectionsDataModelArry()] {
                colectiontotalAmount += (coustome.Amount ?? 0.0)
            }
            self.totalColection_Lbl.text = "\(colectiontotalAmount)"
            if let list = collectionData.CustomDetail  {
                for each in list {
                    var item = FExpensePieChartModel()
                    item.amount = "\(each.Amount ?? 0.0)"
                    item.accountMasterName = each.SettlementName
                    ary.append(item)
                }
                self.entriesData = ary
            }
            
        }
        else if selectedSegmentIndex == 0 {
            
             for day in colectionArray?.CustomDetail ?? [CollectionsDataModelArry()] {
                 colectiontotalAmount += (day.Amount ?? 0.0)
             }
             self.totalColection_Lbl.text = "\(colectiontotalAmount)"
            
            if let list = collectionData.CustomDetail  {
                for each in list {
                    var item = FExpensePieChartModel()
                    item.amount = "\(each.Amount ?? 0.0)"
                    item.accountMasterName = each.SettlementName
                    ary.append(item)
                }
                self.entriesData = ary
            }
            
         }
       else if selectedSegmentIndex == 1 {
           
            for day in colectionArray?.Day ?? [CollectionsDataModelArry()] {
                colectiontotalAmount += (day.Amount ?? 0.0)
            }
            self.totalColection_Lbl.text = "\(colectiontotalAmount)"
           
           if let list = collectionData.Day  {
               for each in list {
                   var item = FExpensePieChartModel()
                   item.amount = "\(each.Amount ?? 0.0)"
                   item.accountMasterName = each.SettlementName
                   ary.append(item)
               }
               self.entriesData = ary
           }
           
        } else if selectedSegmentIndex == 2 {
            for month in colectionArray?.Month ?? [CollectionsDataModelArry()] {
                colectiontotalAmount += (month.Amount ?? 0.0)
            }
            self.totalColection_Lbl.text = "\(colectiontotalAmount)"
            if let list = collectionData.Month  {
                for each in list {
                    var item = FExpensePieChartModel()
                    item.amount = "\(each.Amount ?? 0.0)"
                    item.accountMasterName = each.SettlementName
                    ary.append(item)
                }
                self.entriesData = ary
            }
            
        } else if selectedSegmentIndex == 3 {
            for year in colectionArray?.Year ?? [CollectionsDataModelArry()] {
                colectiontotalAmount += (year.Amount ?? 0.0)
            }
            self.totalColection_Lbl.text = "\(colectiontotalAmount)"
            if let list = collectionData.Year  {
                for each in list {
                    var item = FExpensePieChartModel()
                    item.amount = "\(each.Amount ?? 0.0)"
                    item.accountMasterName = each.SettlementName
                    ary.append(item)
                }
                self.entriesData = ary
            }
        }
        
        self.totalColection_Lbl.text = CommonDataManager.shared.getNumberWithCommas(cash: Double(self.totalColection_Lbl.text ?? "") ?? 0.0)
        
    }
    private func setupView() {
        view = loadFromNib()
        dictionaries.append(collectionDict)
        print(dictionaries)
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        
//        self.view_container.layer.cornerRadius = 100
//        self.view_container.backgroundColor = .red
//        self.view_container.layer.masksToBounds = true
        
    }
    func updateLegendsList() {
        var data = RoomStatusByDay()
        var list = [RoomACS]()
        let count = entriesData?.count ?? 0
        for i in 0..<count {
            let each = entriesData?[i] ?? FExpensePieChartModel()
            var item = RoomACS()
            item.name = each.accountMasterName
            item.count = each.amount
            item.statusColor = colors[i]
            list.append(item)
        }
        data.list = list
        legendsObj = data
        self.selectedPieChartEntryLabel.text = "\(legendsObj?.list?.first?.name ?? "")-  \(legendsObj?.list?.first?.count ?? "")"
        collectionview_legends.reloadData()
    }
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "CollectionView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
    }
}

extension CollectionView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return legendsObj?.list?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview_legends.dequeueReusableCell(withReuseIdentifier: "ChartLegendsCollectionViewCell", for: indexPath) as! ChartLegendsCollectionViewCell
        cell.backgroundColor = UIColor.yellow
        cell.statusObj = legendsObj?.list?[indexPath.row]
        
        //cell.lbl_statusname.text = legendArrynames[indexPath.row]
        return cell
    }
    
    
}

extension CollectionView: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print("Pie Chharrt Selected Value is:- \(highlight.x)")
        print("Entry Selected calues is \(entry)")
        let selectedEntry = legendsObj?.list?[Int(highlight.x)]
        self.selectedPieChartEntryLabel.text = "\(selectedEntry?.name ?? "")-  \(entry.y)"
    }
}

extension CollectionView: UICollectionViewDelegate {
    
}
