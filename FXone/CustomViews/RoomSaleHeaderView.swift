//
//  RoomSaleHeaderView.swift
//  FXone
//
//  Created by Vinayak on 25/07/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class RoomSaleHeaderView: UIView {

    var view:UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        
    }
    
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "RoomSaleHeaderView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
        
    }

}
