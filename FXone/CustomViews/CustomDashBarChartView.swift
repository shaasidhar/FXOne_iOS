//
//  BarChartView.swift
//  FXone
//
//  Created by Vinayak on 24/08/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class CustomDashBarChartView: UIView {
    
 
    @IBOutlet weak var view_cbcView: CBCview!
    
    @IBOutlet weak var tableview_Legends_List: UITableView!
    let colors = Colors.simplifyRandomColors().shuffled()
    var view:UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
       
    }
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    var chartValues = [BarChartModel]()
    
    //common func to init our view
    private func setupView() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(view)
        
        self.tableview_Legends_List.delegate  = self
        self.tableview_Legends_List.dataSource =  self
        
        tableview_Legends_List.allowsSelection = false
        tableview_Legends_List.register(UINib(nibName: "PieChartLegendLabelsTableViewCell", bundle: nil), forCellReuseIdentifier: "LegendCell")
        
    }
    func loadChartData(list:[BarChartModel]){
        print("chartValues",chartValues)
        chartValues = list
        self.view_cbcView.updateBarChartdata(list: list, colors: colors)
        self.tableview_Legends_List.reloadData()
    }
    
    func loadFromNib() -> UIView{
        let bundle = Bundle.main
        let nib = UINib(nibName: "CustomDashBarChartView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view ?? UIView()
        
    }
}
// Obj to hold bar chart details for fnb and nonfnb
struct BarChartModel :Codable {
    var CYAmount            :Double? // covert this to date
    var LYAmount            :Double?
    var RevenueCodeName     :String?
}
extension CustomDashBarChartView : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chartValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PieChartLegendLabelsTableViewCell = self.tableview_Legends_List.dequeueReusableCell(withIdentifier: "LegendCell") as! PieChartLegendLabelsTableViewCell
        
        cell.lbl_title.text = chartValues[indexPath.row].RevenueCodeName ?? ""
        cell.lbl_title.font = UIFont.systemFont(ofSize: 10)
        cell.lbl_count.font = UIFont.systemFont(ofSize: 10)
        //cell.lbl_count.text = String("\(Int(aryNumberOfPieEntries[indexPath.row].value))k")
        cell.lbl_status.backgroundColor = self.colors[indexPath.row]
        tableview_Legends_List.separatorStyle = .none
        cell.backgroundColor = UIColor.clear

        
        return cell
    }
    
    
}
