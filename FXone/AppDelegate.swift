//
//  AppDelegate.swift
//  FXone
//
//  Created by Vinayak on 4/1/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    let notificationCenter = UNUserNotificationCenter.current()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // undo for new version alert message
//        let updateAvailable = try? isUpdateAvailable()
//        if updateAvailable ?? false {
//
//            let vc = self.window?.rootViewController ?? UIViewController()
//            showAppAlertController(controller: vc, title: "Update", message: "New version available please update")
//            return true
//        }
        
        //**************** push notifications ***************//
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        //Messaging.messaging().delegate = self
        //application.registerForRemoteNotifications()
        
        //For firebase
        FirebaseApp.configure()
        
        // &******************* Local Notifications **********************//
        
        notificationCenter.delegate = self
        
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
        let QA = "qa"
        let DEV = "dev"
        let LIVE = ""
        let STAGING = "staging"
        //URL DataManager
        CommonDataManager.shared.setBaseUrl(env: LIVE, version: "V1.0.0")
        
        let isAutoLogin = UserDefaults.standard.bool(forKey: "ISAUTOLOGIN")
        if isAutoLogin {
          let subscribedProducts = UICoreEngine.sharedInstance.subscribedProducts ?? []
          self.subscribedProductsCode(subscribedProducts: subscribedProducts)
        }else {
          let initial = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let nav = UINavigationController(rootViewController: initial)
            appDelegate.window?.rootViewController = nav
        }
        // Override point for customization after application launch.
        return true
    }
    
    func subscribedProductsCode(subscribedProducts: [SubscribedProducts])  {
        let userDefault = UserDefaults.standard
        let productCode100 = userDefault.object(forKey: "productCode100") as? Int ?? 0
        //userDefault.object(forKey: "productCode100") as? Int ?? 0
        let productCode10010 = userDefault.object(forKey: "productCode10010") as? Int ?? 0
        //userDefault.object(forKey: "productCode10010") as? Int ?? 0
        if productCode100 == 100 && productCode10010 == 10010 {
            self.navigateToHome(navigate: NavigateToHome.RoomOccupancy)
        } else  if productCode100 == 100 {
            self.navigateToHome(navigate: NavigateToHome.RoomOccupancy)
        } else  if productCode10010 == 10010 {
            self.navigateToHome(navigate: NavigateToHome.Finance)
        } else {

        }
        
//        //Hard code for 100
//        if productCode100 == 100 {
//            self.navigateToHome(navigate: NavigateToHome.RoomOccupancy)
//        }
//        //Hard coad For 10010
//        if productCode10010 == 10010 {
//            self.navigateToHome(navigate: NavigateToHome.Finance)
//        }
        
    }
    
    func navigateToHome(navigate: NavigateToHome)  {
        switch navigate {
        case .RoomOccupancy:
            let dash = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Dashboard") as! ViewController
            dash.navigateToHome = navigate
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let nav = UINavigationController(rootViewController: dash)
            appDelegate.window?.rootViewController = nav
        default:
            let financeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FinanceViewController") as! FinanceViewController
            financeVC.navigateToHome = navigate
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let nav = UINavigationController(rootViewController: financeVC)
            appDelegate.window?.rootViewController = nav
        }
    }
    
    func navigateToPropertyVC(totalProperties: [PropertyModel] ,controller: UIView)  {
        let selectHotelsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "selectHotelsVC") as! SelectHotelsPopupVC
        selectHotelsVC.pDelegate = controller as! any PropertyDelegate
        
        selectHotelsVC.totalProperties = totalProperties
        self.window?.rootViewController?.presentPanModal(selectHotelsVC)
    }
    
//    func  showAppAlertController(controller:UIViewController,title: String,message: String){
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alertController.addAction(UIAlertAction(title: "Update", style: .default, handler: { alert in
//
//            guard let url = URL(string: "https://apps.apple.com/gb/app/fx1/id1493938115") else { return }
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }))
//        DispatchQueue.main.async {
//            controller.present(alertController, animated: true)
//        }
//    }

   /* func isUpdateAvailable() throws -> Bool {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "https://itunes.apple.com/lookup?bundleId=\(identifier)") else {
            throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            return version != currentVersion
        }
        throw VersionError.invalidResponse
    }
    */
    /*
     func appUpdateAvailable() -> (Bool,String?) {
         
         guard let info = Bundle.main.infoDictionary,
               let identifier = info["CFBundleIdentifier"] as? String else {
             return (false,nil)
         }
         
      //        let storeInfoURL: String = "http://itunes.apple.com/lookupbundleId=\(identifier)&country=IN"
         let storeInfoURL:String = "https://itunes.apple.com/IN/lookup?
     bundleId=\(identifier)"
         var upgradeAvailable = false
         var versionAvailable = ""
         // Get the main bundle of the app so that we can determine the app's
      version number
         let bundle = Bundle.main
         if let infoDictionary = bundle.infoDictionary {
               // The URL for this app on the iTunes store uses the Apple ID
     for the  This never changes, so it is a constant
             let urlOnAppStore = NSURL(string: storeInfoURL)
             if let dataInJSON = NSData(contentsOf: urlOnAppStore! as URL) {
                 // Try to deserialize the JSON that we got
                 if let dict: NSDictionary = try?
        JSONSerialization.jsonObject(with: dataInJSON as Data, options:
        JSONSerialization.ReadingOptions.allowFragments) as! [String:
          AnyObject] as NSDictionary? {
                     if let results:NSArray = dict["results"] as? NSArray {
                         if let version = (results[0] as! [String:Any]).
            ["version"] as? String {
                             // Get the version number of the current version
              installed on device
                             if let currentVersion =
             infoDictionary["CFBundleShortVersionString"] as? String {
                                 // Check if they are the same. If not, an
               upgrade is available.
                                 print("\(version)")
                                 if version != currentVersion {
                                     upgradeAvailable = true
                                     versionAvailable = version
                                 }
                             }
                         }
                     }
                 }
             }
         }
         return (upgradeAvailable,versionAvailable)
      }



       func checkAppVersion(controller: UIViewController){

     let appVersion = ForceUpdateAppVersion.shared.appUpdateAvailable()

     if appVersion.0 {
         alertController(controller: controller, title: "New Update", message: "New version \(appVersion.1 ?? "") is available")
       }
     }


       func  alertController(controller:UIViewController,title: String,message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
     alertController.addAction(UIAlertAction(title: "Update", style: .default, handler: { alert in
         
         guard let url = URL(string: "itms-apps://itunes.apple.com/app/ewap/id1536714073") else { return }
         if #available(iOS 10.0, *) {
             UIApplication.shared.open(url, options: [:], completionHandler: nil)
         } else {
             UIApplication.shared.openURL(url)
         }
     }))
     DispatchQueue.main.async {
         controller.present(alertController, animated: true)
     }
     
     */
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }



}
@available(iOS 10, *)
//extension AppDelegate : UNUserNotificationCenterDelegate {
//
//    // Receive displayed notifications for iOS 10 devices.
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                willPresent notification: UNNotification,
//                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
//
//        // With swizzling disabled you must let Messaging know about the message, for Analytics
//        // Messaging.messaging().appDidReceiveMessage(userInfo)
//
//        // Print message ID.
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//
//        // Print full message.
//        print(userInfo)
//
//        // Change this to your preferred presentation option
//        completionHandler([])
//    }
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                didReceive response: UNNotificationResponse,
//                                withCompletionHandler completionHandler: @escaping () -> Void) {
//        let userInfo = response.notification.request.content.userInfo
//        // Print message ID.
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//
//        // Print full message.
//        print(userInfo)
//
//        completionHandler()
//    }
//}

//extension AppDelegate: MessagingDelegate {
//
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print("Firebase registration token: \(fcmToken)")
//
//        let dataDict:[String: String] = ["token": fcmToken]
//        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
//        // TODO: If necessary send token to application server.
//        // Note: This callback is fired at each app startup and whenever a new token is generated.
//
//      /*  InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("Error fetching remote instance ID: \(error)")
//            } else if let result = result {
//                print("Remote instance ID token: \(result.token)")
//               // self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
//            }
//        }*/
//    }
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        print("Mesage Data", remoteMessage.appData)
//    }
//}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.identifier == "Local Notification" {
            print("Handling notifications with the Local Notification Identifier")
        }
        
        completionHandler()
    }
    
    func scheduleNotification(notificationType: String) {
        
        let content = UNMutableNotificationContent() // Содержимое уведомления
        let categoryIdentifire = "Delete Notification Type"
        
        content.title = notificationType
        content.body = "This is example notification"
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = categoryIdentifire
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let identifier = "FXOne"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
        let deleteAction = UNNotificationAction(identifier: "DeleteAction", title: "Delete", options: [.destructive])
        let category = UNNotificationCategory(identifier: categoryIdentifire,
                                              actions: [snoozeAction, deleteAction],
                                              intentIdentifiers: [],
                                              options: [])
        
        notificationCenter.setNotificationCategories([category])
    }
}

enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}
