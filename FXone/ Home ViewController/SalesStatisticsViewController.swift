//
//  SalesStatisticsViewController.swift
//  FXone
//
//  Created by Vinayak on 13/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import CalendarDateRangePickerViewController
import Alamofire

class SalesStatisticsViewController: UIViewController {

    @IBOutlet weak var btn_custom: UIButton!
    @IBOutlet weak var view_background: UIView!
    @IBOutlet weak var tableview_report: UITableView!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_currencyCode: UILabel!
    @IBOutlet weak var btn_property: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!
    // Data Members
    //var aryMISR_List = [SalesStatsReportDataModel]()
    var sales_Statistics = SalesStatisticsResponseModel()
    var selectedSegmentIndex = 0
    
    var totalProperties = [PropertyModel]()
    var ispropertySelected : Bool?
    var isGroupPropertySelected : Bool?
    var isCustom : Bool?
    var vc = ViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.prepareJsonData()
        self.tableview_report.isUserInteractionEnabled = false
        self.getSingleProperty()
        //self.segmentTitleConversion()
        
    }
    func getSingleProperty(){
       // loadingView.showLoader(text: "")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        //let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginId":username]//,"PmsCustCode":pmsCode]
        AF.request("\(CommonDataManager.shared.baseUrl)\(SINGLEPROPERTY)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print(response)
                //loadingView.hideLoader()
                do {
                    let response = try JSONDecoder().decode(SinglePropertyModel.self, from: response.data ?? Data())
                    if response.Status == "Success" {
                        if let groupProperties = response.Response {
                            self.parseGroupProperties(list: [groupProperties])//comment : single group to ary of group
                        }
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                loadingView.hideLoader()
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    func parseGroupProperties(list:[PropertyResponseObj]?){
        
        for each in list ?? [] {
            
            var property1 = PropertyModel()
            property1.title = each.GroupDetails?.GroupName ?? ""
            //In case all properties selected (All properties in the group)
            if isGroupPropertySelected ?? false {
                 property1.selected = true
            }
            else {
                property1.selected = false
            }
            // show Group default selected in check box
            
            var ary = [Property1Model]()
            for property in each.PropertyDetails ?? [] {
                var p = Property1Model()
                p.title = property.PropertyName
                p.titlePms = property.PmsCustCode
                
                if ispropertySelected ?? false {
                   p.selected = true
                }
                else {
                    p.selected = false

                }
                
                //In case few property OR properties selected in the gourp
                 // show Property default selected in check box// here is the line show privious selectoion
                ary.append(p)
            }
            property1.property = ary
            totalProperties.append(property1)
            
            //totalProperties.append(contentsOf: totalProperties)
        }
    }
    func prepareJsonData() {
        loadingView.showLoader(text: "")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginID":username,"PmsCustCode":pmsCode,"FromDate": "",
                                "ToDate": ""]
               //print()
               AF.request("\(CommonDataManager.shared.baseUrl)\(SALESSTATSREPORT)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                   switch response.result {
                   case .success:
                       print(response)
                       self.isCustom = false
                       loadingView.hideLoader()
                       do {
                           let response = try JSONDecoder().decode(SalesStatisticsModel.self, from: response.data ?? Data())
                           if response.Status == "Success" {
                            self.sales_Statistics = response.Response ?? SalesStatisticsResponseModel()
                            self.lbl_currencyCode.text = response.Response?.CurrencyCode ?? ""
                            self.tableview_report.reloadData()
                           }
                           else{
                               UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                           }
                       }
                       catch let error {
                           #if DEBUG
                           UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                           #endif
                       }
                   case .failure(let error):
                       print(error.localizedDescription)
                       loadingView.hideLoader()
                       
                       //UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                       //creating No Data label
                       let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
                       label.center = self.tableview_report.center
                       label.textAlignment = .center
                       label.text = "No Data"
                       self.view.addSubview(label)
                   }
               }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
    }
    func setupUI(){
        self.btn_custom.layer.borderWidth = 1
        self.btn_custom.layer.cornerRadius = 5
        self.btn_custom.layer.borderColor = UIColor.hexStringToUIColor(hex: "FF8E00").cgColor
        self.btn_custom.layer.masksToBounds = true
        self.tableview_report.tableFooterView = UIView.init()
        self.view_background.layer.cornerRadius = 10.0
        self.view_background.layer.masksToBounds = true
        self.title = "Sales Statistics"
        self.lbl_title.text = "Sales Statistics"
        tableview_report.backgroundColor = UIColor.clear
        tableview_report.register(UINib(nibName: "SSTableViewCell", bundle: nil), forCellReuseIdentifier: "SSTableViewCell")
    }
    @IBAction func segment(_ sender: Any) {
        self.btn_custom.setTitle("Custom", for: .normal)
        self.btn_custom.backgroundColor = UIColor.white
        self.btn_custom.setTitleColor(UIColor.hexStringToUIColor(hex: "FC7A45"), for: .normal)
        self.btn_custom.layer.borderWidth = 1
        self.btn_custom.layer.borderColor = UIColor.hexStringToUIColor(hex: "FC7A45").cgColor
        self.btn_custom.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.segment.tintColor = UIColor.hexStringToUIColor(hex: "FC7A45")
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
    }
    func segmentTitleConversion(){
        let date = vc.dAcDate.getDateFromString(isoDate: vc.dAcDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMMyy"
        let acdate = formatter.string(from: date)
        let defaultAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
        segment.setTitleTextAttributes(defaultAttributes, for: .normal)
        
        let largerFontAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 9)]
        segment.setTitleTextAttributes(largerFontAttributes, for: .normal)
            
        segment.setTitle(acdate, forSegmentAt: 0)
    }
    @IBAction func showCustomCalendar(_ sender: Any) {
        let accountDate = UserDefaults.standard.object(forKey: "Account_Date") as?  Date ?? Date ()
        let dateRangePickerViewController = CalendarDatePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        dateRangePickerViewController.minimumDate = Calendar.current.date(byAdding: .year, value: -1, to: accountDate)
        dateRangePickerViewController.maximumDate = accountDate//Date() //Calendar.current.date(byAdding: .year, value: 10, to: Date())
        dateRangePickerViewController.selectedStartDate = Calendar.current.date(byAdding: .day, value: -7, to: accountDate)
        dateRangePickerViewController.selectedEndDate = accountDate
        
        let sectionsCount = dateRangePickerViewController.numberOfSections(in: dateRangePickerViewController.collectionView!)
        let lastItemIndex = IndexPath.init(row: 0, section: sectionsCount - 1)
        dateRangePickerViewController.collectionView?.scrollToItem(at: lastItemIndex, at: UICollectionView.ScrollPosition.top, animated: false)
        
        self.navigationController?.present(navigationController, animated: true, completion: nil)
        
//        let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
//        dateRangePickerViewController.delegate = self
//        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
//        dateRangePickerViewController.minimumDate = Date()
//        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 10, to: Date())
//        dateRangePickerViewController.selectedStartDate = Date()
//
//        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    @IBAction func btn_property(_ sender: Any) {
        
        let selectHotelsVC = self.storyboard?.instantiateViewController(withIdentifier: "selectHotelsVC") as! SelectHotelsPopupVC
        selectHotelsVC.pDelegate = self as? PropertyDelegate
        print("self.totalProperties is ======>>>>>",self.totalProperties)
        selectHotelsVC.totalProperties = self.totalProperties
        presentPanModal(selectHotelsVC)
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        if #available(iOS 13.0, *) {
            segment.selectedSegmentTintColor = UIColor.hexStringToUIColor(hex: "FC7A45")
        } else {
            // Fallback on earlier versions
        }
        if sender.selectedSegmentIndex != self.selectedSegmentIndex {
            self.selectedSegmentIndex = sender.selectedSegmentIndex
            //tableview_report.reloadData()
            self.prepareJsonData()
        }
        
    }
    
    @IBAction func property(_ sender: Any) {
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func customSelected(_ sender: Any) {
    }
    
    @IBAction func switchSelected(_ sender: Any) {
    }
}

extension SalesStatisticsViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("numberOfRowsInSection",self.sales_Statistics.Responce?.Day?.count ?? 0)
        if isCustom ?? false {
            return self.sales_Statistics.Responce?.Custom?.count ?? 0
        }else{
            switch self.selectedSegmentIndex {
            case 0:
                return self.sales_Statistics.Responce?.Day?.count ?? 0
            case 1:
                return self.sales_Statistics.Responce?.Month?.count ?? 0
            case 2:
                return self.sales_Statistics.Responce?.Year?.count ?? 0
            default:
                return self.sales_Statistics.Responce?.Day?.count ?? 0
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SSTableViewCell", for: indexPath as IndexPath) as! SSTableViewCell
        cell.view_ssViewLabels.backgroundColor = UIColor.red
        cell.view_ssViewLabels.view.backgroundColor = UIColor.white
        print(self.selectedSegmentIndex)
        
        if self.isCustom ?? false {
            cell.selectedSegmentIndex = 3
            self.isCustom = false
        }else {
            cell.selectedSegmentIndex = self.selectedSegmentIndex
        }
        
        cell.statsReport = self.sales_Statistics.Responce
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SSHeaderView.init(frame: CGRect(x: 0, y: 0, width: self.tableview_report.frame.width, height: 60))
        return view
    }
}
extension SalesStatisticsViewController:CalendarDatePickerViewControllerDelegate{
    func didTapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        applyCoustomeDateWithRange(startDate: startDate, endDate: endDate)
        //print("Selected Date Range : \(startDate)-\(endDate)")
        let str = "\(startDate.toString())-\(endDate.toString())"
        btn_custom.setTitle(str, for: .normal)
        btn_custom.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        btn_custom.titleLabel?.numberOfLines = 2
        btn_custom.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
        btn_custom.titleLabel?.textAlignment = .center
        //custom_DateSelector.titleLabel?.textColor = UIColor.white
        btn_custom.setTitleColor(.white, for: .normal)
        self.dismiss(animated: true, completion: nil)
        self.segment.tintColor = UIColor.lightGray
    }
    func applyCoustomeDateWithRange(startDate: Date!, endDate: Date!) {
        if #available(iOS 13.0, *) {
            segment.selectedSegmentTintColor = .gray
        } else {
            // Fallback on earlier versions
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let startacDate = formatter.string(from: startDate)
        let endacDate = formatter.string(from: endDate)
        print(endacDate)
        print(startacDate)
        loadingView.showLoader(text: "")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginID":username,"PmsCustCode":pmsCode,"FromDate": startacDate,
                         "ToDate": endacDate, "IsCustom": 1] as [String : Any]
        //Alamofire.request("\(CommonDataManager.shared.baseUrl)\(SALESSTATSREPORT)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: dic_headers).responseJSON
        AF.request("\(CommonDataManager.shared.baseUrl)\(SALESSTATSREPORT)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON {
            (response) in
            switch response.result {
                case .success:
                    print(response)
                    self.isCustom = true
                    loadingView.hideLoader()
                    do {
                        let response = try JSONDecoder().decode(SalesStatisticsModel.self, from: response.data ?? Data())
                        if response.Status == "Success" {
                            self.sales_Statistics = response.Response ?? SalesStatisticsResponseModel()
                            self.lbl_currencyCode.text = response.Response?.CurrencyCode ?? ""
                            self.tableview_report.reloadData()
                           }
                           else{
                               UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                           }
                       }
                       catch let error {
                           #if DEBUG
                           UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                           #endif
                       }
                   case .failure(let error):
                       print(error.localizedDescription)
                       loadingView.hideLoader()
                       
                       //creating No Data label
                       let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
                       label.center = self.tableview_report.center
                       label.textAlignment = .center
                       label.text = "No Data"
                       self.view.addSubview(label)
                   }
               }
    }
    
}
