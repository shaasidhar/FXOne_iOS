
//
//  SelectHotelsPopupVC.swift
//  FXone
//
//  Created by Vinayak on 09/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import PanModal
import Alamofire

struct PropertyModel :Decodable {
    var title:String?
    var titlePms:Int?
    var selected:Bool?
    var property:[Property1Model]?
}
struct Property1Model :Decodable {
    var title:String?
    var titlePms:Int?
    var selected:Bool?
}

protocol PropertyDelegate : class {
    func selectedProperties(list:[PropertyModel])
}
class SelectHotelsPopupVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var btn_Apply: UIButton!
    @IBOutlet weak var tableview_selection: UITableView!
    
    var selectHotelProperty = [String]()
    var pDelegate :PropertyDelegate?
    var totalProperties = [PropertyModel]()
    var gruopSelected  = false
    var cellSelected = false
    var selectedIndex : Int?
    var selectedSection : Int?
    var sidemenuView = SideMenuView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview_selection.delegate = self
        tableview_selection.dataSource = self
        tableview_selection.allowsMultipleSelection = false
        print("totalProperties------->>>>>>",totalProperties)
       // self.btn_Apply.isUserInteractionEnabled = false
       // self.btn_Apply.backgroundColor = .darkGray
        tableview_selection.register(UINib(nibName: "PropertyTableViewCell", bundle: nil), forCellReuseIdentifier: "PropertyTableViewCell")
    }
    
    
    func setUI ()
    {
        //self.backgroundView.backgroundColor = UIColor.red
    }
    
    @IBAction func btn_clickApply(_ sender: Any) {
        // update properties selected flag
//        for i in 0..<totalProperties.count {
//            let count =  totalProperties[i].property?.count ?? 0
//            let slectedRow = (selectedIndex ?? 1)-1
//
//            if selectedSection == i {
//                print ("section")
//                print (i)
//                for j in 0..<count {
//                    // for selected row(property) make selected=true and false for others
//                    if slectedRow == j {
//                         totalProperties[i].property?[j].selected = true
//                    }else {
//                         totalProperties[i].property?[j].selected = false
//                    }
//                }
//               // totalProperties[i].selected = true
//            }
//            else {
//                // as this section is not selected, make all rows(properties) in this section as false
//                // also make this section(property-group) as false
//                for j in 0..<count {
//                    totalProperties[i].property?[j].selected = false
//                }
//                totalProperties[i].selected = false
//            }
//        }
        
        print(totalProperties)
        self.pDelegate?.selectedProperties(list:totalProperties )
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func buttonCloseAction(_ sender: Any){
    
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func selectedItem(_ sender : UIButton) {
        let textfieldPosi : CGRect = sender.convert(sender.bounds, to: self.tableview_selection)
        let indexPath = self.tableview_selection.indexPathForRow(at: textfieldPosi.origin)
        print(indexPath?.row ?? 0)
        let indexPth = IndexPath(row: indexPath?.row ?? 0, section: indexPath?.section ?? 0)
        self.tableview_selection.selectRow(at: indexPth, animated: true, scrollPosition: UITableView.ScrollPosition(rawValue: 0)!)
        self.tableview_selection.delegate?.tableView!(self.tableview_selection, didSelectRowAt: indexPth)
/*
        if sender.isSelected  {
            sender.isSelected = false
            self.cellSelected = true
            self.btn_Apply.isUserInteractionEnabled = false
            self.btn_Apply.backgroundColor = .darkGray
            
        }
        else {
            sender.isSelected = true
            self.cellSelected = false
            self.btn_Apply.isUserInteractionEnabled = true
            self.btn_Apply.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
        }
        
        if sender.tag == 100 {
        
            let index = (indexPath?.row ?? 0 ) - 1
            totalProperties[indexPath?.section ?? 0].property?[index].selected = sender.isSelected
            for i in 0..<totalProperties.count {
                
                totalProperties[i].selected = false
            }
            
        }
        else if sender.tag == 101 {

            for i in 0..<totalProperties.count {
                
                if indexPath?.section == i {
                    
                    let ary = totalProperties[i].property
                    
                    var temp = [Property1Model]()
                    let count = ary?.count ?? 0
                    for i in 0..<count {
                        var each  = ary?[i]
                        each?.selected = sender.isSelected
                        temp.append(each ?? Property1Model())
                    }
                    //totalProperties.append(totalProperties[0])
                    totalProperties[i].property = temp
                    totalProperties[i].selected = sender.isSelected
                }
                else {
                
                    if self.cellSelected {
                        let ary = totalProperties[i].property
                        var temp = [Property1Model]()
                        let count = ary?.count ?? 0
                        for i in 0..<count {
                            var each  = ary?[i]
                            each?.selected = false
                            temp.append(each ?? Property1Model())
                        }
                        totalProperties[i].property = temp
                        totalProperties[i].selected = false
                    }
                }
            }
            self.cellSelected = false
        }
        if totalProperties.count > 0 {
             //self.btn_Apply.isUserInteractionEnabled = false
             //self.btn_Apply.backgroundColor = .darkGray
        }
        tableview_selection.reloadData() */
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.totalProperties.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (totalProperties[section].property?.count ?? 0 ) + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dataIndex = indexPath.row - 1
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PropertyTableViewCell", for: indexPath) as! PropertyTableViewCell
        cell.view_selectionview.btn_selection.isUserInteractionEnabled  = true
        cell.view_selectionview.btn_selection.addTarget(self, action: #selector(self.selectedItem(_:)), for:.touchUpInside)
        
        var isSelected = false
        if indexPath.row == 0 {
        
            cell.view_selectionview.lbl_title.text = totalProperties[indexPath.section].title ?? ""
            cell.view_selectionview.lbl_title.font = UIFont.boldSystemFont(ofSize: 15)
            isSelected = totalProperties[indexPath.section].selected ?? false
            cell.view_selectionview.hSpaceConstraint.constant = 20
            cell.view_selectionview.btn_selection.tag = 101
        }
        else {
            cell.view_selectionview.lbl_title.text = totalProperties[indexPath.section].property?[dataIndex].title ?? ""
            UserDefaults.standard.set(totalProperties[indexPath.section].property?[dataIndex].title ?? "", forKey: "Hotel_Name")
            cell.view_selectionview.lbl_title.font = UIFont.systemFont(ofSize: 14)
            isSelected = totalProperties[indexPath.section].property?[dataIndex].selected ?? false
            cell.view_selectionview.hSpaceConstraint.constant = 50
            cell.view_selectionview.btn_selection.tag = 100
        }

        
        if isSelected {
            cell.view_selectionview.btn_selection.isSelected = true
            //cell.view_selectionview.btn_selection.setImage(UIImage.init(named: "Select"), for: .selected)
        }
        else {
            cell.view_selectionview.btn_selection.isSelected = false
            //cell.view_selectionview.btn_selection.setImage(UIImage.init(named: "Deselect"), for: .normal)

        }
        
        
        //cell.view_selectionview.btn_selection.isSelected = isSelected
//        if selectedIndex == indexPath.row {
//            cell.view_selectionview.btn_selection.isSelected = true
//        }
//        else {
//            cell.view_selectionview.btn_selection.isSelected = false
//
//        }
        //group selection deselect
        /*
        if indexPath.row == 0{
            cell.view_selectionview.btn_selection.isSelected = false
            cell.isUserInteractionEnabled = false
        }
        if indexPath.row == 0{
            cell.view_selectionview.btn_selection.isSelected = false
        }
        */
        return cell
        
    }
    
    //select and deselect cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        print("row",selectedIndex)
        selectedSection = indexPath.section
//        for i in 0..<totalProperties.count {
//            let count =  totalProperties[i].property?.count ?? 0
//            let slectedRow = (selectedIndex ?? 1)-1
//
//            if selectedSection == i {
//                print ("section")
//                print (i)
//                for j in 0..<count {
//                    // for selected row(property) make selected=true and false for others
//                    if slectedRow == j {
//                         totalProperties[i].property?[j].selected = true
//                    }else {
//                         totalProperties[i].property?[j].selected = false
//                    }
//                }
//                totalProperties[i].selected = true
//            }
//            else {
//                // as this section is not selected, make all rows(properties) in this section as false
//                // also make this section(property-group) as false
//                for j in 0..<count {
//                    totalProperties[i].property?[j].selected = false
//                }
//                totalProperties[i].selected = false
//            }
//        }
//        if totalProperties[indexPath.section].property?[indexPath.row - 1].selected == true {
//            totalProperties[indexPath.section].property?[indexPath.row - 1].selected = false
//        } else {
//            totalProperties[indexPath.section].property?[indexPath.row - 1].selected = true
//        }
        
        if totalProperties[indexPath.section].selected == true {
            totalProperties[indexPath.section].selected = false
        } else {
            totalProperties[indexPath.section].selected = true
        }
        
        tableview_selection.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        //cell.view_selectionview.btn_selection.isSelected = false
       //
        
//        let cell:PropertyTableViewCell = tableView.cellForRow(at: indexPath) as! PropertyTableViewCell
//        cell.view_selectionview.btn_selection.isSelected = false
        //tableview_selection.reloadData()

    }
}

extension SelectHotelsPopupVC : PanModalPresentable {
    var panScrollable: UIScrollView? {
        
        return nil
    }
    var shortFormHeight: PanModalHeight {
        
        return .contentHeight(800)
    }
    var longFormHeight: PanModalHeight {
        
        return .maxHeightWithTopInset(50)
    }
}
