//
//  RoomRatePopupVC.swift
//  FXone
//
//  Created by Vinayak on 9/27/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import PanModal
import SpreadsheetView
import Alamofire


class RoomRatePopupVC: UIViewController, SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    @IBOutlet weak var spreadsheetView: SpreadsheetView!
    @IBOutlet weak var view_background: UIView!
    @IBOutlet weak var lbl_roomrate: UIView!
    @IBOutlet weak var lbl_dateRange: UIView!
    @IBOutlet weak var lbl_toDate_fromDate: UILabel!
    
    var header = [String]()
    var data = RoomRateByDateDetailModel()
    let roomTypeWidth: CGFloat = 80
    var columnWidth:CGFloat = 50.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        columnWidth = (UIScreen.main.bounds.width - roomTypeWidth - 5)/7
        spreadsheetView.dataSource = self
        spreadsheetView.delegate = self
        spreadsheetView.register(HeaderCell.self, forCellWithReuseIdentifier: String(describing: HeaderCell.self))
        spreadsheetView.register(TextCell.self, forCellWithReuseIdentifier: String(describing: TextCell.self))
        getRoomRateData()
    }
        //let username = "nitish.kumar@idsnext.com"
        //let token = "-0Y_p7kkXN3T1ftoT4beGHEEzPXWWIq6tK3f6UaHdFIO6IelMkUL0bAgofVZSeSrjM0NYZ3VB9llaMs4DNqeXtVWvsW5IC-6undmPNLG8TQgEVPUaxDUSyiHIr-gzV4c74zOj1Y81AxR-byfM-DS8RoweMp1TV-p1PvT2yeEv7byoOvIDl6hixosfsFwhXS_ZmOMT75QFV-0YHxO3m9mBy5VZr7h14T-nQTHgU8HrRn_vS9Etph1JracmzVB21peSIMkun7NTq9k_YGABImG6parkrCCzTWe1uV50Zf6_mA28vrK7OVJVH05hg7XtCsFoAXW6OyW__cFO9mH9xDjTodJtIxKTluRYXFHDycAYutuQbwk6o4h5D1bXWiEWXX_0Qc46Q2WqFdGr47ytJeo9kh7Br6McL0usHT9g4pASgn1KlRVj6IfuSnn2vJ38f8PvutoXFH_sOHXkUsRCNrecGLWZ1rO0VFEpadIU55ViGxpEx863K6UwXlIK6AE-CZV"
    /*func getFandBDetailsData(){
     loadingView.showLoader(text: "")
     let username = UserDefaults.standard.object(forKey: "User_Id") as! String
     let token = UserDefaults.standard.object(forKey: "Token") as! String
     let dic_headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
     let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
     let dicParams = ["LoginID":username,"PmsCustCode":pmsCode,"FromDate": "","ToDate": ""]
     Alamofire.request("\(CommonDataManager.shared.baseUrl)\(FANDBDETAILS)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: dic_headers).responseJSON { (response) in
     switch response.result {
     case .success:
     print(response)
     loadingView.hideLoader()
     do {
     let results = try JSONDecoder().decode(FANBDetailModel.self, from: response.data ?? Data())
     if results.Status == "Success" {
     self.aryFABlist = results.Response?.FNBSalesData?.FNBSalesData ?? []
     CommonDataManager.shared.ary_FandBDetailsList = self.aryFABlist
     self.parseTabsData()
     }
     else{
     UICoreEngine.sharedInstance.ShowAlertOnWindows(message: results.StatusDescription ?? "", title: "")
     }
     }
     catch let error {
     #if DEBUG
     UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
     #endif
     }
     case .failure(let error):
     print(error.localizedDescription)
     UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
     }
     }
     }*/
    func getRoomRateData() {
        loadingView.showLoader(text: "")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginId":username,"PMSCustCode":pmsCode]
        
        AF.request("\(CommonDataManager.shared.baseUrl)\(ROOMRATE)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print(response)
                loadingView.hideLoader()
                do {
                    self.data = try JSONDecoder().decode(RoomRateByDateDetailModel.self, from: response.data ?? Data())
                    if self.data.Status == "Success" {
                       // print(self.data.Response?.responce)
                        // get from & to dates from response
                        var fromDate : Date = Date()
                        if let respFromDate = self.data.Response?.FromDate {
                            fromDate = CommonDataManager.shared.parseDate(dateString: respFromDate) ?? Date()
                        }
                        var toDate : Date = Date()
                        if let respToDate = self.data.Response?.ToDate {
                            toDate = CommonDataManager.shared.parseDate(dateString: respToDate) ?? Date()
                        }
                        self.populateDateRange(toDate: toDate, fromDate: fromDate)
                        self.header = ["Room Type"]
                        // Iterate by 1 day
                        // Feel free to change this variable to iterate by week, month etc.
                        /*let dayDurationInSeconds: TimeInterval = 60*60*24
                        for date in stride(from: fromDate, to: toDate.addingTimeInterval(dayDurationInSeconds), by: dayDurationInSeconds) {
                            self.header.append(self.getTheDateStringFor(date: date))
                        }*/
                        let roomTypeRows = self.data.Response?.responce?[0].RateDetail ?? []
                        for each in roomTypeRows {
                            let dateString = each.RateDate ?? ""
                            let d1aray = dateString.components(separatedBy: "T")
                            let d1 = d1aray[0]
                            let date1 = d1.stringToDate(dateString: d1)
                            let ac = self.getTheDateStringFor(date: date1)
                            
                            self.header.append(ac)
                        }
                        self.spreadsheetView.reloadData()
                    }
                    else {
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: self.data.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                loadingView.hideLoader()
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    
    @IBAction func btn_ok(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func populateDateRange(toDate: Date, fromDate: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.lbl_toDate_fromDate.text = "("+dateFormatter.string(from: fromDate) + " to " + dateFormatter.string(from: toDate)+")"
        self.lbl_toDate_fromDate.font = UIFont.FXRobotoRegular(size: 12)
    }
    
    func getTheDateStringFor(date: Date) -> String {
        //let date = Date.init(timeIntervalSinceNow: TimeInterval(60 * 60 * 24 * index))
        let calendar = Calendar.current
        // choose which date and time components are needed
        let requestedComponents: Set<Calendar.Component> = [
            .day,
            .weekday
        ]
        let components = calendar.dateComponents(requestedComponents, from: date)
        let day = components.day
        let weekDay = components.weekday
        var weekDayName = ""
        
        switch weekDay {
        case 1:
            weekDayName = "Sun"
        case 2:
            weekDayName = "Mon"
        case 3:
            weekDayName = "Tue"
        case 4:
            weekDayName = "Wed"
        case 5:
            weekDayName = "Thu"
        case 6:
            weekDayName = "Fri"
        case 7:
            weekDayName = "Sat"
        default:
            weekDayName = ""
        }
        let resultString = "\(day ?? 0) \n \(weekDayName)"
        return resultString
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        spreadsheetView.flashScrollIndicators()
    }
    
    // MARK: DataSource
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return header.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return (data.Response?.responce?.count ?? 0) + 1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return roomTypeWidth
        }
        return columnWidth
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        return columnWidth
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return (data.Response?.responce?.count ?? 0) + 1
    }
    
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return header.count
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        //print("row: \(indexPath.row) -->column : \(indexPath.column)" )
        if case 0 = indexPath.row {
            let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderCell.self), for: indexPath) as! HeaderCell
            
            cell.label.text = header[indexPath.column]
            cell.setNeedsLayout()
            cell.gridlines.top = .solid(width: 1, color: UIColor.lightGray)
            cell.gridlines.left = .none
            cell.gridlines.right = .solid(width: 1, color: UIColor.lightGray)
            cell.gridlines.bottom = .none
            
            return cell
        } else {
            
            let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: TextCell.self), for: indexPath) as! TextCell
            cell.label.text = ""
            let roomTypeRows = self.data.Response?.responce ?? []
            
            if indexPath.column == 0 {
                cell.label.font = UIFont.FXRobotoBlackBold(size: 9)
                cell.label.text = roomTypeRows[indexPath.row - 1].RoomTypeName ?? ""
            } else {
                cell.label.font = UIFont.FXRobotoRegular(size: 9)
                if roomTypeRows.count != 0 {
                    let roomRateList = roomTypeRows[indexPath.row - 1].RateDetail ?? []
                    
                    if roomRateList.count != 0 {
                        if let _ = roomRateList[exist: indexPath.column - 1] {
                            cell.label.text = "\(roomRateList[indexPath.column - 1].Rate ?? 0.0)"
                        }
                        
                    }
                }
            }
            
            cell.gridlines.top = .none
            cell.gridlines.left = .none
            cell.gridlines.right = .solid(width: 1, color: UIColor.lightGray)
            cell.gridlines.bottom = .solid(width: 1, color: UIColor.lightGray)
            return cell
            
        }
    }
    
    /// Delegate
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        self.spreadsheetView.deselectItem(at: indexPath, animated: false)
    }
}

extension Collection where Indices.Iterator.Element == Index {
    subscript (exist index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
} 
extension RoomRatePopupVC : PanModalPresentable {
    var panScrollable: UIScrollView? {
        return nil
    }
    
    var shortFormHeight: PanModalHeight {
        
        return .contentHeight(300)
    }
    
    var longFormHeight: PanModalHeight {
        
        return .maxHeightWithTopInset(50)
    }
}
extension Date: Strideable {
    public func distance(to other: Date) -> TimeInterval {
        return other.timeIntervalSinceReferenceDate - self.timeIntervalSinceReferenceDate
    }
    
    public func advanced(by n: TimeInterval) -> Date {
        return self + n
    }
}


