//
//  MISReportViewController.swift
//  FXone
//
//  Created by Vinayak on 12/09/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import CalendarDateRangePickerViewController
import Alamofire

class MISReportViewController: UIViewController {
    // UI Members
        
    @IBOutlet weak var buttonsSegment: UISegmentedControl!
    @IBOutlet weak var btn_custom: UIButton!
    @IBOutlet weak var view_background: UIView!
    @IBOutlet weak var tableview_report: UITableView!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var btn_property: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!
    var selectedBottomSegmentIndex = 0
    var selectedRevenueSegmentIndex = 0
    // Data Members
    var aryMISR_List = [MISReportModel]()
    var selectHotelsVC: SelectHotelsPopupVC?
    
    var dicAlltabs = [String:AnyObject]()
    var mISReportData = MISReportDataModel()
    var islastYearEnabled = false
    
    var totalProperties = [PropertyModel]()
    var ispropertySelected : Bool?
    var isGroupPropertySelected : Bool?
    var vc = ViewController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.getMISToken()
        //self.prepareJsonData()
        self.getSingleProperty()
        
        self.segmentTitleConversion()
        
    }
    
    @IBAction func bottonSegement(_ sender: UISegmentedControl) {
                
        self.lbl_subtitle.text = sender.selectedSegmentIndex == 0 ? "Revenue Groups" : sender.selectedSegmentIndex == 1 ? "Outlet Sales" : "Room Statistics"
        selectedBottomSegmentIndex = sender.selectedSegmentIndex
        self.updateDataBySelectedIndex(index: sender.selectedSegmentIndex)

    }
    func segmentTitleConversion(){
        let date = vc.dAcDate.getDateFromString(isoDate: vc.dAcDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMMyy"
        let acdate = formatter.string(from: date)
        let defaultAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
        segment.setTitleTextAttributes(defaultAttributes, for: .normal)
        
        let largerFontAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 9)]
        segment.setTitleTextAttributes(largerFontAttributes, for: .normal)
            
        segment.setTitle(acdate, forSegmentAt: 0)
    }
    
    func updateDataBySelectedIndex(index:Int){
        aryMISR_List.removeAll()
        if index == 0 {
            //print(mISReportData.Response?.Day?.FoodAndBevarage ?? FoodandBeverageModel?[] )
            let fb = selectedRevenueSegmentIndex == 0 ? mISReportData.Response?.Day?.FoodAndBevarage ?? [] : selectedRevenueSegmentIndex == 1 ? mISReportData.Response?.Month?.FoodAndBevarage ?? [] : mISReportData.Response?.Year?.FoodAndBevarage ?? []
                for each in fb {
                    var item = MISReportModel()
                    print("here MISReportModel",item )
                    item.name = each.Description  ?? ""
                    item.acyValue = "\(each.NetAmount ?? 0)"
                    item.bcyValue = "\(each.Budget ?? 0)"
                    item.actualLY = "\(each.LastyearDay ?? 0)"
                    aryMISR_List.append(item)
                }
            let fb1 = selectedRevenueSegmentIndex == 0 ? mISReportData.Response?.Day?.OtherSales ?? [] : selectedRevenueSegmentIndex == 1 ? mISReportData.Response?.Month?.OtherSales ?? [] : mISReportData.Response?.Year?.OtherSales ?? []
            for each in fb1 {
                var item = MISReportModel()
                print("here MISReportModel",item )
                item.name = each.Description  ?? ""
                item.acyValue = "\(each.NetAmount ?? 0)"
                item.bcyValue = "\(each.Budget ?? 0)"
                item.actualLY = "\(each.LastyearDay ?? 0)"
                aryMISR_List.append(item)
            }
            let fb2 = selectedRevenueSegmentIndex == 0 ? mISReportData.Response?.Day?.Rate ?? [] : selectedRevenueSegmentIndex == 1 ? mISReportData.Response?.Month?.Rate ?? [] : mISReportData.Response?.Year?.Rate ?? []
            for each in fb2 {
                var item = MISReportModel()
                print("here MISReportModel",item )
                item.name = each.Description  ?? ""
                item.acyValue = "\(each.NetAmount ?? 0)"
                item.bcyValue = "\(each.Budget ?? 0)"
                item.actualLY = "\(each.LastyearDay ?? 0)"
                aryMISR_List.append(item)
            }
            
            tableview_report.reloadData()
            
        }
        else if index == 1 {
            
            let fb1 = selectedRevenueSegmentIndex == 0 ? mISReportData.Response?.Day?.FoodAndBevarage ?? [] : selectedRevenueSegmentIndex == 1 ? mISReportData.Response?.Month?.FoodAndBevarage ?? [] : mISReportData.Response?.Year?.FoodAndBevarage ?? []
                        
                        for each in fb1 {
                            var item = MISReportModel()
                            
                            if  each.RevenueCode == "104" || each.RevenueCode == "105" || each.RevenueCode == "106" || each.RevenueCode == "107" {
                                print("Dont add these values")
                            }else if  each.RevenueCode != "104" || each.RevenueCode != "105" || each.RevenueCode != "106" || each.RevenueCode != "107" {
                                print(" add these values")
                                item.name = each.Description  ?? ""
                                item.acyValue = "\(each.NetAmount ?? 0)"
                                item.bcyValue = "\(each.Budget ?? 0)"
                                item.actualLY = "\(each.LastyearDay ?? 0)"
                                aryMISR_List.append(item)
                            }
                            
                        }
            
            tableview_report.reloadData()
            
        }
        else if index == 2 {
            
            let fb = selectedRevenueSegmentIndex == 0 ? mISReportData.Response?.Day?.OccupancyDetail ?? MISOccupancyDetailModel() : selectedRevenueSegmentIndex == 1 ? mISReportData.Response?.Month?.OccupancyDetail ?? MISOccupancyDetailModel() : mISReportData.Response?.Year?.OccupancyDetail ?? MISOccupancyDetailModel()
            
            let lastYearfb = selectedRevenueSegmentIndex == 0 ? mISReportData.Response?.Day?.LastYearOccupancyDetail ?? MISLYOccupancyDetailModel()  : selectedRevenueSegmentIndex == 1  ? mISReportData.Response?.Month?.LastYearOccupancyDetail ?? MISLYOccupancyDetailModel() : mISReportData.Response?.Year?.LastYearOccupancyDetail ?? MISLYOccupancyDetailModel()
            
            var dic = [String:Any]()
            do {
                dic = try (fb.allProperties() )
            }catch let error {
                print(error.localizedDescription)
            }
                
            var lastYeardic = [String:Any]()
                do {
                    lastYeardic = try (lastYearfb.allProperties() ?? ["":""] )
                }catch let error {
                    print(error.localizedDescription)
                }
            
            for (key,value) in dic {
                
                var item = MISReportModel()
                item.name = key
                
                if let thing = value as? Int  {
                    item.acyValue = "\(thing)"
                }
                if let thing = value as? Double {
                    item.acyValue = "\(thing)"
                }
                print("Key:\(key) ---->>>>Value:\(value)")
                
                item.bcyValue = "0"
                
                for (key, value) in lastYeardic {
                    
                    if let thing = value as? Int  {
                        item.actualLY = "\(thing)"
                        //print("Key:\(key) ---->>>>Value:\(value)")
                        //print("Value",thing)
                    }
//                    if let thing = value as? Double {
//                        item.actualLY = "\(thing)"
//                        //print("Value",thing)
//                    }
                    print("Dictionary Keys:\(key) ---->>>>Values:\(value)")
                }
                aryMISR_List.append(item)
            }
           // }
            tableview_report.reloadData()
            
        }
    }
    
    func getMISToken()  {
        loadingView.showLoader(text: "")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let password = UserDefaults.standard.object(forKey: "Password") as! String
        let dic_params = ["userid":username,"password":password,"productcode":"100"]
        var headers: HTTPHeaders = [:]
        headers = ["Content-Type":"application/json"]
        print("URL:", "\(CommonDataManager.shared.baseUrl1)\(GET_MISTOKEN)")
        print(dic_params, username , password)
        AF.request("\(CommonDataManager.shared.baseUrl1)\(GET_MISTOKEN)", method:.post, parameters:dic_params ,encoding: JSONEncoding.default, headers:headers ).responseJSON { (response) in
            
            switch response.result {
            case .success:
                print("URL:", "\(CommonDataManager.shared.baseUrl1)\(GET_MISTOKEN)")
                print(response)
                loadingView.hideLoader()
                do {
                    let response = try JSONDecoder().decode(MISTokenModel.self, from: response.data ?? Data())
                    if response.Status == "Success" {
                        
                        let token = response.Response ?? ""
                        
                        UserDefaults.standard.set(token, forKey: "MISToken")
                        UserDefaults.standard.synchronize()
                        self.getReportsData()
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    print(error.localizedDescription)
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
                break
            case .failure(let error):
                
                loadingView.hideLoader()
                print(error)
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    
    func getReportsData(){
        
        loadingView.showLoader(text: "")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        let misToken = UserDefaults.standard.object(forKey: "MISToken") as! String
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" :"Bearer \(misToken)" ,"Content-Type":"application/json"]
        var dicParams: Parameters = ["LoginID": username,"PMSCustCode": pmsCode]
        //var segments = ["Day","Month","Year"]
        let accountDate = UserDefaults.standard.object(forKey: "Account_Date") as?  Date ?? Date ()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let acDate = formatter.string(from: accountDate)
        let fromMonthDate = acDate.dropLast(3)
        dicParams.updateValue(acDate, forKey: "FromDate")
        dicParams.updateValue(acDate, forKey: "ToDate")
        dicParams.updateValue("\(fromMonthDate)\("-01")", forKey: "FromMonth")
        dicParams.updateValue(1, forKey: "Month")
        dicParams.updateValue(1, forKey: "Year")
        dicParams.updateValue(1, forKey: "Day")
        
        //let url = "https://fcreportsapi.azurewebsites.net/v1/report/MISRevenueReport"
        print("URL:", "\(CommonDataManager.shared.baseUrl1)\(MISREPORT)")
        print("nParams:\(dicParams), nHeaders:\(headers), acDate:\(acDate), fromMonthDate:\(fromMonthDate)")
        AF.request("\(CommonDataManager.shared.baseUrl1)\(MISREPORT)", method: .post, parameters: dicParams,encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            switch response.result {
            case .success:
                print("getReportsData",response)
                loadingView.hideLoader()
                do {
                    let response = try JSONDecoder().decode(MISReportDataModel.self, from: response.data ?? Data())
                    if response.Status == "Success" {
                        self.mISReportData = response
                        self.updateDataBySelectedIndex(index: 0)
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "Error")
                    }
                }
                catch let error {
                    #if DEBUG
                    loadingView.hideLoader()
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                loadingView.hideLoader()
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
            
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
    }
    
    @IBAction func lastYearAction(_ sender: UISwitch) {
        if sender.isOn {
            self.islastYearEnabled = true
        }
        else {
            self.islastYearEnabled = false
        }
        self.updateDataBySelectedIndex(index: selectedBottomSegmentIndex)
    }
    
    @IBAction func showCustomCalendar(_ sender: Any) {
        
        let dateRangePickerViewController = CalendarDatePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        dateRangePickerViewController.minimumDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
        dateRangePickerViewController.maximumDate = Date() //Calendar.current.date(byAdding: .year, value: 10, to: Date())
        dateRangePickerViewController.selectedStartDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        dateRangePickerViewController.selectedEndDate = Date()

        let sectionsCount = dateRangePickerViewController.numberOfSections(in: dateRangePickerViewController.collectionView!)
        let lastItemIndex = IndexPath.init(row: 0, section: sectionsCount - 1)
        dateRangePickerViewController.collectionView?.scrollToItem(at: lastItemIndex, at: UICollectionView.ScrollPosition.top, animated: false)

        self.navigationController?.present(navigationController, animated: true, completion: nil)
        
    }
    func setupUI(){
        self.btn_custom.layer.borderWidth = 1
        self.btn_custom.layer.borderColor = UIColor.hexStringToUIColor(hex: "FF8E00").cgColor
        self.btn_custom.layer.masksToBounds = true
        self.btn_custom.layer.cornerRadius = 5
        self.tableview_report.tableFooterView = UIView.init()
        self.view_background.layer.cornerRadius = 10.0
        self.view_background.layer.masksToBounds = true
        self.title = "MIS Report"
        tableview_report.backgroundColor = UIColor.clear
        tableview_report.register(UINib(nibName: "MISReportTableViewCell", bundle: nil), forCellReuseIdentifier: "MISReportTableViewCell")
        
        self.buttonsSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        buttonsSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "FF8E00")], for: .normal)
        buttonsSegment.backgroundColor = .white
        
        self.buttonsSegment.setImage(UIImage.textEmbeded(image: UIImage.init(named: "Revenue Groups") ?? UIImage.init(), string: "Revenue Groups", isImageBeforeText: false), forSegmentAt: 0)
        self.buttonsSegment.setImage(UIImage.textEmbeded(image: UIImage.init(named: "Outlet Sales") ?? UIImage.init(), string: "Outlet sales", isImageBeforeText: false), forSegmentAt: 1)
        self.buttonsSegment.setImage(UIImage.textEmbeded(image: UIImage.init(named: "Room Statistics") ?? UIImage.init(), string: "Room Statistics", isImageBeforeText: false), forSegmentAt: 2)
    }
    
    @IBAction func revenueByDayMonthyear(_ sender: UISegmentedControl) {
        
        self.btn_custom.setTitle("Custom", for: .normal)
        self.btn_custom.backgroundColor = UIColor.white
        self.btn_custom.setTitleColor(UIColor.hexStringToUIColor(hex: "FF8E00"), for: .normal)
        self.btn_custom.layer.borderWidth = 1
        self.btn_custom.layer.borderColor = UIColor.hexStringToUIColor(hex: "FC7A45").cgColor
        self.btn_custom.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.segment.tintColor = UIColor.hexStringToUIColor(hex: "")
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)

        selectedRevenueSegmentIndex = sender.selectedSegmentIndex
        self.updateDataBySelectedIndex(index: selectedBottomSegmentIndex)
    }
    
    @IBAction func property(_ sender: Any) {
        
        let selectHotelsVC = self.storyboard?.instantiateViewController(withIdentifier: "selectHotelsVC") as! SelectHotelsPopupVC
        selectHotelsVC.pDelegate = self as? PropertyDelegate
        print("self.totalProperties is ======>>>>>",self.totalProperties)
        selectHotelsVC.totalProperties = self.totalProperties
        presentPanModal(selectHotelsVC)
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func customSelected(_ sender: Any) {
//        let dateRangePickerViewController = CalendarDatePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
//        dateRangePickerViewController.delegate = self
//        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
//        dateRangePickerViewController.minimumDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
//        dateRangePickerViewController.maximumDate = Date() //Calendar.current.date(byAdding: .year, value: 10, to: Date())
//        dateRangePickerViewController.selectedStartDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
//        dateRangePickerViewController.selectedEndDate = Date()
//
//        let sectionsCount = dateRangePickerViewController.numberOfSections(in: dateRangePickerViewController.collectionView)
//        let lastItemIndex = IndexPath.init(row: 0, section: sectionsCount - 1)
//        dateRangePickerViewController.collectionView?.scrollToItem(at: lastItemIndex, at: UICollectionView.ScrollPosition.top, animated: false)
//        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    func getSingleProperty(){
       // loadingView.showLoader(text: "")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        //let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginId":username]//,"PmsCustCode":pmsCode]
        AF.request("\(CommonDataManager.shared.baseUrl)\(SINGLEPROPERTY)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print(response)
                //loadingView.hideLoader()
                do {
                    let response = try JSONDecoder().decode(SinglePropertyModel.self, from: response.data ?? Data())
                    if response.Status == "Success" {
                        if let groupProperties = response.Response {
                            self.parseGroupProperties(list: [groupProperties])//comment : single group to ary of group
                        }
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                loadingView.hideLoader()
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    func parseGroupProperties(list:[PropertyResponseObj]?){
        
        for each in list ?? [] {
            
            var property1 = PropertyModel()
            property1.title = each.GroupDetails?.GroupName ?? ""
            //In case all properties selected (All properties in the group)
            if isGroupPropertySelected ?? false {
                 property1.selected = true
            }
            else {
                property1.selected = false
            }
            // show Group default selected in check box
            
            var ary = [Property1Model]()
            for property in each.PropertyDetails ?? [] {
                var p = Property1Model()
                p.title = property.PropertyName
                p.titlePms = property.PmsCustCode
                
                if ispropertySelected ?? false {
                   p.selected = true
                }
                else {
                    p.selected = false

                }
                
                //In case few property OR properties selected in the gourp
                 // show Property default selected in check box// here is the line show privious selectoion
                ary.append(p)
            }
            property1.property = ary
            totalProperties.append(property1)
            
            //totalProperties.append(contentsOf: totalProperties)
        }
    }
    @objc func didPullToRefresh() {
        // get pms code from properties list
        if let pmsCustCode = getPMSFromSelectedProperty() {
            // set pmscode globally
            UserDefaults.standard.set(String(pmsCustCode), forKey: "PmsCustCode")
        }
        //self.getSalesByDuration()
        //self.getRoomOccupancyData()
        self.getMISToken()
        DispatchQueue.main.async {
            let subviews = self.tableview_report.subviews
            for view in subviews {
                if view is UIRefreshControl {
                    let control = view as? UIRefreshControl
                    control?.endRefreshing()
                }
            }
            
        }
        
    }
    func getPMSFromSelectedProperty() -> Int? {
        for i in 0..<self.totalProperties.count {
            let count = self.totalProperties[i].property?.count ?? 0
            
            if totalProperties[i].selected == true {
                for j in 0..<count {
                    if totalProperties[i].property?[j].selected == true{
                        return totalProperties[i].property?[j].titlePms
                    }
                }
            }
            else {
                for j in 0..<count {
                    if totalProperties[i].property?[j].selected == true{
                        return totalProperties[i].property?[j].titlePms
                    }
                }
            }
        }
        return nil
    }
}

extension MISReportViewController :PropertyDelegate {
func selectedProperties(list: [PropertyModel]) {
    self.totalProperties.removeAll()
    //self.mainList.removeAll()
    self.totalProperties = list
    didPullToRefresh()
    //totalProperties[indexPath.section].property?[dataIndex].selected ?? false
    for i in 0..<self.totalProperties.count {
        if self.totalProperties[i].selected == true {
            isGroupPropertySelected = true
        }
        else {
            isGroupPropertySelected = false
        }
        let count = self.totalProperties[i].property?.count ?? 0
        for j in 0..<count {
        if totalProperties[i].property?[j].selected == true{
                ispropertySelected = true
        }else {
                ispropertySelected = false
            }
        }
    }
    //self.tableview_listSales.reloadData()
    var aryCount = list.filter { (each) -> Bool in
        each.selected == true
    }
//    if aryCount.count > 1 {
//        self.baseHeightConst.constant = 80
//        self.collectionViewHeightConstraint.constant = 0
//        self.tableview_listSales.allowsSelection = false
//    }
//    else {
//        self.baseHeightConst.constant = 180
//        self.tableview_listSales.allowsSelection = true
//        self.collectionViewHeightConstraint.constant = 180
//    }
    
    }
}
extension MISReportViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryMISR_List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MISReportTableViewCell", for: indexPath as IndexPath) as! MISReportTableViewCell
        //print("Valuessssssssssssss",aryMISR_List[indexPath.row])
        cell.misRoport  = aryMISR_List[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = MISHeaderView.init(frame: CGRect(x: 0, y: 0, width: self.tableview_report.frame.width, height: 60))
        if islastYearEnabled {
            view.lbl_actualCy.text = "LY"
            view.lbl_budgetCy.text = "LY"
        }
        else {
            view.lbl_actualCy.text = "Actual"
            view.lbl_budgetCy.text = "CY"
        }
        if selectedBottomSegmentIndex == 0 {
            view.lbl_Title.text = "Revenu Group"
        }
        else if selectedBottomSegmentIndex == 1{
            view.lbl_Title.text = "Outlet Sales"
        }
        else {
            view.lbl_Title.text = "Room Statics"
        }
        return view
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view = MISRFooterView.init(frame: CGRect(x: 0, y: 0, width: self.tableview_report.frame.width, height: 60))
        if selectedBottomSegmentIndex != 2 {
            var budgetTotal:Double = 0.0
            var acTotal:Double = 0.0
            var lydTotal:Double = 0.0
            for each in aryMISR_List{
                let avString = each.acyValue ?? ""
                let bvString = each.bcyValue ?? ""
                let alyString = each.actualLY ?? ""
                let av = Double(avString) ?? 0.0
                let bv = Double(bvString) ?? 0.0
                let lyd = Double(alyString) ?? 0.0
                acTotal += av
                budgetTotal += bv
                lydTotal += lyd
            }
            view.backgroundColor = .lightGray
            view.view.backgroundColor = Colors.hexStringToUIColor(hex: "#EFEFF4")
            view.lbl_title.text = "Total"
            let actuTotal = CommonDataManager.shared.getNumberWithCommas(cash: Double(acTotal))
            view.lbl_actualAmount.text = "\(actuTotal)"
            let budgTotal = CommonDataManager.shared.getNumberWithCommas(cash: Double(budgetTotal))
            view.lbl_budgetAmount.text = "\(budgTotal)"
            let lyTotal = CommonDataManager.shared.getNumberWithCommas(cash: Double(lydTotal))
            view.lbl_LyAmount.text = "\(lyTotal)"
            return view
        }
        else {
            view.backgroundColor = .white
            view.view.backgroundColor = .white
            view.lbl_title.text = ""
            view.lbl_actualAmount.text = ""
            view.lbl_budgetAmount.text = ""
            view.lbl_LyAmount.text = ""
            return view
        }
        //return nil
    }
}

extension MISReportViewController:CalendarDatePickerViewControllerDelegate{
    func didTapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        //print("Selected Date Range : \(startDate)-\(endDate)")
        var str = "\(startDate.toString())-\(endDate.toString())"
        btn_custom.setTitle(str, for: .normal)
        btn_custom.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        btn_custom.titleLabel?.numberOfLines = 2
        btn_custom.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
        btn_custom.titleLabel?.textAlignment = .center
        //custom_DateSelector.titleLabel?.textColor = UIColor.white
        btn_custom.setTitleColor(.white, for: .normal)
        self.dismiss(animated: true, completion: nil)
        self.segment.tintColor = UIColor.lightGray
    }
}

extension UIImage {
    static func textEmbeded(image: UIImage,
                            string: String,
                            isImageBeforeText: Bool,
                            segFont: UIFont? = nil) -> UIImage {
        let font = segFont ?? UIFont.systemFont(ofSize: 10.5)
        let expectedTextSize = (string as NSString).size(withAttributes: [.font: font])
        let width = expectedTextSize.width + image.size.width + 5
        let height = max(expectedTextSize.height, image.size.width)
        let size = CGSize(width: width, height: height + 25)
        
        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { context in
            let fontTopPosition: CGFloat = (height - expectedTextSize.height) / 2
            let textOrigin: CGFloat = isImageBeforeText
                ? image.size.width + 5
                : 0
            let textPoint: CGPoint = CGPoint.init(x: textOrigin + 13, y: fontTopPosition+22)
            string.draw(at: textPoint, withAttributes: [.font: font])
            let alignment: CGFloat = isImageBeforeText
                ? 0
                : expectedTextSize.width + 5
            let rect = CGRect(x: 36.5,
                              y: (height - image.size.height) / 2 - 2,
                              width: image.size.width + 5,
                              height: image.size.height + 5)
            image.draw(in: rect)
        }
    }
}




