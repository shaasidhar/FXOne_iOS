//
//  CurrencyRatepopupVC.swift
//  FXone
//
//  Created by Vinayak on 4/12/22.
//  Copyright © 2022 IDS. All rights reserved.
//

import UIKit

class CurrencyRatepopupVC: UIViewController {
    
    @IBOutlet weak var btnCurncyList: UIButton!
    @IBOutlet weak var txtfldValue: UITextField!
    
    let transparentView = UIView()
    let tableView = UITableView()
    var selectedBtn = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func addTransparentView(frams : CGRect) {
        let window = UIApplication.shared.keyWindow
        transparentView.frame = window?.frame ?? self.view.frame
        self.view.addSubview(transparentView)
        
        tableView.frame = CGRect(x: frams.origin.x, y: frams.origin.y + frams.height, width: frams.width, height: 0)
        tableView.layer.cornerRadius = 5
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(removeTransprentView))
        transparentView.addGestureRecognizer(tapgesture)
        transparentView.alpha = 0
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.5
            self.tableView.frame = CGRect(x: frams.origin.x, y: frams.origin.y + frams.height, width: frams.width, height: 200)
        }, completion: nil)
    }
    @objc func removeTransprentView() {
        let frams = selectedBtn.frame
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0
            self.tableView.frame = CGRect(x: frams.origin.x, y: frams.origin.y + frams.height, width: frams.width, height: 0)
        }, completion: nil)
    }
    @IBAction func onClickSelectCrncy(_ sender: Any) {
        selectedBtn = btnCurncyList
        addTransparentView(frams: btnCurncyList.frame)
    }
    

}
