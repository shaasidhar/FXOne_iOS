//
//  RoomRateCells.swift
//  FXone
//
//  Created by Vinayak on 10/4/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import Foundation
import UIKit
import SpreadsheetView

class HeaderCell: Cell {
    let label = UILabel()
    
    override var frame: CGRect {
        didSet {
            label.frame = bounds.insetBy(dx: 1, dy: 1)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        label.frame = bounds
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.font = UIFont.FXRobotoBlackBold(size: 12)
        label.textAlignment = .center
        label.numberOfLines = 2
        label.backgroundColor = UIColor.lightGray
        contentView.addSubview(label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class TextCell: Cell {
    let label = UILabel()
    
    override var frame: CGRect {
        didSet {
            label.frame = bounds.insetBy(dx: 4, dy: 2)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 1, alpha: 0.2)
        selectedBackgroundView = backgroundView
        
        label.frame = bounds
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.font = UIFont.FXRobotoRegular(size: 12)
        label.sizeToFit()
        label.textAlignment = .center
        
        contentView.addSubview(label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
