//
//  RoomOcpcyViewController.swift
//  FXone
//
//  Created by Vinayak on 25/07/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import Alamofire
import CalendarDateRangePickerViewController

var todaySalesObj :RoomSaleCustomModel?
var monthSalesObj :RoomSaleCustomModel?
var yearSalesObj :RoomSaleCustomModel?

var todayRoomSalesObj :RoomSaleCustomModel?
var monthRoomSalesObj :RoomSaleCustomModel?
var yearRoomSalesObj :RoomSaleCustomModel?

var todayFBSalesObj :RoomSaleCustomModel?
var monthFBSalesObj :RoomSaleCustomModel?
var yearFBSalesObj :RoomSaleCustomModel?

var todayOFBSalesObj :RoomSaleCustomModel?
var monthOFBSalesObj :RoomSaleCustomModel?
var yearOFBSalesObj :RoomSaleCustomModel?


class RoomOcpcyViewController: UIViewController {
    @IBOutlet weak var tableview_detail: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var lbl_screenTtile: UILabel!
    var isExpanded = false
    var index :Int?
    var selectedItem = [String:Any]()
    var str_screenTtile = ""
    var dicTotalRoomsale = TotalRoomSaleModel()
    var dicRoomSaleDetails = DetailsRoomSaleDetail()
    var items = [Any]()
    /* roomTypeName: Value->Dictionary of segment_name(gusttyp,marketseg,busisurc),
     value:[any:(rsp.RoomSaleDetails.GuestStatusDetail[0].RoomTypeName/Data)] */
    var roomTypeDict : Dictionary<String,Dictionary<String,Any>> = [String:[String:Any]]()
    var fandb_detailsResponse = FANBDetailModel()
    var isRoomSaleDetails = false
    var isnonFNBDetails = false
    var isFandBDetails = false
    var curncyCode = String()
    
    var isCustomeOneSale = false
    var isCustomeFandBDetails = false
    var isCustomenonFNBDetails = false
    
    @IBOutlet weak var view_navheader: UIView!
    @IBOutlet weak var btn_property: UIButton!
    @IBOutlet weak var button_notification: UIButton!
    @IBOutlet weak var tableview_trendList: UITableView!
    @IBOutlet weak var tableHeightConstarint: NSLayoutConstraint!
    @IBAction func goBack(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var btn_custom: UIButton!
   
    override func viewWillAppear(_ animated: Bool) {
        btn_custom.layer.borderWidth = 1
        btn_custom.layer.borderColor = UIColor.hexStringToUIColor(hex: "#000000").cgColor
        btn_custom.layer.cornerRadius = 5.0
        self.navigationController?.navigationBar.isHidden = true
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)

    }
    
    var aryList = [[String:Any]]()
    var aryDetailsList = [[String:Any]]()
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    var aryNFABlist = [NonFAndBSalesDataObj]()
    var aryFABlist = [FAndBSalesDataObj]()
    var selectedIndex = 0
    var slelectedsegmentIndexValue: Int?
    var todaySales :RoomSaleCustomModel?
    var monthSales :RoomSaleCustomModel?
    var yearSales :RoomSaleCustomModel?
    var details : RoomSaleDetailModel?
    var customeDetails : RoomSaleCustomModel?
    var alos :aLosModel?
    var str_TypeOFSale = ""
    var isCustomDetailed = false
    var isLiveEnabled = false
    var dAcDate = ""
    var accounttingDate = ""
    
    var startDateDetailed = NSDate()
    var endDateDetailed = NSDate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segment.selectedSegmentIndex = slelectedsegmentIndexValue ?? 0
        if isLiveEnabled {
            segment.selectedSegmentIndex = 0
            self.slelectedsegmentIndexValue = 0
        }
        //print(str_screenTtile)
        self.lbl_screenTtile.text = str_screenTtile
        if str_screenTtile == "FXONE03" {
            self.lbl_screenTtile.text = "F&B Details"
        } else if str_screenTtile == "FXONE04" {
            self.lbl_screenTtile.text = "Other Revenue Details"
        } else if str_screenTtile == "FXONE02" {
            self.lbl_screenTtile.text = "Room Sales"
        }
        loadDetailsData()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.updateSelectedItem),
            name: NSNotification.Name(rawValue: "SELECTEDITEM"),
            object: nil)
        segmentTitleConversion()
    }
    func segmentTitleConversion(){
        let date = dAcDate.getDateFromString(isoDate: dAcDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMMyy"
        let acdate = formatter.string(from: date)
        let defaultAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
        segment.setTitleTextAttributes(defaultAttributes, for: .normal)
        
        let largerFontAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 9)]
        segment.setTitleTextAttributes(largerFontAttributes, for: .normal)
                
        let currentDate = date
        var calendar = Calendar.current
        let modifiedDate = calendar.date(byAdding: .day, value: 0, to: currentDate)
        formatter.dateFormat = "ddMMMyy"
        let date_Today = formatter.string(from: modifiedDate!)
        segment.setTitle(date_Today, forSegmentAt: 0)
        
        let ac_date : Date? = CommonDataManager.shared.parseDate(dateString: self.accounttingDate)
        let acdate_Live = CommonDataManager.shared.getDateStringDDMMMYYYYWithOutSpaces(fromDate: ac_date ?? Date())
        segment.setTitle(acdate_Live, forSegmentAt: 1)
        segment.setTitle("MTD", forSegmentAt: 2)
        segment.setTitle("YTD", forSegmentAt: 3)
    }
    func loadDetailsData() {
        if str_TypeOFSale == "FXONE02" {
            if isCustomDetailed  {
                self.isRoomSaleDetails = true
                self.isFandBDetails = false
                self.isnonFNBDetails = false
                self.isCustomeOneSale = true
                self.didTapDoneWithDateRange(startDate: startDateDetailed as Date, endDate: endDateDetailed as Date)
                tableHeightConstarint.constant = 230
            }else {
                //selectedIndex = 0
                self.getOnesaleDetailsData()
                self.isRoomSaleDetails = true
                self.isFandBDetails = false
                self.isnonFNBDetails = false
                self.isCustomeOneSale = true
                tableHeightConstarint.constant = 230
            }
            
            //segment.selectedSegmentIndex = slelectedsegmentIndex ?? 0
            
        }
        else if str_screenTtile == "FXONE03"{
            if isCustomDetailed  {
                self.isRoomSaleDetails = false
                self.isFandBDetails = true
                self.isnonFNBDetails = false
               // self.isCustomeOneSale = true
                self.isCustomeFandBDetails = true
                self.didTapDoneWithDateRange(startDate: startDateDetailed as Date, endDate: endDateDetailed as Date)
                tableHeightConstarint.constant = 230
            }else{
                self.getFandBDetailsData()
                self.isRoomSaleDetails = false
                self.isFandBDetails = true
                self.isnonFNBDetails = false
                self.isCustomeFandBDetails = true
                tableHeightConstarint.constant = 350
            }
        }
        else if str_screenTtile == "FXONE04"{
            if isCustomDetailed  {
                self.isRoomSaleDetails = false
                self.isFandBDetails = false
                self.isnonFNBDetails = true
                //self.isCustomeOneSale = true
                self.isCustomenonFNBDetails = true
                self.didTapDoneWithDateRange(startDate: startDateDetailed as Date, endDate: endDateDetailed as Date)
                tableHeightConstarint.constant = 230
            }else{
                self.getNonFandBDetailsData()
                 self.isRoomSaleDetails = false
                 self.isnonFNBDetails = true
                 self.isFandBDetails = false
                // selectedIndex = 2
                 self.isCustomenonFNBDetails = true
                 tableHeightConstarint.constant = 350
            }
        }
        tableview_trendList.backgroundColor = UIColor.white
        self.tableview_detail.layer.cornerRadius = 10
        self.tableview_detail.allowsSelection = false
        self.tableview_detail.register(UINib.init(nibName: "BubbleDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "BubbleDetailTableViewCell")
        
        self.tableview_trendList.register(UINib.init(nibName: "RoomDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "RoomDetailsTableViewCell")
        tableview_trendList.tableFooterView = UIView.init()
        tableview_trendList.allowsSelection = false
    }
    @objc func updateSelectedItem() {
        //self.updateSelectedItemList()
        //self.tableview_trendList.reloadData()
    }
    
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        if #available(iOS 13.0, *) {
            segment.selectedSegmentTintColor = UIColor.hexStringToUIColor(hex: "#FC7A45")
        } else {
            // Fallback on earlier versions
        }
        self.roomTypeDict.removeAll()
        self.aryDetailsList.removeAll()
        //self.aryList.removeAll()
        self.slelectedsegmentIndexValue =  sender.selectedSegmentIndex
        isLiveEnabled = false
        if sender.selectedSegmentIndex == 0 {
            isLiveEnabled = true
            
        }
        self.isCustomDetailed = false
        loadDetailsData()
//        if str_screenTtile == "FXONE02"  {
//            self.getOnesaleDetailsData()
//        }else if str_screenTtile == "FXONE03"  {
//            self.getFandBDetailsData()
//        }else if str_screenTtile == "FXONE04"  {
//            self.getNonFandBDetailsData()
//        }
        
        self.btn_custom.setTitle("Custom", for: .normal)
        self.btn_custom.backgroundColor = UIColor.hexStringToUIColor(hex: "F2F2F3")
        self.btn_custom.setTitleColor(UIColor.hexStringToUIColor(hex: "FC7A45"), for: .normal)
        self.btn_custom.layer.borderWidth = 1
        self.btn_custom.layer.borderColor = UIColor.hexStringToUIColor(hex: "#000000").cgColor
        self.btn_custom.titleLabel?.font = UIFont.FXRobotoRegular(size: 13) //systemFont(ofSize: 15)
        self.segment.tintColor = UIColor.hexStringToUIColor(hex: "FC7A45")
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
        
    }
    
    @IBAction func showCustomCalendar(_ sender: Any) {
        let dateRangePickerViewController = CalendarDatePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = self
        let accountDate = UserDefaults.standard.object(forKey: "Account_Date") as?  Date ?? Date ()

        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        dateRangePickerViewController.minimumDate = Calendar.current.date(byAdding: .year, value: -1, to: accountDate)
        dateRangePickerViewController.maximumDate = accountDate //Calendar.current.date(byAdding: .year, value: 10, to: Date())
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        print("accountDate======>>>>>",accountDate)
        
        dateRangePickerViewController.selectedStartDate = Calendar.current.date(byAdding: .day, value: -7, to: accountDate)
        
        dateRangePickerViewController.selectedEndDate = accountDate
        let sectionsCount = dateRangePickerViewController.numberOfSections(in: dateRangePickerViewController.collectionView!)
        let lastItemIndex = IndexPath.init(row: 0, section: sectionsCount - 1)
        dateRangePickerViewController.collectionView?.scrollToItem(at: lastItemIndex, at: UICollectionView.ScrollPosition.top, animated: false)
        
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func getFandBDetailsData(){
        loadingView.showLoader(text: " ")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        var dicParams :[String:Any] = ["LoginID":username,"PmsCustCode":pmsCode,"Fromdate": "","Todate": "","CurrencyCode":"INR","ExchangeRate":1.0,"IsCustom":isCustomDetailed ? 1 : 0]
        if isLiveEnabled {
            let date = dAcDate.getDateFromString(isoDate: dAcDate)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let acdate = formatter.string(from: date)
            dicParams.updateValue(acdate, forKey: "Fromdate")
            dicParams.updateValue(acdate, forKey: "Todate")
            dicParams.updateValue(1, forKey: "IsCustom")
        }
        AF.request("\(CommonDataManager.shared.baseUrl)\(FANDBDETAILS)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print("getFandBDetailsData response is-->",response)
                loadingView.hideLoader()
                do {
                    let results = try JSONDecoder().decode(FANBDetailModel.self, from: response.data ?? Data())
                    print(results)
                    if results.Status == "Success" {
                        self.fandb_detailsResponse = results
                        if self.slelectedsegmentIndexValue == 0 {
                            self.aryFABlist = results.Response?.Custom?.FNBSalesData ?? []
                            self.customeDetails = results.Response?.Custom?.DashBoard?.first ?? RoomSaleCustomModel()
                        }else if self.slelectedsegmentIndexValue == 1 {
                            self.aryFABlist = results.Response?.Day?.FNBSalesData ?? []
                            
                        }else if self.slelectedsegmentIndexValue == 2 {
                            self.aryFABlist = results.Response?.Month?.FNBSalesData ?? []
                           
                        } else if self.slelectedsegmentIndexValue == 3 {
                            self.aryFABlist = results.Response?.Year?.FNBSalesData ?? []
                        }
                        CommonDataManager.shared.ary_FandBDetailsList = self.aryFABlist
                        self.parseTabsData()
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: results.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                loadingView.hideLoader()
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    func getNonFandBDetailsData(){
        loadingView.showLoader(text: "")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        var dicParams : [String:Any] = ["LoginID":username,"PmsCustCode":pmsCode,"Fromdate": "","Todate": "","CurrencyCode":"INR","ExchangeRate":1.0,"IsCustom":isCustomDetailed ? 1 : 0]
        if isLiveEnabled {
            let date = dAcDate.getDateFromString(isoDate: dAcDate)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let acdate = formatter.string(from: date)

            dicParams.updateValue(acdate, forKey: "Fromdate")
            dicParams.updateValue(acdate, forKey: "Todate")
            dicParams.updateValue(1, forKey: "IsCustom")
//            dicParams =  ["LoginID": "user3@idsnext.com","PmsCustCode":7520,"CurrencyCode":"INR","ExchangeRate": 0,"FromDate": "2022-08-11","ToDate": "2022-08-11","IsCustom": true]
        }
        AF.request("\(CommonDataManager.shared.baseUrl)\(NONFANDBDETAILS)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print("getNonFandBDetailsData response -->",response)
                loadingView.hideLoader()
                do {
                    let results = try JSONDecoder().decode(NonFANBDetailModel.self, from: response.data ?? Data())
                    if results.Status == "Success" {
                        if self.slelectedsegmentIndexValue == 0 {
                            // NonFNBSalesData  chnage to Year
                            self.aryNFABlist = results.Response?.NonFNBSalesData?.Custom?.NonFNBSalesData ?? []
                            print("NON-F&B Year",self.aryNFABlist)
                            self.customeDetails = results.Response?.NonFNBSalesData?.Custom?.DashBoard?.first ?? RoomSaleCustomModel()
                            

                        }else if self.slelectedsegmentIndexValue == 1 {
                            
                            // NonFNBSalesData  chnage to Day
                            self.aryNFABlist = results.Response?.NonFNBSalesData?.Day ?? []
                            print("NON-F&B Day",self.aryNFABlist)
                            
                            

                        }else if self.slelectedsegmentIndexValue == 2 {
                            // NonFNBSalesData  chnage to Month
                            self.aryNFABlist = results.Response?.NonFNBSalesData?.Month ?? []
                            print("NON-F&B Month",self.aryNFABlist)
                            
                            
                        } else if self.slelectedsegmentIndexValue == 3 {
                            /// NonFNBSalesData  chnage to Year
                            self.aryNFABlist = results.Response?.NonFNBSalesData?.Year ?? []
                            print("NON-F&B Year",self.aryNFABlist)
                        }
                        
                        CommonDataManager.shared.ary_nonFandBDetailsList = self.aryNFABlist
                        self.parseTabsData()
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: results.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                loadingView.hideLoader()
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    func getOnesaleDetailsData(){
        loadingView.showLoader(text: "")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        var dicParams : [String:Any] = ["LoginId":username,"PMSCustCode":pmsCode,"Fromdate": "", "Todate": "","CurrencyCode":"INR","ExchangeRate":1.0,"IsCustom":isCustomDetailed ? 1 : 0]
        if isLiveEnabled {
            let date = dAcDate.getDateFromString(isoDate: dAcDate)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let acdate = formatter.string(from: date)

            dicParams.updateValue(acdate, forKey: "Fromdate")
            dicParams.updateValue(acdate, forKey: "Todate")
            dicParams.updateValue(1, forKey: "IsCustom")
        }
        AF.request("\(CommonDataManager.shared.baseUrl)\(ONESALEDETAIS)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print("\(CommonDataManager.shared.baseUrl)\(ONESALEDETAIS)")
            print("ONESALEDETAIS Request:", token, headers, dicParams)
            print("ONESALEDETAIS Response:",response)
            switch response.result {
            case .success:
                print("getOnesaleDetailsData response",response)
                loadingView.hideLoader()
                do {
                    let response = try JSONDecoder().decode(Detail_RoomSaleModel.self, from: response.data ?? Data())
                    if response.Status == "Success" {
                        if self.slelectedsegmentIndexValue == 0 {
                            
                            self.dicTotalRoomsale = response.Response?.Custom?.RoomSale ?? TotalRoomSaleModel()
                            self.dicRoomSaleDetails = response.Response?.Custom?.RoomSaleDetails ?? DetailsRoomSaleDetail()
                            self.accounttingDate = response.Response?.AccountingDate ?? ""
                            self.customeDetails =  response.Response?.Custom?.DashBoard?.first ?? RoomSaleCustomModel()
                            
                            
                            
                           
                        } else if self.slelectedsegmentIndexValue == 1 {
                            self.dicTotalRoomsale = response.Response?.Day?.RoomSale ?? TotalRoomSaleModel()
                            self.accounttingDate = response.Response?.AccountingDate ?? ""
                            self.dicRoomSaleDetails = response.Response?.Day?.RoomSaleDetails ?? DetailsRoomSaleDetail()
                            
                            
                            
                            
                            
                            
                            
                            
                        } else if self.slelectedsegmentIndexValue == 2 {
                            
                            self.dicTotalRoomsale = response.Response?.Month?.RoomSale ?? TotalRoomSaleModel()
                            self.accounttingDate = response.Response?.AccountingDate ?? ""
                            self.dicRoomSaleDetails = response.Response?.Month?.RoomSaleDetails ?? DetailsRoomSaleDetail()
                            self.monthSales = response.Response?.Custom?.DashBoard?.first ?? RoomSaleCustomModel()
                            
                        } else if self.slelectedsegmentIndexValue == 3 {
                            self.dicTotalRoomsale = response.Response?.Year?.RoomSale ?? TotalRoomSaleModel()
                            self.accounttingDate = response.Response?.AccountingDate ?? ""
                            self.dicRoomSaleDetails = response.Response?.Year?.RoomSaleDetails ?? DetailsRoomSaleDetail()
                            //self.yearSales =  response.Response?.Year ?? RoomSaleCustomModel()
                            
                        }
                        self.segmentTitleConversion()
                        self.tableview_detail.reloadData()
                        // Currency Code
                        if let currency_code = response.Response?.CurrencyCode {
                            self.curncyCode = currency_code
                        }
                        self.parseTabsData()
                        //self.aryList[0].updateValue(roolmDetailDic, forKey: "DetailRoomDic")
                    }
                    else{
                        
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                    }
                    
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                loadingView.hideLoader()
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
        
    }
    
    @objc func expandDetails(sender:UIButton){
        if (sender.isSelected == true)
        {
            tableHeightConstarint.constant = 230
            sender.isSelected = false;
        }
        else {
            sender.isSelected = true;
            tableHeightConstarint.constant = 480
        }
        tableview_detail.reloadData()
    }
    @objc func expandDetailsTable2(sender:UIButton){
        let btn = sender
        
        if isRoomSaleDetails {
            let key = roomTypeDict.getElement(index: btn.tag)?.key ?? ""
            var dic1 = roomTypeDict[key] ?? [String:Any]()
            if (sender.isSelected == true)
            {
                index = nil
                sender.isSelected = false;
                dic1.updateValue(false, forKey: "isOpened")
            }
            else {
                sender.isSelected = true;
                dic1.updateValue(true, forKey: "isOpened")
            }
            roomTypeDict.updateValue(dic1, forKey: key)
        }
        else if isFandBDetails {
            if (sender.isSelected == true)
            {
                //index = nil
                sender.isSelected = false;
                aryFABlist[btn.tag].isOpened = false
                
            }
            else {
                //index = btn.tag
                sender.isSelected = true;
                aryFABlist[btn.tag].isOpened = true
                //tableview_trendList.reloadData()
            }
        }
        else {
            
        }
        tableview_trendList.reloadData()
    }
}
extension RoomOcpcyViewController :UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableview_trendList {
            if isRoomSaleDetails {
                print("roomTypeDict.count=====>>>>",roomTypeDict.count)
                return roomTypeDict.count
            }else if isnonFNBDetails {
                aryNFABlist =  aryNFABlist.removingDuplicates(withSame: \.RevenueCodeName)
                return aryNFABlist.count
            }else {
                aryFABlist =  aryFABlist.removingDuplicates(withSame: \.OutletName)
                return aryFABlist.count
            }
            
        }
        else if tableView == tableview_detail {
            print("aryDetailsList.count=====>>>>",aryDetailsList.count)
            return self.aryDetailsList.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tableview_trendList {
            let roomsaleview = RoomSaleHeaderView.init(frame: CGRect(x: 0, y: 0 , width: tableView.frame.width, height: 60))
            return  roomsaleview
        }
        else {
            return UIView.init()
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tableview_trendList {
            
            return 50
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view = UIView.init()
        view.frame = CGRect(x: 0, y: 0, width:self.tableview_trendList.frame.width, height: 50)
        let labelTotal = UILabel.init()
        labelTotal.frame = CGRect(x:self.tableview_trendList.frame.width / 2 , y: 0, width: self.tableview_trendList.frame.width / 2, height: 30)
        labelTotal.backgroundColor = .white
        labelTotal.font = UIFont.FXRobotoBlackMedium(size: 16)
        var total :Float = 0.0
        
        let labelTax = UILabel.init()
        var totalTax :Float = 0.0
        labelTax.frame = CGRect(x:self.tableview_trendList.frame.width / 2 , y: 30, width: self.tableview_trendList.frame.width / 2, height: 30)
        labelTax.backgroundColor = .white
        labelTax.font = UIFont.FXRobotoBlackMedium(size: 16)
        
        if isRoomSaleDetails {
            
            for i in 0..<aryList.count {
                let each = aryList[i]["Item"] as? Details_RoomSaleObject
                let avString = each?.CurrentYear ?? 0.0
                total += avString
                let avString1 = each?.TaxAmount ?? 0.0
                totalTax += avString1
            }
        }
        else if isnonFNBDetails {
            
            for i in 0..<aryNFABlist.count {
                let each = aryNFABlist[i]
                let avString = Float(each.Amount ?? 0.0)
                total += avString
                let avString1 = each.TaxAmount ?? 0.0
                totalTax += avString1
                
            }
        }
        else {
            for i in 0..<aryFABlist.count {
                let each = aryFABlist[i]
                let amnt = Float(each.AMOUNT ?? 0.0)
                total = total + amnt
                let avString1 = each.TaxAmount ?? 0.0
                totalTax += avString1
            }
        }
        
        labelTotal.textAlignment = .center
        let str = CommonDataManager.shared.getNumberWithCommas(cash: Double(total))
        let taxStr = CommonDataManager.shared.getNumberWithCommas(cash: Double(totalTax))
        labelTotal.text = "Total    \(str)"
        
        labelTax.textAlignment = .center
        labelTax.text = "Tax    \(taxStr)"
        
        view.addSubview(labelTotal)
        view.addSubview(labelTax)
        
        return view
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == tableview_trendList {
            
            return 60
        }
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableview_trendList {
            if isRoomSaleDetails{
                let kv = roomTypeDict.getElement(index: indexPath.row)
                let isopend =  kv?.value["isOpened"] as? Bool
                if isopend ?? false {
                    return 300
                }
                else {
                    return 50
                }
            }
            else if isnonFNBDetails {
                
                return 50
            }
            else {
                let isopend = aryFABlist[indexPath.row].isOpened ?? false
                if isopend ?? false {
                    return 380 // expand for fnb list basr chart "300"
                }
                else {
                
                    return 50
                    
                }
            }
        }
        return tableView.frame.height
    }
    
    func parseTabsData(){
        
        if isRoomSaleDetails {
            // added each room name from RoomSale obj
            for roomSale in dicTotalRoomsale.RoomType ?? [Details_RoomSaleObject]() {
                if let roomType = roomSale.RoomTypeName {
                    roomTypeDict.updateValue([String:Any](), forKey: roomType)
                    roomTypeDict[roomType]?.updateValue(false, forKey: "isOpened")
                    roomTypeDict[roomType]?.updateValue([Any](), forKey: "items")
                }
            }
            
            // get GuestType Detials from RoomSaleDetails obj
            for guestType in dicRoomSaleDetails.GuestStatusDetail ?? [GuestStatusDetailObj]() {
                if let roomTypeName = guestType.RoomTypeName {
                    if let data = guestType.Data {
                        var detaildic = [String:Any]()
                        detaildic.updateValue("Guest Type", forKey: "Title")
                        detaildic.updateValue(data, forKey: "data")
                        var items:[Any] = roomTypeDict[roomTypeName]?["items"] as? [Any] ?? [Any]()
                        items.append(detaildic)
                        roomTypeDict[roomTypeName]?.updateValue(items, forKey: "items")
                    }
                }
            }
            // get MarketSgmentWise Detials from RoomSaleDetails obj
            for marketSegType in dicRoomSaleDetails.MarketSegmentDetails ?? [MarketSegmentDetailsObj]() {
                if let roomTypeName = marketSegType.RoomTypeName {
                    if let data = marketSegType.data {
                        var detaildic = [String:Any]()
                        detaildic.updateValue("Market Segment", forKey: "Title")
                        detaildic.updateValue(data, forKey: "data")
                        var items:[Any] = roomTypeDict[roomTypeName]?["items"] as? [Any] ?? [Any]()
                        items.append(detaildic)
                        roomTypeDict[roomTypeName]?.updateValue(items, forKey: "items")
                    }
                }
            }
            
            // get Business Source Detials from RoomSaleDetails obj
            for businesSourceType in dicRoomSaleDetails.BusinessSourceDetails ?? [BusinessSourceDetailsObj]() {
                if let roomTypeName = businesSourceType.RoomTypeName {
                    if let data = businesSourceType.data {
                        var detaildic = [String:Any]()
                        detaildic.updateValue("Business Source", forKey: "Title")
                        detaildic.updateValue(data, forKey: "data")
                        var items:[Any] = roomTypeDict[roomTypeName]?["items"] as? [Any] ?? [Any]()
                        items.append(detaildic)
                        roomTypeDict[roomTypeName]?.updateValue(items, forKey: "items")
                    }
                }
            }
            
            var items = [Any]()
            print("::::::::::::\(roomTypeDict)")
            var detaildic1 = [String:Any]()
            detaildic1.updateValue(dicTotalRoomsale.RoomType ?? "", forKey: "Item1")
            detaildic1.updateValue("Room Type", forKey: "Title")
            
            var detaildic2 = [String:Any]()
            detaildic2.updateValue(dicTotalRoomsale.GuestStatus ?? "", forKey: "Item2")
            detaildic2.updateValue("Guest Type", forKey: "Title")
            
            var detaildic3 = [String:Any]()
            detaildic3.updateValue(dicTotalRoomsale.MarketSegment ?? "", forKey: "Item3")
            detaildic3.updateValue("Market Segment", forKey: "Title")
            
            var detaildic4 = [String:Any]()
            detaildic4.updateValue(dicTotalRoomsale.BusinessSource ?? "", forKey: "Item4")
            detaildic4.updateValue("Bussiness Source", forKey: "Title")
            
            items.append(detaildic1)
            items.append(detaildic2)
            items.append(detaildic3)
            items.append(detaildic4)
            CommonDataManager.shared.detailItems = items
            
            CommonDataManager.shared.selectedDetailItem = 1
            
            self.updateSelectedItemList()
        }
        else {
        }
        
        var mainDic = [String:Any]()
        mainDic.updateValue(self.dicTotalRoomsale, forKey: "MainDic")
        self.aryDetailsList.append(mainDic)
        
        self.tableview_detail.reloadData()
        self.tableview_trendList.reloadData()
        
    }
    
    func updateSelectedItemList(){
        
       // if CommonDataManager.shared.selectedDetailItem == 1 {
            
            let itemsCount = dicTotalRoomsale.RoomType?.count ?? 0
            var temApry = [[String:Any]]()
            for i in 0..<itemsCount{
                var dic = [String:Any]()
                let each = dicTotalRoomsale.RoomType?[i] ?? Details_RoomSaleObject()
                dic.updateValue(false, forKey: "isOpened")
                dic.updateValue(each, forKey: "Item")
                temApry.append(dic)
            }
            aryList = temApry
        
    }
    //tableview func
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableview_trendList {
            
            let cell = tableview_trendList.dequeueReusableCell(withIdentifier: "RoomDetailsTableViewCell") as! RoomDetailsTableViewCell
            
            if isRoomSaleDetails {
                
                cell.backgroundColor = .clear
                cell.layer.cornerRadius = 10
                cell.layer.masksToBounds = true
                cell.btn_expand.tag = indexPath.row
                cell.selectedItem = 1
                //if CommonDataManager.shared.selectedDetailItem == 1 {
                let kv = roomTypeDict.getElement(index: indexPath.row)
                print("aryList======>>>>",aryList[indexPath.row])
                let roomobj = aryList[indexPath.row]["Item"] as? Details_RoomSaleObject
                cell.roomobj = roomobj ?? Details_RoomSaleObject()
               
                let isopend = kv?.value["isOpened"] as? Bool
                if isopend ?? false {
                    cell.btn_expand.isSelected = true
                    
                    let fxview = FXPieChartView.init(frame: CGRect(x: cell.frame.origin.x, y: cell.view_lablesBase.frame.size.height, width: cell.frame.width, height: 250))
                    var items = [Any]()
                    if let roomType = cell.roomobj?.RoomTypeName {
                        items = roomTypeDict[roomType]?["items"] as? [Any] ?? [Any]()
                    }
                    fxview.configureScrollItems(items:items )
                    cell.contentView.addSubview(fxview)
                }
                else {
                    cell.btn_expand.isSelected = false
                }
                cell.btn_expand.addTarget(self, action: #selector(expandDetailsTable2), for: .touchUpInside)

            }
            else if isFandBDetails{
                let cell = tableview_trendList.dequeueReusableCell(withIdentifier: "RoomDetailsTableViewCell") as! RoomDetailsTableViewCell
                cell.backgroundColor = .clear
                cell.layer.cornerRadius = 10
                cell.layer.masksToBounds = true
                cell.FnBDetails = aryFABlist[indexPath.row]
                cell.btn_expand.tag = indexPath.row
                let isopend = aryFABlist[indexPath.row].isOpened
                if isopend ?? false {
                    cell.btn_expand.isSelected = true
                    let fxview = FBExpandbleView.init(frame: CGRect(x: cell.frame.origin.x, y: cell.view_lablesBase.frame.size.height, width: tableView.frame.size.width, height: 300))
                    fxview.selectedIndex = self.slelectedsegmentIndexValue ?? 0
                    fxview.isFromCustome = false
                    if slelectedsegmentIndexValue == 1 {
                        fxview.fandBSalesDetails = fandb_detailsResponse.Response?.Day?.FNBSalesDetails?[indexPath.row] ?? FNBSalesDetailsObj()
                        fxview.cutomeSales = fandb_detailsResponse.Response?.Day?.FNBSalesData?[indexPath.row] ?? FAndBSalesDataObj()
                    }
                    else if slelectedsegmentIndexValue == 2 {
                        fxview.fandBSalesDetails = fandb_detailsResponse.Response?.Month?.FNBSalesDetails?[indexPath.row]
                        fxview.cutomeSales = fandb_detailsResponse.Response?.Month?.FNBSalesData?[indexPath.row] ?? FAndBSalesDataObj()
                    }
                    else if slelectedsegmentIndexValue == 3 {
                        fxview.fandBSalesDetails = fandb_detailsResponse.Response?.Year?.FNBSalesDetails?[indexPath.row] ?? FNBSalesDetailsObj()
                        fxview.cutomeSales = fandb_detailsResponse.Response?.Year?.FNBSalesData?[indexPath.row] ?? FAndBSalesDataObj()
                    }
                    cell.contentView.addSubview(fxview)
                }
                else {
                    cell.btn_expand.isSelected = false
                }
                cell.btn_expand.addTarget(self, action: #selector(expandDetailsTable2), for: .touchUpInside)
                return cell
            }
                // isNON_FNBDetails
            else {
                let cell = tableview_trendList.dequeueReusableCell(withIdentifier: "RoomDetailsTableViewCell") as! RoomDetailsTableViewCell
                
                cell.nonFABDetails = aryNFABlist[indexPath.row]
                return cell
            }
            return cell
        }
        else {
            let cell = tableview_detail.dequeueReusableCell(withIdentifier: "BubbleDetailTableViewCell") as! BubbleDetailTableViewCell
            cell.backgroundColor = .clear
            cell.layer.cornerRadius = 10
            cell.layer.masksToBounds = true
           // if self.fandb_detailsResponse.Response?.CurrencyCode ==
            cell.lbl_currencyCode.text = self.fandb_detailsResponse.Response?.CurrencyCode
            if cell.lbl_currencyCode.text == "" {
                cell.lbl_currencyCode.text = "INR"
            }
            print(cell.lbl_currencyCode.text ?? "INR")
            cell.selectedIndex = slelectedsegmentIndexValue ?? 0
            if isCustomDetailed  {
                cell.isFromCustome = true
                cell.cutomeSales = customeDetails
                
            }else {
                
                if slelectedsegmentIndexValue == 0 {
                    cell.isFromCustome = true
                    cell.cutomeSales = customeDetails
                    
                }
                else if slelectedsegmentIndexValue == 1 {
                    cell.todaySales = todaySalesObj
                    
                    
                }
                else if slelectedsegmentIndexValue == 2 {
                    cell.monthSales = monthSalesObj
                    
                    
                    
                } else if slelectedsegmentIndexValue == 3 {
                    cell.yearSales = yearSalesObj
                }
            }

            if isRoomSaleDetails {
                cell.isPie = true
                //cell.lbl_currencyCode.text = ""
                    cell.lbl_currencyCode.text = self.curncyCode
                if cell.lbl_currencyCode.text == nil {
                    cell.lbl_currencyCode.text = "INR"
                }
                cell.lbl_lyCdCmLyCurrencyCode.text = self.curncyCode
                //cell.lbl_revParCurrencyCode
//               if cell.lbl_lyCdCmLyCurrencyCode.text == nil {
//                    cell.lbl_lyCdCmLyCurrencyCode.text = "INR"
//                }
                
                var yValue = 0.0
                if cell.isPie {
                    DispatchQueue.main.async {
                        yValue  = Double(Float(cell.fxview?.frame.origin.y ?? 0.0) + Float(cell.fxview?.frame.height ?? 0.0 ))
                    cell.fxview?.configureScrollItems(items: CommonDataManager.shared.detailItems ?? [])
                        cell.roomsaleview?.btn_expand.tag = indexPath.row
                        cell.roomsaleview?.lbl_revParCurrencyCode.text = self.curncyCode
                        cell.roomsaleview?.lbl_aDRCurrencyCode.text = self.curncyCode
                        cell.roomsaleview?.btn_expand.addTarget(self, action: #selector(self.expandDetails), for: .touchUpInside)
                        if cell.roomsaleview?.btn_expand.isSelected ?? false {
                            cell.roomsaleview?.frame = CGRect(x: 0, y:Int(yValue), width: Int(cell.roomsaleview?.frame.width ?? 0), height:Int(cell.roomsaleview?.frame.height ?? 0))
                        }
                        else {
                            
                            cell.roomsaleview?.frame = CGRect(x:Int(cell.fxview?.frame.origin.x ?? 0), y:Int(cell.frame.height) - 80, width: Int(cell.roomsaleview?.frame.width ?? 0), height:Int(cell.roomsaleview?.frame.height ?? 0))
                            
                        }
                        //cell.roomsaleview?.updateViewData(details: self.details ?? RoomSaleDetailModel(), alos: self.alos ?? aLosModel())

                        if self.isCustomDetailed  || self.isLiveEnabled {
                            cell.roomsaleview?.updateRoomSaleData(details: self.customeDetails)
                            
                        }
                      else {
                        if self.slelectedsegmentIndexValue == 1{
                            //cell.todaySales = todaySales
                            cell.roomsaleview?.updateRoomSaleData(details: todaySalesObj)
                        }
                        
                        else if self.slelectedsegmentIndexValue == 2 {
                            cell.roomsaleview?.updateRoomSaleData(details: monthSalesObj)
                            
                        }
                        else if self.slelectedsegmentIndexValue == 3 {
                            cell.roomsaleview?.updateRoomSaleData(details: yearSalesObj)
                        }
                        }
                        
                    }
                    
                }
                
            }
            else if isnonFNBDetails {
                cell.isNonFnBDetails = true
                cell.lbl_currencyCode.text = self.curncyCode
                cell.lbl_lyCdCmLyCurrencyCode.text = self.curncyCode
                print(" cell.isBarBhartLoadedbefore is ====>",cell.isBarBhartLoadedbefore)
                if cell.isBarBhartLoadedbefore {
                    cell.updateBarchayData()
                }
            } else {
                // F&B Details
               if cell.isBarBhartLoadedbefore {
                    cell.updateBarchayData()
                }
            }
            return cell
        }
    }
}

extension RoomOcpcyViewController:CalendarDatePickerViewControllerDelegate{
    func didTapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        self.isCustomDetailed = true
        self.roomTypeDict.removeAll()
        self.aryDetailsList.removeAll()
            applyCoustomeDateWithRange(startDate: startDate, endDate: endDate)
        let str = "\(startDate.toString())-\(endDate.toString())"
        btn_custom.setTitle(str, for: .normal)
        btn_custom.titleLabel?.font = UIFont.FXRobotoRegular(size: 9) //systemFont(ofSize: 10)
        btn_custom.titleLabel?.numberOfLines = 2
        btn_custom.backgroundColor = UIColor.hexStringToUIColor(hex: "FC7A45")
        btn_custom.titleLabel?.textAlignment = .center
        //custom_DateSelector.titleLabel?.textColor = UIColor.white
        btn_custom.setTitleColor(.white, for: .normal)
        self.dismiss(animated: true, completion: nil)
        self.segment.tintColor = UIColor.lightGray
        self.segment.selectedSegmentIndex = -1
        self.segment.selectedSegmentTintColor = .none
        
    }
    func applyCoustomeDateWithRange(startDate: Date!, endDate: Date!) {
        if #available(iOS 13.0, *) {
            segment.selectedSegmentTintColor = .gray
        } else {
            // Fallback on earlier versions
        }
        var endPointUrl = ""
        if isCustomeOneSale {
            endPointUrl = "KPI/FxOneSaleDetail"
        }else if isCustomeFandBDetails {
            endPointUrl = "KPI/FXOneFNBSale"
        }else if isCustomenonFNBDetails {
            endPointUrl = "KPI/FXOneNonFNBSale"
        }
        print(endPointUrl)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let startacDate = formatter.string(from: startDate)
        let endacDate = formatter.string(from: endDate)
        print(endacDate)
        print(startacDate)
            loadingView.showLoader(text: "")
            let username = UserDefaults.standard.object(forKey: "User_Id") as! String
            let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginID":username,"PmsCustCode":pmsCode,"FromDate": startacDate,
                             "ToDate": endacDate,"CurrencyCode":"INR","ExchangeRate":1.0,"IsCustom": 1] as [String : Any]
        print(dicParams)
        //IsCustom  ToDate  FromDate
            //print()
            AF.request("\(CommonDataManager.shared.baseUrl)\(endPointUrl)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success:
                    print("getOnesaleDetailsData response",response)
                    loadingView.hideLoader()
                    do {
                        if self.isCustomeOneSale {
                        let response = try JSONDecoder().decode(Detail_RoomSaleModel.self, from: response.data ?? Data())
                        if response.Status == "Success" {
                            self.dicTotalRoomsale = response.Response?.Custom?.RoomSale ?? TotalRoomSaleModel()
                            self.dicRoomSaleDetails = response.Response?.Custom?.RoomSaleDetails ?? DetailsRoomSaleDetail()
                            self.customeDetails = response.Response?.Custom?.DashBoard?.first ?? RoomSaleCustomModel()
                            // Currency Code
                            if let currency_code = response.Response?.CurrencyCode {
                                self.curncyCode = currency_code
                            }
                            self.parseTabsData()
                            //self.aryList[0].updateValue(roolmDetailDic, forKey: "DetailRoomDic")
                            
                        }
                        else{
                            
                            UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                            }
                        }
                        if self.isCustomeFandBDetails {
                            let results = try JSONDecoder().decode(FANBDetailModel.self, from: response.data ?? Data())
                            if results.Status == "Success" {
                                self.fandb_detailsResponse = results
                                self.aryFABlist = results.Response?.Custom?.FNBSalesData ?? []
                                self.customeDetails = results.Response?.Custom?.DashBoard?.first ?? RoomSaleCustomModel()
                            CommonDataManager.shared.ary_FandBDetailsList = self.aryFABlist
                                
                                self.parseTabsData()
                                if let accountDate_Today = GlobalAccountDateData.shared.accountDate_Global {
                                    self.accounttingDate = accountDate_Today
                                }
                                if let accountDate_Live = GlobalAccountDateData.shared.accountDate_Global_Live {
                                    self.dAcDate = accountDate_Live
                                }
                                self.segmentTitleConversion()
                            }
                        }
                        if self.isCustomenonFNBDetails {
                            let results = try JSONDecoder().decode(NonFANBDetailModel.self, from: response.data ?? Data())
                            if results.Status == "Success" {
                                self.aryNFABlist = results.Response?.NonFNBSalesData?.Custom?.NonFNBSalesData ?? [] //results.Response?.NonFNBSalesData?.Custom ?? []
                                self.customeDetails = results.Response?.NonFNBSalesData?.Custom?.DashBoard?.first ?? RoomSaleCustomModel()
                                CommonDataManager.shared.ary_nonFandBDetailsList = self.aryNFABlist
                                self.parseTabsData()
                                if let variableValue = GlobalAccountDateData.shared.accountDate_Global {
                                    var date_acct : String?
                                    date_acct = variableValue
                                }
                            }
                            else{
                                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: results.StatusDescription ?? "", title: "")
                            }

                        }
                    }
                    catch let error {
                        #if DEBUG
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                        #endif
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    loadingView.hideLoader()
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                }
            }
            
    }
        
}

extension Sequence {
    func removingDuplicates<T: Hashable>(withSame keyPath: KeyPath<Element, T>) -> [Element] {
        var seen = Set<T>()
        return filter { element in
            guard seen.insert(element[keyPath: keyPath]).inserted else { return false }
            return true
        }
    }
}
