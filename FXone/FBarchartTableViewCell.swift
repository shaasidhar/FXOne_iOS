//
//  FBarchartTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 02/06/22.
//  Copyright © 2022 IDS. All rights reserved.
//

import UIKit
import Charts


class FBarchartTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var barChartView: BarChartView!
    var colors = Colors.chartColors()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        shadowView.layer.cornerRadius = 10
        shadowView.layer.masksToBounds = true
        shadowView.dropShadow()
        self.backgroundColor = .clear
    }
    var aryChartlbls = [String]()
    var total = 0.0
    var entriesData: [FExpensePieChartModel]? {
        didSet {
            updateDate()
        }
    }
    func updateDate() {
        
        var entries = [BarChartDataEntry]()
        let count = entriesData?.count ?? 0
        if count == 0 {
            return
        }
        for x in 0..<count {
            let each = entriesData?[x]
            aryChartlbls.append(each?.accountMasterName ?? "")
            let value = Double(each?.amount ?? "") ?? 0.0
            total += value
            entries.append(BarChartDataEntry(x: Double(x), y: Double(value)))
        }
        let set = BarChartDataSet(entries)
        set.colors = colors
        
        self.barChartView?.xAxis.valueFormatter = IndexAxisValueFormatter(values:aryChartlbls)
        self.barChartView?.setVisibleXRangeMaximum(6)
        self.barChartView?.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        self.barChartView?.setVisibleXRangeMaximum(6)
        
        self.barChartView?.drawBarShadowEnabled = false
        self.barChartView?.drawValueAboveBarEnabled = false
        
        self.barChartView?.maxVisibleCount = 60
        
        let xAxis = self.barChartView!.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1
        xAxis.labelCount = 7
        //xAxis.valueFormatter = DayAxisValueFormatter(chart: self.barChartView!)
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.minimumFractionDigits = 0
        leftAxisFormatter.maximumFractionDigits = 1
        leftAxisFormatter.negativeSuffix = ""
        leftAxisFormatter.positiveSuffix = ""
        
        let leftAxis = barChartView!.leftAxis
        leftAxis.labelFont = .systemFont(ofSize: 10)
        leftAxis.labelCount = 8
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        leftAxis.labelPosition = .outsideChart
        leftAxis.spaceTop = 0.15
        leftAxis.axisMinimum = 0 // FIXME: HUH?? this replaces startAtZero = YES
        
        let rightAxis = barChartView!.rightAxis
        rightAxis.enabled = false
        rightAxis.labelFont = .systemFont(ofSize: 10)
        rightAxis.labelCount = 8
        rightAxis.valueFormatter = leftAxis.valueFormatter
        rightAxis.spaceTop = 0.15
        rightAxis.axisMinimum = 0
        
        let l = barChartView!.legend
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .circle
        l.formSize = 9
        l.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
        l.xEntrySpace = 4

        
        
        let data = BarChartData(dataSet: set)
        barChartView.data = data
    }
    
    func salesAmount(sales: FExpenseModel)  {
        self.lblTotal.text = CommonDataManager.shared.suffixNumber(number: sales.Sales ?? 0.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
