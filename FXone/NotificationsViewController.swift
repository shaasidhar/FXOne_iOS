//
//  NotificationsViewController.swift
//  FXone
//
//  Created by Vinayak on 27/04/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    @IBOutlet weak var scrollview_Items: UIScrollView!
    
    @IBOutlet weak var scrollview_list: UIScrollView!
    
    var ary_Notificationtabs = [NotificationTabs]()
    var ary_Current_List = [NotificationObject]()
    var ary_listOfTableViews = [UITableView]()
    var ary_listOfCurrent_AryList = [[NotificationObject]]()
    
    var selecTedModule = 1000
    var previousItem = 1000
    var selectedModuleName = ""
    let stripView = UIView(frame: CGRect(x:0, y:0, width:0,height:0))
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var tab1 = NotificationTabs()
        tab1.key = "FXFrontDesk"
        tab1.value = "FX Front Desk"
        tab1.count = 20
        
        var tab2 = NotificationTabs()
        tab2.key = "FXPOS"
        tab2.value = "FX POS"
        tab2.count = 5
        
        var tab3 = NotificationTabs()
        tab3.key = "FXHouseKeeping"
        tab3.value = "FX HouseKeeping"
        tab3.count = 15
        
        var tab4 = NotificationTabs()
        tab4.key = "FXGuestService"
        tab4.value = "FXGuestService"
        tab4.count = 2
        
        var tab5 = NotificationTabs()
        tab5.key = "FXGuestService"
        tab5.value = "FX GuestService"
        tab5.count = 45
        ary_Notificationtabs.append(tab1)
        ary_Notificationtabs.append(tab2)
        ary_Notificationtabs.append(tab3)
        ary_Notificationtabs.append(tab4)
        ary_Notificationtabs.append(tab5)
        
        var notifi = NotificationObject()
        notifi.title = "Room Upgrade"
        notifi.value = "3 Room are upgrated from standared delux "
        notifi.time = "3 mins ago"
        notifi.value_list = ["Martin henges reg #1213113 upgraded to delux","Mart Luther reg #1213113 upgraded to delux","Richard parker reg #1213113 upgraded to delux"]
        notifi.slide_open = true
        
        var notifi1 = NotificationObject()
        notifi1.title = "VIP Arrival"
        notifi1.value = "2 VIP's are arriving today"
        notifi1.time = "5 mins ago"
        notifi1.slide_open = false
        
    
        ary_Current_List.append(notifi)
        ary_Current_List.append(notifi1)
        
        
        
        self.navigationItem.title = "Notifications"
        self.navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.hexStringToUIColor(hex: "FF8E00")
        self.configureMainScrollViewItems(items:ary_Notificationtabs)
        self.scrollview_list.isPagingEnabled = true
        
        // Do any additional setup after loading the view.
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        
        selecTedModule = Int(self.scrollview_list.contentOffset.x / self.scrollview_list.bounds.size.width)
        selecTedModule += 1000
        print(selecTedModule)
        let tmpButton = self.view.viewWithTag(selecTedModule ) as? UIButton
        selectedScrollItem(tmpButton!)
        
    }
    func configureMainScrollViewItems(items:[NotificationTabs]) {
        var xPos = 0
        var slideScrollFrame = self.scrollview_list.frame
        self.scrollview_list.contentSize = CGSize(width: self.view.frame.width * CGFloat(items.count), height: 1)
        self.scrollview_list.backgroundColor = .clear
        scrollview_list.delegate = self
        scrollview_list.showsHorizontalScrollIndicator = false
        scrollview_list.showsVerticalScrollIndicator = false
        
        
        for i in 0 ..< items.count
        {
            ary_listOfCurrent_AryList.append(ary_Current_List)
            let itemButton = UIButton(type: UIButton.ButtonType.custom)
            itemButton.setTitle(items[i].value ?? "", for:.normal)
            itemButton.accessibilityLabel = items[i].key ?? ""
            itemButton.setTitleColor(UIColor.lightGray, for:.normal)
            itemButton.tag=i+1000
            itemButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            let btnsize: CGSize = (itemButton.titleLabel?.text!.size(withAttributes: [kCTFontAttributeName as NSAttributedString.Key: UIFont.systemFont(ofSize:14)]))!
            itemButton.frame = CGRect(x: xPos, y: 0, width: Int(btnsize.width + 40), height: Int(self.scrollview_Items.frame.size.height))
            itemButton.addTarget(self, action: #selector(NotificationsViewController.selectedScrollItem(_:)), for:.touchUpInside)
            
            let lblCount = UILabel(frame: CGRect(x: itemButton.frame.width - 15, y: (itemButton.frame.height / 2 ) - 10 , width: 20, height: 20))
            lblCount.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
            lblCount.text = "\(items[i].count ?? 0)"
            lblCount.layer.cornerRadius = lblCount.frame.height / 2
            lblCount.layer.masksToBounds = true
            lblCount.textAlignment = .center
            //lblCount.sizeToFit()
            lblCount.font = UIFont.systemFont(ofSize: 8)
            lblCount.textColor = UIColor.white
            itemButton.addSubview(lblCount)
            itemButton.backgroundColor = UIColor.clear
            self.scrollview_Items.backgroundColor = UIColor.clear
            self.scrollview_Items.addSubview(itemButton)
            self.scrollview_Items.showsHorizontalScrollIndicator = false
            self.scrollview_Items.showsVerticalScrollIndicator = false
            
            if selecTedModule == itemButton.tag
            {
                var tmpButton = self.view.viewWithTag(selecTedModule) as? UIButton
                tmpButton?.setTitleColor(UIColor.hexStringToUIColor(hex: "FF8E00"), for:.normal)
                selectedModuleName = tmpButton?.accessibilityLabel ?? ""
                let screenSize: CGRect = UIScreen.main.bounds
                stripView.frame  =  CGRect(x:(tmpButton?.frame.origin.x)! , y: (tmpButton?.frame.size.height)! + (tmpButton?.frame.origin.y)!-3, width: (tmpButton?.frame.size.width)! , height:3)
                stripView.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
                self.scrollview_Items.addSubview(stripView)
            }
            
            let listTableview = UITableView()
            listTableview.backgroundColor = .clear

            listTableview.frame = CGRect(x:(self.view.frame.width  * CGFloat(i) ) + 10, y: 10 , width:self.view.frame.size.width - 20, height:self.view.frame.size.height  - scrollview_list.frame.origin.y)
            listTableview.tableFooterView = UIView()
            listTableview.backgroundColor = .clear
            listTableview.separatorStyle = .singleLine
            listTableview.rowHeight = UITableView.automaticDimension
            self.navigationController?.toolbar.isTranslucent = false;
            listTableview.layer.cornerRadius = 10.0
            listTableview.layer.masksToBounds = true
            // registering your nib
            let jobCellNib = UINib(nibName: "NotificationListTableViewCell", bundle: nil)
            listTableview.register(jobCellNib, forCellReuseIdentifier: "NotificationCell")
            listTableview.allowsSelection = true
            
            
            let expandCell = UINib(nibName: "ExpandingTableViewCell", bundle: nil)
            listTableview.register(expandCell, forCellReuseIdentifier: "Expand")

            self.ary_listOfTableViews.append(listTableview)
            self.scrollview_list.addSubview(listTableview)
            
            xPos = Int((itemButton.frame.origin.x) + (itemButton.frame.size.width + 5 ))
            self.scrollview_Items.contentSize = CGSize(width: CGFloat(xPos), height: self.scrollview_Items.frame.size.height)
            
            
            
        }
        let tmpButton : UIButton = self.view.viewWithTag(selecTedModule) as! UIButton
        self.scrollview_Items.scrollRectToVisible((tmpButton.frame), animated: true)
        let tableViewObj = self.ary_listOfTableViews[selecTedModule - 1000]
        tableViewObj.reloadData()
        
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
        slideScrollFrame.origin.x = slideScrollFrame.size.width * CGFloat(selecTedModule - 1000)
        slideScrollFrame.origin.y = 0
        
        print(tableViewObj.frame)
        print(slideScrollFrame)
        self.scrollview_list.scrollRectToVisible(slideScrollFrame, animated:true)
        
    }
    
    @objc func selectedScrollItem(_ sender : UIButton) {
        print(sender.tag)
        var tmpButton : UIButton?
        var btnDeselected : UIButton?
        if previousItem != sender.tag{
            
            var slideScrollFrame = self.scrollview_list.frame
            
        
            tmpButton = self.view.viewWithTag(sender.tag) as! UIButton
            tmpButton?.setTitleColor(UIColor.orange, for: .normal)
            
            btnDeselected = self.view.viewWithTag(previousItem) as! UIButton
            btnDeselected?.setTitleColor(UIColor.lightGray, for: .normal)
            
            
            self.scrollview_Items.scrollRectToVisible((tmpButton?.frame)!, animated: true)
            selectedModuleName = tmpButton?.accessibilityLabel ?? ""
            let screenSize: CGRect = UIScreen.main.bounds
            stripView.frame  =  CGRect(x:(tmpButton?.frame.origin.x)! , y: (tmpButton?.frame.size.height)! + (tmpButton?.frame.origin.y)!-3, width: (tmpButton?.frame.size.width)! , height:3)
            stripView.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
            self.scrollview_Items.addSubview(self.stripView)
            selecTedModule = sender.tag
            previousItem = sender.tag
            
            let tableViewObj = self.ary_listOfTableViews[selecTedModule - 1000]
            tableViewObj.delegate = self
            tableViewObj.dataSource = self
            tableViewObj.reloadData()
        
            
            slideScrollFrame.origin.x = slideScrollFrame.size.width * CGFloat(selecTedModule - 1000)
            slideScrollFrame.origin.y = scrollview_list.frame.origin.y
            print(tableViewObj.frame)
            print(slideScrollFrame)
            self.scrollview_list.scrollRectToVisible(slideScrollFrame, animated:true)
        }
    }

}

extension NotificationsViewController  : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.ary_Current_List.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  ary_Current_List[indexPath.section].slide_open ?? false
        {
            ary_Current_List[indexPath.section ].slide_open = false
            let sections = IndexSet.init(integer: indexPath.section )
            tableView.reloadSections(sections, with: .none)
        }
        else {
            
            ary_Current_List[indexPath.section ].slide_open = true
            let sections = IndexSet.init(integer: indexPath.section )
            tableView.reloadSections(sections, with: .none)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  ary_Current_List[section].slide_open == true
        {
            return (ary_Current_List[section].value_list?.count ?? 0  ) + 1
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var viewObj = UIView()
        
        viewObj = UIView(frame: CGRect(x:0,y:0,width:tableView.frame.width,height:10))
            viewObj.backgroundColor = .clear
        
        return viewObj
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dataIndex = indexPath.row - 1
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationListTableViewCell
            cell.lbl_time.text = ary_Current_List[indexPath.section].time ?? ""
            cell.lbl_title.text = ary_Current_List[indexPath.section].title ?? ""
            cell.lbl_description.text = ary_Current_List[indexPath.section].value ?? ""
            if  ary_Current_List[indexPath.section].slide_open ?? false
            {
                //cell.btn_expand.setTitle("-", for: .normal)
               cell.btn_expand.setImage(UIImage.init(named: "up"), for: .normal)
                // up arrow
            }
            else{
               // cell.btn_expand.setTitle("+", for: .normal)
                cell.btn_expand.setImage(UIImage.init(named: "down"), for: .normal)
                // down arrow
            }
            return cell
            
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Expand", for: indexPath) as! ExpandingTableViewCell
            
            cell.lbl_title.text = self.ary_Current_List[indexPath.section].value_list?[dataIndex] ?? ""
            return cell
        }
            
    }
}
