//
//  ChartLegendsCollectionViewCell.swift
//  FXone
//
//  Created by Vinayak on 7/17/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import MarqueeLabel

class ChartLegendsCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var lbl_statuscolor: UILabel!
    //@IBOutlet weak var lbl_statusname: UILabel!
    @IBOutlet weak var lbl_statusname: MarqueeLabel!
    @IBOutlet weak var lbl_count: UILabel!
    
    struct RoomACSAlter {
        var name :String?
        var count :String?
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // MarqueeLabel
        if lbl_statusname.text?.count ?? 0 >= 10 {
            lbl_statusname.type = .continuous
            lbl_statusname.speed = .duration(10.0)
            lbl_statusname.animationCurve = .easeInOut
            lbl_statusname.fadeLength = 10.0
            lbl_statusname.leadingBuffer = 5.0
            lbl_statusname.trailingBuffer = 20.0
        }
    }
    
    var statusObj : RoomACS?{
        
        didSet{
            self.updateCellData()
            
        }
    }
    
    func updateCellData()
    {
        print(statusObj ?? "")
        lbl_statusname.text = statusObj?.name        
        lbl_count.text = statusObj?.count
        lbl_statuscolor.backgroundColor = statusObj?.statusColor ?? UIColor.white
       
    }
    func getRandomColor() -> UIColor {
        //Generate between 0 to 1
        let red:CGFloat = CGFloat(drand48())
        let green:CGFloat = CGFloat(drand48())
        let blue:CGFloat = CGFloat(drand48())
        
        return UIColor(red:red, green: green, blue: blue, alpha: 1.0)
    }

}
