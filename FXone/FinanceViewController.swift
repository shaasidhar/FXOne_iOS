//
//  FinanceViewController.swift
//  FXone
//
//  Created by Vinayak on 01/06/22.
//  Copyright © 2022 IDS. All rights reserved.
//

import UIKit
import Alamofire
import MarqueeLabel


class FinanceViewController: UIViewController {

    
    @IBOutlet weak var navigationView: UIView!
    
    @IBOutlet weak var financeTableview: UITableView!
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var btnCustom: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lbl_PropName: MarqueeLabel!
    @IBOutlet weak var navigationViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var logo_IconFX: UIImageView!
    
    var sideBarView : SideMenuView!
    var selectedSegmentIndex = 0
    var filteredFinancialYearObject: FinanceYearResponseModel?
    var aryEntries: [FExpensePieChartModel]? = [FExpensePieChartModel]()
    var salesEntries: [FExpensePieChartModel]? = [FExpensePieChartModel]()
    var dashBoardTypeListObj = [[String:Any]]()
    var dashboardData = JSON()
    var cashAmount = 0.0
    var bankAmount = 0.0
    var FEExpense =  FExpenseModel()
    var cashandBankModel =  CashandBankModel()
    var cashandBankArrModel =  [CashandBankModel]()
    var navigateToHome = NavigateToHome.RoomOccupancy
    var listAry = [RoomStatusByDay]()
    var totalProperties = [PropertyModel]()
    var isFinanceDashboard = false
    var mainList = [[String:Any]]()
    var ispropertySelected : Bool?
    var isGroupPropertySelected : Bool?
    var ary_listofRAStatus = [RoomACS]()
    let colorCode = UserDefaults.standard.object(forKey: "Color_Code") as! String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNibFiles()
        getFinancialYearData()
        marqueeLabel()
        self.getAppLogoImage()
        self.getSingleProperty()
        switch self.navigateToHome {
        case .Finance:
            self.navigationViewHeightConstraint.constant = 135
        default:
            self.navigationViewHeightConstraint.constant = 100
        }
        //self.logo_IconFX.layer.cornerRadius = 5
    }
    
    func getAppLogoImage() {
        if let image_logoURL = UserDefaults.standard.object(forKey: "imageLogo_URL") as? String {
            let imageUrl = URL(string: image_logoURL)
            let urlRequest = URLRequest(url: imageUrl ?? URL(fileURLWithPath: ""))
            let dataTask = URLSession.shared.dataTask(with: urlRequest, completionHandler: { data, response, error in
                if let imageData = data {
                    DispatchQueue.main.async {
                        self.logo_IconFX.image = UIImage(data: imageData)
                    }
                }
            })
            
            dataTask.resume()
        }
    }
    
    // for the time being commented, app is crashing 20/02/2024 by srm
    func marqueeLabel(){
        lbl_PropName.type = .continuous
        lbl_PropName.speed = .duration(10.0)
        lbl_PropName.animationCurve = .easeInOut
        lbl_PropName.fadeLength = 10.0
        lbl_PropName.leadingBuffer = 5.0
        lbl_PropName.trailingBuffer = 20.0
        lbl_PropName.text = UserDefaults.standard.object(forKey: "Hotel_Name") as? String
    }
    
    func getSingleProperty() {
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        //let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginId":username]//,"PmsCustCode":pmsCode]
        AF.request("\(CommonDataManager.shared.baseUrl)\(SINGLEPROPERTY)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print(response)
                do {
                    let response = try JSONDecoder().decode(SinglePropertyModel.self, from: response.data ?? Data())
                    if response.Status == "Success" {
                        if let groupProperties = response.Response {
                            self.parseGroupProperties(list: [groupProperties])//comment : single group to ary of group
                        }
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    
    func parseGroupProperties(list:[PropertyResponseObj]?){
        
            for each in list ?? [] {
            var property1 = PropertyModel()
            property1.title = each.GroupDetails?.GroupName ?? ""
            //In case all properties selected (All properties in the group)
            if isGroupPropertySelected ?? false {
                 property1.selected = true
            }
            else {
                property1.selected = false
            }
            // show Group default selected in check box
            
            var ary = [Property1Model]()
            for property in each.PropertyDetails ?? [] {
                var p = Property1Model()
                p.title = property.PropertyName
                p.titlePms = property.PmsCustCode
                
                if ispropertySelected ?? false {
                   p.selected = true
                }
                else {
                    p.selected = false

                }
                //In case few property OR properties selected in the gourp
                 // show Property default selected in check box// here is the line show privious selectoion
                ary.append(p)
            }
            property1.property = ary
            totalProperties.append(property1)
            
            //totalProperties.append(contentsOf: totalProperties)
        }
        if totalProperties.count > 0 {
            totalProperties[0].property?[0].selected = true
        }

    }

    func getFinancialYearData(){
        loadingView.showLoader(text: "")
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let productId = UserDefaults.standard.object(forKey: "Product_Id") as! Int
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        let dic_headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["UserId":username,"PmsCustCode":pmsCode,"ProductId":"\(productId)"]
        //let url = CommonDataManager.shared.baseUrl + GET_FINANCIALYEARDATA
        AF.request("https://fxfasprodapi.idsnext.live/api/TransactionType/financeYear" /*"\(CommonDataManager.shared.baseUrl2)\(FINANCEYEAR)"*/, method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case.success:
               
                loadingView.hideLoader()
                do {
                    let response = try JSONDecoder().decode(FinanceYearModel.self, from: response.data ?? Data())
                    print("FinancialYearData: \(response)")
                    let count = response.response?.count ?? 0
                    for i in 0..<count {
                        let each = response.response?[i]
                        let startDateString = each?.startDate ?? ""
                        let endDateString = each?.endDate ?? ""
                        
                        let stDate = startDateString.getDateFromString(isoDate: startDateString)
                        let endDate = endDateString.getDateFromString(isoDate: endDateString)
                        let date = Date()
                        if date.isBetween(stDate, and: endDate) {
                            self.filteredFinancialYearObject = each
                        }
                    
                    }
                    self.getAgeingDefinitionDetail()
                
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                loadingView.hideLoader()
                
            }
        }
        
    }
    
    func getAgeingDefinitionDetail() {
        loadingView.showLoader(text: "")

        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let productId = UserDefaults.standard.object(forKey: "Product_Id") as! Int
        let defaultGroupCode = UserDefaults.standard.object(forKey: "DefaultGroupCode") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        let dic_headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["UserId": username,
                         "ProductId": "\(productId)",
                         "PropertyGroupCode": defaultGroupCode,
                         "PmsCustCode": pmsCode,
                         "AgeingFor": ""]
        //let url = CommonDataManager.shared.baseUrl + GET_FINANCIALYEARDATA
        //"\(CommonDataManager.shared.baseUrl)\(AccountDateNightAudit)")
        AF.request("https://fxfasprodapi.idsnext.live/api/Ageing/GetAgeingDefinitionDetail"/*\(CommonDataManager.shared.baseUrl2)\(AGEINGDEFINATION)*/, method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case.success:
               
                loadingView.hideLoader()
                do {
                    let response = try JSONDecoder().decode(AgeingFinanceYearResponseModel.self, from: response.data ?? Data())
                    print("AgeingDefinitionDetail: \(response)")
                    let todayDate = Date().getDateYYYYMMDDString(date: Date())
                    var aryAginglist1 = [[String:Any]]()
                    var aryAginglist2 = [[String:Any]]()
                    var dicAge1 = [String:Any]()
                
                    dicAge1.updateValue(0, forKey: "srlNum")
                    dicAge1.updateValue(0, forKey: "frmAge")
                    dicAge1.updateValue(0, forKey: "tooAge")
                    dicAge1.updateValue("days", forKey: "txtAge")
                    dicAge1.updateValue("CURRENT", forKey: "columnName")
                    
                    aryAginglist1.append(dicAge1)
                    aryAginglist2.append(dicAge1)
                    var dic = [String:Any]()
                    dic = ["DashBoardType": "Payable",
                    "DatePeriod": 0,
                    "FromDate": "2023-04-01",//todayDate,
                    "ToDate": "2024-03-31",//todayDate,
                    "PreviousFromDate": todayDate,
                    "PreviousToDate": todayDate,
                    "AsOnDate": todayDate,
                    "IsSelected": true,
                    "RecordDetailType": ""]
                    
                    var dic2 = [String:Any]()
                    dic2 = ["DashBoardType": "Receivable",
                    "DatePeriod": 0,
                    "FromDate": "2023-04-01",//todayDate,
                    "ToDate": "2024-03-31",//todayDate,
                    "PreviousFromDate": todayDate,
                    "PreviousToDate": todayDate,
                    "AsOnDate": todayDate,
                    "IsSelected": true,
                    "CompareTo": "",
                    "RecordDetailType": ""]
                    
                    for each in response.response ?? [] {
                
                        if each.ageingfor == "Supplier" {
                            var dicAging = [String:Any]()
                        
                            dicAging.updateValue(each.srlNum ?? 0, forKey: "srlNum")
                            dicAging.updateValue(each.frmAge ?? 0, forKey: "frmAge")
                            dicAging.updateValue(each.tooAge ?? 0, forKey: "tooAge")
                            dicAging.updateValue(each.txtAge ?? "", forKey: "txtAge")
                            dicAging.updateValue("\(each.frmAge ?? 0)-\(each.tooAge ?? 0) \(each.txtAge ?? "")", forKey: "columnName")
                            aryAginglist1.append(dicAging)
                        } else if each.ageingfor == "Customer" {
                            var dicAging = [String:Any]()
                            
                            dicAging.updateValue(each.srlNum ?? 0, forKey: "srlNum")
                            dicAging.updateValue(each.frmAge ?? 0, forKey: "frmAge")
                            dicAging.updateValue(each.tooAge ?? 0, forKey: "tooAge")
                            dicAging.updateValue(each.txtAge ?? "", forKey: "txtAge")
                            dicAging.updateValue("\(each.frmAge ?? 0)-\(each.tooAge ?? 0) \(each.txtAge ?? "")", forKey: "columnName")
                            aryAginglist2.append(dicAging)
                        }
                    }
                    dic.updateValue(aryAginglist1, forKey: "AgeingPeriodList")
                    dic2.updateValue(aryAginglist2, forKey: "AgeingPeriodList")
                    self.dashBoardTypeListObj.append(dic)
                    self.dashBoardTypeListObj.append(dic2)
                    
                    var dic3 = [String:Any]()
                    dic3 = ["DashBoardType": "CashandBank",
                            "DatePeriod": 0,
                            "FromDate": "2023-04-01",//todayDate,
                            "ToDate": "2024-03-31",//todayDate,
                            "PreviousFromDate": todayDate,
                            "PreviousToDate": todayDate,
                            "AsOnDate": todayDate,
                            "IsSelected": true,
                            "RecordDetailType": "",
                            "CompareTo": "",
                            "AgeingPeriodList": []]
                    var dic4 = [String:Any]()
                    dic4 = ["DashBoardType": "Sales",
                            "DatePeriod": 9,
                            "FromDate": "2023-04-01",//todayDate,
                            "ToDate": "2024-03-31",//todayDate,
                            "PreviousFromDate": todayDate,
                            "PreviousToDate": todayDate,
                            "AsOnDate": todayDate,
                            "IsSelected": true,
                            "RecordDetailType": "Comparision",
                            "CompareTo": "MONTH",
                            "AgeingPeriodList": []]
                    
                    var dic5 = [String:Any]()
                    dic5 = ["DashBoardType": "Expense",
                            "DatePeriod": 9,
                            "FromDate": "2023-04-01",//todayDate,
                            "ToDate": "2024-03-31",//todayDate,
                            "PreviousFromDate": todayDate,
                            "PreviousToDate": todayDate,
                            "AsOnDate": todayDate,
                            "IsSelected": true,
                            "RecordDetailType": "Top",
                            "CompareTo": "MONTH",
                            "AgeingPeriodList": []]
                    
                    var dic6 = [String:Any]()
                    dic6 = ["DashBoardType": "ProfitandLoss",
                            "DatePeriod": 9,
                            "FromDate": "2023-04-01",
                            "ToDate": "2024-03-31",//todayDate,
                            "PreviousFromDate": todayDate,
                            "PreviousToDate": todayDate,
                            "AsOnDate": todayDate,
                            "IsSelected": true,
                            "RecordDetailType": "",
                            "CompareTo": "MONTH",
                            "AgeingPeriodList": []]
                    
                    self.dashBoardTypeListObj.append(dic3)
                    self.dashBoardTypeListObj.append(dic4)
                    self.dashBoardTypeListObj.append(dic5)
                    self.dashBoardTypeListObj.append(dic6)
                    
                    self.getDashBoardData()
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                loadingView.hideLoader()
                
            }
        }
        
    }
    func getDashBoardData(){
        loadingView.showLoader(text: " ")

        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let productId = UserDefaults.standard.object(forKey: "Product_Id") as! Int
        let defaultGroupCode = UserDefaults.standard.object(forKey: "DefaultGroupCode") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        let dic_headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        
        
        let startDateString = filteredFinancialYearObject?.startDate ?? ""
        let stDate = startDateString.getDateFromString(isoDate: startDateString)
        let st = stDate.getDateMMDDYYString(date: stDate)
        
        let dicParams: [String:Any] = ["UserId": username,
                         "ProductId": "\(productId)",
                         "PropertyGroupCode": "\(defaultGroupCode)",
                         "PmsCustCode": [pmsCode],
                         "FinancialStartDate": st,
                        "DashBoardTypeList": dashBoardTypeListObj]
        //let url = CommonDataManager.shared.baseUrl + GET_FINANCIALYEARDATA
        AF.request("https://fxfasprodapi.idsnext.live/api/DashBoard/GetDashboardDetails"/*\(CommonDataManager.shared.baseUrl2)\(FINANCEDASHBORADDETAILS)*/, method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case.success:
               
                loadingView.hideLoader()
               
                if let json = response.data {
                    let data = JSON(data: json)
                    print("Fas DashBoardData: \(data)")
                    DispatchQueue.main.async {
                        self.dashboardData = data
                        self.getFExpenseDetails(dashboardData: self.dashboardData)
                        self.financeTableview.reloadData()
                    }
                   
                    
                
                }
            case .failure(let error):
                print(error.localizedDescription)
                loadingView.hideLoader()
                
            }
        }
        
    }
    func getFExpenseDetails(dashboardData: JSON) {
        
        let expenseDetails = dashboardData.dictionaryValue["response"]?.dictionaryValue["ExpenseDetail"]?.arrayValue
        
        FEExpense.Expense = dashboardData.dictionaryValue["response"]?.dictionaryValue["Expense"]?.doubleValue ?? 0.0
//        "Expense": 31376073.730000,
//        "Expense": 31376073.730000,

        let count = expenseDetails?.count ?? 0
        for i in 0..<count {
            let each = expenseDetails?[i].dictionaryValue
            var item = FExpensePieChartModel()
            item.amount = each?["Amount"]?.stringValue ?? ""
            item.accounttCode = each?["AccountCode"]?.stringValue ?? ""
            item.accountMasterName = each?["AccountMasterName"]?.stringValue ?? ""
            self.aryEntries?.append(item)
        }
        // Cash and bank amount
        var bankAccount = dashboardData.dictionaryValue["response"]?.dictionaryValue["CashandBank"]?.arrayValue
        let count1 = bankAccount?.count ?? 0
        
        for i in 0..<count1 {
            let each = bankAccount?[i].dictionaryValue
            let key = each?["GeneralLedgerType"]?.stringValue ?? ""
            let amount = each?["Amount"]?.stringValue ?? ""
            let accountCode = each?["AccountCode"]?.stringValue ?? ""
            let accountMasterName = each?["AccountMasterName"]?.stringValue ?? ""
            cashandBankModel.amount = amount
            cashandBankModel.accounttCode = accountCode
            cashandBankModel.accountMasterName = accountMasterName
            cashandBankModel.generalLedgerType = key
            cashandBankArrModel.append(cashandBankModel)
            if key.uppercased() == "BANK" {
                let a = Double(amount) ?? 0.0
                bankAmount += a
            } else if key.uppercased() == "CASH" {
                let a = Double(amount) ?? 0.0
                cashAmount += a
            }
        }
        // Sales data
        let salesDetails = dashboardData.dictionaryValue["response"]?.dictionaryValue["SalesDetail"]?.arrayValue
        //Sales
        FEExpense.Sales = dashboardData.dictionaryValue["response"]?.dictionaryValue["Sales"]?.doubleValue
        let count2 = salesDetails?.count ?? 0
        for j in 0..<count2 {
            let each = salesDetails?[j].dictionaryValue
            var item = FExpensePieChartModel()
            item.amount = each?["Amount"]?.stringValue ?? ""
            item.accountMasterName = each?["HeadName"]?.stringValue ?? ""
            self.salesEntries?.append(item)
        }
        
        financeTableview.reloadData()
    }
    
    func registerNibFiles() {
        financeTableview.sectionHeaderTopPadding = 0
        financeTableview.separatorStyle = .none
        financeTableview.showsVerticalScrollIndicator  = false
        
//        self.financeTableview.register(UINib.init(nibName: "FBankAccountTableViewCell", bundle: nil), forCellReuseIdentifier: "FBankAccountTableViewCell")
        self.financeTableview.register(UINib.init(nibName: "BankCashTableViewCell", bundle: nil), forCellReuseIdentifier: "BankCashTableViewCell") 
        
        self.financeTableview.register(UINib.init(nibName: "FProfitLoassTableViewCell", bundle: nil), forCellReuseIdentifier: "FProfitLoassTableViewCell")
        
        
        self.financeTableview.register(UINib.init(nibName: "FPayableTableViewCell", bundle: nil), forCellReuseIdentifier: "FPayableTableViewCell")
        
        self.financeTableview.register(UINib.init(nibName: "FPieChartTableViewCell", bundle: nil), forCellReuseIdentifier: "FPieChartTableViewCell")
        
        self.financeTableview.register(UINib.init(nibName: "FBarchartTableViewCell", bundle: nil), forCellReuseIdentifier: "FBarchartTableViewCell")
    }
    @IBAction func showMenu(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        print("Menu Button Tapped....")
        sideBarView = SideMenuView(frame:  CGRect(x:50, y:0, width: self.view.frame.width - 50, height: self.view.frame.height), viewController: self,navigateTo: "Finance")

        sideBarView.delegate = self
    }
    
    @IBAction func propertyAction(_ sender: Any) {
    
        let selectHotelsVC = self.storyboard?.instantiateViewController(withIdentifier: "selectHotelsVC") as! SelectHotelsPopupVC
        selectHotelsVC.pDelegate = self
//        if totalProperties.count > 0 {
//            totalProperties[0].property?[0].selected = true
//        }
//        else {
//            totalProperties[0].property?[0].selected = false
//        }
        print("self.totalProperties is ======>>>>>",self.totalProperties)
        selectHotelsVC.totalProperties = self.totalProperties
        presentPanModal(selectHotelsVC)
    }
    
    @objc override func handleTap(sender: UITapGestureRecognizer) {
        self.removeAlphaView()
        sideBarView.hideSlideMenu()
    }
    @IBAction func finaceSegment(_ sender: UISegmentedControl) {
        if #available(iOS 13.0, *) {
            segmentControl.selectedSegmentTintColor = UIColor.hexStringToUIColor(hex: "#FC7A45")
        } else {
            // Fallback on earlier versions
        }
        
        if sender.selectedSegmentIndex != self.selectedSegmentIndex {
            self.selectedSegmentIndex = sender.selectedSegmentIndex
           
        }
        self.segmentControl.tintColor = UIColor.hexStringToUIColor(hex: "#FC7A45")
        self.segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
    }
    @objc func didTapOverdueButton(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            var dic = dashboardData.dictionaryValue["response"]?.dictionaryValue["Receivable"]?.arrayValue.first?.dictionaryValue
            dic?.removeValue(forKey: "CompanyCode")
            dic?.removeValue(forKey: "CompanyName")
            dic?.removeValue(forKey: "NetDebit")
            dic?.removeValue(forKey: "NetCredit")
            dic?.removeValue(forKey: "Total")
            var str = ""
            dic?.forEach({
                print($0.1)
                str.append("\n\($0.0):     \($0.1)")
           })
            
            showAlert(withTitle: "", withMessage: str)
            
        } else if sender.tag == 2 {
            
            var dic = dashboardData.dictionaryValue["response"]?.dictionaryValue["Payable"]?.arrayValue.first?.dictionaryValue
            dic?.removeValue(forKey: "CompanyCode")
            dic?.removeValue(forKey: "CompanyName")
            dic?.removeValue(forKey: "NetDebit")
            dic?.removeValue(forKey: "NetCredit")
            dic?.removeValue(forKey: "Total")
            dic?.removeValue(forKey: "UnAdjusted Debit")
            dic?.removeValue(forKey: "SupplierCode")
            dic?.removeValue(forKey: "NetCredit")
            dic?.removeValue(forKey: "SupplierName")
            var str = ""
            dic?.forEach({
                print($0.1)
                str.append("\n\($0.0):     \($0.1)")
           })
            
            showAlert(withTitle: "", withMessage: str)
        }
    }
    func showAlert(withTitle title: String, withMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        alert.addAction(ok)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    func calculateAmount(amountVal: String) -> String  {
        var amount = amountVal
        amount = amount.replacingOccurrences(of: ",", with: ".")
        if amount.contains("-") {
            amount = amount.replacingOccurrences(of: "-", with: "")
            let amountArr = amount.components(separatedBy: .punctuationCharacters)
            if amountArr.count >= 2 {
                
               return "-\(amountArr[0]).\(amountArr[1])"
            }
        } else {
            let amountArr = amount.components(separatedBy: .punctuationCharacters)
            if amountArr.count >= 2 {
                
                return "\(amountArr[0]).\(amountArr[1])"
            }
        }
        return ""
    }
}
extension FinanceViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currencyCode = UserDefaults.standard.object(forKey: "currencyCode") as? String ?? ""
        if indexPath.section == 0  {
            let cell = financeTableview.dequeueReusableCell(withIdentifier: "FPayableTableViewCell") as! FPayableTableViewCell
            cell.lblheader.text = "Receivable"
           
            var dAmount = dashboardData.dictionaryValue["response"]?.dictionaryValue["Receivable"]?.arrayValue.first?["NetDebit(\(currencyCode))"].stringValue ?? ""
            dAmount = dAmount.replacingOccurrences(of: ",", with: "")
            cell.btnOverdue.tag = indexPath.section + 1
            cell.btnOverdue.addTarget(self, action: #selector(didTapOverdueButton), for: .touchUpInside)
            var cAmount = dashboardData.dictionaryValue["response"]?.dictionaryValue["Receivable"]?.arrayValue.first?["NetCredit(\(currencyCode))"].stringValue ?? ""
            cAmount = cAmount.replacingOccurrences(of: ",", with: "")
          
            cell.lblUnAdjustedvalue.text = CommonDataManager.shared.suffixNumber(number: Double(cAmount) ?? 0.0)
            
            var tAmount = dashboardData.dictionaryValue["response"]?.dictionaryValue["Receivable"]?.arrayValue.first?["Total(\(currencyCode))"].stringValue ?? ""
            tAmount = tAmount.replacingOccurrences(of: ",", with: "")
            cell.lblOutstandingAmount.text = CommonDataManager.shared.suffixNumber(number: Double(tAmount) ?? 0.0)
            
            
            let per = ( (Double(dAmount) ?? 0.0) / ((Double(dAmount) ?? 0.0) + (Double(cAmount) ?? 0.0))) * 100
            
            cell.progressbar.progressValue = per
            cell.lblNetDebitAmount.text = CommonDataManager.shared.suffixNumber(number: Double(dAmount) ?? 0.0)
            
            
            return cell
        } else if indexPath.section == 1 {
            let cell = financeTableview.dequeueReusableCell(withIdentifier: "FPayableTableViewCell") as! FPayableTableViewCell
            cell.lblheader.text = "Payable"
            
            cell.btnOverdue.tag = indexPath.section + 1
            cell.btnOverdue.addTarget(self, action: #selector(didTapOverdueButton), for: .touchUpInside)
            
            var dAmount = dashboardData.dictionaryValue["response"]?.dictionaryValue["Payable"]?.arrayValue.first?["NetCredit(\(currencyCode))"].stringValue ?? ""
            dAmount = dAmount.replacingOccurrences(of: ",", with: "")
            
            var cAmount = dashboardData.dictionaryValue["response"]?.dictionaryValue["Payable"]?.arrayValue.first?["UnAdjusted Debit(\(currencyCode))"].stringValue ?? ""
            cAmount = cAmount.replacingOccurrences(of: ",", with: "")
            cell.lblUnAdjustedvalue.text = CommonDataManager.shared.suffixNumber(number: Double(cAmount) ?? 0.0)
            
            
            var tAmount = dashboardData.dictionaryValue["response"]?.dictionaryValue["Payable"]?.arrayValue.first?["Total(\(currencyCode))"].stringValue ?? ""
            tAmount = tAmount.replacingOccurrences(of: ",", with: "")
            cell.lblOutstandingAmount.text = CommonDataManager.shared.suffixNumber(number: Double(tAmount) ?? 0.0)
            
            let per = ( (Double(dAmount) ?? 0.0) / ((Double(dAmount) ?? 0.0) + (Double(cAmount) ?? 0.0))) * 100
            
            cell.progressbar.progressValue = per
            cell.lblNetDebitAmount.text = CommonDataManager.shared.suffixNumber(number: Double(dAmount) ?? 0.0)
            
            
            return cell
        } else if indexPath.section == 2 {
            let cell = financeTableview.dequeueReusableCell(withIdentifier: "FPieChartTableViewCell") as! FPieChartTableViewCell
            
            cell.entriesData = aryEntries
            cell.getExpenseDat(expense: FEExpense)
            //FEExpense = FEExpense ?? FExpenseModel()
            return cell
        } else if indexPath.section == 3 {

            guard let bankCashCell = self.financeTableview.dequeueReusableCell(withIdentifier: "BankCashTableViewCell", for: indexPath) as? BankCashTableViewCell else {
               return UITableViewCell()
            }
            if cashandBankArrModel.count != 0 {
                
                bankCashCell.getBankCashDetails(cashandBankArrModel: cashandBankArrModel)
            }
            bankCashCell.selectionStyle = .none
            return bankCashCell
        } else if indexPath.section == 4 {
            let cell = financeTableview.dequeueReusableCell(withIdentifier: "FProfitLoassTableViewCell") as! FProfitLoassTableViewCell
            var profitLoss = dashboardData.dictionaryValue["response"]?.dictionaryValue["ProfitandLoss"]?.dictionaryValue
            let netprofit = profitLoss?["NetProfit"]?.stringValue ?? ""
            let np = Double(netprofit) ?? 0.0
            cell.lblNetProfit.text = CommonDataManager.shared.suffixNumber(number: np )
            
            let income = profitLoss?["NetIncome"]?.stringValue ?? ""
            let ic = Double(income) ?? 0.0
            cell.lblIncome.text = CommonDataManager.shared.suffixNumber(number: ic )
            
            let netExpense = profitLoss?["NetExpense"]?.stringValue ?? ""
            let ne = Double(netExpense) ?? 0.0
            cell.lblExpenses.text = CommonDataManager.shared.suffixNumber(number: ne )
            
            
            let expPer = ( ne / ic ) * 100
            cell.expenseProgressbar.progressValue = expPer
            
            let incPer = ( np / ic ) * 100
            cell.incomeprogressBar.progressValue = incPer
    
            
            return cell
        } else if indexPath.section == 5 {
            let cell = financeTableview.dequeueReusableCell(withIdentifier: "FBarchartTableViewCell") as! FBarchartTableViewCell
            cell.entriesData = salesEntries
            cell.salesAmount(sales: self.FEExpense)
            return cell
        }
        else {
            return UITableViewCell.init()
        }
        
    }
    

}

extension FinanceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 4 || indexPath.section ==  2  {
            return 230
        } else if indexPath.section == 5 {
            return 300
        } else if indexPath.section == 3 {
            return CGFloat(100 * self.cashandBankArrModel.count + 40)
        }
        return 200
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
}


extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

struct CashandBankModel {
    var amount: String?
    var accounttCode: String?
    var accountMasterName: String?
    var generalLedgerType: String?
}

extension FinanceViewController :SideMenuDelegate{
    func sideBarDelegateRow(menuObj: String) {
        self.removeAlphaView()
        sideBarView.hideSlideMenu()
        if menuObj == "Subscribed"{
            
            let stObj = UIStoryboard.init(name: "Main", bundle: nil)
            let itemsDash = stObj.instantiateViewController(withIdentifier: "ItemsDashBoard") as! ItemsDashBoardViewController
            self.navigationController?.pushViewController(itemsDash, animated: true)
        }
        else if menuObj == "About"{
            
        }
        else if menuObj == "Logout"{
            
            UICoreEngine.sharedInstance.showAlertOnLogout(message: "Do you want to logout", title: "Alert", isNoBUtton: true, viewController: self)
        }
        
    }
    
}

extension FinanceViewController: PropertyDelegate {
    func selectedProperties(list: [PropertyModel]) {
        self.totalProperties.removeAll()
        self.mainList.removeAll()
        self.totalProperties = list
        didPullToRefresh()
        
       
        for i in 0..<self.totalProperties.count {
            if self.totalProperties[i].selected == true {
                isGroupPropertySelected = true
            }
            let count = self.totalProperties[i].property?.count ?? 0
            for j in 0..<count {
            if totalProperties[i].property?[j].selected == true{
                    ispropertySelected = true
            }else {
                    ispropertySelected = false
                }
            }
        }
        //self.tableview_listSales.reloadData()
        var aryCount = list.filter { (each) -> Bool in
            each.selected == true
        }
}
    
    @objc func didPullToRefresh() {
        // get pms code from properties list
        self.listAry.removeAll()
        self.ary_listofRAStatus.removeAll()
        if let pmsCustCode = getPMSFromSelectedProperty() {
            // set pmscode globally
            UserDefaults.standard.set(String(pmsCustCode), forKey: "PmsCustCode")
        }
    }
    
    // get PMSCode
    func getPMSFromSelectedProperty() -> Int? {
        for i in 0..<self.totalProperties.count {
            let count = self.totalProperties[i].property?.count ?? 0
            
            if totalProperties[i].selected == true {
                for j in 0..<count {
                    if totalProperties[i].property?[j].selected == true{
                        return totalProperties[i].property?[j].titlePms
                    }
                }
            }
            else {
                for j in 0..<count {
                    if totalProperties[i].property?[j].selected == true{
                        return totalProperties[i].property?[j].titlePms
                    }
                }
            }
        }
        return nil
    }
}
