//
//  FPayableTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 01/06/22.
//  Copyright © 2022 IDS. All rights reserved.
//

import UIKit
import LinearProgressBar

class FPayableTableViewCell: UITableViewCell {

    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var imageviewbg: UIImageView!
    @IBOutlet weak var lblheader: UILabel!
    @IBOutlet weak var btnOverdue: UIButton!
    
    @IBOutlet weak var lblNetDebitAmount: UILabel!
    @IBOutlet weak var lblOutstandingAmount: UILabel!
    
    @IBOutlet weak var progressbar: LinearProgressBar!
    @IBOutlet weak var lblUnAdjustedvalue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        shadowView.layer.cornerRadius = 10
        shadowView.layer.masksToBounds = true
        shadowView.dropShadow()
        self.backgroundColor = .clear
        
        btnOverdue.layer.cornerRadius = 5
        btnOverdue.layer.masksToBounds = true
        btnOverdue.layer.borderWidth = 1
        btnOverdue.layer.borderColor = UIColor.orange.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension UIView {

  // OUTPUT 1
  func dropShadow(scale: Bool = true) {
      
      self.layer.shadowColor = UIColor.black.cgColor
      self.layer.shadowOpacity = 1
      self.layer.shadowOffset = CGSize.zero
      self.layer.shadowRadius = 5
  }

  // OUTPUT 2
  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset = offSet
    layer.shadowRadius = radius

    layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
}
