//
//  BankCashTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 2/12/24.
//  Copyright © 2024 IDS. All rights reserved.
//

import UIKit

class BankCashTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var bankCashTableView: UITableView!
    
    var cashandBankArrModel =  [CashandBankModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        shadowView.layer.cornerRadius = 20
        shadowView.layer.masksToBounds = true
        shadowView.dropShadow()
        self.backgroundColor = .clear
        
        self.bankCashTableView.register(UINib.init(nibName: "BankCashListTableViewCell", bundle: nil), forCellReuseIdentifier: "BankCashListTableViewCell")
        bankCashTableView.dataSource = self
        bankCashTableView.dataSource = self
    }
    
    func getBankCashDetails(cashandBankArrModel:  [CashandBankModel])  {
        self.cashandBankArrModel = cashandBankArrModel
        bankCashTableView.dataSource = self
        bankCashTableView.dataSource = self
        bankCashTableView.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension BankCashTableViewCell: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cashandBankArrModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let bankCashListCell = self.bankCashTableView.dequeueReusableCell(withIdentifier: "BankCashListTableViewCell", for: indexPath) as? BankCashListTableViewCell else {
           return  UITableViewCell()
        }
        let cashObj = cashandBankArrModel[indexPath.row]
        bankCashListCell.cashBankListMethod(cashandBankModel: cashObj)
        bankCashListCell.selectionStyle = .none
        return bankCashListCell
    }
}
