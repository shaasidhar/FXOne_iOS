//
//  LoginDataModel.swift
//  FXone
//
//  Created by Vinayak on 7/30/19.
//  Copyright © 2019 IDS. All rights reserved.
//
import Foundation

/******************************************  MIS TOKEN Model ******************************/
struct MISTokenModel        :Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response:String?
}

/******************************************  Login Model ******************************/
struct LoginModel           :Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response            :LoginResponse?
}
struct LoginResponse : Codable {
    var Access_Token : String?
    var PropertyAdminRights : [AdminResponse]?
    var UserDetails : UserDetailsData?
    var SubscribedProducts: [SubscribedProducts]?
    var DefaultHotel: [DefaultHotel]?
    var UserRights: [LoginUserRights]?
}
struct LoginUserRights: Codable {
    var PmsCustCode:Int?
    var PropertyGroupCode:String?
    var Roles:[LoginRoleModel]?
}
struct LoginRoleModel: Codable {
    var AccessRights:String?
    var FunctionCode:String?
    var FunctionName:String?
}
struct AdminResponse : Codable {
    var PmsCustCode:Int?
    var PropertyGroupCode:String?
}
struct UserDetailsData : Codable  {
    var UserName : String?
    var DefaultGroupCode: String?
    var DefaultPMSCode: Int?
    var UserType: String?
}
struct DefaultHotel : Codable  {
    var ID : Int?
    var HotelName : String?
    var ShortLogo : String?
}
struct SubscribedProducts : Codable  {
    var ProductID : Int?
    var ProductsCode : Int?
    var ExpiryDate : String?
    var Notification : Bool?
    var ColorCode : String?
}

/******************************************  Dashboard Model ******************************/

struct DFANDBModel : Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response            :DFANDBResponse?
}
struct DFANDBResponse: Codable {
    var ToDayDetail :[RoomSaleCustomModel]?
    var YearDetail  :[RoomSaleCustomModel]?
    var MonthDetail :[RoomSaleCustomModel]?
    var CustomDetail:[RoomSaleCustomModel]?
    var Day:[RoomSaleCustomModel]?
    var Month:[RoomSaleCustomModel]?
    var Year:[RoomSaleCustomModel]?
}
struct DashBoardModel     :Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response            :DashBoardResponse?
}
struct DashBoardResponse  :Codable {
    var CurrencyCode         :String?
    var AccountingDate       :String?
    var FinancialDate       :String?
    var FromMonthDate       :String?
    var RoomSale             :RoomSaleModel?
    var FoodandBeverage      :FoodandBeverageModel?
    var NonFoodandBeverage   :NonFoodandBeverageModel?
    var Collections          :CollectionsDataModel?
    var Depoist              :DepoistDataModel?
    var PaidOut              :PaidOutDataModel?
}
struct PaidOutDataModel : Codable {
    var Day : [PaidOutDataModelArry]?
    var Month : [PaidOutDataModelArry]?
    var Year : [PaidOutDataModelArry]?
    var CustomDetail : [PaidOutDataModelArry]?
}
struct PaidOutDataModelArry :Codable {
    var Description : String?
    var Amount : Double?
    var LastyearAmount : Double?
    var key: String?
}
struct DepoistDataModel : Codable {
    var Day : [DepoistDataModelArry]?
    var Month : [DepoistDataModelArry]?
    var Year : [DepoistDataModelArry]?
    var CustomDetail : [DepoistDataModelArry]?
}
struct DepoistDataModelArry :Codable {
    var Description : String?
    var Amount : Double?
    var LastyearAmount : Double?
    var TransactionAmount : Double?
    var TaxAmount : Double?
    var key: String?
}
struct RoomSaleModel     :Codable {
    var ToDayDetail          :[RoomSaleCustomModel]?
    var YearDetail           :[RoomSaleCustomModel]?
    var MonthDetail          :[RoomSaleCustomModel]?
    var CustomDetail         :[RoomSaleCustomModel]?
}
struct FoodandBeverageModel :Codable {
    var ToDayDetail :[RoomSaleCustomModel]?
    var YearDetail  :[RoomSaleCustomModel]?
    var MonthDetail :[RoomSaleCustomModel]?
    var CustomDetail:[RoomSaleCustomModel]?
}
struct NonFoodandBeverageModel : Codable {
    var TodayDetail :[RoomSaleCustomModel]?
    var YearDetail  :[RoomSaleCustomModel]?
    var MonthDetail :[RoomSaleCustomModel]?
    var CustomDetail:[RoomSaleCustomModel]?
}
struct RoomSaleDetailModel :Codable {
    var ARR     :Double?
    var ARRLY   :Int?
    var RevPar  :Double?
    var RevParLy:Int?
}
struct aLosModel   :Codable {
    var Selection       :String?
    var CurrentYearALOS :Double?
    var LastYearALOS    :Double?
}

struct RoomSaleCustomModel : Codable {
    var Selection     :String?
    var Yesterday_TD  :Double?
    var Today        :Double?
    var LYToday      :Double?
    var Month         :Double?
    var LastMonth     :Double?
    var Budget        :Double?
    var Year          :Double?
    var LastYear      :Double?
    var ARR          :Double?
    var ARRLY        :Double?
    var RevPar       :Double?
    var RevParLy     :Double?
    var CurrentYearALOS :Double?
    var LastYearALOS :Double?
    var key: String?
    var TransactionAmount: Double?
    var TaxAmount: Double?
    var islive: Bool?
}
// Collection data model

struct DashCollectionDataModel :Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response            :CollectionsDataModel?
}

struct CollectionsDataModel : Codable {
    var Day : [CollectionsDataModelArry]?
    var Month : [CollectionsDataModelArry]?
    var Year : [CollectionsDataModelArry]?
    var CustomDetail : [CollectionsDataModelArry]?
    var isLive: Bool?
}
struct CollectionsDataModelArry: Codable {
    var SettlementName : String?
    var Amount : Double?
    var LastyearAmount : Double?
    var key: String?
    var Description : String?
}
/*************************  Hotel Group&Listed Property TOKEN Model ***************************/
struct HotelPropertyTokenModel : Codable {
    var User:String?
    var PropertyID:Int?
    var ProductCode:Int?
    var Access_Token:String?
}

