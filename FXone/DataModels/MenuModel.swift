//
//  MenuModel.swift
//  FXone
//
//  Created by Vinayak on 21/04/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import Foundation
// Menu model
struct MenuModel : Codable {
    var name:String?
    var key:String?
}
// Tabs Model
struct NotificationTabs : Codable {
    var key:String?
    var value : String?
    var count:Int?
    
}

struct NotificationObject : Codable{
    var title:String?
    var value:String?
    var time:String?
    var slide_open:Bool?
    var value_list :[String?]?
}
