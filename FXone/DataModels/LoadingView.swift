//
//  LoadingView.swift
//  SDM Utility Porttal
//
//  Created by apple on 12/07/18.
//  Copyright © 2018 apple. All rights reserved.
//
    
import Foundation
import UIKit

class loadingView: UIView {

    static var activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
    static var app = UIApplication.shared.delegate as? AppDelegate

    static let containerView = UIView(frame: CGRect(x:0, y:0, width:0,height:0))

    static let bgView = UIView(frame: CGRect(x:0, y:0, width:0,height:0))
    static let titleView = UILabel(frame: CGRect(x:0, y:0, width:0,height:0))

    class func showLoader(text: String){
        
       // let width = UIDevice.isPhone ? ((UIApplication.shared.keyWindow?.frame.size.width ?? 400)/4):((UIApplication.shared.keyWindow?.frame.size.width ?? 400)/6)
       // let height = UIDevice.isPhone ? ((UIApplication.shared.keyWindow?.frame.size.height ?? 500)/7) : ((UIApplication.shared.keyWindow?.frame.size.height ?? 500)/8)
       DispatchQueue.main.async {
        containerView.frame =  CGRect(x:0, y:0, width:UIApplication.shared.keyWindow?.frame.size.width ?? 200,height:UIApplication.shared.keyWindow?.frame.size.height ?? 200)
        let colorCode = UserDefaults.standard.object(forKey: "Color_Code") as? String
        containerView.backgroundColor = UIColor.clear
            activityView.color = UIColor.hexStringToUIColor(hex: colorCode ?? "#FC7A45")
        containerView.layer.cornerRadius = 10.0


        containerView.center = CGPoint(x: (app?.window?.bounds.midX)!, y: (app?.window?.bounds.midY)!)

        //Here the spinnier is initialized

        activityView.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
        activityView.center = CGPoint(x: (containerView.bounds.midX), y: (containerView.bounds.midY))
        activityView.startAnimating()
        containerView.addSubview(activityView)
        bgView.frame = CGRect(x:0, y:0, width:(app?.window?.frame.size.width)!,height:(app?.window?.frame.size.height)!)
            bgView.backgroundColor = UIColor.black
            //bgView.alpha = 1
            bgView.backgroundColor = UIColor.clear
            bgView.bringSubviewToFront(containerView)
            //bgView.addSubview(containerView)
           titleView.frame = CGRect(x: 0, y:activityView.frame.maxY + 5, width: 160,height: 60)
           titleView.center = CGPoint(x: (containerView.bounds.midX), y: (containerView.bounds.midY + 50))
            titleView.text = text
            titleView.textColor = .black
            containerView.addSubview(titleView)
            UIApplication.shared.keyWindow?.addSubview(bgView)
            UIApplication.shared.keyWindow?.addSubview(containerView)
        }
    }
    
    static func hideLoader() {

        DispatchQueue.main.async {
            activityView.stopAnimating()
            titleView.removeFromSuperview()
            bgView.removeFromSuperview()
            activityView.removeFromSuperview()
            containerView.removeFromSuperview()
        }
    }

}

