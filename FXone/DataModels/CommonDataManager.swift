//
//  CommonDataManager.swift
//  FXone
//
//  Created by Vinayak on 2/25/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import Foundation
import UIKit
open class CommonDataManager {
    
    static let shared = CommonDataManager()
    
    init() {
    }
    func getActualBudgetValue(budget:String,actual:String) -> (Int,Int) {
        
        let a:Double = Double(actual) ?? 0.0
        let b :Double = Double(budget) ?? 0
        let d :Double = 100
        if a == 0.0 && b == 0.0 {
            return (0,0)
        }
        if a != 0 && b == 0 {
            return (0,0)
        }
    
        let c = a/b * d
        let per1 = c / 2
        let  w = c + per1
        return (Int(c),Int(w))

    }
    var detailItems : [Any]?
    var selectedDetailItem : Int?
    var ary_nonFandBDetailsList : [NonFAndBSalesDataObj]?
    var ary_FandBDetailsList : [FAndBSalesDataObj]?
    var baseUrl = ""
    var baseUrl1 = ""
    var baseUrl2 =  ""
    var vC : ViewController?
    func setBaseUrl(env:String,version:String){
        
        let url = "http://fooperationsapi\(env).azurewebsites.net/\(version)/"
        let url1 = "https://fcreportsapi\(env).azurewebsites.net/v1/"
        let url2 = "https://fxfas\(env)api.azurewebsites.net/"
        self.baseUrl = url
        self.baseUrl1 = url1
        self.baseUrl2 = url2
        print(self.baseUrl)
        print(self.baseUrl1)
        print(self.baseUrl2)
    }
    func getlastYearCurrentyearPerValue(lastYear:String,currentyear:String) -> Int {
        
        let a:Double = Double(currentyear) ?? 0.0
        let b :Double = Double(lastYear) ?? 0.0
        
        if a == 0.0 && b == 0.0 {
            return 0
        }
        let d :Double = 100
        let c = (a/b * d) - 100
        guard !(c.isNaN || c.isInfinite) else {
            return 0 // or do some error handling
        }
      //  Thread 1: Fatal error: Double value cannot be converted to Int because it is either infinite or NaN
        return Int(c)
        
    }
    func getDateString() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "d MMM" // "EE" to get short style
        let dayInWeek = dateFormatter.string(from: Date())
        return dayInWeek
    }
    func getDateStngDDMMMYYYY(fromDate : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "d MMM yyyy" // "EE" to get short style
        let dayInWeek = dateFormatter.string(from: Date())
        return dayInWeek
    }
    func getDateStringDDMMMYYYY(fromDate : Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "d MMM yyyy" // "EE" to get short style
        return dateFormatter.string(from: fromDate)
    }
    func getDateStringDDMMMYYYYWithOutSpaces(fromDate : Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "ddMMMyy" // "EE" to get short style
        return dateFormatter.string(from: fromDate)
    }
    
    func parseDate(dateString: String) -> Date? {
        let date_formatter = DateFormatter()
            date_formatter.dateFormat = "MM/dd/yyyy h:mm:ss a" //10/12/2019 5:30:00 AM
        return date_formatter.date(from:dateString)
    }
    func formatForeignCurrency(_ number: Double) -> String {
        let abbreviations = ["", "K", "M", "B", "T", "P", "E"]
        var index = 0
        var num = number
        while num >= 1000 && index < abbreviations.count - 1 {
            num /= 1000
            index += 1
        }
        let roundedNumber = String(format: "%.2f", num)
        return "\(roundedNumber)\(abbreviations[index])"
    }
    func suffixNumber(number:Double) -> String {
        let numberStr = String(number)
        let currency = UserDefaults.standard.string(forKey: "currency_Code")
        print(currency ?? "")
        var num:Double = number;
        let sign = ((num < 0) ? "-" : "" );
        
        num = fabs(num);
        
        if (num < 1000.0){
            return "\(sign)\(num)";
        }else {
        }
        if currency == "INR"  {
                    // MARK:- round up for INR (K, L, CR)
                    let exp:Int = Int(log10(num)); //log10(1000));
                print(exp)
                   // let units:[String] = ["K","L","CR"];
            
                    if exp > 6 {
                        let val = num / 10000000
                        var str = String(val)
                        var ary = str.components(separatedBy: ".")
                        if ary[1].count > 1 {
                            if numberStr.contains("-") {
                                return "\(sign)\(ary[0]).\(convertdigitsTwoDigits(val: String(ary[1].prefix(3)))) CR"
                            } else {
                                return "\(ary[0]).\(convertdigitsTwoDigits(val: String(ary[1].prefix(3)))) CR"
                            }
                        }
                        else {
                            if numberStr.contains("-") {
                                return "\(sign)\(str) CR"
                            } else {
                                return "\(str) CR"
                            }
                        }
                    }
                    else if exp >= 5 {
                        let val = num / 100000
                        var str = String(val)
                        var ary = str.components(separatedBy: ".")
                        if ary[1].count > 1 {
                            if numberStr.contains("-") {
                                return "\(sign)\(ary[0]).\(convertdigitsTwoDigits(val: String(ary[1].prefix(3)))) L"
                            } else {
                                return "\(ary[0]).\(convertdigitsTwoDigits(val: String(ary[1].prefix(3)))) L"
                            }
                        }
                        else {
                            if numberStr.contains("-") {
                                return "\(sign)\(str) L"
                            } else {
                                return "\(str) L"
                            }
                        }
                    }
                    else  {
            
                        let val = num / 1000
                        var str = String(val)
                        var ary = str.components(separatedBy: ".")
                        if ary[1].count > 1 {
//                            let decimal = Double(ary[1].prefix(3)) ?? 0.0
//                            let val =  decimal / 10.0
//                            let round = ceil(val) 
//                            let doubleRound = Int(round)
                            if numberStr.contains("-") {
                                return "\(sign)\(ary[0]).\(convertdigitsTwoDigits(val: String(ary[1].prefix(3)))) K"
                            } else {
                                return "\(ary[0]).\(convertdigitsTwoDigits(val: String(ary[1].prefix(3)))) K"
                            }
                        }
                        else {
                            if numberStr.contains("-") {
                                return "\(sign)\(str) K"
                            } else {
                                return "\(str) K"
                            }
                        }
                    }
        }
        else {
            return(String(formatForeignCurrency(Double(num))))
        }
    
    }
    
    func convertdigitsTwoDigits(val: String) -> Int {
        let decimal = Double(val) ?? 0.0
        let val =  decimal / 10.0
        let round = ceil(val)
        let doubleRound = Int(round)
        return doubleRound
    }
    
    func getNumberWithCommas(cash: Double) ->String{
        
        if Double(cash) > 999 {
            let amount = self.suffixNumber(number:cash)
            return amount
        }
        else {
            let str = Double(cash).withCommas()
            var ary = str.components(separatedBy: ".")
            if ary.count > 1 {
                if ary[1].count > 1 {
                return "\(ary[0]).\(ary[1].prefix(2))"
                }
                else {
                return "\(str)"
                }
            }
            else {
                return "\(str)"
            }
            return "\(str)"
        }
    }
    
    
    func getNumberWithCommasSevnDig(cash:Double) ->String{
        
        if Double(cash) > 9999999 {
            let amount = self.suffixNumber(number:cash)
            return amount
        }
        else {
            let str = Double(cash).withCommas()
            var ary = str.components(separatedBy: ".")
            if ary.count > 1 {
                if ary[1].count > 1 {
                return "\(ary[0]).\(ary[1].prefix(2))"
                }
                else {
                return "\(str)"
                }
            }
            else {
                return "\(str)"
            }
            //return str
        }
    }
    func getNumberWithINRCommas(cash:Double) ->String{
        if Double(cash) > 999 {
            let amount = self.suffixNumber(number:cash)
            return amount
        }
        else {
            let str = Double(cash).withCommas()
            var ary = str.components(separatedBy: ".")
            if ary.count > 1 {         // was 1, changed to 10 on 6feb2024
            if ary[1].count > 1 {       //was 1, changed to 10 on 6feb2024
                return "\(ary[0]).\(ary[1].prefix(2))"
            }
            else {
                return "\(str)"
            }
                
            }
            else {
                return "\(str)"

            }
            //return str
        }
    }
}


