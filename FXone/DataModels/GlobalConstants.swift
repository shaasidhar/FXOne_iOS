//
//  GlobalConstants.swift
//  FXone
//
//  Created by Vinayak on 2/18/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import Foundation
let GET_TOKEN = "user/login"
let OCCUPANYAPI = "KPI/GetDashBoardDetails"
let ROOMRATE = "KPI/FxOneDefulatRate"
let MISREPORT = "report/MISRevenueReport"
let SINGLEPROPERTY = "KPI/PropertyDetails"
let GET_MISTOKEN = "report/User/Login"
let GET_CURRENCYLIST = "Parameters/GetCurrencyList"
let GET_FINANCIALYEARDATA = "TransactionType/financeYear"
//Live endpoints
let DASHBOARDDETAIL = "KPI/FxOneDashBoard"
let ONESALEDETAIS = "KPI/FxOneSaleDetail"
let NONFANDBDETAILS = "KPI/FXOneNonFNBSale"
let FANDBDETAILS = "KPI/FXOneFNBSale"
let SALESSTATSREPORT = "KPI/FXOneSalesStatistics"
let DashBoard_RoomSale = "FxOneDashBoard_RoomSale"
let DashBoard_FNBSale = "FxOneDashBoard_FNBSale"
let DashBoard_NonFNBSale = "FxOneDashBoard_NonFNBSale"
let DashBoard_Collection = "FxOneDashBoard_Collection"
let DashBoard_Deposit = "FxOneDashBoard_Deposit"
let DashBoard_PaidOut = "FxOneDashBoard_PaidOut"
let AccountDateNightAudit = "Parameters/GetNightAuditStatus"
let FINANCEYEAR = "api/TransactionType/financeYear"
let AGEINGDEFINATION = "api/Ageing/GetAgeingDefinitionDetail"
let FINANCEDASHBORADDETAILS = "api/DashBoard/GetDashboardDetails"

/*
// QA revamp endpoints
let DASHBOARDDETAIL = "KPI/FxOneDashBoard_Revamp"
let ONESALEDETAIS = "KPI/FxOneSaleDetail_Revamp"
let NONFANDBDETAILS = "KPI/FXOneNonFNBSale_Revamp"
let FANDBDETAILS = "KPI/FXOneFNBSale_Revamp"
let SALESSTATSREPORT = "KPI/FXOneSalesStatistics_Revamp"
*/
//****************************************************************//
//new API Revamp
/*
 https://fooperationsapiqa.azurewebsites.net/V1.0.0/KPI/FxOneDashBoard_Revamp
 https://fooperationsapiqa.azurewebsites.net/V1.0.0/KPI/FxOneSaleDetail_Revamp
 https://fooperationsapiqa.azurewebsites.net/V1.0.0/KPI/FXOneFNBSale_Revamp
 https://fooperationsapiqa.azurewebsites.net/V1.0.0/KPI/FXOneNonFNBSale_Revamp
 https://fooperationsapiqa.azurewebsites.net/V1.0.0/KPI/FXOneSalesStatistics_Revamp
*/
