//
//  DataModels.swift
//  FXone
//
//  Created by Vinayak on 8/6/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import Foundation

//************************************** OCCUPANCY Response **************************//
struct RoomOccupancyModel :Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response            :RoomOccupancyModel_Response?
}
struct RoomOccupancyModel_Response : Codable {
    var PercentageOccupied   :Float?
    var PercentageOccupiedLy :Float?
    var AccountingDate: String?
    var Rooms:OccupancyRoomData?
}
struct OccupancyRoomData:Codable,Loopable {
//    var TotalRooms:Int?
    var Occupied:Int?
//    var PercentageOccupied:Float?
    var OOOandOOS:Int?
    var MGT:Int?
    var TodaysExpectedArrival:Int?
    var ToSell:Int?
    var TotalRooms: Int?

}

/******************************************  Room Sale Detail Model ******************************/
struct Detail_RoomSaleModel :Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response            :Detail_RoomSaleModel_Response?
}

struct Detail_RoomSaleModel_Response : Codable {
    var CurrencyCode:String?
    var CurrencyName:String?
    var AccountingDate:String?
    var Day : DayRoomSaleModel?
    var Month : MonthRoomSaleModel?
    var Year : YearRoomSaleModel?
    var Custom : CustomRoomSaleModel?
}

struct CustomRoomSaleModel: Codable {
    var RoomSale:TotalRoomSaleModel?
    var RoomSaleDetails:DetailsRoomSaleDetail?
    var DashBoard: [RoomSaleCustomModel?]?
}

struct DayRoomSaleModel: Codable {
    var RoomSale:TotalRoomSaleModel?
    var RoomSaleDetails:DetailsRoomSaleDetail?
}
struct MonthRoomSaleModel: Codable {
    var RoomSale:TotalRoomSaleModel?
    var RoomSaleDetails:DetailsRoomSaleDetail?
}
struct YearRoomSaleModel: Codable {
    var RoomSale:TotalRoomSaleModel?
    var RoomSaleDetails:DetailsRoomSaleDetail?
}
struct TotalRoomSaleModel : Codable {
    var RoomType          :[Details_RoomSaleObject]?
    var GuestStatus       :[GuestTypeObject]?
    var MarketSegment     :[MarketSegmentWaiseObject]?
    var BusinessSource    :[BusinessSourceObject]?
}
struct Details_RoomSaleObject : Codable {
    var RoomTypeName:String?
    var RoomTypeCode:String?
    var CurrentYear:Float?
    var CYTrend:Float?
    var LYTrend:Float?
    var TaxAmount:Float?
}
struct GuestTypeObject : Codable {
    var GuestStatusName:String?
    var GuestStatusCode:String?
    var CurrentYear:Float?
    //var LastYear:Float?
}
struct MarketSegmentWaiseObject : Codable {
    var MarketSegmentName:String?
    var MarketSegmentCode:String?
    var CurrentYear:Float?
   // var LastYear:Float?
}
struct BusinessSourceObject : Codable {
    var BusinessSourceName:String?
    var BusinessSourceCode:String?
    var CurrentYear:Float?
    //var LastYear:Float?
}

struct DetailsRoomSaleDetail: Codable{
    var GuestStatusDetail          :[GuestStatusDetailObj]?
    var MarketSegmentDetails        :[MarketSegmentDetailsObj]?
    var BusinessSourceDetails       :[BusinessSourceDetailsObj]?
}
struct GuestStatusDetailObj : Codable {
    var RoomTypeName : String?
    var Data : [GuestStatusData]?
}
struct GuestStatusData : Codable {
    var GuestStatusName:String?
    var Amount:Double?
}
struct MarketSegmentDetailsObj : Codable {
    var RoomTypeName : String?
    var data : [MarketSegmentData]?
}
struct MarketSegmentData : Codable {
    var MarketSegmentName:String?
    var Amount:Double?
}
struct BusinessSourceDetailsObj : Codable {
    var RoomTypeName : String?
    var data : [BusinessSourceData]?
}
struct BusinessSourceData : Codable {
    var BusinessSourceName:String?
    var Amount:Double?
}

//************************************** NONF&B DETAILS Response **************************//
struct NonFANBDetailModel :Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response            :NonFNBSalesDataResponseModel?
}
struct NonFNBSalesDataResponseModel :Codable {
    var CurrencyCode:String?
    var CurrencyName:String?
    var NonFNBSalesData : NonFNBSalesDataModel?
}
struct NonFNBSalesDataModel : Codable{
    var Day:[NonFAndBSalesDataObj]?
    var Month:[NonFAndBSalesDataObj]?
    var Year:[NonFAndBSalesDataObj]?
    var Custom:CustomDataObj?
}
struct NonFAndBSalesDataObj : Codable{
    var Section:String?
    var RevenueGroup:String?
    var RevenueCodeName:String?
    var Amount:Double?
    var LastYear:Double?
    var CYTrend:Int?
    var LYTrend:Int?
    var TaxAmount:Float?
}
struct CustomDataObj : Codable{
    var DashBoard: [RoomSaleCustomModel]?
    var NonFNBSalesData: [NonFAndBSalesDataObj]?
}
struct CustomDashboardData: Codable {
    var Selection: String?
    var Yesterday_TD: Double?
    var Budget: Double?
    var Year: Double?
    var LastYear: Double?
}
//struct CustomNonFAndBSalesData: Codable {
//    var Section:String?
//    var RevenueGroup:String?
//    var RevenueCodeName:String?
//    var Amount:Double?
//    var LastYear:Double?
//    var CYTrend:Int?
//    var LYTrend:Int?
//}

//************************************** F&B DETAILS Response **************************//
struct FANBDetailModel :Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response            :FNBSalesDataResponseModel?
}

struct FNBSalesDataResponseModel :Codable {
    var CurrencyCode:String?
    var CurrencyName:String?
    var Day : DayFNBSalesDataModel?
    var Month : MonthFNBSalesDataModel?
    var Year : YearFNBSalesDataModel?
    var Custom : CustomFandBDatModel?
    //var FNBSalesData : FNBSalesDataModel?
}
struct CustomFandBDatModel :Codable {
        var FNBSalesData:[FAndBSalesDataObj]?
        var FNBSalesDetails:[FNBSalesDetailsObj]?
        var DashBoard: [RoomSaleCustomModel?]?
    }
struct DayFNBSalesDataModel :Codable {
    var FNBSalesData:[FAndBSalesDataObj]?
    var FNBSalesDetails:[FNBSalesDetailsObj]?
}
struct MonthFNBSalesDataModel :Codable {
    var FNBSalesData:[FAndBSalesDataObj]?
    var FNBSalesDetails:[FNBSalesDetailsObj]?
}
struct YearFNBSalesDataModel :Codable {
    var FNBSalesData:[FAndBSalesDataObj]?
    var FNBSalesDetails:[FNBSalesDetailsObj]?
}
struct FAndBSalesDataObj : Codable{
    //var Section:String?
    var OutletCode:String?
    var OutletName:String?
    var Descriptions:String?
    var AMOUNT:Double?
    var LastYearAmount:Double?
    var CYTrend:Int?
    var LYTrend:Int?
    var isOpened:Bool?
    var Budget: Double?
    var TaxAmount:Float?
}

struct FNBSalesDetailsObj : Codable {
    var OutletName:String?
    var SalesByMenu:[FnBDeatailSalesByMenuObj]?
    var SellingMenu:[FnBDetailSellingMenuObj]?
}
struct FnBDeatailSalesByMenuObj : Codable {
    var MenuType:String?
    var AMOUNT:Double?
    var CYTrend: Double?
    var LYTrend: Double?
    var Budget: Double?
    var APCover: Double?
    var APCheck: Double?
}
//struct SalesByMenuobj : Codable {
//    var SalesByMenu:[FnBDeatailSalesByMenuObj]?
//    var SellingMenu:[FnBDetailSellingMenuObj]?
//}
//struct SellingMenuobj : Codable {
//    var SalesByMenu:[FnBDeatailSalesByMenuObj]?
//    var SellingMenu:[FnBDetailSellingMenuObj]?
//}
struct FnBDetailSellingMenuObj : Codable{
    var MenuType:String?
    var Count:Float?
}

//********************* Room Rate By Date & Type Model **************************//
struct RoomRateByDateDetailModel :Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response            :RoomRateByDateResponseModel?
}

struct RoomRateByDateResponseModel :Codable {
    var FromDate            :String? // covert this to date
    var ToDate              :String? // covert this to date
    var responce            :[RoomRateByDateObj]?
}

struct RoomRateByDateObj :Codable {
    var RoomTypeCode        :String?
    var RoomTypeName        :String?
    var RateDetail          :[RateDetailByDateObj]?
}

struct RateDetailByDateObj  :Codable {
    var RateDate            :String? // covert this to date
    var RateDay             :String?
    var Rate                :Double?
}

//********************* MIS Report Model **************************//

struct MISReportDataModel :Codable {
    var Status              :String?
    var StatusCode          :String?
    var StatusDescription   :String?
    var Response            :MISReportResponseModel?
}

struct MISReportResponseModel :Codable {
    var Day            :MISDayReport? // covert this to date
    var Month          :MISMonthReport? // covert this to date
    var Year           :MISYearReport?
}

struct MISDayReport : Codable {
    var FoodAndBevarage: [MISFoodAndBevarageDataModel]?
    var Rate :  [MISRateModel]?
    var OtherSales: [MISOtherSalesDataModel]?
    var OccupancyDetail:MISOccupancyDetailModel?
    var LastYearOccupancyDetail:MISLYOccupancyDetailModel?
}
struct MISRateModel :Codable {
    var Section     :String?
    var Description : String?
    var RevenueCode : String?
    var NetAmount   : Double?
    var LastyearDay : Double?
    var Budget : Double?
}
struct MISMonthReport : Codable {
    var FoodAndBevarage:[MISFoodAndBevarageDataModel]?
    var Rate : [MISRateModel]?
    var OtherSales:[MISOtherSalesDataModel]?
    var OccupancyDetail:MISOccupancyDetailModel?
    var LastYearOccupancyDetail:MISLYOccupancyDetailModel?
}
struct MISYearReport : Codable {
    var FoodAndBevarage:[MISFoodAndBevarageDataModel]?
    var Rate : [MISRateModel]?
    var OtherSales:[MISOtherSalesDataModel]?
    var OccupancyDetail:MISOccupancyDetailModel?
    var LastYearOccupancyDetail:MISLYOccupancyDetailModel?
}
struct MISFoodAndBevarageDataModel : Codable {
    var Section:String?
    var Description:String?
    var RevenueCode:String?
    var NetAmount:Double?
    var LastyearDay:Double?
    var Budget:Double?
}

struct MISOtherSalesDataModel : Codable {
    var Section:String?
    var Description:String?
    var RevenueCode:String?
    var NetAmount:Double?
    var LastyearDay:Double?
    var Budget:Double?
}
struct MISOccupancyDetailModel : Codable,Loopable {
    var UnderRepairRoom : Int?
    var RoomsSold : Int?
    var PAX : Int?
    var Complimentary : Int?
    var TotalRooms : Int?
    var HouseUse : Int?
    var ExpectedArrival : Int?
    var ExpectedDeparture : Int?
    var ROOMSAVAILABLE : Int?
    var ARR : Double?
    var ARP : Double?
    var OccupancyPercentage : Double?
}
struct MISLYOccupancyDetailModel : Codable,Loopable {
    var UnderRepairRoom : Int?
    var RoomsSold : Int?
    var PAX : Int?
    var Complimentary : Int?
    var TotalRooms : Int?
    var HouseUse : Int?
    var ExpectedArrival : Int?
    var ExpectedDeparture : Int?
    var ROOMSAVAILABLE : Int?
    var ARR : Double?
    var ARP : Double?
    var OccupancyPercentage : Double?
}
// Menu model
struct MISReportModel : Codable {
    var name:String?
    var bcyValue:String?
    var acyValue:String?
    var actualLY:String?
    var RevenueCode: String?
}

//********************* Group Name Property Model **************************//
struct GroupPropertyModel : Codable {
    var Response:String?
    var Code:String?
    var Description:String?
    var Data:[GroupPropertyDataModel]?
}
struct GroupPropertyDataModel : Codable {
    var PropertyGroupCode:String?
    var PropertyGroupName:String?
}

//********************* Property List Name Model **************************//
struct PropertyListModel : Codable {
    var Response:String?
    var Code:Int?
    var Description:String?
    var Data:[PropertyListDataModel]?
}
struct PropertyListDataModel : Codable {
    var PMSCUSTCODE:Int?
    var PropertyName:String?
    var IsGSTApplicable:Bool?
    var IsInterfaceApplicable:Bool?
}

//********************* New Group & Property Name Model **************************//
struct SinglePropertyModel : Codable {
    var Status:String?
    var StatusCode:String?
    var StatusDescription:String?
    var Response:PropertyResponseObj?
}
struct PropertyResponseObj : Codable {
    var GroupDetails:GroupDetailDataModel?
    var PropertyDetails:[PropertyDetailsDataModel]?
}
struct GroupDetailDataModel : Codable {
    var GroupCode:String?
    var GroupName:String?
}
struct PropertyDetailsDataModel : Codable {
    var IsInterfaceApplicable:Bool
    var PropertyGroupCode:Int
    var IsGSTApplicable:Bool
    var PropertyName:String
    var PmsCustCode:Int
}

//********************* SalesStatistics Model **************************//
struct SalesStatisticsModel : Codable {
    var Status: String?
    var StatusCode: String?
    var StatusDescription: String?
    var Response: SalesStatisticsResponseModel?
}
struct SalesStatisticsResponseModel : Codable {
    var CurrencyCode: String?
    var CurrencyName: String?
    var Responce: SalesStatisticsDataModel?
}

struct SalesStatisticsDataModel : Codable {
    var Day: [SalesStatisticsDataModel_Day]?
    var Month: [SalesStatisticsDataModel_Month]?
    var Year: [SalesStatisticsDataModel_Year]?
    var Custom: [SalesStatisticsDataModel_Custom]?
}
struct SalesStatisticsDataModel_Day : Codable {
    var HotelName: String?
    var ADR: Double?
    var LYADR: String?
    var RevPer: Double?
    var LYRevPer: String?
    var Amount: Double?
    var LYAmount: String?
}
struct SalesStatisticsDataModel_Month : Codable {
    var HotelName: String?
    var ADR: Double?
    var LYADR: String?
    var RevPer: Double?
    var LYRevPer: String?
    var Amount: Double?
    var LYAmount: String?
}
struct SalesStatisticsDataModel_Year : Codable {
    var HotelName: String?
    var ADR: Double?
    var LYADR: String?
    var RevPer: Double?
    var LYRevPer: String?
    var Amount: Double?
    var LYAmount: String?
}
struct SalesStatisticsDataModel_Custom : Codable {
    var HotelName: String?
    var ADR: Double?
    var LYADR: String?
    var RevPer: Double?
    var LYRevPer: String?
    var Amount: Double?
    var LYAmount: String?
}
//********************* CurrencyList Model **************************//
struct CurrencyListModel : Codable {
    var Status: String?
    var StatusCode: String?
    var StatusDescription: String?
    var Response: [CurrencyListResponseModel]?
}
struct CurrencyListResponseModel : Codable {
    var CurrencyID : Int?
    var CurrencyCode: String?
    var CurrencyName: String?
    var CurrencySymbol: String?
    var Classification: String?
    var ConversionType: String?
    var IsExchangeRateTaged: Bool?
    var ExchangeRate: Double?
}

struct DAccountCodeModel: Codable {

    var Status: String?
    var StatusCode: String?
    var StatusDescription: String?
    var Response: DAResponse?

}

struct DAResponse: Codable {

    var AccountingDate: String?
    var Status: String?
    var ServerStatus: String?
    var ServerStatusLastUpdated: String?

}
