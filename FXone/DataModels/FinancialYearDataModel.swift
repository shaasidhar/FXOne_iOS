//
//  FinancialYearDataModel.swift
//  FXone
//
//  Created by Sudhakar Patam on 04/06/22.
//  Copyright © 2022 IDS. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

//SUNIL ADDED
extension CALayer {
    func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.03,
        x: CGFloat = 0,
        y: CGFloat = 3,
        blur: CGFloat = 6,
        spread: CGFloat = 0)
    {
    masksToBounds = false
    shadowColor = color.cgColor
    shadowOpacity = alpha
    shadowOffset = CGSize(width: x, height: y)
    shadowRadius = blur / 2.0
    if spread == 0 {
        shadowPath = nil
    } else {
        let dx = -spread
        let rect = bounds.insetBy(dx: dx, dy: dx)
        shadowPath = UIBezierPath(rect: rect).cgPath
    }
    }
}

struct FinanceYearModel : Codable {
    var status: Int?
    var message: String?
    var response: [FinanceYearResponseModel]?
    var number: Int?
    var billNumber: String?
    var outPutData: String?
    var listRecords: String?
}
struct FinanceYearResponseModel : Codable {
    var financialYear: String?
    var numberOfMonths: Int?
    var startDate: String?
    var endDate: String?
    var startMonthYear: Int?
    var endMonthYear: Int?
    var isClosed: Bool?
    var isActive: Bool?
}

struct AgeingFinanceYearResponseModel : Codable {
    var status: Int?
    var message: String?
    var response: [AgeingResponse]?
    var number: Int?
    var billNumber: String?
    var outPutData: String?
    var listRecords: String?
}

struct AgeingResponse: Codable {
    let userId: String?
    let productId: Int?
    let isGlobal: Bool?
    let pmsCustCode: Int?
    let propertyGroupCode: String?
    let numberofperiods: Int?
    let ageingfor: String?
    let createdBy: String?
    let createdDateTime: String?
    let modifiedBy: String?
    let modifiedDateTime: String?
    let isDeleted: Bool?
    let deletedBy: String?
    let deletedDateTime: String?
    let updateType: String?
    let ageingID: Int?
    let srlNum: Int?
    let frmAge: Int?
    let tooAge: Int?
    let txtAge: String?
    let columnName: Int?
}


extension Date {
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)) ~= self
    }
}
extension String {
    
    func getDateFromString(isoDate: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: isoDate) ?? Date()
    }
    
    
}
extension Date {
    func getDateMMDDYYString(date: Date) -> String {


        // Create Date Formatter
        let dateFormatter = DateFormatter()

        // Set Date Format
        dateFormatter.dateFormat = "MM-dd-YYYY"

        // Convert Date to String
        return dateFormatter.string(from: date)
        
    }
    
    func getDateYYYYMMDDString(date: Date) -> String {


        // Create Date Formatter
        let dateFormatter = DateFormatter()

        // Set Date Format
        dateFormatter.dateFormat = "YYYY-MM-dd"

        // Convert Date to String
        return dateFormatter.string(from: date)
        
    }
    
}
