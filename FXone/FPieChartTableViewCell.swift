//
//  FPieChartTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 02/06/22.
//  Copyright © 2022 IDS. All rights reserved.
//

import UIKit
import Charts

class FPieChartTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var expensesList: UITableView!
    @IBOutlet weak var pieChart: PieChartView!
    
    var colors = Colors.chartColors()
    var FEExpense = FExpenseModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        shadowView.layer.cornerRadius = 10
        shadowView.layer.masksToBounds = true
        shadowView.dropShadow()
        self.backgroundColor = .clear
        expensesList.delegate = self
        expensesList.dataSource = self
        expensesList.separatorStyle = .none
        expensesList.sectionHeaderTopPadding = 0
        
        self.expensesList.register(UINib.init(nibName: "FPieChartExpensesListTableViewCell", bundle: nil), forCellReuseIdentifier: "FPieChartExpensesListTableViewCell")
        
    }
    
    var entriesData: [FExpensePieChartModel]? {
        didSet {
            updateDate()
            expensesList.reloadData()
        }
    }
    
    func getExpenseDat(expense: FExpenseModel)  {
        pieChart.centerText = "INR \(CommonDataManager.shared.suffixNumber(number: expense.Expense ?? 0.0))"
    }
    func updateDate() {
        
        var entries = [ChartDataEntry]()
        let count = entriesData?.count ?? 0
        var total = 0.0
        for x in 0..<count {
            let each = entriesData?[x]
            let x = (Double(each?.amount ?? "") ?? 0.0)
            total += x
//            var a = CommonDataManager.shared.suffixNumber(number: x)
//            a = a.replacingOccurrences(of: "L", with: "")
//            a = a.replacingOccurrences(of: "CR", with: "")
//            a = a.replacingOccurrences(of: "K", with: "")
//            a = a.trimmingCharacters(in: .whitespaces)
            entries.append(ChartDataEntry(x:x, y: x))
        }
        let set = PieChartDataSet(entries)
        set.colors = colors
        set.drawValuesEnabled = true
        set.valueLinePart1OffsetPercentage = 0.8
        set.valueLinePart1Length = 0.2
        set.valueLinePart2Length = 0.4
        //set.xValuePosition = .outsideSlice
        set.yValuePosition = .outsideSlice
        
        pieChart.legend.formSize = 0
        pieChart.legend.enabled = false
        pieChart.setExtraOffsets(left: 5, top: 0, right: 5, bottom: 0)
        
        pieChart.animate(xAxisDuration: 2, easingOption: .easeOutBack)
        
        pieChart.drawEntryLabelsEnabled = true
        pieChart.setNeedsDisplay()
        
        pieChart.drawCenterTextEnabled = true
        
        
        pieChart.holeRadiusPercent = 0.8
        
        let data = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = ""
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        data.setValueFont(.systemFont(ofSize: 11, weight: .light))
        data.setValueTextColor(.black)
        
        pieChart.data = data
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entriesData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = expensesList.dequeueReusableCell(withIdentifier: "FPieChartExpensesListTableViewCell") as! FPieChartExpensesListTableViewCell
        cell.lblColoursName.text = "\(entriesData?[indexPath.row].accountMasterName ?? "") \(CommonDataManager.shared.getNumberWithCommasSevnDig(cash: Double(entriesData?[indexPath.row].amount ?? "") ?? 0.0))"
        cell.lblColours.layer.cornerRadius = 2
        cell.lblColours.backgroundColor = colors[indexPath.row]
        cell.lblColours.layer.masksToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
struct FExpensePieChartModel {
    var amount: String?
    var accounttCode: String?
    var accountMasterName: String?
}

struct FExpenseModel {
    var Expense: Double?
    var Sales: Double?
    
}
