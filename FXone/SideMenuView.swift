//
//  SideMenuView.swift
//  FXone
//
//  Created by Vinayak on 2/18/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

protocol SideMenuDelegate  {
    func sideBarDelegateRow(menuObj:String)
}


class SideMenuView: UIView {
    
    var delegate : SideMenuDelegate?
    var ary_MenuList = [MenuModel]()
    var ary_MenuListOptions = [MenuModel]()
    var currentViewController = UIViewController()
    var navigateToHome = String()
    //let userDefault = UserDefaults.standard
    let productCode100 = UserDefaults.standard.object(forKey: "productCode100") as? Int ?? 0
    //userDefault.object(forKey: "productCode100") as? Int ?? 0
    let productCode10010 = UserDefaults.standard.object(forKey: "productCode10010") as? Int ?? 0
    //userDefault.object(forKey: "productCode10010") as? Int ?? 0
    var listAry = [RoomStatusByDay]()
    var totalProperties = [PropertyModel]()
    var isFinanceDashboard = false
    var mainList = [[String:Any]]()
    var ispropertySelected : Bool?
    var isGroupPropertySelected : Bool?
    var ary_listofRAStatus = [RoomACS]()
    
    func LoadMenuView() {
        menuTableView.delegate = self
        menuTableView.dataSource = self
        menuTableView.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("faild to load sidemenu view")
    }
    
    let menuTableView : UITableView = {
        let table  = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init?(frame:CGRect,viewController :UIViewController,navigateTo: String){
        self.init(frame: frame)
            
        let window = UIApplication.shared.keyWindow
        currentViewController = viewController
        currentViewController.addAlphaView()
        guard let subviews = window?.subviews else { return }
        for view in subviews {
            if view == SideMenuView() {
                view.removeFromSuperview()
                }
            }
        window?.addSubview(self)
        
        let roles = UICoreEngine.sharedInstance.userRoles
        
        var menu1 = MenuModel()
        menu1.name = "Front Office"
        menu1.key = "Front Office"
        var menu12 = MenuModel()
        menu12.name = "Finance"
        menu12.key = "financeDashboard"
        var menu2 = MenuModel()
        menu2.name = "MIS Report"
        menu2.key = "misReport"
        var menu3 = MenuModel()
        menu3.name = "Sales Statistics"
        menu3.key = "salesStatistics"
        var menu4 = MenuModel()
        menu4.name = "Logout"
        menu4.key = "logout"
        var menu5 = MenuModel()
        menu5.name = "Property"
        menu5.key = "Property"
        self.navigateToHome = navigateTo
        let userDefault = UserDefaults.standard
        let productCode100 = userDefault.object(forKey: "productCode100") as? Int ?? 0
        //userDefault.object(forKey: "productCode100") as? Int ?? 0
        let productCode10010 = userDefault.object(forKey: "productCode10010") as? Int ?? 0
        //userDefault.object(forKey: "productCode10010") as? Int ?? 0
        
        if productCode100 == 100 && productCode10010 == 10010 {
            ary_MenuListOptions.append(menu1)
            let propAdmin = UICoreEngine.sharedInstance.userAdmin
            if propAdmin?.UserType == "User" {
                print("this is not propAdmin")
                
                let result =  roles?.filter { $0.FunctionCode == "FXONE06" &&  $0.AccessRights == "View" }
                
                        if result?.count ?? 0 > 0 {
                            
                           // ary_MenuListOptions.append(menu12)
                        }
                
                        let result1 =  roles?.filter { $0.FunctionCode == "FXONEM01"  &&  $0.AccessRights == "View"}
                        if result1?.count ?? 0 > 0 {
                            ary_MenuList.append(menu2)
                            //ary_MenuListOptions.append(menu2)
                        }
                        
                        let result2 =  roles?.filter { $0.FunctionCode == "FXONEM02"  &&  $0.AccessRights == "View" }
                        if result2?.count ?? 0 > 0 {
                            ary_MenuList.append(menu3)
                           // ary_MenuListOptions.append(menu3)
                        }
                let result3 =  roles?.filter { $0.FunctionCode == "FXONEM03"  &&  $0.AccessRights == "View" }
                if result3?.count ?? 0 > 0 {
                    switch navigateTo {
                    case "Finance":
                        ary_MenuList.append(menu1)
                    default:
                        ary_MenuList.append(menu12)
                    }
                   // ary_MenuListOptions.append(menu3)
                }
                
            }
            else if propAdmin?.UserType == "PropertyAdmin" {
                
                switch navigateTo {
                case "Finance":
                    ary_MenuList.append(menu1)
                default:
                    ary_MenuList.append(menu12)
                }
                ary_MenuList.append(menu5)
                ary_MenuList.append(menu2)
                ary_MenuList.append(menu3)
            }
            ary_MenuListOptions.append(menu4)
            ary_MenuList.append(menu4)
        } else  if productCode100 == 100 {
            //ary_MenuList.append(menu1)
            let propAdmin = UICoreEngine.sharedInstance.userAdmin
            if propAdmin?.UserType == "User" {
                print("this is not propAdmin")
//          let result =  roles?.filter { $0.FunctionCode == "FXONE06" &&  $0.AccessRights == "View" }
//       if result?.count ?? 0 > 0 {
//          ary_MenuList.append(menu12)
//       }
                let result1 =  roles?.filter { $0.FunctionCode == "FXONEM01"  &&  $0.AccessRights == "View"}
                
                if result1?.count ?? 0 > 0 {
                    ary_MenuList.append(menu2)
                }
                        
                let result2 =  roles?.filter { $0.FunctionCode == "FXONEM02"  &&  $0.AccessRights == "View"
                }
                
                if result2?.count ?? 0 > 0 {
                    ary_MenuList.append(menu3)
                }
            }
            else if propAdmin?.UserType == "PropertyAdmin" {
                //ary_MenuList.append(menu12)
                ary_MenuList.append(menu2)
                ary_MenuList.append(menu3)
            }
                
            ary_MenuList.append(menu4)
        } else  if productCode10010 == 10010 {
            //ary_MenuList.append(menu12)
            ary_MenuList.append(menu4)
        }
        
        //comment when role access enabled
//        ary_MenuList.append(menu12)
//        ary_MenuList.append(menu2)
//        ary_MenuList.append(menu3)
        
        //undo for role access
       
        menuTableView.separatorStyle = .singleLine
            
        menuTableView.sectionHeaderHeight = 150
        menuTableView.delegate = self
        menuTableView.dataSource = self
    // menuTableView.frame = CGRect(x: 0, y: -20, width: frame.width, height:frame.height)
        menuTableView.register(UINib(nibName: "SideMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
            
        self.addSubview(menuTableView)
        menuTableView.separatorStyle = .singleLine
        menuTableView.backgroundColor = UIColor.white
        menuTableView.tableFooterView = UIView()
        self.backgroundColor = .white
        self.LoadMenuView()
            
        let navBar = UIView.init()
        navBar.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 77.5)
        navBar.backgroundColor = UIColor.hexStringToUIColor(hex: "FC7A45")
        let lbl_title = UILabel(frame: CGRect(x: 20, y: (navBar.frame.height / 2 ) , width: self.frame.width - 10,height: 20))
        lbl_title.text  = UserDefaults.standard.object(forKey: "User_Name") as? String
        lbl_title.font = UIFont.FXRobotoRegular(size: 16)
        lbl_title.textColor = UIColor.white
        navBar.addSubview(lbl_title)
        self.addSubview(navBar)
        menuTableView.frame = CGRect(x: 0, y: navBar.frame.size.height, width: self.frame.width, height: self.frame.height)
        self.addSubview(menuTableView)
        self.getSingleProperty()
    }
    
    func getSingleProperty() {
        let username = UserDefaults.standard.object(forKey: "User_Id") as! String
        let token = UserDefaults.standard.object(forKey: "Token") as! String
        var headers: HTTPHeaders = [:]
        headers = ["Authorization" : "Bearer \(token)","Content-Type":"application/json"]
        //let pmsCode = UserDefaults.standard.object(forKey: "PmsCustCode") as! String
        let dicParams = ["LoginId":username]//,"PmsCustCode":pmsCode]
        AF.request("\(CommonDataManager.shared.baseUrl)\(SINGLEPROPERTY)", method: .post, parameters: dicParams, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print(response)
                do {
                    let response = try JSONDecoder().decode(SinglePropertyModel.self, from: response.data ?? Data())
                    if response.Status == "Success" {
                        if let groupProperties = response.Response {
                            self.parseGroupProperties(list: [groupProperties])//comment : single group to ary of group
                        }
                    }
                    else{
                        UICoreEngine.sharedInstance.ShowAlertOnWindows(message: response.StatusDescription ?? "", title: "")
                    }
                }
                catch let error {
                    #if DEBUG
                    UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
                    #endif
                }
            case .failure(let error):
                print(error.localizedDescription)
                UICoreEngine.sharedInstance.ShowAlertOnWindows(message: error.localizedDescription, title: "")
            }
        }
    }
    
    func parseGroupProperties(list:[PropertyResponseObj]?){
        
            for each in list ?? [] {
            var property1 = PropertyModel()
            property1.title = each.GroupDetails?.GroupName ?? ""
            //In case all properties selected (All properties in the group)
            if isGroupPropertySelected ?? false {
                 property1.selected = true
            }
            else {
                property1.selected = false
            }
            // show Group default selected in check box
            
            var ary = [Property1Model]()
            for property in each.PropertyDetails ?? [] {
                var p = Property1Model()
                p.title = property.PropertyName
                p.titlePms = property.PmsCustCode
                
                if ispropertySelected ?? false {
                   p.selected = true
                }
                else {
                    p.selected = false

                }
                //In case few property OR properties selected in the gourp
                 // show Property default selected in check box// here is the line show privious selectoion
                ary.append(p)
            }
            property1.property = ary
            totalProperties.append(property1)
            
            //totalProperties.append(contentsOf: totalProperties)
        }
        if totalProperties.count > 0 {
            totalProperties[0].property?[0].selected = true
        }

    }
    func slideSelectedmenuItem(menuObj:MenuModel) {
        
        
            switch menuObj.key {
                
            case "Front Office":
                
                let stObj = UIStoryboard.init(name: "Main", bundle: nil)
                let jobsList = stObj.instantiateViewController(withIdentifier: "Dashboard") as! ViewController
                
                if currentViewController.title == "Front Office" {
                }
                else {
                    self.removeFromSuperview()
                    if productCode100 == 100 && productCode10010 == 10010 {
                        currentViewController.navigationController?.pushViewController(jobsList, animated: true)
                    }
                }
                currentViewController.removeAlphaView()
                self.hideSlideMenu()
            case "financeDashboard":
                
                let stObj = UIStoryboard.init(name: "Main", bundle: nil)
                let misReport = stObj.instantiateViewController(withIdentifier: "FinanceViewController") as! FinanceViewController
                
                if currentViewController.title == "Finance" {
                }else {
                    self.removeFromSuperview()
                    if productCode100 == 100 && productCode10010 == 10010 {
                        currentViewController.navigationController?.pushViewController(misReport, animated: true)
                    }
                }
                currentViewController.removeAlphaView()
                self.hideSlideMenu()
                
            case "Property":
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.navigateToPropertyVC(totalProperties: self.totalProperties, controller: self)
                currentViewController.removeAlphaView()
                self.hideSlideMenu()
            case "misReport":
                
                let stObj = UIStoryboard.init(name: "Main", bundle: nil)
                let misReport = stObj.instantiateViewController(withIdentifier: "MISReportViewController") as! MISReportViewController
                
                if currentViewController.title == "MIS Report" {
                }else {
                    self.removeFromSuperview()
                    currentViewController.navigationController?.pushViewController(misReport, animated: true)
                }
                currentViewController.removeAlphaView()
                self.hideSlideMenu()
                
            case "salesStatistics":
                
                let stObj = UIStoryboard.init(name: "Main", bundle: nil)
                let misReport = stObj.instantiateViewController(withIdentifier: "SalesStatisticsViewController") as! SalesStatisticsViewController
                
                if currentViewController.title == "Sales Statistics" {
                }else {
                    self.removeFromSuperview()
                    currentViewController.navigationController?.pushViewController(misReport, animated: true)
                }
                currentViewController.removeAlphaView()
                self.hideSlideMenu()
        
            case "logout":
                print("Profile Clickd")
                UICoreEngine.sharedInstance.showAlertOnLogout(message: "Do you want to logout?", title: "Alert!", isNoBUtton: true, viewController: currentViewController)
                currentViewController.removeAlphaView()
                self.hideSlideMenu()
                
            default:
                print("selected non")
            }
    }
}

extension SideMenuView : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Num: \(indexPath.row)")
//        if productCode100 == 100 && productCode10010 == 10010 {
//            if ary_MenuList[indexPath.row].key == "financeDashboard" {
//                self.slideSelectedmenuItem(menuObj: ary_MenuList[indexPath.row])
//                ary_MenuList = ary_MenuListOptions
//                self.menuTableView.reloadData()
//            } else {
//
//            }
//        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            self.slideSelectedmenuItem(menuObj: ary_MenuList[indexPath.row])
        //}
       
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ary_MenuList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! SideMenuTableViewCell
        
        cell.menuObj = ary_MenuList[indexPath.row]
        cell.backgroundColor = .clear
        
        return cell
    }
}

extension SideMenuView: PropertyDelegate {
    func selectedProperties(list: [PropertyModel]) {
        self.totalProperties.removeAll()
        self.mainList.removeAll()
        self.totalProperties = list
        didPullToRefresh()
        
       
        for i in 0..<self.totalProperties.count {
            if self.totalProperties[i].selected == true {
                isGroupPropertySelected = true
            }
            let count = self.totalProperties[i].property?.count ?? 0
            for j in 0..<count {
            if totalProperties[i].property?[j].selected == true{
                    ispropertySelected = true
            }else {
                    ispropertySelected = false
                }
            }
        }
        //self.tableview_listSales.reloadData()
        var aryCount = list.filter { (each) -> Bool in
            each.selected == true
        }
}
    
    @objc func didPullToRefresh() {
        // get pms code from properties list
        self.listAry.removeAll()
        self.ary_listofRAStatus.removeAll()
        if let pmsCustCode = getPMSFromSelectedProperty() {
            // set pmscode globally
            UserDefaults.standard.set(String(pmsCustCode), forKey: "PmsCustCode")
        }
    }
    
    // get PMSCode
    func getPMSFromSelectedProperty() -> Int? {
        for i in 0..<self.totalProperties.count {
            let count = self.totalProperties[i].property?.count ?? 0
            
            if totalProperties[i].selected == true {
                for j in 0..<count {
                    if totalProperties[i].property?[j].selected == true{
                        return totalProperties[i].property?[j].titlePms
                    }
                }
            }
            else {
                for j in 0..<count {
                    if totalProperties[i].property?[j].selected == true{
                        return totalProperties[i].property?[j].titlePms
                    }
                }
            }
        }
        return nil
    }
}



