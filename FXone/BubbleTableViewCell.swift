//
//  BubbleTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 07/04/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import CoreCharts
import MarqueeLabel


class BubbleTableViewCell: UITableViewCell {

    @IBOutlet weak var liveSwitch: UISwitch!
    @IBOutlet weak var lblLiveStatus: UILabel!
    
    @IBOutlet weak var lbl_budgethelpMessage: UILabel!
    @IBOutlet weak var lbl_LY_CY_per: UILabel!
    @IBOutlet var view_content: UIView!
    @IBOutlet weak var lbl_actual_title: UILabel!
    @IBOutlet weak var view_actuallWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var budgetViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lbl_actualPercen: UILabel!
    @IBOutlet weak var view_budgetFill: UIView!
    @IBOutlet weak var view_actualFill: UIView!
    @IBOutlet weak var lbl_salesType: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_lastyearAmount: UILabel!
    @IBOutlet weak var lbl_todayAmount: UILabel!
    //@IBOutlet weak var todayAmountLbl: MarqueeLabel!
    @IBOutlet weak var lbl_dateStatus: UILabel!
    @IBOutlet weak var lbl_budget: UILabel!
    @IBOutlet var view_itemsContainer: UIView!
    @IBOutlet weak var lbl_actualAmount: UILabel!
    @IBOutlet weak var lbl_LYTD_LMTD_LY: UILabel!
    @IBOutlet weak var view_barChartContainer: UIView!
    @IBOutlet var imageview_corner: UIImageView!
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var lbl_CyCurrency: UILabel!
    
    @IBOutlet weak var lblNet: UILabel!
    @IBOutlet weak var lblTax: UILabel!
   
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.view_content.backgroundColor = UIColor.clear
        self.view_itemsContainer.backgroundColor = .white
        view_itemsContainer.backgroundColor = .white
        view_itemsContainer.layer.cornerRadius = 10
        view_itemsContainer.layer.masksToBounds = true
        
    }
    
    var selectedIndex = 0
    var sectionIndex = 0
    var isFromCustom = false
    var todaySales :RoomSaleCustomModel? {
        didSet{
            self.updateCellData()
        }
    }
    var monthSales :RoomSaleCustomModel? {
        didSet{
            self.updateCellData()
        }
    }
    var yearSales :RoomSaleCustomModel? {
        didSet{
            self.updateCellData()
        }
    }
    var customSale :RoomSaleCustomModel? {
        didSet{
            self.updateCellData()
        }
    }
    
    func updateCellData(){
        //lbl_actualPercen.font = UIFont.systemFont(ofSize: 12)
        
        lbl_actual_title.textColor = UIColor.hexStringToUIColor(hex: "FF8E00")
        view_actualFill.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
        
        print("selectedIndex==============>>>>>>",selectedIndex)
        
        if selectedIndex == 1 {
            self.lbl_title.text = "\(todaySales?.Selection ?? "")"
            self.lbl_budget.text = "\(todaySales?.Budget ?? 0.0)"
            self.lbl_todayAmount.text = "\(todaySales?.Today ?? 0.0)"
            self.lbl_lastyearAmount.text = "\(todaySales?.LYToday ?? 0.0)"
            self.lbl_actualAmount.text = "\(todaySales?.Yesterday_TD ?? 0.0)"
            let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(todaySales?.LYToday ?? 0.0)", currentyear: "\(todaySales?.Today ?? 0.0)")
            let net = "\(todaySales?.TransactionAmount ?? 0.0)"
            let tax = "\(todaySales?.TaxAmount ?? 0.0)"
//            lblNet.text = "Net: \(net)"
//            lblTax.text = "Tax: \(tax)"
            lblNet.text = "Net: \(CommonDataManager.shared.getNumberWithCommas(cash: todaySales?.TransactionAmount ?? 0.0))"
            lblTax.text = "Tax: \(CommonDataManager.shared.getNumberWithCommas(cash: todaySales?.TaxAmount ?? 0.0))"
            if value == 0 {
               self.lbl_LY_CY_per.text = " \(value)%"
               self.lbl_LY_CY_per.textColor = .black
           }
            else if value >= 100{
                self.lbl_LY_CY_per.text = "+ 100%"
                self.lbl_LY_CY_per.textColor = .green
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else if value > 0 {
                self.lbl_LY_CY_per.text = "+ \(value)%"
                self.lbl_LY_CY_per.textColor = .green
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else{
                self.lbl_LY_CY_per.text = " \(value)%"
                self.lbl_LY_CY_per.textColor = .red
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
            }
            self.lbl_LYTD_LMTD_LY.text = "LYCD"
            self.lbl_salesType.text = "Today"
        }
        else if selectedIndex == 2 {
            self.lbl_title.text = "\(monthSales?.Selection ?? "")"
            self.lbl_budget.text = "\(monthSales?.Budget ?? 0.0)"
            self.lbl_todayAmount.text = "\(monthSales?.Month ?? 0.0)"
            self.lbl_lastyearAmount.text = "\(monthSales?.LastMonth ?? 0.0)"
            self.lbl_actualAmount.text = "\(monthSales?.Yesterday_TD ?? 0.0)"
            
            let net = "\(monthSales?.TransactionAmount ?? 0.0)"
            let tax = "\(monthSales?.TaxAmount ?? 0.0)"
//            lblNet.text = "Net: \(net)"
//            lblTax.text = "Tax: \(tax)"
            lblNet.text = "Net: \(CommonDataManager.shared.getNumberWithCommas(cash: Double(net) ?? 0.0))"
            lblTax.text = "Tax: \(CommonDataManager.shared.getNumberWithCommas(cash: Double(tax) ?? 0.0))"
            let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(monthSales?.LastMonth ?? 0.0)", currentyear: "\(monthSales?.Month ?? 0.0)")
            if value == 0{
                self.lbl_LY_CY_per.text = "+\(value)%"
                self.lbl_LY_CY_per.textColor = .black
            }
            else if value >= 100{
                self.lbl_LY_CY_per.text = "+ 100%"
                self.lbl_LY_CY_per.textColor = .green
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else if value > 0 {
                self.lbl_LY_CY_per.text = "+\(value)%"
                self.lbl_LY_CY_per.textColor = .green
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else{
                self.lbl_LY_CY_per.text = "\(value)%"
                self.lbl_LY_CY_per.textColor = .red
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
            }
            self.lbl_LYTD_LMTD_LY.text = "LYCM"
            self.lbl_salesType.text = "MTD"
        }
        else if selectedIndex == 3 {
            self.lbl_title.text = "\(yearSales?.Selection ?? "")"
            self.lbl_budget.text = "\(yearSales?.Budget ?? 0.0)"
            self.lbl_todayAmount.text = "\(yearSales?.Year ?? 0)"
            self.lbl_lastyearAmount.text = "\(yearSales?.LastYear ?? 0.0)"
            self.lbl_actualAmount.text = "\(yearSales?.Yesterday_TD ?? 0)"
            
            let net = "\(yearSales?.TransactionAmount ?? 0.0)"
            let tax = "\(yearSales?.TaxAmount ?? 0.0)"
//            lblNet.text = "Net: \(net)"
//            lblTax.text = "Tax: \(tax)"
            lblNet.text = "Net: \(CommonDataManager.shared.getNumberWithCommas(cash: Double(net) ?? 0.0))"
            lblTax.text = "Tax: \(CommonDataManager.shared.getNumberWithCommas(cash: Double(tax) ?? 0.0))"
            let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(yearSales?.LastYear ?? 0)", currentyear: "\(yearSales?.Year ?? 0)")
            if value == 0 {
                self.lbl_LY_CY_per.text = "+\(value)%"
                self.lbl_LY_CY_per.textColor = .black
            }
            else if value >= 100{
                self.lbl_LY_CY_per.text = "+ 100%"
                self.lbl_LY_CY_per.textColor = .green
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else if value > 0 {
                self.lbl_LY_CY_per.text = "+\(value)%"
                self.lbl_LY_CY_per.textColor = .green
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else{
                self.lbl_LY_CY_per.text = "\(value)%"
                self.lbl_LY_CY_per.textColor = .red
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
            }
            self.lbl_LYTD_LMTD_LY.text = "LY"
            self.lbl_salesType.text = "YTD"

        } else if selectedIndex == 0  || isFromCustom {
            self.lbl_title.text = "\(yearSales?.Selection ?? "")"
            self.lbl_budget.text = "\(yearSales?.Budget ?? 0.0)"
            self.lbl_todayAmount.text = "\(yearSales?.Month ?? 0)"
            self.lbl_lastyearAmount.text = "\(yearSales?.LastYear ?? 0.0)"
            self.lbl_actualAmount.text = "\(yearSales?.Yesterday_TD ?? 0)"
            
            let net = "\(yearSales?.TransactionAmount ?? 0.0)"
            let tax = "\(yearSales?.TaxAmount ?? 0.0)"
//            lblNet.text = "Net: \(net)"
//            lblTax.text = "Tax: \(tax)"
            lblNet.text = "Net: \(CommonDataManager.shared.getNumberWithCommas(cash: Double(net) ?? 0.0))"
            lblTax.text = "Tax: \(CommonDataManager.shared.getNumberWithCommas(cash: Double(tax) ?? 0.0))"
            let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(yearSales?.LastYear ?? 0)", currentyear: "\(yearSales?.Year ?? 0)")
            if value == 0{
                self.lbl_LY_CY_per.text = "+\(value)%"
                self.lbl_LY_CY_per.textColor = .black
            }
            else if value >= 100{
                self.lbl_LY_CY_per.text = "+ 100%"
                self.lbl_LY_CY_per.textColor = .green
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else if value > 0 {
                self.lbl_LY_CY_per.text = "+\(value)%"
                self.lbl_LY_CY_per.textColor = .green
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else{
                self.lbl_LY_CY_per.text = "\(value)%"
                self.lbl_LY_CY_per.textColor = .red
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
            }
            self.lbl_LYTD_LMTD_LY.text = "LY"
            self.lbl_salesType.text = "Live"

        }
        else if selectedIndex == 1001 {
            self.lbl_title.text = "\(customSale?.Selection ?? "")"
            self.lbl_budget.text = "\(customSale?.Budget ?? 0.0)"
            self.lbl_todayAmount.text = "\(customSale?.Year ?? 0.0)"
            self.lbl_lastyearAmount.text = "\(customSale?.LastYear ?? 0.0)"
            self.lbl_actualAmount.text = "\(customSale?.Yesterday_TD ?? 0.0)"
            let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(customSale?.LastYear ?? 0.0)", currentyear: "\(customSale?.Year ?? 0.0)")
            
            let net = "\(customSale?.TransactionAmount ?? 0.0)"
            let tax = "\(customSale?.TaxAmount ?? 0.0)"
//            lblNet.text = "Net: \(net)"
//            lblTax.text = "Tax: \(tax)"
            lblNet.text = "Net: \(CommonDataManager.shared.getNumberWithCommas(cash: Double(net) ?? 0.0))"
            lblTax.text = "Tax: \(CommonDataManager.shared.getNumberWithCommas(cash: Double(tax) ?? 0.0))"
            if value == 0{
                self.lbl_LY_CY_per.text = "+\(value)%"
                self.lbl_LY_CY_per.textColor = .black
            }
            else if value >= 100{
                self.lbl_LY_CY_per.text = "+ 100%"
                self.lbl_LY_CY_per.textColor = .green
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else if value > 0 {
                self.lbl_LY_CY_per.text = "+ \(value)%"
                self.lbl_LY_CY_per.textColor = .green
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else{
                self.lbl_LY_CY_per.text = " \(value)%"
                self.lbl_LY_CY_per.textColor = .red
                self.lbl_LY_CY_per.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
            }
            self.lbl_LYTD_LMTD_LY.text = "LYCD"
            self.lbl_salesType.text = "Today"
        }
    
        let budget = lbl_budget.text ?? "0"
        let actual = lbl_actualAmount.text ?? "0"
        let b = Int(Double(budget) ?? 0.0) ?? 0
        let a = Int(Double(actual) ?? 0.0) ?? 0
        if b <= 0 {
            lbl_actualPercen.text = "100%"
            view_actuallWidthConstraint.constant = CGFloat(150)
            budgetViewWidthConstraint.constant = 10 // butger bar occy
        }
        else if a > b {
            lbl_actualPercen.text = "100%+"
            view_actuallWidthConstraint.constant = CGFloat(160)
        }

        else {
            
            let per = CommonDataManager.shared.getActualBudgetValue(budget:lbl_budget.text ?? "0", actual: lbl_actualAmount.text ?? "0")
            print("Perce::::\(per)")
            lbl_actualPercen.text = "\(per.0)%"
            view_actuallWidthConstraint.constant = CGFloat(per.1)
        }
        
        if b == 0 {
            self.view_barChartContainer.isHidden = true
            //self.lbl_budgetNotMention.isHidden = false
            self.lbl_budgethelpMessage.isHidden = false
            self.lbl_budgethelpMessage.text = "Budget Not Defined"
        }
        else {
            self.view_barChartContainer.isHidden = false
            self.lbl_budgethelpMessage.isHidden = true
            //self.lbl_budgetNotMention.isHidden = true
        }
        
        
        //self.lbl_todayAmount.text = "10000500"
        let double_todayCost = Double(self.lbl_todayAmount.text ?? "") ?? 0.0
        self.lbl_todayAmount.text = CommonDataManager.shared.getNumberWithCommas(cash: double_todayCost)
        self.lbl_actualAmount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(self.lbl_actualAmount.text ?? "") ?? 0.0)
        self.lbl_budget.text = CommonDataManager.shared.getNumberWithCommas(cash: Double(self.lbl_budget.text ?? "") ?? 0.0)
//        self.lbl_lastyearAmount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(self.lbl_lastyearAmount.text ?? "") ?? 0.0)

        let textToCompare = self.lbl_title.text
        if textToCompare?.caseInsensitiveCompare("RoomSale") == .orderedSame {
            self.imageview_corner.image = UIImage.init(named: "Room Sale")
            self.lbl_title.text = "Room Sales"
        }
        else if textToCompare?.caseInsensitiveCompare("FoodandBeverage") == .orderedSame {
            self.imageview_corner.image = UIImage.init(named: "F&B")
            self.lbl_title.text = "F&B Revenue"
        }
        else if textToCompare?.caseInsensitiveCompare("OtherSale") == .orderedSame {
            self.imageview_corner.image = UIImage.init(named: "Non F&B")
            self.lbl_title.text = "Other Revenue"
        }
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
