//
//  RoomOccupancyCollectionViewCell.swift
//  FXone
//
//  Created by Vinayak on 7/23/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import Charts
import RKPieChart
import MarqueeLabel
import Accelerate

class RoomOccupancyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var count_Lbl: UILabel!
    @IBOutlet weak var lbl_ly: UILabel!
    @IBOutlet weak var lbl_percentage: UILabel!
    //@IBOutlet weak var lbl_percentage3: UILabel!
    @IBOutlet weak var lbl_percentage3: MarqueeLabel!
    @IBOutlet weak var lbl_percentage2: UILabel!
    @IBOutlet weak var lbl_cy: UILabel!
    @IBOutlet weak var collectionview_legend: UICollectionView!
    
    @IBOutlet weak var percentage3StatusImgView: UIImageView!
    @IBOutlet weak var btn_rates: UIButton!
    @IBOutlet var view_chartdata: UIView!
    
    @IBOutlet weak var lbl_date: UILabel!
    
    var gradientLayer: CAGradientLayer!

    @IBOutlet var lbl_chartViewTitle: UILabel!
    @IBOutlet var pieChart: PieChartView!
    
    //var entry1 = PieChartDataEntry(value:   0)
    //var entry2 = PieChartDataEntry(value: 60)
    var numberofEntires = [PieChartDataEntry]()
    
    @IBOutlet var view_itemsContainer: UIView!
    @IBOutlet var view_Container: UIView!
    
    @IBOutlet weak var activityView: UIView!
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var items = [RKPieChartItem]()
    
    var count = [String]()
    //var count: [String] = []
    //var count = [String](repeating: "", count : 5)

    
    var legendArrynames = ["Occupied","TodaysExpectedArrival","OOOandOOS","ToSell","MGT"]
    
    func updateChartdata(){
        
        let chartDataSet = PieChartDataSet(entries: numberofEntires, label: "")
        let chartData = PieChartData(dataSet: chartDataSet)
        let colors = [UIColor.blue,.green]
        chartDataSet.colors = colors
        pieChart.data =  chartData
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        self.view_Container.backgroundColor = UIColor.clear
        //self.view_itemsContainer.backgroundColor = .white
        //view_itemsContainer.backgroundColor = .white
        view_itemsContainer.layer.cornerRadius = 10
        view_itemsContainer.layer.masksToBounds = true
        
        collectionview_legend.delegate = self
        collectionview_legend.dataSource = self
        collectionview_legend.register(UINib(nibName: "ChartLegendsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ChartLegendsCollectionViewCell")
        
        //marQuee
        lbl_percentage3.type = .continuous
        lbl_percentage3.speed = .duration(10.0)
        lbl_percentage3.animationCurve = .easeInOut
        lbl_percentage3.fadeLength = 10.0
        lbl_percentage3.leadingBuffer = 5.0
        lbl_percentage3.trailingBuffer = 20.0
        
    }
    
    var roomData : RoomStatusByDay? {
        didSet{
            self.items.removeAll()
            collectionview_legend.reloadData()
            self.setupCellData()
        }
    }
    func setupCellData(){
        var total = 0
        print("total Chart Data is ", total)
        //let total = vC.totalRooms // for total rooms from API in roomOccup
        for each in roomData?.list ?? []{
            var count = Int(each.count ?? "0") ?? 0
            if count <= 0 {
                count = 0
            }
            total += count
                let color = each.statusColor ?? UIColor.white
            
            
            let firstItem: RKPieChartItem = RKPieChartItem(ratio: uint(count) , color: color, title: "s")
            items.append(firstItem)
        }
        print("total Chart Data is ", total)
        //self.count_Lbl.text = "\(total)"
        //sleep(5)
//        let chartView = RKPieChartView(items: items, centerTitle: "Total \(total ?? 0)") // to the total with         out lbl
        let chartView = RKPieChartView(items: items, centerTitle: "Total")
        chartView.circleColor = .red
        chartView.translatesAutoresizingMaskIntoConstraints = false
        chartView.arcWidth = 10
        chartView.isIntensityActivated = false
        chartView.style = .butt
        chartView.isTitleViewHidden = true
        chartView.isAnimationActivated = true
        self.view_chartdata.backgroundColor = .clear
        self.view_chartdata.addSubview(chartView)
        
        chartView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        chartView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        chartView.centerXAnchor.constraint(equalTo: self.view_chartdata.centerXAnchor).isActive = true
        chartView.centerYAnchor.constraint(equalTo: self.view_chartdata.centerYAnchor).isActive = true
        
    }
}

extension RoomOccupancyCollectionViewCell :UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return roomData?.list?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview_legend.dequeueReusableCell(withReuseIdentifier: "ChartLegendsCollectionViewCell", for: indexPath) as! ChartLegendsCollectionViewCell
        cell.backgroundColor = UIColor.clear
        cell.statusObj = roomData?.list?[indexPath.row]
        cell.lbl_statusname.text = roomData?.list?[indexPath.row].name ?? ""
        return cell
    }
    
}
