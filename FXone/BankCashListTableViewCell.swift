//
//  BankCashListTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 2/12/24.
//  Copyright © 2024 IDS. All rights reserved.
//

import UIKit

class BankCashListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    //var cashandBankModel =  CashandBankModel()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func cashBankListMethod(cashandBankModel: CashandBankModel)  {
        typeLabel.text = cashandBankModel.generalLedgerType ?? ""
        nameLabel.text = cashandBankModel.accountMasterName ?? ""
        idLabel.text = cashandBankModel.accounttCode ?? ""
        amountLabel.text = cashandBankModel.amount ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
