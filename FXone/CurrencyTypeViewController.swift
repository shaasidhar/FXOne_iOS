//
//  CurrencyTypeViewController.swift
//  FXone
//
//  Created by Vinayak on 17/05/22.
//  Copyright © 2022 IDS. All rights reserved.
//

import UIKit

protocol SelectedCurrencyDelegate: AnyObject {
    func selectedCurrency(curency: CurrencyListResponseModel,index:Int, amount: Double)
}

class CurrencyTypeViewController: UIViewController {

    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var tfdCurrency: UITextField!
    @IBOutlet weak var tableview_Currency: UITableView!
    @IBOutlet weak var btnCurrencyType: UIButton!
    var aryCurrency = [CurrencyListResponseModel]()
    weak var delegate: SelectedCurrencyDelegate?
    var enteredAmount: Double = 0.0
    var selectedIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview_Currency.isHidden = true
        tableview_Currency.dataSource = self
        tableview_Currency.delegate = self
        tfdCurrency.isHidden = true
        tableview_Currency.register(CellClass.self, forCellReuseIdentifier: "Cell")
        
        let results = aryCurrency.filter {$0.Classification == "LocalCurrency" }
        if results.count > 0 {
            btnCurrencyType.setTitle(results.first?.CurrencyName ?? "", for: .normal)
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func confirmedClicked(_ sender: UIButton) {
        self.delegate?.selectedCurrency(curency: aryCurrency[selectedIndex], index: selectedIndex, amount: enteredAmount)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func currencyClicked(_ sender: UIButton) {
        tableview_Currency.isHidden = false
        tfdCurrency.isHidden = true
    }


}
extension CurrencyTypeViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        enteredAmount = Double(textField.text ?? "") ?? 0.0
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        enteredAmount = Double(textField.text ?? "") ?? 0.0
        return true
    }
}
extension CurrencyTypeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryCurrency.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview_Currency.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = aryCurrency[indexPath.row].CurrencyName ?? ""
        cell.textLabel?.font = UIFont.init(name: "Arial", size: 10)
        return cell
    }
    
}

extension CurrencyTypeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview_Currency.deselectRow(at: indexPath, animated: true)
        self.tfdCurrency.isHidden = false
        self.selectedIndex = indexPath.row
        tableview_Currency.isHidden = true
        btnCurrencyType.setTitle(aryCurrency[selectedIndex].CurrencyName ?? "", for: .normal)
        tfdCurrency.text = "\(aryCurrency[selectedIndex].ExchangeRate ?? 0.0)"
    }
}
