//
//  QuickLinkCollectionViewCell.swift
//  FXone
//
//  Created by Vinayak on 28/03/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit

class QuickLinkCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageview_item: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var menuObj :MenuModel?{
        didSet{
            setCellData()
        }
    }
    func setCellData() {
        
        if menuObj?.name == "FX Front Desk" {
            imageview_item.image = UIImage.init(named: "FrontDesk.png")
            lbl_title.text = menuObj?.name
        }
        else if menuObj?.name == "FX POS"{
            imageview_item.image = UIImage.init(named: "POS.png")
            lbl_title.text = menuObj?.name
        }
        else if menuObj?.name == "FX HouseKeeping"{
            imageview_item.image = UIImage.init(named: "HouseKeeping.png")
            lbl_title.text = menuObj?.name
        }
        else if menuObj?.name == "FX GuestService"{
            imageview_item.image = UIImage.init(named: "GuestService.png")
            lbl_title.text = menuObj?.name
        }
        else if menuObj?.name == "FX CRS"{
            imageview_item.image = UIImage.init(named: "FX-CRS.png")
            lbl_title.text = menuObj?.name
        }
    }
}
