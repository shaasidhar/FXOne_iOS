//
//  FBankAccountTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 01/06/22.
//  Copyright © 2022 IDS. All rights reserved.
//

import UIKit

class FBankAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var lblCashAmount: UILabel!
    
    @IBOutlet weak var lblbankAmount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        shadowView.layer.cornerRadius = 10
        shadowView.layer.masksToBounds = true
        shadowView.dropShadow()
        self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
