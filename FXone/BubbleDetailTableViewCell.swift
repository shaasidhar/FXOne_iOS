//
//  BubbleDetailTableViewCell.swift
//  FXone
//
//  Created by Vinayak on 25/07/19.
//  Copyright © 2019 IDS. All rights reserved.
//

import UIKit
import CoreCharts
import MarqueeLabel

class BubbleDetailTableViewCell: UITableViewCell {
    
    @IBOutlet var view_content: UIView!
    @IBOutlet weak var lbl_actual_title: UILabel!
    
   
    @IBOutlet weak var view_actualWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lbl_actualPercen: UILabel!
    @IBOutlet weak var view_budgetFill: UIView!
    @IBOutlet weak var view_actualFill: UIView!
    @IBOutlet weak var lbl_salesType: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_lastyearAmount: UILabel!
    @IBOutlet weak var lbl_todayAmount: UILabel!
    @IBOutlet weak var lbl_budget: UILabel!
    @IBOutlet weak var lbl_budgethelpMessage: UILabel!
    @IBOutlet weak var view_barChartContainer: UIView!
    @IBOutlet weak var lbl_cyLyPer: UILabel!
    @IBOutlet weak var lbl_currencyCode: UILabel!
    @IBOutlet weak var lbl_lyCdCmLy: UILabel!
    @IBOutlet weak var lbl_lyCdCmLyCurrencyCode: UILabel!
    
    @IBOutlet weak var lbl_actualAmount: UILabel!
    
    
    @IBOutlet weak var view_container: UIView!
    var barChartView:CustomDashBarChartView?
    var fxview:FXPieChartView?
    var roomsaleview:RoomSaleFooterView?
    
    var isPie = false
    var isNonFnBDetails = false
    var isBarBhartLoadedbefore =  false
    var isFromCustome = false
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            
            if self.isPie {
                
                self.fxview = FXPieChartView.init(frame: CGRect(x: 0, y:self.view_container.frame.size.height, width: self.view_container.frame.width, height: 250))
                
                //self.fxview?.configureScrollItems(items: CommonDataManager.shared.detailItems ?? [])
                self.contentView.addSubview(self.fxview ?? FXPieChartView())
                
                self.roomsaleview = RoomSaleFooterView.init(frame: CGRect(x: self.fxview?.frame.origin.x ?? 0, y: self.frame.height - 80, width: self.fxview?.frame.width ?? 0 - ( self.fxview?.frame.origin.x ?? 0 * 2), height: 80))
                
                let viewObj = UIView()
                let width = self.frame.size.width - 30
                viewObj.frame = CGRect(x: 0, y:5, width: width, height: 1)
                viewObj.backgroundColor = UIColor.lightGray
                self.roomsaleview?.addSubview(viewObj)
                self.contentView.addSubview(self.roomsaleview ?? RoomSaleFooterView() )
                
            }
            else {
                //self.isNONFBDetails = false
                self.isBarBhartLoadedbefore = true
                var aryBarChart = [BarChartModel]()
                self.barChartView = CustomDashBarChartView.init(frame: CGRect(x: 0, y:self.view_container.frame.size.height, width: self.view_container.frame.width, height: 200)) as CustomDashBarChartView
                
                if self.isNonFnBDetails {
                    for eachNonFnB in CommonDataManager.shared.ary_nonFandBDetailsList ?? [] {
                        var barModel = BarChartModel()
                        barModel.CYAmount = eachNonFnB.Amount
                        barModel.LYAmount = eachNonFnB.LastYear
                        barModel.RevenueCodeName = eachNonFnB.RevenueCodeName
                        aryBarChart.append(barModel)
                    }
                    
                } else {
                    for eachFnB in CommonDataManager.shared.ary_FandBDetailsList ?? [] {
                        var barModel = BarChartModel()
                        barModel.CYAmount = eachFnB.AMOUNT
                        barModel.LYAmount = eachFnB.LastYearAmount
                        barModel.RevenueCodeName = eachFnB.OutletName
                        aryBarChart.append(barModel)
                    }
                }
                
                //self.barChartView?.loadChartData(list: CommonDataManager.shared.ary_nonFandBDetailsList ?? [])
                print(aryBarChart.count)
                       if aryBarChart.count == 0 {
                           self.contentView.addSubview(self.barChartView ?? CustomDashBarChartView())
                           
                       }
                       else {
                            self.barChartView?.loadChartData(list: aryBarChart)
                           self.contentView.addSubview(self.barChartView ?? CustomDashBarChartView())
                       }
            }
    
       
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    

    var selectedIndex = 0
    var todaySales :RoomSaleCustomModel? {
        didSet{
            self.updateCellData()
        }
    }
    var monthSales :RoomSaleCustomModel? {
        didSet{
            self.updateCellData()
        }
    }
    var yearSales :RoomSaleCustomModel? {
        didSet{
            self.updateCellData()
        }
    }
    var cutomeSales :RoomSaleCustomModel? {
        didSet{
            self.updateCellData()
        }
    }
    func updateBarchayData() {
        print("updateBarchayData")
        self.isBarBhartLoadedbefore = true
        var aryBarChart = [BarChartModel]()
        self.barChartView = CustomDashBarChartView.init(frame: CGRect(x: 0, y:self.view_container.frame.size.height, width: self.view_container.frame.width, height: 200)) as CustomDashBarChartView
        
        if self.isNonFnBDetails {
            for eachNonFnB in CommonDataManager.shared.ary_nonFandBDetailsList ?? [] {
                var barModel = BarChartModel()
                barModel.CYAmount = eachNonFnB.Amount
                barModel.LYAmount = eachNonFnB.LastYear
                barModel.RevenueCodeName = eachNonFnB.RevenueCodeName
                aryBarChart.append(barModel)
            }
            
        } else {
            for eachFnB in CommonDataManager.shared.ary_FandBDetailsList ?? [] {
                var barModel = BarChartModel()
                barModel.CYAmount = eachFnB.AMOUNT
                barModel.LYAmount = eachFnB.LastYearAmount
                barModel.RevenueCodeName = eachFnB.OutletName
                aryBarChart.append(barModel)
            }
        }
        
        //self.barChartView?.loadChartData(list: CommonDataManager.shared.ary_nonFandBDetailsList ?? [])
        print(aryBarChart.count)
        if aryBarChart.count == 0 {
            self.contentView.addSubview(self.barChartView ?? CustomDashBarChartView())
            
        }
        else {
             self.barChartView?.loadChartData(list: aryBarChart)
            self.contentView.addSubview(self.barChartView ?? CustomDashBarChartView())
        }
       
        
        
    }
    func updateCellData(){
        
        let per = CommonDataManager.shared.getActualBudgetValue(budget: "", actual: "")
        print("Perce::::\(per)")
        lbl_actualPercen.text = "\(per.0) %"
        view_actualWidthConstraint.constant = CGFloat(per.1)
        lbl_actual_title.textColor = UIColor.hexStringToUIColor(hex: "FF8E00")
        view_actualFill.backgroundColor = UIColor.hexStringToUIColor(hex: "FF8E00")
        
        if isFromCustome {
            self.lbl_budget.text = "\(cutomeSales?.Budget ?? 0.0)"
            self.lbl_todayAmount.text = "\(cutomeSales?.Year ?? 0.0)"
            self.lbl_lastyearAmount.text = "\(cutomeSales?.LastYear ?? 0.0)"
            self.lbl_actualAmount.text = "\(cutomeSales?.Yesterday_TD ?? 0.0)"
            let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(cutomeSales?.LastYear ?? 0.0)", currentyear: "\(cutomeSales?.Year ?? 0.0)")
            if value == 0 {
                self.lbl_cyLyPer.text = " \(value)%"
                self.lbl_cyLyPer.textColor = .black
                self.lbl_cyLyPer.addImageWithDetailsPage(name: "", behindText: true)
            }
            else if value >= 100 {
                self.lbl_cyLyPer.text = "+100%"
                self.lbl_cyLyPer.textColor = .green
                self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else if value > 0 {
                self.lbl_cyLyPer.text = "+\(value)%"
                self.lbl_cyLyPer.textColor = .green
                self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
            }
            else {
                self.lbl_cyLyPer.text = " \(value)%"
                self.lbl_cyLyPer.textColor = .red
                self.lbl_cyLyPer.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
            }
            self.isFromCustome = false
        }else {
            if selectedIndex == 1 {
                //self.lbl_title.text = "\(todaySales?.Selection ?? "")"
                self.lbl_budget.text = "\(todaySales?.Budget ?? 0.0)"
                self.lbl_todayAmount.text = "\(todaySales?.Today ?? 0.0)"
                self.lbl_lastyearAmount.text = "\(todaySales?.LYToday ?? 0.0)"
                self.lbl_actualAmount.text = "\(todaySales?.Yesterday_TD ?? 0.0)"
                let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(todaySales?.LYToday ?? 0.0)", currentyear: "\(todaySales?.Today ?? 0.0)")
                if value == 0 {
                    self.lbl_cyLyPer.text = " \(value)%"
                    self.lbl_cyLyPer.textColor = .black
                }
                else if value >= 100 {
                    self.lbl_cyLyPer.text = "+100%"
                    self.lbl_cyLyPer.textColor = .green
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
                }
                else if value > 0 {
                    self.lbl_cyLyPer.text = "+\(value)%"
                    self.lbl_cyLyPer.textColor = .green
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
                }
                else {
                    self.lbl_cyLyPer.text = " \(value)%"
                    self.lbl_cyLyPer.textColor = .red
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
                }
            }
            else if selectedIndex == 2 {
                //self.lbl_title.text = "\(monthSales?.Selection ?? "")"
                self.lbl_budget.text = "\(monthSales?.Budget ?? 0.0)"
                self.lbl_todayAmount.text = "\(monthSales?.Month ?? 0.0)"
                self.lbl_lastyearAmount.text = "\(monthSales?.LastMonth ?? 0.0)"
                self.lbl_actualAmount.text = "\(monthSales?.Yesterday_TD ?? 0.0)"
                let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(monthSales?.LastMonth ?? 0.0)", currentyear: "\(monthSales?.Month ?? 0.0)")
                if value == 0 {
                    self.lbl_cyLyPer.text = " \(value)%"
                    self.lbl_cyLyPer.textColor = .black
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "", behindText: true)
                }
                else if value >= 100 {
                    self.lbl_cyLyPer.text = "+ 100%"
                    self.lbl_cyLyPer.textColor = .green
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
                }
                else if value > 0 {
                    self.lbl_cyLyPer.text = "+\(value)%"
                    self.lbl_cyLyPer.textColor = .green
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
                }
                else {
                    self.lbl_cyLyPer.text = " \(value)%"
                    self.lbl_cyLyPer.textColor = .red
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
                }
            }
            else if selectedIndex == 3 {
                //self.lbl_title.text = "\(yearSales?.Selection ?? "")"
                self.lbl_budget.text = "\(yearSales?.Budget ?? 0.0)"
                self.lbl_todayAmount.text = "\(yearSales?.Year ?? 0)"
                self.lbl_lastyearAmount.text = "\(yearSales?.LastYear ?? 0.0)"
                self.lbl_actualAmount.text = "\(yearSales?.Yesterday_TD ?? 0)"
                let value = CommonDataManager.shared.getlastYearCurrentyearPerValue(lastYear: "\(yearSales?.LastYear ?? 0.0)", currentyear: "\(yearSales?.Year ?? 0)")
                if value == 0 {
                    self.lbl_cyLyPer.text = " \(value)%"
                    self.lbl_cyLyPer.textColor = .black
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "", behindText: true)
                }
                else if value >= 100 {
                    self.lbl_cyLyPer.text = "+ 100%"
                    self.lbl_cyLyPer.textColor = .green
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
                }
                else if value > 0 {
                    self.lbl_cyLyPer.text = "+\(value)%"
                    self.lbl_cyLyPer.textColor = .green
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Increase 1", behindText: true)
                }
                else {
                    self.lbl_cyLyPer.text = " \(value)%"
                    self.lbl_cyLyPer.textColor = .red
                    self.lbl_cyLyPer.addImageWithDetailsPage(name: "Decrease 1", behindText: true)
                }
                
            }
        }
        
        
        let budget = lbl_budget.text ?? "0"
        let actual = lbl_actualAmount.text ?? "0"
        let b = Int(Double (budget) ?? 0.0) 
        let a = Int(Double (actual) ?? 0.0) 
        if b <= 0 {
            lbl_actualPercen.text = "100%"
            view_actualWidthConstraint.constant = CGFloat(150)
            //budgetViewWidthConstraint.constant = 2 // butger bar occy
        }
        else if a > b {
            lbl_actualPercen.text = "100%+"
            view_actualWidthConstraint.constant = CGFloat(160)
        }
        else {
            
            let per = CommonDataManager.shared.getActualBudgetValue(budget:lbl_budget.text ?? "0", actual: lbl_actualAmount.text ?? "0")
            print("Perce::::\(per)")
            lbl_actualPercen.text = "\(per.0)%"
            view_actualWidthConstraint.constant = CGFloat(per.1)
        }
        
        if b == 0 {
            view_barChartContainer.isHidden = true
            lbl_budgethelpMessage.isHidden = false
            lbl_budgethelpMessage.text = "Budget Not Defined"
        }
        else {
            view_barChartContainer.isHidden = false
            lbl_budgethelpMessage.isHidden = true
        }
        let double_todayCost = Double(self.lbl_todayAmount.text ?? "") ?? 0.0
        //let cash = NSNumber(value:double_todayCost )
        self.lbl_todayAmount.text = "\(CommonDataManager.shared.getNumberWithCommas(cash: double_todayCost))"
        self.lbl_actualAmount.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(self.lbl_actualAmount.text ?? "") ?? 0.0)
        self.lbl_budget.text = CommonDataManager.shared.getNumberWithCommas(cash:  Double(self.lbl_budget.text ?? "") ?? 0.0)
    }

}

